/**
 * Created by User on 21/01/2018.
 */

var path = require('path');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './js/App.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }/*
    ,
    plugins: [
        new UglifyJSPlugin()
    ]*/
};