var InventoryItemTypes = {
    WOOD: 100,
    GEMS: 200,
    FOOD: 300,
    SCROLL: 400
};

var spellStats = {

    //  -   -   Light spells.   -   -
    'Heal Orb': {
        code: 'l',
        energyCost: 50,
        recastCooldown: 600,
        startFromCaster: true
    },

    'Plant Sapling': {
        code: 'llwwee',
        energyCost: 300,
        recastCooldown: 2000,
        gridAligned: true
    },

    //  -   -   Dark spells.    -   -
    'Raven Strike': {
        code: 'd',
        energyCost: 50,
        recastCooldown: 250,
        startFromCaster: true
    },

    'Target Bolt': {
        code: 'dd',
        energyCost: 0,
        recastCooldown: 400,
        startFromCaster: true
    },

    'Summon Critter': {
        code: 'ddp',
        energyCost: 100,
        recastCooldown: 100,
        castRange: 100
    },

    'Destroy Minion': {
        code: 'df',
        recastCooldown: 200
    },

    //  -   -   Water spells.   -   -
    'Splash': {
        code: 'w',
        energyCost: 50,
        recastCooldown: 250,
        startFromCaster: true
    },

    'Summon Nimbus': {
        code: 'wwd',
        energyCost: 20,
        recastCooldown: 250,
        castRange: 100
    },

    //  -   -   Fire spells.    -   -
    'Fire Bolt': {
        code: 'f',
        energyCost: 100,
        recastCooldown: 500,
        startFromCaster: true
    },

    'Flamethrower': {
        code: 'ff',
        energyCost: 20,
        recastCooldown: 100,
        startFromCaster: true
    },

    //  -   -   Earth spells.   -   -
    'Rock Throw': {
        code:               'e',
        energyCost:         40,
        recastCooldown:     350,
        startFromCaster:    true
    },

    'Stone Shards': {
        code:               'eee',
        energyCost:         70,
        recastCooldown:     600,
        startFromCaster:    true
    },

    'Barrier': {
        code:               'eeee',
        energyCost:         100,
        recastCooldown:     200,
        castRange:          100,
        gridAligned:        true
    },

    //  -   -   Psychic spells. -   -
    'Mind Bullet': {
        code: 'p',
        energyCost: 40,
        recastCooldown: 300,
        startFromCaster: true
    },

    'Draining Bolt': {
        code: 'pp',
        energyCost: 20,
        recastCooldown: 500,
        startFromCaster: true
    },

    'Invisibility': {
        code: 'ppd',
        energyCost: 10,
        recastCooldown: 300
    },

    //  -   -   Void spells.    -   -
    'Deflect': {
        code: 'v',
        energyCost: 40,
        recastCooldown: 500,
        startFromCaster: true
    },

    'Tele Orb': {
        code: 'vv',
        energyCost: 40,
        recastCooldown: 1000,
        startFromCaster: true
    },

    'Summon Inverter': {
        code: 'vvv',
        energyCost: 100,
        recastCooldown: 1000,
        castRange: 150
    },

    'Recall Minions': {
        code: 'vvd',
        energyCost: 100,
        recastCooldown: 1000
    },

    'Build Order Core':{
        code: 'vevevev',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 4},
            {itemType: InventoryItemTypes.GEMS, amount: 4}
        ],
        castRange: 120,
        recastCooldown: 1000
    },

    'Build Wood Wall': {
        code: 'vee',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 1}
        ],
        castRange: 60,
        recastCooldown: 500
    },

    'Build Stone Wall': {
        code: 'veee',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 1},
            {itemType: InventoryItemTypes.GEMS, amount: 1}
        ],
        castRange: 60,
        recastCooldown: 500
    },

    'Build Wood Door': {
        code: 'veev',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 2}
        ],
        castRange: 60,
        recastCooldown: 500
    }

};

export {spellStats};