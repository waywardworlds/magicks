/**
 * Created by User on 20/01/2018.
 */

import './DefaultSettings';
import './states/Boot';
import './states/Preload';
import './states/LogIn';
import './states/ManageAccount';
import './states/Menu';
import './states/GameGuide';
import './states/guides/GuideElements';
import './states/guides/GuideVoid';
import './states/guides/GuidePsychic';
import './states/guides/GuideLight';
import './states/guides/GuideDark';
import './states/guides/GuideWater';
import './states/guides/GuideFire';
import './states/guides/GuideEarth';
import './states/guides/GuideSpells';
import './states/guides/GuideOrders';
import './states/guides/GuideResources';
import './states/WorldSelect';
import './states/Game';

import './Main';