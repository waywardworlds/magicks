/**
 * Created by User on 17/12/2016.
 */

"use strict";

import {spellStats} from './SpellStats';

class GUI {

    constructor(game){

        this.game = game;

        this.GUIGroup = this.game.add.group();
        this.GUIGroup.fixedToCamera = true;

        // This is used to stop any input on a button from also casting a spell.
        // Even if using the joystick, only one pointer should be down on a GUI button at
        // once, with any other pointers that are down being used for casting spells.
        this.anyPointerOnGUIButton = false;

        //var settings = magicks.settings.GUI;
/*
        // Add the buttons for the options and the other drop-down menu items.
        var addOptions = function (space, frameName, callback, callbackContext) {
            var button = callbackContext.game.add.button(settings.options.x + (space * settings.options.scale / 4), settings.options.y, 'game-atlas', callback, callbackContext, undefined, undefined, undefined, undefined, callbackContext.GUIGroup);
            button.scale.setTo(settings.options.scale); FIGURE OUT OPTIONS POSITIONING RELATIVE TO OPTIONS BUTTON
            button.events.onInputDown.add(callbackContext.buttonOnDown, callbackContext);
            button.events.onInputUp.add(callbackContext.buttonOnUp, callbackContext);
            //button.anchor.setTo(0);
            button.frameName = frameName;
            button.visible = false;

            return button;
        };

        var space = 0;
        this.options    = addOptions(space, 'btn-options', this.optionsPressed, this); space+=1;
        this.audioOn    = addOptions(space, 'btn-audio-on', this.audioOnPressed, this);
        this.audioOff   = addOptions(space, 'btn-audio-off', this.audioOffPressed, this); space+=1;
        this.fullscreen = addOptions(space, 'btn-fullscreen', this.fullscreenPressed, this);space+=1;
        this.exit       = addOptions(space, 'btn-exit', this.exitPressed, this);

        this.options.visible = true;*/

        // Show the element combo wheel.
        if(this.gamepadEnabled || magicks.settings.spellWheelEnabled === false){
            this.addElementButtonsBar();
        }
        else{
            this.addElementButtonsWheel();
        }
        // Add the stat bars.
        this.addHitPointsBar();
        this.addEnergyBar();
        this.addReviveBar();

        this.optionsSpacing = 6;
        this.optionsList = [];

        this.addOptionsButton();
        this.addInventoryButton();
        this.addMiniMapButton();
        this.addOrderButton();
        //this.addAudioOnButton();
        //this.addAudioOffButton();
        this.addFullscreenButton();
        this.addExitButton();
        this.addChatButton();
        // Hide all the other buttons at the start.
        //this.optionsPressed();

        this.addChatInput();
        this.addRespawnButton();
        this.addDiscordButton();
        this.addAlertText();
        this.addCastRangeCircle();
        this.addSpellComponentBar();
        this.addInventoryPanel();
        this.addMiniMapPanel();
        this.addOrderPanel();

        if(magicks.adBlockEnabled){
            //this.addAdBlockMessage();
        }

        // Add the button that will clear the stack of spell components.
        this.clearSpellComponentsButton = this.game.add.button(this.game.game.width/2, this.game.game.height - 27, null, function () {
            //console.log("clear elements button pressed");
            this.clearSpellComponents();
        }, this);
        this.clearSpellComponentsButton.width = this.spellComponentBar.width + 30;
        this.clearSpellComponentsButton.height = 55;
        this.clearSpellComponentsButton.anchor.setTo(0.5);
        this.clearSpellComponentsButton.events.onInputDown.add(this.buttonOnDown, this);
        this.clearSpellComponentsButton.events.onInputUp.add(this.buttonOnUp, this);
        this.GUIGroup.add(this.clearSpellComponentsButton);
    }

    /**
     * Add the cast range circle to the display.
     */
    addCastRangeCircle () {
        this.castRangeCircle = this.game.add.graphics(0, 0);
        // The cast range circle should move with the player entity like any other GUI component.
        //this.game.entities[this.game.playerEntityId].addChild(this.castRangeCircle); //TODO: still some weird shit with adding the cast circle to the player id fucking with the elements bar...
    }

    /**
     * Change the radius of the cast range circle, if the new spell has a cast range, or hide it the new spell doesn't have a cast range.
     */
    updateCastRangeCircle () {
        //console.log("in updatecastrangecircle");
        var spellStatObj = spellStats.codes[this.game.spellCode];
        // Check some stats have been defined for the current spell.
        if(spellStatObj){
            // Check the current spell has a cast range property.
            if(spellStatObj.castRange){
                this.castRangeCircle.visible = true;
                // Remove the old circle.
                this.castRangeCircle.clear();
                // Draw the new one with the diameter of 2x the cast range.
                this.castRangeCircle.lineStyle(6, 0xffffff, 0.4);//0.4
                this.castRangeCircle.drawCircle(0, 0, spellStatObj.castRange * 2);
            }
            else {
                this.castRangeCircle.visible = false;
                //console.log("spellstatobj doesn't have cast range: " + spellStatObj);
            }
        }
        else{
            //console.log("spellstatobj not valid: " + spellStatObj);
            this.castRangeCircle.visible = false;
        }
    }

    /**
     * Add the buttons for each of the 7 elements, in a horizontal row layout.
     */
    addElementButtonsBar () {
        //console.log("adding elements bar");
        // Add a container for the element buttons. Useful for hiding them when a player is dead/unconscious, or fading them out when no valid spell with next element.
        /*this.elementButtons = {
            setVisibility: function (visible) {
                for(var i=0; i<7; i+=1){
                    this.children[i].visible = visible;
                }
            },
            add: function (child) {
                this.children.push(child);
            },
            children: []
        };*/

        this.elementButtons = this.game.add.group();
        this.elementButtons.setVisibility = function (visible) {
            //console.log("in setvisiblity: " + visible);
            for (var i=0; i<7; i+=1) {
                //this.children[i].visible = visible;
                if(visible === true){
                    this.children[i].inputEnabled = true;
                    this.game.add.tween(this.children[i].scale).to({x: 1.4, y: 1.4}, 100, "Linear", true, 0);
                    this.game.add.tween(this.children[i]).to({alpha: 100}, 100, "Linear", true, 0);
                }
                else {
                    this.children[i].inputEnabled = false;
                    this.game.add.tween(this.children[i].scale).to({x: 0, y: 0}, 100, "Linear", true, 0);
                    this.game.add.tween(this.children[i]).to({alpha: 0}, 100, "Linear", true, 0);
                }
            }
        };
        this.elementButtons.showButton = function (element) {
            this.byElement[element].inputEnabled = true;
            this.game.add.tween(this.byElement[element].scale).to({x: 1.4, y: 1.4}, 100, "Linear", true, 0);
            this.game.add.tween(this.byElement[element]).to({alpha: 100}, 100, "Linear", true, 0);
        };
        // An object to reference the buttons by the element that they are for.
        this.elementButtons.byElement = {};

        // Set up the element selection buttons.
        var y = 380;
        var x = 0;
        var btn;
        for(var i=0; i<7; i+=1){
            btn = {};
            // Add a text to each button for the keyboard character that will also add an element.
            var keyText = this.game.add.text(-8, -8, '', {
                font: "26px Alagard",
                fill: magicks.colours.white,
                align: "center",
                stroke: magicks.colours.black,
                strokeThickness: 5
            });
            keyText.anchor.setTo(0.5);
            keyText.scale.setTo(0.5);
            // Add the buttons, from left to right.
            switch(i){
                case 0:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('light');
                    }, this);
                    btn.frameName = 'btn-light';
                    this.elementButtons.byElement.light = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addLight]);
                    break;
                case 1:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('dark');
                    }, this);
                    btn.frameName = 'btn-dark';
                    this.elementButtons.byElement.dark = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addDark]);
                    break;
                case 2:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('water');
                    }, this);
                    btn.frameName = 'btn-water';
                    this.elementButtons.byElement.water = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addWater]);
                    break;
                case 3:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('fire');
                    }, this);
                    btn.frameName = 'btn-fire';
                    this.elementButtons.byElement.fire = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addFire]);
                    break;
                case 4:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('earth');
                    }, this);
                    btn.frameName = 'btn-earth';
                    this.elementButtons.byElement.earth = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addEarth]);
                    break;
                case 5:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('psychic');
                    }, this);
                    btn.frameName = 'btn-psychic';
                    this.elementButtons.byElement.psychic = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addPsychic]);
                    break;
                case 6:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('void');
                    }, this);
                    btn.frameName = 'btn-void';
                    this.elementButtons.byElement.void = btn;
                    keyText.text = String.fromCharCode(Phaser.KeyCode[magicks.settings.controls.addVoid]);
                    break;
            }
            btn.anchor.setTo(0.5);
            btn.events.onInputDown.add(this.buttonOnDown, this);
            btn.events.onInputUp.add(this.buttonOnUp, this);
            btn.input.pixelPerfectOver = true;
            btn.addChild(keyText);
            this.elementButtons.add(btn);
            // Move the next button along.
            x += 40;
        }
        // Center the group of buttons.
        this.elementButtons.x = (this.game.game.width / 2) - (this.elementButtons.width / 2) + (btn.width / 2);

        this.GUIGroup.add(this.elementButtons);
    };

    /**
     * Add the buttons for each of the 7 elements, in a wheel layout.
     */
    addElementButtonsWheel () {
        //console.log("adding element wheel");
        this.elementButtons = this.game.add.group();
        this.elementButtons.setVisibility = function (visible) {
            //console.log("in setvisiblity: " + visible);
            for (var i=0; i<7; i+=1) {
                //this.children[i].visible = visible;
                if(visible === true){
                    this.children[i].inputEnabled = true;
                    this.game.add.tween(this.children[i].scale).to({x: scale * 0.9, y: scale * 0.9}, 100, "Linear", true, 0);
                    this.game.add.tween(this.children[i]).to({alpha: 100}, 100, "Linear", true, 0);
                }
                else {
                    this.children[i].inputEnabled = false;
                    this.game.add.tween(this.children[i].scale).to({x: 0, y: 0}, 100, "Linear", true, 0);
                    this.game.add.tween(this.children[i]).to({alpha: 0}, 100, "Linear", true, 0);
                }
            }
        };
        this.elementButtons.showButton = function (element) {
            this.byElement[element].inputEnabled = true;
            this.game.add.tween(this.byElement[element].scale).to({x: scale * 0.9, y: scale * 0.9}, 100, "Linear", true, 0);
            this.game.add.tween(this.byElement[element]).to({alpha: 100}, 100, "Linear", true, 0);
        };
        // An object to reference the buttons by the element that they are for.
        this.elementButtons.byElement = {};

        // Set up the element selection buttons.
        var angleDegrees = 0,
            angleRadians = 0,
            radius = 160,
            centerX = 680,
            centerY = 350,
            scale = 3.5;
        // When an element button is pressed, it will add that element to the end of the spell combo for the next spell.
        // If another spell is already active, it will be wiped and this element will become the first in a new spell combo.
        var btn = this.game.add.button(centerX, centerY, 'game-atlas', function () {
            this.addSpellComponent('void');
        }, this);
        btn.frameName = 'btn-void';
        this.elementButtons.byElement.void = btn;
        btn.scale.setTo(scale * 0.9);
        btn.anchor.setTo(0.5);
        btn.x -= 40;
        btn.y -= 40;
        btn.events.onInputDown.add(this.buttonOnDown, this);
        btn.events.onInputUp.add(this.buttonOnUp, this);
        btn.input.pixelPerfectOver = true;
        this.elementButtons.add(btn);

        for(var i=0; i<6; i+=1){

            angleRadians = angleDegrees * Math.PI / 180;

            var x = Math.cos(angleRadians) * (radius * scale / 6.8) + centerX;
            var y = Math.sin(angleRadians) * (radius * scale / 6.8) + centerY;

            btn = {};
            switch(i){
                case 0:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('fire');
                    }, this);
                    btn.frameName = 'btn-fire';
                    this.elementButtons.byElement.fire = btn;
                    break;
                case 1:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('water');
                    }, this);
                    btn.frameName = 'btn-water';
                    this.elementButtons.byElement.water = btn;
                    break;
                case 2:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('dark');
                    }, this);
                    btn.frameName = 'btn-dark';
                    this.elementButtons.byElement.dark = btn;
                    break;
                case 3:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('light');
                    }, this);
                    btn.frameName = 'btn-light';
                    this.elementButtons.byElement.light = btn;
                    break;
                case 4:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('psychic');
                    }, this);
                    btn.frameName = 'btn-psychic';
                    this.elementButtons.byElement.psychic = btn;
                    break;
                case 5:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        this.addSpellComponent('earth');
                    }, this);
                    btn.frameName = 'btn-earth';
                    this.elementButtons.byElement.earth = btn;
                    break;
            }
            btn.anchor.setTo(0.5);
            btn.x -= 40;
            btn.y -= 40;
            btn.events.onInputDown.add(this.buttonOnDown, this);
            btn.events.onInputUp.add(this.buttonOnUp, this);
            btn.input.pixelPerfectOver = true;
            this.elementButtons.add(btn);
            angleDegrees += 60;

            //console.log("button scale: " + this.elementButtons.children[0].scale);
        }

        this.GUIGroup.add(this.elementButtons);

    }

    addHitPointsBar () {
        // Check if the health bar is added already. If so, remove it and add this new one.
        // Player max hitpoints might have changed and the amount of icons shown will need to be updated.
        if(this.hitPointsBar){
            this.hitPointsBar.destroy();
        }
        this.hitPointsBar = this.game.add.group();
        this.GUIGroup.add(this.hitPointsBar);

        var xOffset = 0;
        // Add a whole hitpoint icon for every 4 hitpoints.
        for(var i=0; i<this.game.maxHitPoints; i+=4){
            var icon = this.game.add.sprite(xOffset, 0, 'game-atlas', 'hitpoint-4', this.hitPointsBar);
            icon.scale.setTo(2);
            icon.anchor.setTo(0.5);
            xOffset += 30;
        }

        this.hitPointsBar.y = 10 + (icon.height / 2);
        // Center the row of icons.
        this.hitPointsBar.x = (this.game.game.width / 2) - (this.hitPointsBar.width / 2) + (icon.width / 2);
    }

    addEnergyBar () {
        // Check if the energy bar is added already. If so, remove it and add this new one.
        // Player max energy might have changed and the amount of icons shown will need to be updated.
        if(this.energyBar){
            this.energyBar.destroy();
        }
        this.energyBar = this.game.add.group();
        this.GUIGroup.add(this.energyBar);

        var xOffset = 0;
        // Add a whole energy icon for every 4 energy.
        for(var i=0; i<this.game.maxEnergy; i+=4){
            var icon = this.game.add.sprite(xOffset, 0, 'game-atlas', 'energy-4', this.energyBar);
            icon.scale.setTo(2);
            icon.anchor.setTo(0.5);
            xOffset += 30;
        }

        this.energyBar.y = 44 + (icon.height / 2);
        // Center the row of icons.
        this.energyBar.x = (this.game.game.width / 2) - (this.energyBar.width / 2) + (icon.width / 2);
    }

    addReviveBar () {
        // Check if the revive bar is added already. If so, remove it and add this new one.
        // Player max revive might have changed and the amount of icons shown will need to be updated.
        if(this.reviveBar){
            this.reviveBar.destroy();
        }
        this.reviveBar = this.game.add.group();
        this.reviveBar.visible = false;
        this.GUIGroup.add(this.reviveBar);

        var xOffset = 0;
        // Add a whole revive icon for every 4 hitpoints. Revive points is based on max hitpoints.
        for(var i=0; i<this.game.maxHitPoints; i+=4){
            var icon = this.game.add.sprite(xOffset, 0, 'game-atlas', 'revive-4', this.reviveBar);
            icon.scale.setTo(2);
            icon.anchor.setTo(0.5);
            xOffset += 30;
        }

        this.reviveBar.y = 27 + (icon.height / 2);
        // Center the row of icons.
        this.reviveBar.x = (this.game.game.width / 2) - (this.reviveBar.width / 2) + (icon.width / 2);
    }

    addOptionsButton () {
        this.optionsButton = this.game.add.button(this.game.game.width, 0, 'game-atlas', this.optionsPressed, this);
        this.optionsButton.frameName = 'btn-options';
        this.optionsButton.events.onInputDown.add(this.buttonOnDown, this);
        this.optionsButton.events.onInputUp.add(this.buttonOnUp, this);
        this.optionsButton.anchor.setTo(1, 0);
        this.optionsButton.scale.setTo(magicks.settings.GUI.options.scale);

        this.GUIGroup.add(this.optionsButton);
        this.optionsList.push(this.optionsButton);
    }

    addAudioOnButton () {
        var previousOption = this.optionsList[this.optionsList.length-1];
        this.audioOnButton = this.game.add.button(previousOption.x, previousOption.y + previousOption.height + this.optionsSpacing, 'game-atlas', this.audioOnPressed, this);
        this.audioOnButton.frameName = 'btn-audio-on';
        this.audioOnButton.events.onInputDown.add(this.buttonOnDown, this);
        this.audioOnButton.events.onInputUp.add(this.buttonOnUp, this);
        this.audioOnButton.anchor.setTo(1, 0);
        this.audioOnButton.scale.setTo(magicks.settings.GUI.options.scale);
        this.audioOnButton.alpha = 0.5;

        this.GUIGroup.add(this.audioOnButton);
        this.optionsList.push(this.audioOnButton);
    }

    addAudioOffButton () {
        this.audioOffButton = this.game.add.button(this.optionsButton.x, this.orderButton.y + this.orderButton.height + this.optionsSpacing, 'game-atlas', this.audioOffPressed, this);
        this.audioOffButton.frameName = 'btn-audio-off';
        this.audioOffButton.events.onInputDown.add(this.buttonOnDown, this);
        this.audioOffButton.events.onInputUp.add(this.buttonOnUp, this);
        this.audioOffButton.anchor.setTo(1, 0);
        this.audioOffButton.scale.setTo(magicks.settings.GUI.options.scale);
        this.audioOffButton.alpha = 0.5;
        this.audioOffButton.visible = false;

        this.GUIGroup.add(this.audioOffButton);
    }

    addFullscreenButton () {
        var previousOption = this.optionsList[this.optionsList.length-1];
        this.fullscreenButton = this.game.add.button(previousOption.x, previousOption.y + previousOption.height + this.optionsSpacing, 'game-atlas', this.fullscreenPressed, this);
        this.fullscreenButton.frameName = 'btn-fullscreen';
        this.fullscreenButton.events.onInputDown.add(this.buttonOnDown, this);
        this.fullscreenButton.events.onInputUp.add(this.buttonOnUp, this);
        this.fullscreenButton.anchor.setTo(1, 0);
        this.fullscreenButton.scale.setTo(magicks.settings.GUI.options.scale);

        this.GUIGroup.add(this.fullscreenButton);
        this.optionsList.push(this.fullscreenButton);
    }

    addExitButton () {
        var previousOption = this.optionsList[this.optionsList.length-1];
        this.exitButton = this.game.add.button(previousOption.x, previousOption.y + previousOption.height + this.optionsSpacing, 'game-atlas', this.exitPressed, this);
        this.exitButton.frameName = 'btn-exit';
        this.exitButton.events.onInputDown.add(this.buttonOnDown, this);
        this.exitButton.events.onInputUp.add(this.buttonOnUp, this);
        this.exitButton.anchor.setTo(1, 0);
        this.exitButton.scale.setTo(magicks.settings.GUI.options.scale);

        this.GUIGroup.add(this.exitButton);
        this.optionsList.push(this.exitButton);
    }

    addInventoryButton () {
        var previousOption = this.optionsList[this.optionsList.length-1];
        this.inventoryButton = this.game.add.button(previousOption.x, previousOption.y + previousOption.height + this.optionsSpacing, 'game-atlas', function () {
            if(this.inventoryPanel.visible === true){
                this.inventoryPanel.visible = false;
            }
            else {
                this.inventoryPanel.visible = true;
                this.inventoryPanel.bringToTop();
            }
            this.optionsPressed();
        }, this);
        this.inventoryButton.frameName = 'btn-inventory';
        this.inventoryButton.events.onInputDown.add(this.buttonOnDown, this);
        this.inventoryButton.events.onInputUp.add(this.buttonOnUp, this);
        this.inventoryButton.anchor.setTo(1, 0);
        this.inventoryButton.scale.setTo(magicks.settings.GUI.options.scale);

        this.GUIGroup.add(this.inventoryButton);
        this.optionsList.push(this.inventoryButton);
    }

    addMiniMapButton () {
        var previousOption = this.optionsList[this.optionsList.length-1];
        this.miniMapButton = this.game.add.button(previousOption.x, previousOption.y + previousOption.height + this.optionsSpacing, 'game-atlas', function () {
            //console.log("minimap pressed");
            if(this.miniMapPanel.visible === true){
                this.miniMapPanel.visible = false;
            }
            else {
                this.miniMapPanel.visible = true;
                this.miniMapPanel.bringToTop();
            }
            this.optionsPressed();
        }, this);
        this.miniMapButton.frameName = 'btn-minimap';
        this.miniMapButton.events.onInputDown.add(this.buttonOnDown, this);
        this.miniMapButton.events.onInputUp.add(this.buttonOnUp, this);
        this.miniMapButton.anchor.setTo(1, 0);
        this.miniMapButton.scale.setTo(magicks.settings.GUI.options.scale);

        this.GUIGroup.add(this.miniMapButton);
        this.optionsList.push(this.miniMapButton);
    }

    addOrderButton () {
        var previousOption = this.optionsList[this.optionsList.length-1];
        this.orderButton = this.game.add.button(previousOption.x, previousOption.y + previousOption.height + this.optionsSpacing, 'game-atlas', function () {
            //console.log("order button pressed");
            if(this.orderPanel.visible === true){
                this.orderPanel.visible = false;
            }
            else {
                this.orderPanel.visible = true;
                this.orderPanel.bringToTop();
            }
            this.optionsPressed();
        }, this);
        this.orderButton.frameName = 'btn-order';
        this.orderButton.events.onInputDown.add(this.buttonOnDown, this);
        this.orderButton.events.onInputUp.add(this.buttonOnUp, this);
        this.orderButton.anchor.setTo(1, 0);
        this.orderButton.scale.setTo(magicks.settings.GUI.options.scale);
        //this.orderButton.visible = false;

        this.GUIGroup.add(this.orderButton);
        this.optionsList.push(this.orderButton);

        //TODO: Do the order panel, order name, member list, ranks, permissions, promotions.
    }

    addChatButton () {
        this.chatButton = this.game.add.button(0, this.optionsButton.y, 'game-atlas', function () {

            if(this.game.gameplayInputEnabled === false){
                return;
            }
            // If the chat input is hidden, show it.
            if(this.chatInput.visible === false){
                this.chatInput.visible = true;
                this.chatInput.startFocus();
            }
            else {
                this.chatInput.visible = false;
                this.chatInput.endFocus();
                // Don't send empty messages.
                if(this.chatInput.value.length > 0){
                    // Send the message to the game server, to broadcast to all players in the same zone as the player.
                    socketGS.emit('chat', this.chatInput.value);
                    // Clear the input box ready for the next time they want to enter a message.
                    this.chatInput.setText('');
                }
            }

        }, this);
        this.chatButton.frameName = 'btn-chat';
        this.chatButton.events.onInputDown.add(this.buttonOnDown, this);
        this.chatButton.events.onInputUp.add(this.buttonOnUp, this);
        //this.chatButton.anchor.setTo(1, 0);
        this.chatButton.scale.setTo(magicks.settings.GUI.options.scale);

        this.GUIGroup.add(this.chatButton);
    }

    addAlertText () {
        this.alertText = this.game.add.text(this.game.game.width / 2, 70, '', {
            font: "30px Alagard",
            fill: magicks.colours.grey,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7,
            wordWrap: true,
            wordWrapWidth: 400,
            boundsAlignV: "top"
        });
        this.alertText.anchor.setTo(0.5, 0);
        this.alertText.visible = false;
        this.GUIGroup.add(this.alertText);
    }

    addChatInput () {
        var style = {
            font: "24px Acme",//'24px Alagard',
            fill: '#1e1e1e',
            fillAlpha: 0.8,
            fontWeight: 'bold',
            width: 220,
            height: 25,
            padding: 8,
            borderWidth: 3,
            borderColor: magicks.colours.white,
            max: 74,
            placeHolder: '   Enter message...',
            placeHolderColor: "#676767"
        };
        this.chatInput = this.game.add.inputField(0, 139, style);
        // Need to faff around a bit to get the input box centered properly.
        this.chatInput.x = (this.game.game.width / 2) - (this.chatInput.width / 2) - (style.padding) - (style.borderWidth / 2);
        this.chatInput.visible = false;
        this.GUIGroup.add(this.chatInput);
    }

    addRespawnButton () {
        // Text over the respawn button.
        var respawnText = this.game.add.text(0, -10, 'Respawn', {
            font: "60px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7
        });
        respawnText.anchor.setTo(0.5);
        respawnText.scale.setTo(0.22);
        // Respawn button.
        this.respawnButton = new PhaserNineSlice.NineSlice(this.game, this.game.game.width / 2, 100, 'menus-atlas', 'nineslice-button', 70, 44, {top: 7});
        this.respawnButton.scale.setTo(3);
        this.respawnButton.anchor.setTo(0.5);
        this.respawnButton.inputEnabled = true;
        this.respawnButton.events.onInputDown.add(this.respawnPressed, this);
        this.respawnButton.events.onInputDown.add(this.buttonOnDown, this);
        this.respawnButton.events.onInputUp.add(this.buttonOnUp, this);
        this.respawnButton.visible = false;
        this.respawnButton.addChild(respawnText);
        this.GUIGroup.add(this.respawnButton);

        // Show the value of the count down timer.
        this.respawnTimerText = this.game.add.text(0, 10, 10, {
            font: "40px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7
        });
        this.respawnTimerText.anchor.setTo(0.5);
        this.respawnTimerText.scale.setTo(0.4);
        this.respawnButton.addChild(this.respawnTimerText);

        this.respawnTimer = this.game.time.create(false);
        this.respawnTimer.loop(Phaser.Timer.SECOND, function(){
            //console.log("adblock timer counting down");
            if(this.respawnTimerText.text > 0){
                this.respawnTimerText.text -= 1;
                this.respawnTimerText.visible = true;
                respawnText.y = -10;
                this.respawnButton.inputEnabled = false;
                this.respawnButton.alpha = 0.7;
            }
            else {
                //console.log("timer is < 0, respawning");
                this.respawnTimer.stop(false);
                this.respawnTimerText.text = 10;
                this.respawnTimerText.visible = false;
                respawnText.y = 0;
                this.respawnButton.inputEnabled = true;
                this.respawnButton.alpha = 1;
            }
        }, this);

    }

    addDiscordButton () {
        // Text over the Discord button.
        var text = this.game.add.text(0, -200, 'Join the community on\nour Discord server!', {
            font: "60px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 8
        });
        text.anchor.setTo(0.5);
        //text.scale.setTo(0.22);
        this.discordButton = this.game.add.button(120, this.game.game.height - 40, 'menus-atlas', function () {
            window.open("https://discord.gg/w3SwSuA", "_blank")
        });
        this.discordButton.scale.setTo(0.3);
        this.discordButton.anchor.setTo(0.5);
        this.discordButton.frameName = 'discord-logo-white';
        this.discordButton.addChild(text);
        this.discordButton.visible = false;
        this.GUIGroup.add(this.discordButton);
    }

    addAdBlockMessage () {
        var message = "Having fun? If so, then please\n\n\nAds keep the game running and allow us to make it even better.\nWatching an ad is quicker than waiting for this respawn timer.";
        // Text over the adblock message.
        var adBlockMessageText = this.game.add.text(0, -10, message, {
            font: "40px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7,
            wordWrap: true,
            wordWrapWidth: 800,
            boundsAlignV: "top"
        });
        adBlockMessageText.anchor.setTo(0.5);
        adBlockMessageText.scale.setTo(0.22);
        var adBlockMessageTextEmphasis = this.game.add.text(0, -8, "disable your ad blocker\n\n", {
            font: "54px Alagard",
            fill: magicks.colours.red,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7,
            wordWrap: true,
            wordWrapWidth: 800,
            boundsAlignV: "top"
        });
        adBlockMessageTextEmphasis.anchor.setTo(0.5);
        adBlockMessageTextEmphasis.scale.setTo(0.30);

        // Ab block message border.
        this.adBlockMessage = new PhaserNineSlice.NineSlice(this.game, this.game.game.width / 2, this.game.game.height / 2, 'menus-atlas', 'nineslice-button', 200, 120, {top: 7});
        this.adBlockMessage.scale.setTo(3);
        this.adBlockMessage.anchor.setTo(0.5);
        this.adBlockMessage.visible = false;
        this.adBlockMessage.addChild(adBlockMessageText);
        this.adBlockMessage.addChild(adBlockMessageTextEmphasis);

        // Show the value of the count down timer.
        this.adBlockTimerText = this.game.add.text(0, 46, 20, {
            font: "40px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7
        });
        this.adBlockTimerText.anchor.setTo(0.5);
        this.adBlockTimerText.scale.setTo(0.5);
        this.adBlockMessage.addChild(this.adBlockTimerText);

        this.adBlockTimer = this.game.time.create(false);
        this.adBlockTimer.loop(Phaser.Timer.SECOND, function(){
            //console.log("adblock timer counting down");
            if(this.adBlockTimerText.text > 0){
                this.adBlockTimerText.text -= 1;
            }
            else {
                //console.log("timer is < 0, respawning");
                this.adBlockTimer.stop(false);
                this.adBlockTimerText.text = 20;
                this.adBlockMessage.visible = false;
                socketGS.emit('respawn');
            }
        }, this);
        //this.adBlockTimer.start();

        this.GUIGroup.add(this.adBlockMessage);
    }

    /**
     * Add a row of the empty spell component icons, that get updated with the frames for a specific element when one is added to the current spell.
     */
    addSpellComponentBar () {
        //console.log("addSpellComponentBar");
        this.spellComponentBar = this.game.add.group();
        // Add a counter for how many of the element slots have an element in them. Used for the index
        // to check in the strings of the spell codes to see if the next element is valid for a spell.
        //this.spellComponentBar.filledSlots = 0;
        this.GUIGroup.add(this.spellComponentBar);

        var xOffset = 0;
        var icon = this.game.add.sprite(xOffset, 0, 'game-atlas', 'component-empty', this.spellComponentBar);
        icon.scale.setTo(6);
        icon.anchor.setTo(0.5);
        xOffset += 30;

        for(var i=0; i<6; i+=1){
            icon = this.game.add.sprite(xOffset, 0, 'game-atlas', 'component-empty', this.spellComponentBar);
            icon.scale.setTo(4);
            icon.anchor.setTo(0.5);
            xOffset += 34;
        }

        this.spellComponentBar.y = this.game.game.height - 40 + (icon.height / 2);
        // Center the row of icons.
        this.spellComponentBar.x = (this.game.game.width / 2) - (this.spellComponentBar.width / 2) + (icon.width / 2) + 6;
        // Move the first icon to the left a bit.
        // Do this after centering the bar, or the whole thing will be offset.
        this.spellComponentBar.children[0].x -= 12;//;(this.spellComponentBar.children[0].width / 2) - 2;
        //this.spellComponentBar.children[0].y -= (this.spellComponentBar.children[0].height / 2) - 2;

        // Set them all to their empty scales.
        this.clearSpellComponents();
    }

    addInventoryPanel () {
        var panelScale = 3;

        this.inventoryPanel = new PhaserNineSlice.NineSlice(this.game, 0, 0, 'menus-atlas', 'nineslice-button', 78, 92, {top: 7});
        this.inventoryPanel.scale.setTo(panelScale);
        this.inventoryPanel.anchor.setTo(0.5);
        this.inventoryPanel.inputEnabled = true;
        this.inventoryPanel.input.enableDrag();
        this.inventoryPanel.input.dragDistanceThreshold = 10;
        this.inventoryPanel.events.onInputDown.add(function () {
            this.inventoryPanel.bringToTop();
        }, this);
        this.inventoryPanel.events.onInputDown.add(this.buttonOnDown, this);
        this.inventoryPanel.events.onInputUp.add(this.buttonOnUp, this);

        var closeButton = this.game.add.button(-10 + (this.inventoryPanel.width / 2) / panelScale, 10 + (-this.inventoryPanel.height / 2) / panelScale, 'menus-atlas', function () {
            this.inventoryPanel.visible = false;
        }, this);
        closeButton.frameName = 'btn-cancel';
        closeButton.scale.setTo(0.4);
        closeButton.anchor.setTo(0.5);
        closeButton.events.onInputDown.add(this.buttonOnDown, this);
        closeButton.events.onInputUp.add(this.buttonOnUp, this);
        this.inventoryPanel.addChild(closeButton);

        this.inventoryPanel.itemDetailsPanel = new PhaserNineSlice.NineSlice(this.game, 0, 0, 'menus-atlas', 'nineslice-button', 58, 46, {top: 7});
        var extraOpacity = new PhaserNineSlice.NineSlice(this.game, 0, 0, 'menus-atlas', 'nineslice-button', 58, 46, {top: 7});
        extraOpacity.anchor.setTo(1, 0);
        this.inventoryPanel.itemDetailsPanel.addChild(extraOpacity);
        this.inventoryPanel.itemDetailsPanel.anchor.setTo(1, 0);
        this.inventoryPanel.itemDetailsPanel.y = -(this.inventoryPanel.height / 2) / panelScale;
        this.inventoryPanel.itemDetailsPanel.x = -((this.inventoryPanel.width*0.5) / panelScale);// this.inventoryPanel.itemDetailsPanel.width / panelScale;
        this.inventoryPanel.addChild(this.inventoryPanel.itemDetailsPanel);
        //console.log(this.inventoryPanel.itemDetailsPanel.width);

        this.inventoryPanel.itemDetailsPanel.itemNameText = this.game.add.text(0, 0, '', {
            font: "24px Acme",
            fill: magicks.colours.black,
            align: "center",
            //stroke: magicks.colours.white,
            strokeThickness: 1
        });
        //this.inventoryPanel.itemDetailsPanel.itemNameText.addFontWeight('bolder', 0);
        this.inventoryPanel.itemDetailsPanel.itemNameText.scale.setTo(0.2);
        this.inventoryPanel.itemDetailsPanel.itemNameText.anchor.setTo(0.5, 0.5);
        this.inventoryPanel.itemDetailsPanel.itemNameText.x -= this.inventoryPanel.itemDetailsPanel.width / 2;
        this.inventoryPanel.itemDetailsPanel.itemNameText.y = 7;// -(this.inventoryPanel.itemDetailsPanel.height / 2) - 16;
        this.inventoryPanel.itemDetailsPanel.addChild(this.inventoryPanel.itemDetailsPanel.itemNameText);

        this.inventoryPanel.itemDetailsPanel.descriptionText = this.game.add.text(0, 0, '', {
            font: "24px Acme",
            fill: magicks.colours.black,
            align: "center",
            //stroke: magicks.colours.black,
            //strokeThickness: 1,
            wordWrap: true,
            wordWrapWidth: 270
        });
        this.inventoryPanel.itemDetailsPanel.descriptionText.scale.setTo(0.2);
        this.inventoryPanel.itemDetailsPanel.descriptionText.anchor.setTo(0.5, 0);
        this.inventoryPanel.itemDetailsPanel.descriptionText.x -= this.inventoryPanel.itemDetailsPanel.width / 2;
        this.inventoryPanel.itemDetailsPanel.descriptionText.y = 11;//(this.inventoryPanel.itemDetailsPanel.height / 2) + 16;
        this.inventoryPanel.itemDetailsPanel.addChild(this.inventoryPanel.itemDetailsPanel.descriptionText);
        this.inventoryPanel.itemDetailsPanel.visible = false;

        var text = this.game.add.text(-8, closeButton.y, 'Inventory', {
            font: "28px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 5
        });
        text.scale.setTo(0.3);
        text.anchor.setTo(0.5);
        this.inventoryPanel.addChild(text);

        this.inventoryPanel.slots = this.game.add.group();
        this.inventoryPanel.items = this.game.add.group();

        var slot,
            item,
            i,
            j,
            spacing = 18,
            inventoryIndex = 0,
            itemOnUpCallback = function (item, pointer) {
                // Is the item outside the bounds of the inventory panel when the press is released.
                if(Phaser.Rectangle.intersects(item.getBounds(), this.inventoryPanel.getBounds())){
                    //console.log("item intersects inventory panel");
                    item.x = item.origX;
                    item.y = item.origY;
                }
                // Item is out of the inventory panel. Drop it on the floor.
                else {
                    //console.log("item is out of the inventory panel");
                    item.x = item.origX;
                    item.y = item.origY;
                    // Let the "remove_item" event deal with hiding the item sprite.

                    // Tell the server to drop this item for this entity.
                    socketGS.emit("drop_item", {index: item.inventoryIndex, targetX: pointer.worldX / GAME_SCALE, targetY: pointer.worldY / GAME_SCALE});
                }
            };
        for(i=0; i<4; i+=1){
            for(j=0; j<4; j+=1){

                slot = this.game.add.button(j * spacing, i * spacing, 'game-atlas', this.inventorySlotPressed, this);
                slot.frameName = 'btn-item-slot';
                slot.scale.setTo(0.7);
                slot.anchor.setTo(0.5);
                slot.alpha = 0.5;
                slot.inventoryIndex = inventoryIndex;
                slot.itemDetails = {};
                slot.itemDetails.itemName = '';
                slot.itemDetails.description = '';
                // The time this slot button was most recently pressed. Used for double-pressing.
                slot.lastPressedTime = Date.now();
                slot.events.onInputDown.add(this.showItemDetails, this);
                slot.events.onInputDown.add(this.buttonOnDown, this);
                slot.events.onInputUp.add(this.buttonOnUp, this);

                item = this.game.add.sprite(j * spacing, i * spacing, 'game-atlas', 'heal-orb-1');
                item.origX = j * spacing;
                item.origY = i * spacing;
                item.inventoryIndex = inventoryIndex;
                item.scale.setTo(0.6);
                item.anchor.setTo(0.5);
                item.visible = false;
                item.inputEnabled = true;
                item.input.enableDrag();
                //item.input.dragDistanceThreshold = 20;
                item.events.onInputDown.add(this.showItemDetails, this);
                item.events.onInputDown.add(this.buttonOnDown, this);
                item.events.onInputUp.add(this.buttonOnUp, this);
                item.events.onInputUp.add(this.inventorySlotPressed, this);
                item.events.onInputUp.add(itemOnUpCallback, this);

                inventoryIndex+=1;

                this.inventoryPanel.slots.add(slot);
                this.inventoryPanel.items.add(item);
                //console.log("new slot at x: " + j * spacing + ", y: " + i * spacing);

            }
        }

        this.inventoryPanel.addChild(this.inventoryPanel.slots);
        this.inventoryPanel.addChild(this.inventoryPanel.items);

        this.inventoryPanel.slots.x -= (this.inventoryPanel.slots.width / 2) - (slot.width / 2);
        this.inventoryPanel.slots.y -= slot.height + 3;
        this.inventoryPanel.items.x -= (this.inventoryPanel.slots.width / 2) - (slot.width / 2);
        this.inventoryPanel.items.y -= slot.height + 3;

        //console.log("inventory panel added");

        this.inventoryPanel.x = 620;// this.game.game.width - (this.inventoryPanel.width / 2) - 20;
        this.inventoryPanel.y = 215;//this.game.game.height - (this.inventoryPanel.height / 2) - 20;
        this.inventoryPanel.visible = false;

        this.GUIGroup.add(this.inventoryPanel);

        this.inventoryPanel.itemTypes = {
            WOOD: {
                frameName: "wood-icon",
                itemName: "Wood",
                description: "Used in building Order structures."
            },
            GEMS: {
                frameName: "gems-icon",
                itemName: "Gems",
                description: "Used in building Order structures, as a power source, and as a currency. Can be consumed to regain energy."
            },
            FOOD:{
                frameName: "food-icon",
                itemName: "Food",
                description: "Used to recruit NPC allies to your Order. Can be consumed to regain hitpoints."
            },
            SCROLL: {
                frameName: "scroll-icon",
                itemName: "Scroll",
                description: "Use this scroll to learn this spell."
            },
            CORE: {
                frameName: "core-icon",
                itemName: "Order Core",
                description: "Drop on the ground to build a Core."
            }
        }

    }

    addMiniMapPanel () {
        var panelScale = 3;

        this.miniMapPanel = new PhaserNineSlice.NineSlice(this.game, 0, 0, 'menus-atlas', 'nineslice-button', 78, 92, {top: 7});
        this.miniMapPanel.scale.setTo(panelScale);
        this.miniMapPanel.anchor.setTo(0.5);
        this.miniMapPanel.inputEnabled = true;
        this.miniMapPanel.input.enableDrag();
        this.miniMapPanel.input.dragDistanceThreshold = 10;
        this.miniMapPanel.events.onInputDown.add(function () {
            this.miniMapPanel.bringToTop();
        }, this);
        this.miniMapPanel.events.onInputDown.add(this.buttonOnDown, this);
        this.miniMapPanel.events.onInputUp.add(this.buttonOnUp, this);

        var closeButton = this.game.add.button(-10 + (this.miniMapPanel.width / 2) / panelScale, 10 + (-this.miniMapPanel.height / 2) / panelScale, 'menus-atlas', function () {
            this.miniMapPanel.visible = false;
        }, this);
        closeButton.frameName = 'btn-cancel';
        closeButton.scale.setTo(0.4);
        closeButton.anchor.setTo(0.5);
        closeButton.events.onInputDown.add(this.buttonOnDown, this);
        closeButton.events.onInputUp.add(this.buttonOnUp, this);
        this.miniMapPanel.addChild(closeButton);

        var text = this.game.add.text(-8, closeButton.y, 'Mini-map', {
            font: "28px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 5
        });
        text.scale.setTo(0.3);
        text.anchor.setTo(0.5);
        this.miniMapPanel.addChild(text);

        // Use the bitmap data to create the map. 1 pixel for each floor tile.
        this.miniMapPanel.miniMap = this.game.tilemap.miniMapBitmapData.addToWorld(0, 8, 0, 0, 2, 2);
        this.miniMapPanel.miniMap.scale.setTo(1);
        this.miniMapPanel.miniMap.anchor.setTo(0.5);

        this.miniMapPanel.addChild(this.miniMapPanel.miniMap);

        this.miniMapPanel.x = 620-30;
        this.miniMapPanel.y = 215+15;
        this.miniMapPanel.visible = false;

        this.GUIGroup.add(this.miniMapPanel);
    }

    addOrderPanel () {
        var panelScale = 3;

        this.orderPanel = new PhaserNineSlice.NineSlice(this.game, 0, 0, 'menus-atlas', 'nineslice-button', 78, 92, {top: 7});
        this.orderPanel.scale.setTo(panelScale);
        this.orderPanel.anchor.setTo(0.5);
        this.orderPanel.inputEnabled = true;
        this.orderPanel.input.enableDrag();
        this.orderPanel.input.dragDistanceThreshold = 10;
        this.orderPanel.events.onInputDown.add(function () {
            this.orderPanel.bringToTop();
        }, this);
        this.orderPanel.events.onInputDown.add(this.buttonOnDown, this);
        this.orderPanel.events.onInputUp.add(this.buttonOnUp, this);

        var closeButton = this.game.add.button(-10 + (this.orderPanel.width / 2) / panelScale, 10 + (-this.orderPanel.height / 2) / panelScale, 'menus-atlas', function () {
            this.orderPanel.visible = false;
        }, this);
        closeButton.frameName = 'btn-cancel';
        closeButton.scale.setTo(0.4);
        closeButton.anchor.setTo(0.5);
        closeButton.events.onInputDown.add(this.buttonOnDown, this);
        closeButton.events.onInputUp.add(this.buttonOnUp, this);
        this.orderPanel.addChild(closeButton);

        var text = this.game.add.text(-14, closeButton.y, 'Order', {
            font: "28px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 5
        });
        text.scale.setTo(0.3);
        text.anchor.setTo(0.5);
        this.orderPanel.addChild(text);

        this.orderPanel.x = 620-60;
        this.orderPanel.y = 215+30;
        this.orderPanel.visible = false;

        this.orderPanel.craftCoreText = this.game.add.text(0, -10, 'You are not in an Order.\nBuild a Core to start one.', {
            font: "28px Acme",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 5
        });
        this.orderPanel.craftCoreText.scale.setTo(0.25);
        this.orderPanel.craftCoreText.anchor.setTo(0.5);
        this.orderPanel.addChild(this.orderPanel.craftCoreText);

        this.orderPanel.craftCoreButton = this.game.add.button(0, 20, 'game-atlas', function () {
            console.log("craft core btn pressed");
            socketGS.emit('craft', 'CORE');
        }, this);
        this.orderPanel.addChild(this.orderPanel.craftCoreButton);
        this.orderPanel.craftCoreButton.frameName = 'btn-craft-core';
        this.orderPanel.craftCoreButton.anchor.setTo(0.5);

        this.GUIGroup.add(this.orderPanel);
    }

    inventorySlotPressed (btn) {
        //console.log("\ninventory slot pressed");

        var slot = this.inventoryPanel.slots.children[btn.inventoryIndex];

        if(slot.lastPressedTime + 1000 > Date.now()){
            //console.log("double pressed in time");
            // Check there is actually an item in that slot.
            if(this.inventoryPanel.items.children[btn.inventoryIndex].visible === true){
                socketGS.emit("use_item", btn.inventoryIndex);
            }
        }
        else {
            //console.log("double pressed too slow");
        }

        this.inventoryPanel.itemDetailsPanel.visible = false;

        slot.lastPressedTime = Date.now();

    }

    showItemDetails (btn) {
        var slot = this.inventoryPanel.slots.children[btn.inventoryIndex];
        // If the item in this slot is not visible, there is no item there.
        if(this.inventoryPanel.items.children[btn.inventoryIndex].visible === true){
            // Show the item details panel.
            this.inventoryPanel.itemDetailsPanel.visible = true;

            this.inventoryPanel.itemDetailsPanel.itemNameText.text = slot.itemDetails.itemName;
            this.inventoryPanel.itemDetailsPanel.descriptionText.text = slot.itemDetails.description;
        }

    }

    updateHitPoints (newHitPoints) {
        //console.log("update hitpoints");
        var hitPointsLeft = newHitPoints;
        var children = this.hitPointsBar.children;
        for(var i=0, len=children.length; i<len; i+=1){
            if(hitPointsLeft >= 4){
                children[i].frameName = 'hitpoint-4';
                hitPointsLeft -= 4;
                _this.add.tween(children[i].scale).to({x: 2, y: 2}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(hitPointsLeft === 3){
                children[i].frameName = 'hitpoint-3';
                hitPointsLeft -= 3;
                _this.add.tween(children[i].scale).to({x: 1.8, y: 1.8}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(hitPointsLeft === 2){
                children[i].frameName = 'hitpoint-2';
                hitPointsLeft -= 2;
                _this.add.tween(children[i].scale).to({x: 1.6, y: 1.6}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(hitPointsLeft === 1){
                children[i].frameName = 'hitpoint-1';
                hitPointsLeft -= 1;
                _this.add.tween(children[i].scale).to({x: 1.4, y: 1.4}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(hitPointsLeft === 0){
                children[i].frameName = 'hitpoint-0';
                _this.add.tween(children[i].scale).to({x: 1.2, y: 1.2}, 500, Phaser.Easing.Bounce.Out, true);
            }
        }
    }

    updateEnergy (newEnergy) {
        var energyLeft = newEnergy;
        var children = this.energyBar.children;
        for(var i= 0, len=children.length; i<len; i+=1){
            if(energyLeft >= 4){
                children[i].frameName = 'energy-4';
                energyLeft -= 4;
                _this.add.tween(children[i].scale).to({x: 2, y: 2}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(energyLeft === 3){
                children[i].frameName = 'energy-3';
                energyLeft -= 3;
                _this.add.tween(children[i].scale).to({x: 1.8, y: 1.8}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(energyLeft === 2){
                children[i].frameName = 'energy-2';
                energyLeft -= 2;
                _this.add.tween(children[i].scale).to({x: 1.6, y: 1.6}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(energyLeft === 1){
                children[i].frameName = 'energy-1';
                energyLeft -= 1;
                _this.add.tween(children[i].scale).to({x: 1.4, y: 1.4}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else if(energyLeft === 0){
                children[i].frameName = 'energy-0';
                _this.add.tween(children[i].scale).to({x: 1.2, y: 1.2}, 500, Phaser.Easing.Bounce.Out, true);
            }
        }
    }

    updateRevive (newRevive) {
        //console.log("in update revive");
        var reviveLeft = newRevive;
        var children = this.reviveBar.children;
        for(var i=0, len=children.length; i<len; i+=1){
            if(reviveLeft >= 4){
                children[i].frameName = 'revive-4';
                reviveLeft -= 4;
                _this.add.tween(children[i].scale).to({x: 2, y: 2}, 250, Phaser.Easing.Bounce.Out, true);
            }
            else if(reviveLeft === 3){
                children[i].frameName = 'revive-3';
                reviveLeft -= 3;
                _this.add.tween(children[i].scale).to({x: 1.8, y: 1.8}, 250, Phaser.Easing.Bounce.Out, true);
            }
            else if(reviveLeft === 2){
                children[i].frameName = 'revive-2';
                reviveLeft -= 2;
                _this.add.tween(children[i].scale).to({x: 1.6, y: 1.6}, 250, Phaser.Easing.Bounce.Out, true);
            }
            else if(reviveLeft === 1){
                children[i].frameName = 'revive-1';
                reviveLeft -= 1;
                _this.add.tween(children[i].scale).to({x: 1.4, y: 1.4}, 250, Phaser.Easing.Bounce.Out, true);
            }
            else if(reviveLeft === 0){
                children[i].frameName = 'revive-0';
                _this.add.tween(children[i].scale).to({x: 1.2, y: 1.2}, 250, Phaser.Easing.Bounce.Out, true);
            }
        }
    }

    updateAlertText (text) {
        this.alertText.visible = true;
        this.alertText.alpha = 1;
        this.alertText.text = text+''; // Make sure it is a string.
        var tween = this.game.add.tween(this.alertText).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true, 1000 + (text.length * 120));
        tween.onComplete.add(function () {
            this.alertText.visible = false;
        }, this);
    }

    /**
     * Change the frame of the next empty spell component slot in the spell components bar to be the frame of the given element.
     * @param {String} newElementString - The name of the element to add. i.e. 'void', 'fire' the frame of.
     */
    fillNextEmptySpellComponentFrame (newElementString) {
        //console.log("fillNextEmptySpellComponentFrame");
        var children = this.spellComponentBar.children;
        for(var i=0, len=children.length; i<len; i+=1){
            // Find the first empty component slot.
            if(children[i].frameName === 'component-empty'){
                // Make the first component (the primary element) a bit bigger.
                if(i === 0){
                    _this.add.tween(children[i].scale).to({x: 6, y: 6}, 500, Phaser.Easing.Bounce.Out, true);
                }
                else {
                    _this.add.tween(children[i].scale).to({x: 4, y: 4}, 500, Phaser.Easing.Bounce.Out, true);
                }
                // Set the frame of this icon to the element that was given.
                children[i].frameName = 'component-' + newElementString;
                return;
            }
        }
    }

    /**
     * Add another element to the stack of elements in the current chain.
     * @param {String} elementString
     */
    addSpellComponent (elementString) {
        //console.log("button.childrenindex:");
        //console.log(button);
        if(this.game.gameplayInputEnabled === false){
            return;
        }

        // Stop more elements being added if the input for that element is disabled.
        // The button might not be visible, but keyboard keys can still call this addSpellComponent function.
        if(this.elementButtons.byElement[elementString].inputEnabled === false){
            return;
        }

        // Get the first character of the element string, which is the element code, to update
        // the current spell code that will be sent to the server when the player casts.
        this.game.spellCode += elementString[0];

        var unlockedSpells = magicks.playerData.unlockedSpells;
        //console.log(magicks.playerData);

        // Add a element coloured slot to the end of the spell component bar.
        this.fillNextEmptySpellComponentFrame(elementString);

        var spellCodeLength = this.game.spellCode.length;

        if(unlockedSpells.hasOwnProperty(this.game.spellCode) || spellCodeLength === 1){
            this.spellComponentBar.alpha = 1;
        }
        else {
            this.spellComponentBar.alpha = 0.7;
        }

        // Hide all of the element buttons. Below, it decides to show only the ones that would be valid.
        this.elementButtons.setVisibility(false);

        this.updateCastRangeCircle();


        //console.log("elem string: " + elementString);
        //console.log("active spell: " + this.game.spellCode);
        //console.log("spellCodeLength: " + spellCodeLength);
        //console.log("");

        // Check what elements would be valid for the next element in the stack.
        // Only let an element be added if it would result in a valid spell.
        for(var spellCode in unlockedSpells){
            if(unlockedSpells.hasOwnProperty(spellCode) || spellCodeLength === 1){
                //console.log("spellCode: " + spellCode);
                //console.log("spellCode substring : " + spellCode.substring(0, spellCodeLength));

                // Check if any spell code exists with this element as the next element in the stack, and also check that the whole spell code matches for the current spell, not just for the next character.
                if(spellCode[spellCodeLength] === 'l' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: l");
                    this.elementButtons.showButton('light');
                }
                else if(spellCode[spellCodeLength] === 'd' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: d");
                    this.elementButtons.showButton('dark');
                }
                else if(spellCode[spellCodeLength] === 'w' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: w");
                    this.elementButtons.showButton('water');
                }
                else if(spellCode[spellCodeLength] === 'f' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: f");
                    this.elementButtons.showButton('fire');
                }
                else if(spellCode[spellCodeLength] === 'e' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: e");
                    this.elementButtons.showButton('earth');
                }
                else if(spellCode[spellCodeLength] === 'p' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: p");
                    this.elementButtons.showButton('psychic');
                }
                else if(spellCode[spellCodeLength] === 'v' && spellCode.substring(0, spellCodeLength) == this.game.spellCode){
                    //console.log("possible next element: v");
                    this.elementButtons.showButton('void');
                }
            }
        }
    }

    /**
     * Clear the current spell and empty all of the component slots in the spell components bar.
     */
    clearSpellComponents () {
        //console.log("clearSpellComponents");
        // Only show the element buttons again if the gameplay input is enabled.
        if(this.game.gameplayInputEnabled === true){
            this.elementButtons.setVisibility(true);
        }
        // Set all of the spell component icons back to empty.
        var children = this.spellComponentBar.children;
        for(var i=0, len=children.length; i<len; i+=1){
            children[i].frameName = 'component-empty';
            // Make the first component (the primary element) a bit bigger.
            if(i === 0){
                _this.add.tween(children[i].scale).to({x: 3, y: 3}, 500, Phaser.Easing.Bounce.Out, true);
            }
            else {
                _this.add.tween(children[i].scale).to({x: 2, y: 2}, 500, Phaser.Easing.Bounce.Out, true);
            }
        }
        this.spellComponentBar.alpha = 0.7;
        this.game.spellCode = '';
        this.updateCastRangeCircle();
    }

    /**
     * Used to stop all pointer inputs from casting a spell. Don't cast a spell when the input was on a button.
     * Add this to the onInputDown callback of all GUI buttons. events.onInputDown.add(this.GUI.buttonOnDown, this);
     */
    buttonOnDown () {
        //console.log("button pressed");
        this.anyPointerOnGUIButton = true;
    }

    /**
     * Used to allow all pointer inputs to casting a spell again. Can cast a spell the the input isn't on any button.
     * Add this to the onInputUp callback of all GUI buttons. events.onInputUp.add(this.GUI.buttonOnUp, this);
     */
    buttonOnUp () {
        //console.log("button released");
        this.anyPointerOnGUIButton = false;
    }

    /**
     * Show/hide the options menu.
     */
    optionsPressed () {
        //console.log("options pressed");
        var scale = magicks.settings.GUI.options.scale;
        // If the option buttons are already visible, hide them.
        if(this.optionsList[1].alpha > 0){
            // Reverse the loop order when hiding.
            for(var i=this.optionsList.length-1, len=0; i>len; i-=1){
                this.game.add.tween(this.optionsList[i].scale       ).to({x: scale * 0.5, y: scale * 0.5},  80, "Linear", true, (0.1*500) * (1 + this.optionsList.length - i));
                this.game.add.tween(this.optionsList[i]             ).to({alpha: 0},                        80, "Linear", true, (0.1*500) * (1 + this.optionsList.length - i));
            }
        }
        // Show the option buttons.
        else {
            for(var i=1, len=this.optionsList.length; i<len; i+=1){
                this.game.add.tween(this.optionsList[i].scale       ).to({x: scale, y: scale},  80, "Linear", true, (0.1*500) * (1 + i));
                this.game.add.tween(this.optionsList[i]             ).to({alpha: 100},          80, "Linear", true, (0.1*500) * (1 + i));
            }
        }
    }

    audioOnPressed () {
        this.optionsPressed();
        this.audioOnButton.visible = false;
        this.audioOffButton.visible = true;
        magicks.settings.audioVolume = 0;
    }

    audioOffPressed () {
        this.optionsPressed();
        this.audioOnButton.visible = true;
        this.audioOffButton.visible = false;
        magicks.settings.audioVolume = 1;
    }

    fullscreenPressed () {
        this.optionsPressed();
        if(this.game.scale.isFullScreen){
            this.game.scale.stopFullScreen();
        }
        else{
            this.game.scale.startFullScreen(false);
        }
    }

    /**
     * Tell the game server that this player wants to leave the game.
     */
    exitPressed () {
        socketGS.emit("leave_world");
        // Empty objects {} are truthy, so this.GUI.stick is null if stick isn't created. ;o
        //if(this.GUI && this.GUI.stick){
        //    // Remove the virtual joystick manually, as it is added to Game.stage (above everything else), not the game world.
        //    this.GUI.stick.destroy();
        //}
        //this.state.start("Menu");
    }

    /**
     * Tell the game server that this player wants to respawn now.
     */
    respawnPressed () {
        //console.log("respawn pressed");
        this.respawnButton.visible = false;
        this.discordButton.visible = false;
        if(magicks.adBlockEnabled){
            //this.adBlockMessage.visible = true;
            this.adBlockTimer.start();
        }
        else {
            // TODO: Show fullscreen ad.



        }
        socketGS.emit('respawn');
    }


}

export {GUI};