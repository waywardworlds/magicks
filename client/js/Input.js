/**
 * Created by User on 17/12/2016.
 */

"use strict";

class Input {
    constructor(game){

        this.game = game;

        // How often updates from the client input are sent to the server.
        this.inputEmitRate = 100;//1000/10;
        this.inputRateCounter = 0;

        // Flags for game update loop.
        this.keyboardEnabled = false;
        this.joystickEnabled = false;

        // User is playing on desktop.
        if(this.game.game.device.desktop === true) {
            this.setupKeyboardControls();
            this.setupMouseControls();
        }
        // User is playing on a mobile device.
        if(this.game.game.device.desktop === false) {
          this.setupJoystickControls();
        }

    }

    setupKeyboardControls () {
        // Flag for game update loop.
        this.keyboardEnabled = true;

        this.keyboardKeys = this.game.input.keyboard.addKeys(
            {
                arrowUp:    Phaser.KeyCode.UP,
                arrowDown:  Phaser.KeyCode.DOWN,
                arrowLeft:  Phaser.KeyCode.LEFT,
                arrowRight: Phaser.KeyCode.RIGHT,

                'up':       Phaser.KeyCode[magicks.settings.controls.moveUp],
                'down':     Phaser.KeyCode[magicks.settings.controls.moveDown],
                'left':     Phaser.KeyCode[magicks.settings.controls.moveLeft],
                'right':    Phaser.KeyCode[magicks.settings.controls.moveRight],

                'light':    Phaser.KeyCode[magicks.settings.controls.addLight],
                'dark':     Phaser.KeyCode[magicks.settings.controls.addDark],
                'water':    Phaser.KeyCode[magicks.settings.controls.addWater],
                'fire':     Phaser.KeyCode[magicks.settings.controls.addFire],
                'earth':    Phaser.KeyCode[magicks.settings.controls.addEarth],
                'psychic':  Phaser.KeyCode[magicks.settings.controls.addPsychic],
                'void':     Phaser.KeyCode[magicks.settings.controls.addVoid],

                'clearSpellComponents': Phaser.KeyCode[magicks.settings.controls.clearSpellComponents],

                'enterChat': Phaser.KeyCode[magicks.settings.controls.enterChat]
            });
        // Check for onDown events, so move input is sent straight away, without having to wait for the next input update in updateKeyboard.

        // Arrow keys. Should always be available, for those who aren't using a WASD keyboard.
        this.keyboardKeys.arrowUp.onDown.add(function () {
            if(this.keyboardKeys.arrowLeft.isDown){
                socketGS.emit("move_input", {angle: 225, force: 1});
            }
            else if(this.keyboardKeys.arrowRight.isDown){
                socketGS.emit("move_input", {angle: 315, force: 1});
            }
            else if(this.keyboardKeys.arrowDown.isDown === false){
                socketGS.emit("move_input", {angle: 270, force: 1});
            }
        }, this);
        this.keyboardKeys.arrowDown.onDown.add(function () {
            if(this.keyboardKeys.arrowLeft.isDown){
                socketGS.emit("move_input", {angle: 135, force: 1});
            }
            else if(this.keyboardKeys.arrowRight.isDown){
                socketGS.emit("move_input", {angle: 45, force: 1});
            }
            else if(this.keyboardKeys.arrowUp.isDown === false){
                socketGS.emit("move_input", {angle: 90, force: 1});
            }
        }, this);
        this.keyboardKeys.arrowLeft.onDown.add(function () {
            if(this.keyboardKeys.arrowUp.isDown){
                socketGS.emit("move_input", {angle: 225, force: 1});
            }
            else if(this.keyboardKeys.arrowDown.isDown){
                socketGS.emit("move_input", {angle: 135, force: 1});
            }
            else if(this.keyboardKeys.arrowRight.isDown === false){
                socketGS.emit("move_input", {angle: 180, force: 1});
            }
        }, this);
        this.keyboardKeys.arrowRight.onDown.add(function () {
            if(this.keyboardKeys.arrowUp.isDown){
                socketGS.emit("move_input", {angle: 315, force: 1});
            }
            else if(this.keyboardKeys.arrowDown.isDown){
                socketGS.emit("move_input", {angle: 45, force: 1});
            }
            else if(this.keyboardKeys.arrowLeft.isDown === false){
                socketGS.emit("move_input", {angle: 0, force: 1});
            }
        }, this);

        // User defined controls.
        this.keyboardKeys.up.onDown.add(function () {
            if(this.keyboardKeys.left.isDown){
                socketGS.emit("move_input", {angle: 225, force: 1});
            }
            else if(this.keyboardKeys.right.isDown){
                socketGS.emit("move_input", {angle: 315, force: 1});
            }
            else if(this.keyboardKeys.down.isDown === false){
                socketGS.emit("move_input", {angle: 270, force: 1});
            }
        }, this);
        this.keyboardKeys.down.onDown.add(function () {
            if(this.keyboardKeys.left.isDown){
                socketGS.emit("move_input", {angle: 135, force: 1});
            }
            else if(this.keyboardKeys.right.isDown){
                socketGS.emit("move_input", {angle: 45, force: 1});
            }
            else if(this.keyboardKeys.up.isDown === false){
                socketGS.emit("move_input", {angle: 90, force: 1});
            }
        }, this);
        this.keyboardKeys.left.onDown.add(function () {
            if(this.keyboardKeys.up.isDown){
                socketGS.emit("move_input", {angle: 225, force: 1});
            }
            else if(this.keyboardKeys.down.isDown){
                socketGS.emit("move_input", {angle: 135, force: 1});
            }
            else if(this.keyboardKeys.right.isDown === false){
                socketGS.emit("move_input", {angle: 180, force: 1});
            }
        }, this);
        this.keyboardKeys.right.onDown.add(function () {
            if(this.keyboardKeys.up.isDown){
                socketGS.emit("move_input", {angle: 315, force: 1});
            }
            else if(this.keyboardKeys.down.isDown){
                socketGS.emit("move_input", {angle: 45, force: 1});
            }
            else if(this.keyboardKeys.left.isDown === false){
                socketGS.emit("move_input", {angle: 0, force: 1});
            }
        }, this);


        this.keyboardKeys.arrowUp.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'y'});
        });
        this.keyboardKeys.arrowDown.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'y'});
        });
        this.keyboardKeys.arrowLeft.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'x'});
        });
        this.keyboardKeys.arrowRight.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'x'});
        });


        this.keyboardKeys.up.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'y'});
        });
        this.keyboardKeys.down.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'y'});
        });
        this.keyboardKeys.left.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'x'});
        });
        this.keyboardKeys.right.onUp.add(function () {
            socketGS.emit("move_input_keyboard_released", {axis: 'x'});
        });


        this.keyboardKeys.light.onDown.add(function () {
            this.game.GUI.addSpellComponent('light');
        }, this);
        this.keyboardKeys.dark.onDown.add(function () {
            this.game.GUI.addSpellComponent('dark');
        }, this);
        this.keyboardKeys.water.onDown.add(function () {
            this.game.GUI.addSpellComponent('water');
        }, this);
        this.keyboardKeys.fire.onDown.add(function () {
            this.game.GUI.addSpellComponent('fire');
        }, this);
        this.keyboardKeys.earth.onDown.add(function () {
            this.game.GUI.addSpellComponent('earth');
        }, this);
        this.keyboardKeys.psychic.onDown.add(function () {
            this.game.GUI.addSpellComponent('psychic');
        }, this);
        this.keyboardKeys.void.onDown.add(function () {
            this.game.GUI.addSpellComponent('void');
        }, this);

        this.keyboardKeys.clearSpellComponents.onDown.add(function () {
            //console.log();
            this.game.GUI.clearSpellComponents();
        }, this);

        this.keyboardKeys.enterChat.onUp.add(function () {
            if(this.game.gameplayInputEnabled === false){
                return;
            }
            // If the chat input is hidden, show it.
            if(this.game.GUI.chatInput.visible === false){
                this.game.GUI.chatInput.visible = true;
                this.game.GUI.chatInput.startFocus();
            }
            else {
                this.game.GUI.chatInput.visible = false;
                this.game.GUI.chatInput.endFocus();
                // Don't send empty messages.
                if(this.game.GUI.chatInput.value.length > 0){
                    // Send the message to the game server, to broadcast to all players in the same zone as the player.
                    socketGS.emit('chat', this.game.GUI.chatInput.value);
                    // Clear the input box ready for the next time they want to enter a message.
                    this.game.GUI.chatInput.setText('');
                }
            }
        }, this);

    }

    setupMouseControls () {
        this.game.input.onDown.add(function (pointer) {
            //console.log("mouse onup");
            // Check the pointer is not over any buttons.
            if(!this.game.GUI.anyPointerOnGUIButton){
                // Check the spell bar is empty.
                if(this.game.spellCode.length === 0){
                    //console.log("input onUp, interact with something");
                    socketGS.emit('interact', {x: pointer.worldX / GAME_SCALE, y: pointer.worldY / GAME_SCALE});
                }
            }
        }, this);
    }

    setupJoystickControls () {
        //console.log("virtual joystick added");
        this.joystickEnabled = true;
        // Set up the virtual joystick plugin.
        this.pad = this.game.game.plugins.add(Phaser.VirtualJoystick);

        this.game.GUI.stick = this.pad.addStick(magicks.settings.GUI.joystick.x, magicks.settings.GUI.joystick.y, 200, 'joystick');
        this.game.GUI.stick.scale = magicks.settings.GUI.joystick.scale;
        //this.GUI.stick.visible = false;

        this.mouseOnStick = false;
        this.pointer1OnStick = false;
        this.pointer2OnStick = false;

        this.game.input.onDown.add(function (pointer) {
            if(this.game.gameplayInputEnabled === false){
                return;
            }
            // Check if the pointer is down over the virtual joystick.
            if(this.game.GUI.stick.baseHitArea.contains(pointer.x, pointer.y)) {
                //console.log("pointer on joystick");
                if(pointer.id === this.game.input.mousePointer.id){
                    this.mouseOnStick = true;
                }
                if(pointer.id === this.game.input.pointer1.id){
                    this.pointer1OnStick = true;
                }
                if(pointer.id === this.game.input.pointer2.id){
                    this.pointer2OnStick = true;
                }
                // The pointer is down over the joystick, so don't bother doing the cast spell stuff.
                return
            }
            //console.log("joystick ondown");
        }, this);

        this.game.input.onUp.add(function (pointer) {
            if(pointer.id === this.game.input.mousePointer.id){
                if(this.mouseOnStick){
                    socketGS.emit('move_input_joystick_released');
                }
                this.mouseOnStick = false;
            }
            if(pointer.id === this.game.input.pointer1.id){
                if(this.pointer1OnStick){
                    socketGS.emit('move_input_joystick_released');
                }
                this.pointer1OnStick = false;
            }
            if(pointer.id === this.game.input.pointer2.id){
                if(this.pointer2OnStick){
                    socketGS.emit('move_input_joystick_released');
                }
                this.pointer2OnStick = false;
            }
            //console.log("joystick onup");
            // Check the pointer is not over any buttons.
            if(!this.game.GUI.anyPointerOnGUIButton){
                // Check the spell bar is empty.
                if(this.game.spellCode.length === 0){
                    //console.log("input onUp, interact with something");
                    socketGS.emit('interact', {x: pointer.worldX / GAME_SCALE, y: pointer.worldY / GAME_SCALE});
                }
            }
        }, this);
    }

    update () {
        // Don't need to emit input updates as fast as possible...
        this.inputRateCounter += this.game.game.time.elapsed;

        if(this.inputRateCounter > this.inputEmitRate){

            if(this.gameplayInputEnabled === false){
                return;
            }

            if(this.joystickEnabled){
                this.updateJoystick();
            }
            if(this.gamepadEnabled){
                //this.updateGamepad();
            }
            if(this.keyboardEnabled){
                //this.updateKeyboard(); TODO: being handled on server now

                this.updateMouse();
            }

            this.inputRateCounter = 0;

        }
    }

    updateKeyboard () {
        // Arrow keys. Should always be available, for those who aren't using a WASD keyboard.
        if(this.keyboardKeys.up.isDown === true){
            socketGS.emit("move_input_keyboard", {axis: 'y', force: -1});
            //this.inputRateCounter = 0;//<-- input rate shenannigans
            //^ add this back in, after the server vel before/after stuff is sorted.
        }
        if(this.keyboardKeys.down.isDown === true){
            socketGS.emit("move_input_keyboard", {axis: 'y', force: 1});
            //this.inputRateCounter = 0;
        }
        if(this.keyboardKeys.left.isDown === true){
            socketGS.emit("move_input_keyboard", {axis: 'x', force: -1});
            //this.inputRateCounter = 0;
        }
        if(this.keyboardKeys.right.isDown === true){
            socketGS.emit("move_input_keyboard", {axis: 'x', force: 1});
            //this.inputRateCounter = 0;
        }
    }

    updateMouse () {
        if(this.game.input.mousePointer.isDown){
            if(!this.game.GUI.anyPointerOnGUIButton){
                this.castSpell(this.game.input.mousePointer);
            }
        }
    }

    updateJoystick () {
        // Find which pointer is being used on the stick.
        if(this.game.input.mousePointer.isDown){
            //console.log("mouse ptr is down");
            if(this.mouseOnStick){
                //console.log("mouse on stick");
                if(this.game.GUI.stick.isDown){
                    socketGS.emit('move_input', {angle: this.game.GUI.stick.angle, force: this.game.GUI.stick.force});
                }
            }
            else if(!this.game.GUI.anyPointerOnGUIButton){
                this.castSpell(this.game.input.mousePointer);
            }
        }
        if(this.game.input.pointer1.isDown){
            if(this.pointer1OnStick){
                if(this.game.GUI.stick.isDown){
                    socketGS.emit('move_input', {angle: this.game.GUI.stick.angle, force: this.game.GUI.stick.force});
                }
            }
            else if(!this.game.GUI.anyPointerOnGUIButton){
                this.castSpell(this.game.input.pointer1);
            }
        }
        if(this.game.input.pointer2.isDown){
            if(this.pointer2OnStick){
                if(this.game.GUI.stick.isDown){
                    socketGS.emit('move_input', {angle: this.game.GUI.stick.angle, force: this.game.GUI.stick.force});
                }
            }
            else if(!this.game.GUI.anyPointerOnGUIButton){
                this.castSpell(this.game.input.pointer2);
            }
        }
    }

    castSpell (pointer) {
        // Make sure there is a spell code value to send.
        if(this.game.spellCode.length > 0){
            socketGS.emit('cast_spell', {x: pointer.worldX / GAME_SCALE, y: pointer.worldY / GAME_SCALE, spellCode: this.game.spellCode});
            this.inputRateCounter = 0;
        }
    }

}

export {Input};