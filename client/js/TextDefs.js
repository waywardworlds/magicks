/**
 * Created by User on 20/01/2017.
 */

var textDefs = {};

textDefs.loadDefs = function (language, game) {
    // Get the JSON data from the phaser cache.
    textDefs = game.cache.getJSON('text-defs');
    // Load only the desired language.
    textDefs = textDefs[language];
    // Assigning the text values will reassign textDefs, so add this function back onto the textDefs object.
    textDefs.loadDefs = this.loadDefs;
};

export {textDefs};