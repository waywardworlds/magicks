/**
 * Created by User on 17/12/2017.
 */

class Tilemap {
    /**
     * Create the tilemap, using map data from the game server.
     * @param {Object} game - The game state.
     * @param {Array} mapData - A 2D array of map data.
     */
    constructor(game, mapData){
        //console.log("Creating tilemap, map data:");
        //console.log(mapData);

        // A reference to the game state.
        this.game = game;

        // How big is each tile, in pixels. Width & height.
        this.tileSize = 16;

        // How many rows and columns does this map zone have. Zones can be different sizes, up to 64 x 64 tiles.
        this.mapCols = mapData.floorGrid[0].length;
        this.mapRows = mapData.floorGrid.length;

        // Resize the game world to the size of the zone.
        game.world.setBounds(0, 0, this.mapCols * this.tileSize * GAME_SCALE*2, this.mapRows * this.tileSize * GAME_SCALE*2);

        // The grids of what tiles types are where.
        // Floor is the ground logic that affects things like player move speed, hazards, element attunement, etc.
        this.floorGrid = [];

        this.landformsGrid = [];
        // Entities are non-movable parts of the environment that can be interacted with, such as structures.
        //this.entitiesGrid = [];
        // Decorations are just visual embellishments that aren't directly interacted with.
        //this.decorationsGrid = [];

        // Initialise the grids to be empty.
        for(var i=0; i<this.mapRows; i+=1){
            this.floorGrid[i] = [];
            this.landformsGrid[i] = [];
            for(var j=0; j<this.mapCols; j+=1){
                this.floorGrid[i][j] = '';
                this.landformsGrid[i][j] = '';
            }
        }

        this.miniMapBitmapData = game.make.bitmapData(this.mapCols, this.mapRows);

        // The map data separated into each layer.
        this.floorLayerData =       mapData.floorGrid;
        this.landformsLayerData =   mapData.landformsGrid;
        //this.landformsLayerData =    mapData[1];
        //this.decorationsLayerData = mapData[2];

        // The bitmap datas to use as the base textures for the image frames.
        this.floorBitmapDatas =         [];

        this.landformsBitmapData =      [];
        //this.decorationsBitmapDatas =   [];

        // The images to use as the animation frames.
        this.floorFrames =          [];
        this.landformsFrame =       undefined;
        //this.decorationsFrames =    [];

        // The sprite object used to draw the frame for each layer to the corresponding bitmap data object.
        // Just reuse this sprite instance to avoid constantly creating a sprite for each tile.
        this.floorsDrawingSprite =      game.add.sprite(0, 0, 'floors-tileset');
        this.landformsDrawingSprite =   game.add.sprite(0, 0, 'landforms-tileset');
        this.floorsDrawingSprite.visible = false;
        this.landformsDrawingSprite.visible = false;

        // Whether the tilemap should be animated with the 3 frame animation, or static with just 1 frame, depending on user settings.
        // Weaker devices might only be able to handle 1 bitmap data for each layer, so only 1 animation frame (i.e. not animated).
        this.animated = magicks.settings.mapAnimationEnabled;
        //this.animated = false;

        if(devMode){
            this.animated = true;
        }

        // Add some bitmap data objects for each layer.
        // Bitmap data objects consume a lot of memory, so be sparing with them.
        var bitmapdataCount = 1;
        if(this.animated){
            bitmapdataCount = 3;
        }

        for(var i=0; i<bitmapdataCount; i+=1){
            // Create the bitmap data objects.
            this.floorBitmapDatas.push(game.make.bitmapData(this.mapCols * this.tileSize, this.mapRows * this.tileSize));
            // Create images from each bitmap data to use as the animation frames.
            this.floorFrames.push(this.floorBitmapDatas[i].addToWorld(0, 0, 0, 0, GAME_SCALE * 2, GAME_SCALE * 2));
            // Hide all frames to start with.
            this.floorFrames[i].visible = false;
        }

        this.landformsBitmapData = game.make.bitmapData(this.mapCols * this.tileSize, this.mapRows * this.tileSize);
        //this.decorationsBitmapDatas.push(   game.make.bitmapData(this.mapCols * this.tileSize, this.mapRows * this.tileSize));

        this.landformsFrame = this.landformsBitmapData.addToWorld(0, 0, 0, 0, GAME_SCALE * 2, GAME_SCALE * 2);
        //this.decorationsFrames.push(this.decorationsBitmapDatas[i].addToWorld(  0, 0, 0, 0, GAME_SCALE * 2, GAME_SCALE * 2));

        this.floorFrames[0].visible = true;
        this.landformsFrame.visible = true;
        //this.decorationsFrames[i].visible = false;

        this.setupFloors();

        this.setupLandforms();

        //this.setupEntities();

    }

    /**
     * Change all of the floor tiles to be what they should be from the map data.
     */
    setupFloors(){
        // Set up the floor objects.
        // The list of all floor objects. The order that they are in this array should reflect their priority.
        this.floorTypes = [
            {tilesetRow: 2,     priority: 0},   // Deep water.
            {tilesetRow: 1,     priority: 1},   // Shallow water.
            {tilesetRow: 6,     priority: 2},   // Dirt.
            {tilesetRow: 7,     priority: 3},   // Mud.
            {tilesetRow: 8,     priority: 4},   // Path.
            {tilesetRow: 3,     priority: 5},   // Sand.
            {tilesetRow: 0,     priority: 6},   // Grass.
            {tilesetRow: 4,     priority: 7},   // Snow.
            {tilesetRow: 5,     priority: 8}    // Lava.

        ];

        // A way to access floors by their code.
        this.floorsByCode = {
            'D': this.floorTypes[0], // Deep water.
            'S': this.floorTypes[1], // Shallow water.
            'A': this.floorTypes[5], // Sand.
            'I': this.floorTypes[2], // Dirt.
            'M': this.floorTypes[3], // Mud.
            'P': this.floorTypes[4], // Path.
            'W': this.floorTypes[7], // Snow.
            'L': this.floorTypes[8], // Lava.
            'G': this.floorTypes[6]  // Grass.
        };

        this.miniMapColourByFloorCode = {
            D: {r: 25, g: 138, b: 177},
            S: {r: 39, g: 195, b: 214},
            A: {r: 247, g: 226, b: 107},
            I: {r: 131, g: 80, b: 27},
            M: {r: 83, g: 59, b: 41},
            P: {r: 197, g: 201, b: 184},
            W: {r: 244, g: 237, b: 232},
            L: {r: 235, g: 69, b: 12},
            G: {r: 52, g: 179, b: 45}
        };

        // Populate the floor grid with the tile data on the floor layer.
        var tileType,
            row,
            col,
            rows,
            cols;
        for(row=0, rows=this.mapRows; row<rows; row+=1){
            for(col=0, cols=this.mapCols; col<cols; col+=1){
                // Get the type string for this tile. i.e. 'G', 'D', etc.
                tileType = this.floorLayerData[row][col];
                // Set this tile in the floor grid to reference the matching floor object.
                this.floorGrid[row][col] = this.floorsByCode[tileType];
            }
        }

        //console.log("floorGrid:");
        //console.log(this.floorGrid);
        //console.log("floor layer data:");
        //console.log(this.floorLayerData);

        // The frame numbers in the tileset that correspond to each border shape.
        this.floorTileNumbers = {
            topEdge:                1,
            rightEdge:              2,
            bottomEdge:             3,
            leftEdge:               4,
            bottomLeftCorner:       5,
            topLeftCorner:          6,
            topRightCorner:         7,
            bottomRightCorner:      8,
            topLeftEdge:            9,
            topRightEdge:           10,
            bottomRightEdge:        11,
            bottomLeftEdge:         12,
            topRightBottomEdge:     13,
            leftRightBottomEdge:    14,
            topBottomLeftEdge:      15,
            topRightLeftEdge:       16,
            allEdges:               17
        };

        // All the data and storage mechanisms should be set up by now, so create the actual floor tile graphics for the map.
        for(row=0; row<this.mapRows; row+=1){
            for(col=0; col<this.mapCols; col+=1){
                this.changeFloorTile(row, col);
            }
        }
    }

    /**
     * Change a target floor tile to a specific floor type.
     * @param {Number} row
     * @param {Number} col
     * @param {Object} [floor]
     */
    changeFloorTile(row, col, floor) {
        // Skip out of grid cells.
        if (row < 0 || row > 63 || col < 0 || col > 63) {
            return;
        }

        var targetPriority;
        // If a floor is specified, use it. Otherwise, use the same floor but
        // still check the adjacent tiles in case the transitions need updating.
        if (floor) {
            //console.log("changing floor tile, floor given: " + floor);
            targetPriority = floor.priority;
            // Update the grid with the new value for the target cell.
            this.floorGrid[row][col] = floor;
        }
        else {
            targetPriority = this.floorGrid[row][col].priority;
            // Use the current floor.
            floor = this.floorGrid[row][col];
        }

        // Draw the new tile. This will be the fully filled tile, not a border.
        this.drawFloorTile(row, col, floor.tilesetRow, 0);
        this.drawMiniMapPixel(row, col, this.miniMapColourByFloorCode[this.floorLayerData[row][col]]);

        // Check if any of the adjacent tiles are of floors that are higher than this one.
        // If so, they need to be drawn on top.
        for(var i=targetPriority; i<this.floorTypes.length; i+=1){
            if(targetPriority<this.floorTypes[i].priority){
                if(this.findAdjacentFloorTile(row, col, this.floorTypes[i])){
                    this.checkAdjacentFloorTiles(row, col, this.floorTypes[i]);
                }
            }
        }

    }

    /**
     * Find any tiles around the target one that have the same floor type.
     * @param {Number} row
     * @param {Number} col
     * @param {Object} floor
     * @returns {boolean}
     */
    findAdjacentFloorTile(row, col, floor){
        for(var rowOffset = -1; rowOffset<2; rowOffset+=1){
            for(var colOffset = -1; colOffset<2; colOffset+=1){
                // Skip the target cell. Only care about the ones around it.
                if(rowOffset === 0 && colOffset === 0){
                    continue;
                }
                // Outside of the map grid bounds.
                if(row+rowOffset < 0 || col+colOffset < 0 || row+rowOffset > 63 || col+colOffset > 63){
                    // Consider areas out of the map grid to be the same as the cardinally adjacent tile.
                    return true;
                }
                if(this.floorGrid[row+rowOffset][col+colOffset] === floor){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks the tiles around the target tile to find which border to draw, then draws it.
     * @param {Number} row
     * @param {Number} col
     * @param {Object} floor
     */
    checkAdjacentFloorTiles(row, col, floor) {

        var adjacents = [false, false, false, false, false, false, false, false];
        var tilesetRow = floor.tilesetRow;

        /*
         0 | 1 | 2
         - # - # -
         3 |   | 4
         - # - # -
         5 | 6 | 7
         */

        var position = 0;
        // Check all the cells in a 3x3 matrix around (and including) the target cell to find the adjacents.
        for (var rowOffset = -1; rowOffset < 2; rowOffset += 1) {
            for (var colOffset = -1; colOffset < 2; colOffset += 1) {
                // Skip the target cell. Only care about the ones around it.
                if (rowOffset === 0 && colOffset === 0) {
                    continue;
                }
                // Check for out of grid elements.
                if (row + rowOffset < 0 || col + colOffset < 0 || row + rowOffset > 63 || col + colOffset > 63) {
                    // Consider areas out of the map grid to be the same as the cardinally adjacent tile.
                    adjacents[position] = false;
                }
                // Check if the adjacent tile is the same type at the target tile. Don't care if it is different.
                else if (this.floorGrid[row + rowOffset][col + colOffset] === floor) {
                    adjacents[position] = true;
                }
                position += 1;
            }
        }

        // - - - Cardinal directions - - -
        // All edges.
        if (adjacents[1] && adjacents[4] && adjacents[6] && adjacents[3]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.allEdges);
        }
        // All except left.
        else if (adjacents[1] && adjacents[4] && adjacents[6]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topRightBottomEdge);
        }
        // All except top.
        else if (adjacents[4] && adjacents[6] && adjacents[3]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.leftRightBottomEdge);
        }
        // All except right.
        else if (adjacents[6] && adjacents[3] && adjacents[1]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topBottomLeftEdge);
        }
        // All except bottom.
        else if (adjacents[3] && adjacents[1] && adjacents[4]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topRightLeftEdge);
        }
        // Top left edge.
        else if (adjacents[1] && adjacents[3]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topLeftEdge);
        }
        // Top right edge.
        else if (adjacents[1] && adjacents[4]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topRightEdge);
        }
        // Bottom right edge.
        else if (adjacents[4] && adjacents[6]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.bottomRightEdge);
        }
        // Bottom left edge.
        else if (adjacents[3] && adjacents[6]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.bottomLeftEdge);
        }
        // Top edge.
        else if (adjacents[1]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topEdge);
            // Horizontal edges.
            if (adjacents[6]) {
                this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.bottomEdge);
            }
        }
        // Right edge.
        else if (adjacents[4]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.rightEdge);
            // Vertical edges.
            if (adjacents[3]) {
                this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.leftEdge);
            }
        }
        // Bottom edge.
        else if (adjacents[6]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.bottomEdge);
        }
        // Left edge.
        else if (adjacents[3]) {
            this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.leftEdge);
        }

        // - - - Corners - - -
        if (adjacents[1] === false) {
            if (adjacents[0] && adjacents[3] === false) {
                this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topLeftCorner);
            }
            if (adjacents[2] && adjacents[4] === false) {
                this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.topRightCorner);
            }
        }
        if (adjacents[6] === false) {
            if (adjacents[5] && adjacents[3] === false) {
                this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.bottomLeftCorner);
            }
            if (adjacents[7] && adjacents[4] === false) {
                this.drawFloorTile(row, col, tilesetRow, this.floorTileNumbers.bottomRightCorner);
            }
        }
    }

    /**
     * Update the target tile, and also all of the ones around it so
     * they have the right transitions for the new target tile.
     * @param {Number} row
     * @param {Number} col
     * @param {Object} floorCode
     */
    updateFloorTile(row, col, floorCode){
        //console.log("updatefloor, floorCode: " + floorCode);
        // Update the target tile.
        this.changeFloorTile(row, col, this.floorsByCode[floorCode]);
        // Update the adjacent tiles.
        this.changeFloorTile(row-1,  col-1);
        this.changeFloorTile(row-1,  col);
        this.changeFloorTile(row-1,  col+1);
        this.changeFloorTile(row,    col-1);
        this.changeFloorTile(row,    col+1);
        this.changeFloorTile(row+1,  col-1);
        this.changeFloorTile(row+1,  col);
        this.changeFloorTile(row+1,  col+1);
    }

    /**
     * Draw a tile onto the bitmap datas.
     * @param {Number} row - The row on the bitmap data to modify.
     * @param {Number} col - The column on the bitmap data to modify.
     * @param {Number} tilesetRow - The row in the floors tileset that the graphics for this terrain type is on.
     * @param {Number} tileNumber - The column in the tileset row for the specific border tile.
     */
    drawFloorTile(row, col, tilesetRow, tileNumber){
        //console.log("drawing tile: " + tileNumber + " from tileset row: " + tilesetRow);
        for(var i=0, len=this.floorBitmapDatas.length; i<len; i+=1){
            this.floorsDrawingSprite.frame = (
                57 * tilesetRow) + // The tileset is 57 tiles wide in total. Each frame has 19 tiles.
                (tileNumber + // Go to column the desired tile is at within its frame.
                (19 * i)) + // Multiply by i to get the animation frame in the tileset for this image.
                1;// Add 1 to offset for the black tile column.
            this.floorBitmapDatas[i].draw(this.floorsDrawingSprite, col * this.tileSize, row * this.tileSize);
        }
    }

    drawMiniMapPixel(row, col, colour){
        this.miniMapBitmapData.setPixel(col, row, colour.r, colour.g, colour.b);
    }

    setupLandforms(){
        // A way to access tileset rows for landforms by their code.
        // Landforms have no draw priority, as they shouldn't be overlapping each other.
        this.tilesetRowByLandformCode = {
            'M': 0,
            'C': 1,
            'S': 2
        };

        this.miniMapColourByLandformCode = {
            M: {r: 171, g: 192, b: 207},
            C: {r: 193, g: 137, b: 76},
            S: {r: 111, g: 124, b: 112}
        };

        // Populate the landforms grid with the tile data on the landforms layer.
        var tileType,
            row,
            col,
            rows,
            cols;
        for(row=0, rows=this.mapRows; row<rows; row+=1){
            for(col=0, cols=this.mapCols; col<cols; col+=1){
                // Get the type string for this tile. i.e. 'M', 'C', etc.
                tileType = this.landformsLayerData[row][col];
                //console.log(tileType);
                // Check that type of entity is valid. Might be '' or some undefined type.
                if(this.tilesetRowByLandformCode[tileType] !== undefined){
                    //console.log("row: " + row + ", col: " + col);
                    // Set this tile in the entities grid to reference the matching entity type object.
                    this.landformsGrid[row][col] = this.tilesetRowByLandformCode[tileType];
                }
            }
        }

        // The frame numbers in the tileset that correspond to each border shape.
        this.landformTileNumbers = {
            filled:                 0,
            topEdge:                1,
            rightEdge:              2,
            bottomEdge:             3,
            leftEdge:               4,
            bottomLeftCorner:       5,
            topLeftCorner:          6,
            topRightCorner:         7,
            bottomRightCorner:      8,
            topLeftEdge:            9,
            topRightEdge:           10,
            bottomRightEdge:        11,
            bottomLeftEdge:         12
        };

        // All the data and storage mechanisms should be set up by now, so create the actual landform tile graphics for the map.
        //for(var code in this.tilesetRowByLandformCode){
        //    console.log("tilesetRow: " + code);
        //    if(this.tilesetRowByLandformCode.hasOwnProperty(code)){
                for(row=0; row<this.mapRows; row+=1){
                    for(col=0; col<this.mapCols; col+=1){
                        if(this.landformsGrid[row][col] === ''){
                            continue;
                        }
                        this.checkAdjacentLandformTiles(row, col, this.landformsGrid[row][col]);
                        this.drawMiniMapPixel(row, col, this.miniMapColourByLandformCode[this.landformsLayerData[row][col]]);
                    }
                }
        //    }
        //}

    }

    checkAdjacentLandformTiles(row, col, tilesetRow){

        var adjacents = [false, false, false, false, false, false, false, false];

        /*
         0 | 1 | 2
         - # - # -
         3 |   | 4
         - # - # -
         5 | 6 | 7
         */

        var position = 0;
        // Check all the cells in a 3x3 matrix around (and including) the target cell to find the adjacents.
        for (var rowOffset = -1; rowOffset < 2; rowOffset += 1) {
            for (var colOffset = -1; colOffset < 2; colOffset += 1) {
                // Skip the target cell. Only care about the ones around it.
                if (rowOffset === 0 && colOffset === 0) {
                    continue;
                }
                // Check for out of grid elements.
                if (row + rowOffset < 0 || col + colOffset < 0 || row + rowOffset > 63 || col + colOffset > 63) {
                    // Consider areas out of the map grid to be the same as the cardinally adjacent tile.
                    adjacents[position] = false;
                }
                // Check if the adjacent tile is the same type at the target tile. Don't care if it is different.
                else if (this.landformsGrid[row + rowOffset][col + colOffset] === tilesetRow) {
                    adjacents[position] = true;
                }
                position += 1;
            }
        }

        if (adjacents[0] && adjacents[1] && adjacents[2] && adjacents[3] && adjacents[4] && adjacents[5] && adjacents[6] && adjacents[7]) {
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.filled);
        }
        else if(adjacents[0] === false && adjacents[1] && adjacents[3]){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.topLeftCorner);
        }
        else if(adjacents[2] === false && adjacents[1] && adjacents[4]){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.topRightCorner);
        }
        else if(adjacents[7] === false && adjacents[4] && adjacents[6]){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.bottomRightCorner);
        }
        else if(adjacents[5] === false && adjacents[3] && adjacents[6]){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.bottomLeftCorner);
        }
        else if(adjacents[1] === false && adjacents[3] === false){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.topLeftEdge);
        }
        else if(adjacents[1] === false && adjacents[4] === false){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.topRightEdge);
        }
        else if(adjacents[6] === false && adjacents[4] === false){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.bottomRightEdge);
        }
        else if(adjacents[6] === false && adjacents[3] === false){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.bottomLeftEdge);
        }
        else if(adjacents[1] === false ){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.topEdge);
        }
        else if(adjacents[3] === false ){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.leftEdge);
        }
        else if(adjacents[4] === false ){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.rightEdge);
        }
        else if(adjacents[6] === false){
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.bottomEdge);
        }
        else {
            this.drawLandformTile(row, col, tilesetRow, this.landformTileNumbers.filled);
        }


    }

    drawLandformTile(row, col, tilesetRow, tileNumber){
        this.landformsDrawingSprite.frame = (
            14 * tilesetRow) + // The tileset is 57 tiles wide in total. Each frame has 19 tiles.
            tileNumber + // Go to column the desired tile is at within its frame.
            1;// Add 1 to offset for the black tile column.
        this.landformsBitmapData.draw(this.landformsDrawingSprite, col * this.tileSize, row * this.tileSize);
    }

    /*updateDecorationsTile(){

    }*/

    changeMap(mapData){

        // How many rows and columns does this map zone have. Zones can be different sizes, up to 64 x 64 tiles.
        this.mapCols = mapData.floorGrid[0].length;
        this.mapRows = mapData.floorGrid.length;

        // Resize the game world to the size of the zone.
        this.game.world.setBounds(0, 0, this.mapCols * this.tileSize * GAME_SCALE*2, this.mapRows * this.tileSize * GAME_SCALE*2);

        // Empty the grids.
        for(var i=0; i<this.mapRows; i+=1){
            this.floorGrid[i] = [];
            this.landformsGrid[i] = [];
            for(var j=0; j<this.mapCols; j+=1){
                this.floorGrid[i][j] = '';
                this.landformsGrid[i][j] = '';
            }
        }

        // The map data separated into each layer.
        this.floorLayerData =       mapData.floorGrid;
        this.landformsLayerData =   mapData.landformsGrid;

        for(var i=0; i<this.floorBitmapDatas.length; i+=1){
            this.floorBitmapDatas[i].resize(this.mapCols * this.tileSize, this.mapRows * this.tileSize);
            this.floorBitmapDatas[i].clear();
        }
        this.landformsBitmapData.resize(this.mapCols * this.tileSize, this.mapRows * this.tileSize);
        this.landformsBitmapData.clear();

        this.setupFloors();

        this.setupLandforms();

    }

}

export {Tilemap};