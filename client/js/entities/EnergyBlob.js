/**
 * Created by User on 10/02/2018.
 */

"use strict";

var EnergyBlob = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.frameName = 'energy-blob-1';
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
EnergyBlob.prototype = Object.create(Phaser.Sprite.prototype);
EnergyBlob.prototype.constructor = EnergyBlob;

EnergyBlob.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    return this;
};

export {EnergyBlob};