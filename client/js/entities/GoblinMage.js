/**
 * Created by User on 13/02/2018.
 */

"use strict";

var GoblinMage = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    var onCompleteCallback = function () {
        //console.log("in onCompleteCallback");
        this.animations.play('idle', 4, true);
    };
    /*this.animations.add('spawn', ['critter-spawn-1', 'critter-spawn-2', 'critter-spawn-3', 'critter-spawn-4'], 4).onComplete.add(function () {
        this.hasSpawned = true;
        this.animations.play('idle', 4, true);
    }, this);*/
    this.animations.add('idle', ['goblin-move-right-1'], 4, true);
    this.animations.add('move', ['goblin-move-right-1', 'goblin-move-right-2', 'goblin-move-right-3', 'goblin-move-right-2'], 6).onComplete.add(onCompleteCallback, this);
    this.animations.add('unconscious',  ['goblin-unconscious-1'], 4, true).onComplete.add(onCompleteCallback, this);

    /*var attack = this.animations.add('attack', ['critter-attack-1', 'critter-attack-2'], 3);
    attack.onComplete.add(function () {
        this.isAttacking = false;
        this.animations.play('idle');
    }, this);
    attack.onStart.add(function () {
        this.isAttacking = true;
    }, this);*/

    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'goblin-move-right-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.addChild(this.effectLayer);

    //this.hasSpawned = false;
    //this.isAttacking = false;
    //this.carryingWood = false;
    this.isUnconscious = false;
};
GoblinMage.prototype = Object.create(Phaser.Sprite.prototype);
GoblinMage.prototype.constructor = GoblinMage;

GoblinMage.prototype.update = function () {
    this.effectLayer.frameName = this.frameName;
};

GoblinMage.prototype.move = function (x) {
    if(this.isUnconscious === true){
        return;
    }
    //if(this.hasSpawned === true){
        //console.log("hasSpawned is false");
        //if(this.isAttacking === false){
            //console.log("attacking is false");
            if(x * GAME_SCALE > this.x){
                this.scale.x = GAME_SCALE;
            }
            else {
                this.scale.x = -GAME_SCALE;
            }

            this.animations.play('move');

        //}
    //}
};

GoblinMage.prototype.attack = function () {
    //console.log("playing attack anim");
    //this.animations.play('attack');
};

GoblinMage.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.isUnconscious = false;
    //this.hasSpawned = false;
    //this.carryingWood = false;
    this.animations.stop();
    this.animations.play('idle');
    return this;
};

GoblinMage.prototype.healed = function () {
    //console.log("in healed func");
    this.effectLayer.tint = magicks.colours.healed;
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

GoblinMage.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.tint = magicks.colours.damaged;
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {GoblinMage};