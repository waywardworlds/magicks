/**
 * Created by User on 05/02/2018.
 */

"use strict";

var WoodDoor = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.frameName = 'wood-door-2';
    this.scale.setTo(GAME_SCALE * 2);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'wood-door-2');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);
};
WoodDoor.prototype = Object.create(Phaser.Sprite.prototype);
WoodDoor.prototype.constructor = WoodDoor;

WoodDoor.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    if(data.operating === true){
        this.close();
    }
    else {
        this.open();
    }
    return this;
};

WoodDoor.prototype.open = function () {
    this.frameName = 'wood-door-2';
    this.effectLayer.frameName = 'wood-door-2';
    return this;
};

WoodDoor.prototype.close = function () {
    this.frameName = 'wood-door-1';
    this.effectLayer.frameName = 'wood-door-1';
    return this;
};

WoodDoor.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {WoodDoor};
