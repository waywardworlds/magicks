/**
 * Created by User on 10/02/2018.
 */

"use strict";

var DrainingBolt = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.frameName = 'draining-bolt-1';
    this.scale.setTo(GAME_SCALE * 0.9);
    this.anchor.setTo(0.5);
};
DrainingBolt.prototype = Object.create(Phaser.Sprite.prototype);
DrainingBolt.prototype.constructor = DrainingBolt;

DrainingBolt.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    return this;
};

export {DrainingBolt};