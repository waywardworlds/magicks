/**
 * Created by User on 21/12/2017.
 */

"use strict";

var WoodWall = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.frameName = 'wood-wall-1';
    this.scale.setTo(GAME_SCALE * 2);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'wood-wall-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);
};
WoodWall.prototype = Object.create(Phaser.Sprite.prototype);
WoodWall.prototype.constructor = WoodWall;

WoodWall.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    return this;
};

WoodWall.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {WoodWall};