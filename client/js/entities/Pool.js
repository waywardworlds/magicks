/**
 * Created by User on 12/07/2017.
 */

"use strict";

var Pool = function (spriteType) {
    Phaser.Group.call(this, _this);

    this.spriteType = spriteType;
};
Pool.prototype = Object.create(Phaser.Group.prototype);
Pool.prototype.constructor = Pool;

Pool.prototype.create = function (data) {
    var sprite = this.getFirstExists(false);
    if(!sprite){
        sprite = new this.spriteType(data);
        this.add(sprite, true);
    }
    sprite.revive();
    sprite.spawn(data.x, data.y, data);
    return sprite;
};

export {Pool};