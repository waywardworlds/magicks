/**
 * Created by User on 19/07/2017.
 */

"use strict";

var TargetBolt = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas', 'target-bolt');

    this.scale.setTo(GAME_SCALE * 1.6);
    this.anchor.setTo(0.5);
};
TargetBolt.prototype = Object.create(Phaser.Sprite.prototype);
TargetBolt.prototype.constructor = TargetBolt;

TargetBolt.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    return this;
};

export {TargetBolt};