/**
 * Created by User on 06/02/2018.
 */

"use strict";

var ScrollPickup = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['scroll-pickup-1', 'scroll-pickup-2', 'scroll-pickup-3', 'scroll-pickup-2'], 4, true);
    this.scale.setTo(GAME_SCALE * 1.2);
    this.anchor.setTo(0.5);
};
ScrollPickup.prototype = Object.create(Phaser.Sprite.prototype);
ScrollPickup.prototype.constructor = ScrollPickup;

ScrollPickup.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    this.animations.play('main');
    // Play the animation from a random frame, so they aren't all bobbing up and down in synch.
    this.animations.currentAnim.setFrame(_this.rnd.integerInRange(0, 2), true);
    return this;
};

export {ScrollPickup};