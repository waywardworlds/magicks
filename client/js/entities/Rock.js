/**
 * Created by User on 05/05/2017.
 */

"use strict";

var Rock = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas', 'rock-1');

    _this.add.tween(this).to({angle: 360}, 1000, null, true, 0, -1);
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
Rock.prototype = Object.create(Phaser.Sprite.prototype);
Rock.prototype.constructor = Rock;

Rock.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    return this;
};

export {Rock};