/**
 * Created by User on 11/07/2017.
 */

"use strict";

var CharacterHuman = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas', 'character-human-base-move-down-1');

    var onCompleteCallback = function () {
        //console.log("in onCompleteCallback");
        //this.animations.play('idle', 4, true);
    };
    this.animations.add('move-up',      ['character-human-base-move-up-1',      'character-human-base-move-up-2',       'character-human-base-move-up-1',       'character-human-base-move-up-3'],      8).onComplete.add(onCompleteCallback, this);
    this.animations.add('move-down',    ['character-human-base-move-down-1',    'character-human-base-move-down-2',     'character-human-base-move-down-1',     'character-human-base-move-down-3'],    8).onComplete.add(onCompleteCallback, this);
    this.animations.add('move-left',    ['character-human-base-move-left-1',    'character-human-base-move-left-2',     'character-human-base-move-left-1',     'character-human-base-move-left-3'],    8).onComplete.add(onCompleteCallback, this);
    this.animations.add('move-right',   ['character-human-base-move-right-1',   'character-human-base-move-right-2',    'character-human-base-move-right-1',    'character-human-base-move-right-3'],   8).onComplete.add(onCompleteCallback, this);

    var attackFinished = function () {
        this.isAttacking = false
    };
    this.animations.add('attack-up',    ['character-human-base-attack-up-1',    'character-human-base-attack-up-2'],    6).onComplete.add(attackFinished, this);
    this.animations.add('attack-down',  ['character-human-base-attack-down-1',  'character-human-base-attack-down-2'],  6).onComplete.add(attackFinished, this);
    this.animations.add('attack-left',  ['character-human-base-attack-left-1',  'character-human-base-attack-left-2'],  6).onComplete.add(attackFinished, this);
    this.animations.add('attack-right', ['character-human-base-attack-right-1', 'character-human-base-attack-right-2'], 6).onComplete.add(attackFinished, this);

    this.animations.add('unconscious',  ['character-human-base-unconscious-1',  'character-human-base-unconscious-2'], 4, true).onComplete.add(onCompleteCallback, this);

    this.displayName = _this.add.text(0, -20, data.displayName, {
        font: 1.3*12 + "px Acme",
        fill: "#ffffff",
        stroke: "#000000",
        strokeThickness: 3
    });
    this.displayName.anchor.setTo(0.5);
    this.displayName.scale.setTo(0.7);
    this.addChild(this.displayName);

    this.chatText = _this.add.text(0, -24, '', {
        font: "18px Acme",
        fill: "#000000",
        stroke: "#ffffff",
        strokeThickness: 3,
        align: 'center',
        wordWrap: true,
        wordWrapWidth: 300,
        boundsAlignV: "top"
    });
    this.chatText.anchor.setTo(0.5, 1);
    this.chatText.scale.setTo(0.7);
    this.addChild(this.chatText);

    this.chatTextTween = _this.add.tween(this.chatText);

    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'character-human-base-move-down-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.addChild(this.effectLayer);

    this.isAttacking = false;
    this.isUnconscious = false;
};
CharacterHuman.prototype = Object.create(Phaser.Sprite.prototype);
CharacterHuman.prototype.constructor = CharacterHuman;

CharacterHuman.prototype.update = function () {
    //console.log("in CharHuman update");
    this.effectLayer.frameName = this.frameName;
};

CharacterHuman.prototype.move = function (x, y) {
    var playerX;
    var playerY;
    // Check the player entity exists.
    if(_this.entities[_this.playerEntityId]){
        playerX = _this.entities[_this.playerEntityId].x;
        playerY = _this.entities[_this.playerEntityId].y;
    }
    // If not, use the middle of the camera (where the player would otherwise be) as the point to check the distance from.
    else {
        playerX = _this.camera.x + _this.camera.width / 2;
        playerY = _this.camera.y + _this.camera.height / 2;
    }

    // Make this display name of the entity that moved visible/bigger as it gets closer to the player entity.
    var xDist = playerX - this.x;
    var yDist = playerY - this.y;
    var distanceFromPlayer = Math.sqrt(xDist*xDist + yDist*yDist);
    var fontDistanceSize = (310 - (distanceFromPlayer-30)) / 10;

    if(distanceFromPlayer > 300){
        this.displayName.visible = false;
    }
    else if(distanceFromPlayer <= 100){
        this.displayName.visible = true;
        this.displayName.setStyle({font: 1.3*12 + "px Acme", fill: "#ffffff", stroke: "#000000", strokeThickness: 4});
    }
    else if(fontDistanceSize > 4){
        this.displayName.visible = true;
        this.displayName.setStyle({font: (1.3*fontDistanceSize * 0.5) + "px Acme", fill: "#ffffff", stroke: "#000000", strokeThickness: 3});
    }

    var angle = Math.atan2(y * GAME_SCALE - this.y, x * GAME_SCALE - this.x) * 180 / Math.PI;

    if(this.isAttacking === true){
        return;
    }
    if(this.isUnconscious === true){
        return;
    }

    if(angle > -135 && angle < -45){
        this.animations.play('move-up');
        this.effectLayer.animations.play('move-up');
    }
    else if(angle >= -45 && angle <= 45){
        this.animations.play('move-right');
        this.effectLayer.animations.play('move-right');
    }
    else if(angle > 45 && angle < 135){
        this.animations.play('move-down');
        this.effectLayer.animations.play('move-down');
    }
    else{
        this.animations.play('move-left');
        this.effectLayer.animations.play('move-left');
    }
};

CharacterHuman.prototype.attack = function (angle) {
    this.isAttacking = true;

    if(angle > -135 && angle < -45){
        this.animations.play('attack-up');
        this.effectLayer.animations.play('attack-up');
    }
    else if(angle > -45 && angle < 45){
        this.animations.play('attack-right');
        this.effectLayer.animations.play('attack-right');
    }
    else if(angle > 45 && angle < 135){
        this.animations.play('attack-down');
        this.effectLayer.animations.play('attack-down');
    }
    else{
        this.animations.play('attack-left');
        this.effectLayer.animations.play('attack-left');
    }
};

CharacterHuman.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    if(data.invisible === true){
        this.alpha = 0;
    }
    else {
        this.alpha = 1;
    }
    this.isAttacking = false;
    this.isUnconscious = false;
    this.displayName.text = data.displayName;
    // Check if this entity is this player's entity, in case they have swapped zones, and if so, swap
    // the entity the cast range circle is a child of to the new entity sprite, as the entities list
    // gets wiped when the zone changes, so sprite the circle is a child of might be the wrong one.
    if(data.id === _this.playerEntityId){
        if(_this.GUI !== undefined){
            //this.addChild(_this.GUI.castRangeCircle); // BUGGY
        }
    }
    return this;
};

CharacterHuman.prototype.healed = function () {
    //console.log("in healed func");
    this.effectLayer.tint = magicks.colours.healed;
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

CharacterHuman.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.tint = magicks.colours.damaged;
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {CharacterHuman};