/**
 * Created by User on 05/05/2017.
 */

"use strict";

var TeleOrb = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['tele-orb-1', 'tele-orb-2', 'tele-orb-3'], 6, true);
    this.scale.setTo(GAME_SCALE * 0.8);
    this.anchor.setTo(0.5);
};
TeleOrb.prototype = Object.create(Phaser.Sprite.prototype);
TeleOrb.prototype.constructor = TeleOrb;

TeleOrb.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {TeleOrb};