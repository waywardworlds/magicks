/**
 * Created by User on 25/12/2017.
 */

"use strict";

var Nimbus = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('spawn', ['nimbus-spawn-1', 'nimbus-spawn-2', 'nimbus-spawn-3'], 4).onComplete.add(function () {
        this.hasSpawned = true;
        this.animations.play('main', 4, true);
    }, this);
    this.animations.add('main', ['nimbus-1'], 4);

    this.scale.setTo(GAME_SCALE * 1.2);
    this.anchor.setTo(0.5);
    this.hasSpawned = false;
    this.isAttacking = false;
};
Nimbus.prototype = Object.create(Phaser.Sprite.prototype);
Nimbus.prototype.constructor = Nimbus;

Nimbus.prototype.move = function (x) {
    if(this.hasSpawned === true){
        if(this.isAttacking === false){
            if(x * GAME_SCALE > this.x){
                this.scale.x = GAME_SCALE;
            }
            else {
                this.scale.x = -GAME_SCALE;
            }
            this.animations.play('main');
        }
    }
};

Nimbus.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.hasSpawned = false;
    this.animations.stop();
    this.animations.play('spawn');
    return this;
};

export {Nimbus};