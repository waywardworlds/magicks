/**
 * Created by User on 21/05/2017.
 */

"use strict";

var RavenStrike = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['raven-strike-1', 'raven-strike-2'], 4, true);
    this.scale.setTo(GAME_SCALE * 0.8);
    this.anchor.setTo(0.5);
};
RavenStrike.prototype = Object.create(Phaser.Sprite.prototype);
RavenStrike.prototype.constructor = RavenStrike;

RavenStrike.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {RavenStrike};