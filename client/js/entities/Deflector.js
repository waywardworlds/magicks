/**
 * Created by User on 04/02/2018.
 */

"use strict";

var Deflector = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.frameName = 'deflector-1';
    this.scale.setTo(GAME_SCALE * 0.8);
    this.anchor.setTo(0.5);
};
Deflector.prototype = Object.create(Phaser.Sprite.prototype);
Deflector.prototype.constructor = Deflector;

Deflector.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    return this;
};

export {Deflector};