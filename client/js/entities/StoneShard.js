/**
 * Created by User on 19/07/2017.
 */
"use strict";

var StoneShard = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas', 'stone-shard-1');

    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
StoneShard.prototype = Object.create(Phaser.Sprite.prototype);
StoneShard.prototype.constructor = StoneShard;

StoneShard.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    return this;
};

export {StoneShard};