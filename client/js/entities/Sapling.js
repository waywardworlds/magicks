/**
 * Created by User on 06/01/2018.
 */

"use strict";

var Sapling = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.frameName = 'sapling-1';
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'sapling-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);
};
Sapling.prototype = Object.create(Phaser.Sprite.prototype);
Sapling.prototype.constructor = Sapling;

Sapling.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    //this.animations.stop();
    //this.animations.play('main');
    return this;
};

Sapling.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {Sapling};