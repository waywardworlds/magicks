/**
 * Created by User on 21/12/2017.
 */

"use strict";

var BigTree = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.frameName = 'big-tree-1';
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'big-tree-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);
};
BigTree.prototype = Object.create(Phaser.Sprite.prototype);
BigTree.prototype.constructor = BigTree;

BigTree.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    //this.animations.stop();
    //this.animations.play('main');
    return this;
};

BigTree.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {BigTree};