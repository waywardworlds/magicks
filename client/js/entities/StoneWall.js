/**
 * Created by User on 21/12/2017.
 */

"use strict";

/*
var WallShapesEnum = {
    HORIZONTAL: "hori",
    VERTICAL: "vert",
    TOP_LEFT: "topL",
    TOP_RIGHT: "topR",
    BOTTOM_LEFT: "botL",
    BOTTOM_RIGHT: "botR"
};
*/
var StoneWall = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.frameName = 'stone-wall-1';
    /*
    this.frameName = 'stone-wall-horizontal';
    if(data.wallShape === WallShapesEnum.TOP_LEFT){
        this.frameName = 'stone-wall-top-left';
    }
    else if(data.wallShape === WallShapesEnum.TOP_RIGHT){
        this.frameName = 'stone-wall-top-right';
    }
    else if(data.wallShape === WallShapesEnum.BOTTOM_LEFT){
        this.frameName = 'stone-wall-bottom-left';
    }
    else if(data.wallShape === WallShapesEnum.BOTTOM_RIGHT){
        this.frameName = 'stone-wall-bottom-right';
    }
    else if(data.wallShape === WallShapesEnum.VERTICAL){
        this.frameName = 'stone-wall-vertical';
    }
*/
    this.scale.setTo(GAME_SCALE * 2);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'stone-wall-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);
};
StoneWall.prototype = Object.create(Phaser.Sprite.prototype);
StoneWall.prototype.constructor = StoneWall;

StoneWall.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    //this.animations.stop();
    //this.animations.play('main');
    return this;
};

StoneWall.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {StoneWall};