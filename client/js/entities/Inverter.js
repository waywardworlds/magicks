/**
 * Created by User on 12/08/2017.
 */

"use strict";

var Inverter = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['tele-orb-1', 'tele-orb-2', 'tele-orb-3'], 4, true);
    this.scale.setTo(GAME_SCALE * 1.2);
    this.anchor.setTo(0.5);
};
Inverter.prototype = Object.create(Phaser.Sprite.prototype);
Inverter.prototype.constructor = Inverter;

Inverter.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {Inverter};