/**
 * Created by User on 30/06/2017.
 */

"use strict";

var Critter = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    var onCompleteCallback = function () {
        //console.log("in onCompleteCallback");
        this.animations.play('idle', 4, true);
    };
    this.animations.add('spawn', ['critter-spawn-1', 'critter-spawn-2', 'critter-spawn-3', 'critter-spawn-4'], 4).onComplete.add(function () {
        this.hasSpawned = true;
        this.animations.play('idle', 4, true);
    }, this);
    this.animations.add('idle', ['critter-idle-1', 'critter-idle-2'], 4, true);
    this.animations.add('move', ['critter-move-1', 'critter-move-2', 'critter-move-3', 'critter-move-2'], 6).onComplete.add(onCompleteCallback, this);
    //this.animations.add('wood', ['critter-with-wood-1', 'critter-with-wood-2', 'critter-with-wood-3', 'critter-with-wood-2'], 6, true);
    var attack = this.animations.add('attack', ['critter-attack-1', 'critter-attack-2'], 3);
    attack.onComplete.add(function () {
        this.isAttacking = false;
        this.animations.play('idle');
    }, this);
    attack.onStart.add(function () {
        this.isAttacking = true;
    }, this);

    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'critter-idle-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.addChild(this.effectLayer);

    this.hasSpawned = false;
    this.isAttacking = false;
    //this.carryingWood = false;
};
Critter.prototype = Object.create(Phaser.Sprite.prototype);
Critter.prototype.constructor = Critter;

Critter.prototype.update = function () {
    this.effectLayer.frameName = this.frameName;
};

Critter.prototype.move = function (x) {
    if(this.hasSpawned === true){
        //console.log("hasSpawned is false");
        if(this.isAttacking === false){
            //console.log("attacking is false");
            if(x * GAME_SCALE > this.x){
                this.scale.x = GAME_SCALE;
            }
            else {
                this.scale.x = -GAME_SCALE;
            }
            //if(this.carryingWood === true){
            //    this.animations.play('wood');
            //}
            //else {
                this.animations.play('move');
           // }
        }
    }
};

Critter.prototype.attack = function () {
    //console.log("playing attack anim");
    this.animations.play('attack');
};

Critter.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.hasSpawned = false;
    //this.carryingWood = false;
    this.animations.stop();
    this.animations.play('spawn');
    return this;
};

Critter.prototype.healed = function () {
    //console.log("in healed func");
    this.effectLayer.tint = magicks.colours.healed;
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

Critter.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.tint = magicks.colours.damaged;
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {Critter};