/**
 * Created by User on 05/05/2017.
 */

"use strict";

var HealOrb = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['heal-orb-1', 'heal-orb-2'], 4, true);
    this.scale.setTo(GAME_SCALE * 0.8);
    this.anchor.setTo(0.5);
};
HealOrb.prototype = Object.create(Phaser.Sprite.prototype);
HealOrb.prototype.constructor = HealOrb;

HealOrb.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {HealOrb};