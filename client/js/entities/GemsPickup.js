/**
 * Created by User on 21/01/2018.
 */

"use strict";

var GemsPickup = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.animations.add('red', ['gem-red-1', 'gem-red-2', 'gem-red-3', 'gem-red-2'], 4, true);
    this.animations.add('green', ['gem-green-1', 'gem-green-2', 'gem-green-3', 'gem-green-2'], 4, true);
    this.animations.add('blue', ['gem-blue-1', 'gem-blue-2', 'gem-blue-3', 'gem-blue-2'], 4, true);
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
GemsPickup.prototype = Object.create(Phaser.Sprite.prototype);
GemsPickup.prototype.constructor = GemsPickup;

GemsPickup.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    // Pick a random coloured gem.
    var rand = _this.rnd.integerInRange(0, 2);
    if(rand === 0){
        this.animations.play('red');
    }
    else if(rand === 1){
        this.animations.play('green');
    }
    else {
        this.animations.play('blue');
    }
    // Play the animation from a random frame, so they aren't all bobbing up and down in synch.
    this.animations.currentAnim.setFrame(_this.rnd.integerInRange(0, 2), true);
    return this;
};

export {GemsPickup};