/**
 * Created by User on 19/07/2017.
 */
"use strict";

var FireBolt = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['fire-bolt-1', 'fire-bolt-2', 'fire-bolt-3'], 4, true);
    this.scale.setTo(GAME_SCALE * 0.8);
    this.anchor.setTo(0.5);
};
FireBolt.prototype = Object.create(Phaser.Sprite.prototype);
FireBolt.prototype.constructor = FireBolt;

FireBolt.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {FireBolt};