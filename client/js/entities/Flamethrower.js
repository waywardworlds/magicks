/**
 * Created by User on 05/05/2017.
 */

"use strict";

var Flamethrower = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    var frames = [];
    // Get the frame for the first part of the animation.
    if(_this.rnd.integerInRange(1, 2) === 1){
        frames.push('flamethrower-1');
    }
    else {
        frames.push('flamethrower-2');
    }
    // Get the frame for the second part of the animation.
    if(_this.rnd.integerInRange(1, 2) === 1){
        frames.push('flamethrower-3');
    }
    else {
        frames.push('flamethrower-4');
    }
    // Get the frame for the third part of the animation.
    var rnd = _this.rnd.integerInRange(1, 3);
    if(rnd === 1){
        frames.push('flamethrower-5');
    }
    else if(rnd === 2){
        frames.push('flamethrower-6');
    }
    else {
        // Get the frame for the last part of the animation.
        frames.push('flamethrower-7');
    }
    this.animations.add('main', frames, 4);

    this.scale.setTo(GAME_SCALE * 0.8);
    this.anchor.setTo(0.5);
};
Flamethrower.prototype = Object.create(Phaser.Sprite.prototype);
Flamethrower.prototype.constructor = Flamethrower;

Flamethrower.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.angle -= _this.rnd.integerInRange(0, 360);
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {Flamethrower};