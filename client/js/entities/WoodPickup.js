/**
 * Created by User on 08/01/2018.
 */

"use strict";

var WoodPickup = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['wood-pickup-1', 'wood-pickup-2', 'wood-pickup-3', 'wood-pickup-2'], 4, true);
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
WoodPickup.prototype = Object.create(Phaser.Sprite.prototype);
WoodPickup.prototype.constructor = WoodPickup;

WoodPickup.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    this.animations.play('main');
    // Play the animation from a random frame, so they aren't all bobbing up and down in synch.
    this.animations.currentAnim.setFrame(_this.rnd.integerInRange(0, 2), true);
    return this;
};

export {WoodPickup};