/**
 * Created by User on 07/01/2018.
 */

"use strict";

var Player = function (data) {

    console.log("creating player");
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas', 'character-human-base-move-down-1');

    var onCompleteCallback = function () {
        //console.log("in onCompleteCallback");
        //this.animations.play('idle', 4, true);
    };
    this.animations.add('move-up',      ['character-human-base-move-up-1',      'character-human-base-move-up-2',       'character-human-base-move-up-1',       'character-human-base-move-up-3'],      8).onComplete.add(onCompleteCallback, this);
    this.animations.add('move-down',    ['character-human-base-move-down-1',    'character-human-base-move-down-2',     'character-human-base-move-down-1',     'character-human-base-move-down-3'],    8).onComplete.add(onCompleteCallback, this);
    this.animations.add('move-left',    ['character-human-base-move-left-1',    'character-human-base-move-left-2',     'character-human-base-move-left-1',     'character-human-base-move-left-3'],    8).onComplete.add(onCompleteCallback, this);
    this.animations.add('move-right',   ['character-human-base-move-right-1',   'character-human-base-move-right-2',    'character-human-base-move-right-1',    'character-human-base-move-right-3'],   8).onComplete.add(onCompleteCallback, this);

    var attackFinished = function () {
        this.isAttacking = false
    };
    this.animations.add('attack-up',    ['character-human-base-attack-up-1',    'character-human-base-attack-up-2'],    6).onComplete.add(attackFinished, this);
    this.animations.add('attack-down',  ['character-human-base-attack-down-1',  'character-human-base-attack-down-2'],  6).onComplete.add(attackFinished, this);
    this.animations.add('attack-left',  ['character-human-base-attack-left-1',  'character-human-base-attack-left-2'],  6).onComplete.add(attackFinished, this);
    this.animations.add('attack-right', ['character-human-base-attack-right-1', 'character-human-base-attack-right-2'], 6).onComplete.add(attackFinished, this);

    this.animations.add('unconscious',  ['character-human-base-unconscious-1',  'character-human-base-unconscious-2'], 4, true).onComplete.add(onCompleteCallback, this);

    this.displayName = _this.add.text(0, -20, data.displayName, {
        font: "12px Alagard",
        fill: "#ff00ff",
        stroke: "#000000",
        strokeThickness: 3
    });
    this.displayName.anchor.setTo(0.5);
    this.displayName.scale.setTo(0.5);
    this.addChild(this.displayName);

    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.isAttacking = false;
    this.isUnconscious = false;
};
Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

Player.prototype.move = function (x, y) {
    var playerX;
    var playerY;
    // Check the player entity exists.
    if(_this.entities[_this.playerEntityId]){
        playerX = _this.entities[_this.playerEntityId].x;
        playerY = _this.entities[_this.playerEntityId].y;
    }
    // If not, use the middle of the camera (where the player would otherwise be) as the point to check the distance from.
    else {
        playerX = _this.camera.x + _this.camera.width / 2;
        playerY = _this.camera.y + _this.camera.height / 2;
    }
    var distanceFromPlayer = 0;
    var xDist;
    var yDist;
    var fontDistanceSize;

    // Make display names visible/bigger as other characters get closer to the player entity.
    _this.pools.Player.forEachExists(function(item) {
        xDist = playerX - item.x;
        yDist = playerY - item.y;
        distanceFromPlayer = Math.sqrt(xDist*xDist + yDist*yDist);
        fontDistanceSize = (310 - (distanceFromPlayer-30)) / 10;

        if(distanceFromPlayer > 300){
            item.displayName.visible = false;
        }
        else if(distanceFromPlayer <= 100){
            item.displayName.visible = true;
            item.displayName.setStyle({font: (12*2) + "px Alagard", fill: "#ff00ff", stroke: "#000000", strokeThickness: 4});
        }
        else if(fontDistanceSize > 4){
            item.displayName.visible = true;
            item.displayName.setStyle({font: (2*fontDistanceSize * 0.5) + "px Alagard", fill: "#ff00ff", stroke: "#000000", strokeThickness: 3});
        }
    });

    var angle = Math.atan2(y * GAME_SCALE - this.y, x * GAME_SCALE - this.x) * 180 / Math.PI;

    if(this.isAttacking === true){
        return;
    }
    if(this.isUnconscious === true){
        return;
    }

    if(angle > -135 && angle < -45){
        this.animations.play('move-up');
    }
    else if(angle >= -45 && angle <= 45){
        this.animations.play('move-right');
    }
    else if(angle > 45 && angle < 135){
        this.animations.play('move-down');
    }
    else{
        this.animations.play('move-left');
    }
};

Player.prototype.attack = function (angle) {
    this.isAttacking = true;

    if(angle > -135 && angle < -45){
        this.animations.play('attack-up');
    }
    else if(angle > -45 && angle < 45){
        this.animations.play('attack-right');
    }
    else if(angle > 45 && angle < 135){
        this.animations.play('attack-down');
    }
    else{
        this.animations.play('attack-left');
    }
};

Player.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.isAttacking = false;
    this.isUnconscious = false;
    this.displayName.text = data.displayName;
    // Check if this entity is this player's entity, in case they have swapped zones, and if so, swap
    // the entity the cast range circle is a child of to the new entity sprite, as the entities list
    // gets wiped when the zone changes, so sprite the circle is a child of might be the wrong one.
    if(data.id === _this.playerEntityId){
        if(_this.GUI !== undefined){
            this.addChild(_this.GUI.castRangeCircle);
        }
    }
    return this;
};

export {Player};