/**
 * Created by User on 05/05/2017.
 */

"use strict";

var Splash = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['splash-1', 'splash-2'], 4, true);
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
Splash.prototype = Object.create(Phaser.Sprite.prototype);
Splash.prototype.constructor = Splash;

Splash.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {Splash};