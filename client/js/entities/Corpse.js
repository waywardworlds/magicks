/**
 * Created by User on 13/07/2017.
 */

"use strict";

var Corpse = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas', 'character-human-corpse');

    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
Corpse.prototype = Object.create(Phaser.Sprite.prototype);
Corpse.prototype.constructor = Corpse;

Corpse.prototype.move = function (x) {
    //if(x * GAME_SCALE > this.x){
    //    this.scale.x = -GAME_SCALE;
    //}
    //else {
    //    this.scale.x = GAME_SCALE;
    //}
};

Corpse.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    return this;
};

export {Corpse};