/**
 * Created by User on 05/05/2017.
 */

"use strict";

var Barrier = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['barrier-1', 'barrier-2', 'barrier-3'], 4);
    this.scale.setTo(GAME_SCALE * 1.2);
    this.anchor.setTo(0.5);
};
Barrier.prototype = Object.create(Phaser.Sprite.prototype);
Barrier.prototype.constructor = Barrier;

Barrier.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {Barrier};