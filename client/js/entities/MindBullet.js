/**
 * Created by User on 05/05/2017.
 */
"use strict";

var MindBullet = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');

    this.animations.add('main', ['mind-bullet-1', 'mind-bullet-2'], 4, true);
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);
};
MindBullet.prototype = Object.create(Phaser.Sprite.prototype);
MindBullet.prototype.constructor = MindBullet;

MindBullet.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    this.rotation = data.rotation;
    this.animations.stop();
    this.animations.play('main');
    return this;
};

export {MindBullet};