/**
 * Created by User on 06/02/2018.
 */

"use strict";

var Shrine = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.frameName = 'shrine-void-1';
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    this.buySpellScrollButton = _this.add.button(0, 0, 'game-atlas', function () {
        //console.log("buy spell scroll button pressed");
        socketGS.emit("buy_spell");
    });
    this.buySpellScrollButton.frameName = 'btn-scroll';
    this.buySpellScrollButton.scale.setTo(2);
    this.buySpellScrollButton.anchor.setTo(0.5);
    this.buySpellScrollButton.visible = false;

    this.addChild(this.buySpellScrollButton);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'shrine-void-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);

};
Shrine.prototype = Object.create(Phaser.Sprite.prototype);
Shrine.prototype.constructor = Shrine;

Shrine.prototype.spawn = function (x, y, data) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;

    switch (data.element){
        case 'l':
            this.frameName = 'shrine-light-1';
            break;
        case 'd':
            this.frameName = 'shrine-dark-1';
            break;
        case 'w':
            this.frameName = 'shrine-water-1';
            break;
        case 'f':
            this.frameName = 'shrine-fire-1';
            break;
        case 'e':
            this.frameName = 'shrine-earth-1';
            break;
        case 'p':
            this.frameName = 'shrine-psychic-1';
            break;
        default:
            this.frameName = 'shrine-void-1';
            break;
    }

    return this;
};

Shrine.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {Shrine};