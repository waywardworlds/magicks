/**
 * Created by User on 12/01/2018.
 */

"use strict";

var OrderCore = function (data) {
    Phaser.Sprite.call(this, _this.game, data.x * GAME_SCALE, data.y * GAME_SCALE, 'game-atlas');
    this.frameName = 'order-core-1';
    this.scale.setTo(GAME_SCALE);
    this.anchor.setTo(0.5);

    // Text over the join order button.
    var joinText = _this.add.text(0, 0, '+', {
        font: "40px Alagard",
        fill: magicks.colours.green,
        align: "center",
        stroke: magicks.colours.black,
        strokeThickness: 7
    });
    joinText.anchor.setTo(0.5);
    joinText.scale.setTo(0.3);

    // Join order button.
    this.joinOrderButton = new PhaserNineSlice.NineSlice(_this, 0, 0, 'menus-atlas', 'nineslice-button', 20, 20, {top: 7});
    this.joinOrderButton.scale.setTo(2);
    this.joinOrderButton.anchor.setTo(0.5);
    this.joinOrderButton.inputEnabled = true;
    this.joinOrderButton.events.onInputUp.add(function () {
        console.log("join order button pressed");
        socketGS.emit("join_order");
    }, this);
    //this.joinOrderButton.events.onInputDown.add(_this.GUI.prototype.buttonOnDown, this);
    //this.joinOrderButton.events.onInputUp.add(_this.GUI.prototype.buttonOnUp, this);
    this.joinOrderButton.visible = false;
    this.joinOrderButton.addChild(joinText);
    this.addChild(this.joinOrderButton);

    this.effectLayer = _this.add.sprite(0, 0, 'game-atlas', 'order-core-1');
    this.effectLayer.anchor.setTo(0.5);
    this.effectLayer.alpha = 0;
    this.effectLayer.tint = magicks.colours.damaged;
    this.addChild(this.effectLayer);

    this.craftPanel = new PhaserNineSlice.NineSlice(_this, 0, 0, 'menus-atlas', 'nineslice-button', 20, 20, {top: 7});
    this.craftPanel.scale.setTo(2);
    this.craftPanel.anchor.setTo(0.5);
    this.craftPanel.inputEnabled = true;
    this.craftPanel.events.onInputUp.add(function () {
        console.log("join order button pressed");
        socketGS.emit("join_order");
    }, this);
    //this.joinOrderButton.events.onInputDown.add(_this.GUI.prototype.buttonOnDown, this);
    //this.joinOrderButton.events.onInputUp.add(_this.GUI.prototype.buttonOnUp, this);
    this.craftPanel.visible = true;
    this.addChild(this.craftPanel);

    //this.craftPanel.woodWallButton = _this.add.button(0, 0, );
    //this.craftPanel.woodWallIcon = _this.add.sprite(0, 0, );
};
OrderCore.prototype = Object.create(Phaser.Sprite.prototype);
OrderCore.prototype.constructor = OrderCore;

OrderCore.prototype.spawn = function (x, y) {
    this.x = x * GAME_SCALE;
    this.y = y * GAME_SCALE;
    //this.animations.stop();
    //this.animations.play('main');
    return this;
};

OrderCore.prototype.damaged = function () {
    //console.log("in damaged func");
    this.effectLayer.alpha = 1;
    _this.add.tween(this.effectLayer).to({alpha: 0}, 500, "Linear", true);
};

export {OrderCore};