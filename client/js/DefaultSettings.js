/**
 * Created by User on 15/09/2017.
 */

"use strict";

import io from 'socket.io-client';

// var socket = io(); <-- moved to Boot, or it won't have
// been defined before the other files try and use it.

// Need to do this to arse around with function context
// to be able to reference state variables neatly.
// Normal 'this' won't work inside of Socket functions, as
// the context will be of the Socket object, so a global
// reference is made to the the current state and used.
window._this = {};

// Whether or not this is being run locally for development.
window.devMode = true;

window.magicks = {
    account: {
        // The number of this account. Something like 2734.
        //accountNumber: undefined,
        accountID: '',
        // The system generated password for this account.
        securityCode: '',
        // Recovery email address.
        recoveryEmail: ''
    },
    // Blank template of the player data of the account game data, sent from the server.
    playerData: {
        displayName: null,
        // What spells this player has unlocked.
        unlockedSpells: []
    },
    // A list of the type numbers of each entity, and their sprite class names.
    typeCatalogue: {},
    // Whether this player is currently in a game world.
    //inGame: false,
    // User configurable settings.
    settings: {},
    // Is an ad blocker enabled.
    adBlockEnabled: false,
    // The small ad element.
    menusAd: null,
    // The fullscreen video ad.
    fullScreenAd: null,

    colours: {
        black: '#1e1e1e',
        white: '#ffffff',
        green: '#00bf16',
        orange: '#ff4d00',
        red: '#ff3131',
        grey: '#cdcdcd',
        damaged: '0xff3131',
        healed: '0x3dff84'
    }

};


// A common factor to scale graphics by.
window.GAME_SCALE = 1.5;

window.socketPM = {};
if(devMode){
    // Localhost.
    window.socketPM = io("http://127.0.0.4:3003");
}
else {
    // DO player manager server.
    window.socketPM = io("http://178.62.85.99:3003");
}

// Attempt to connect to the player manager server every 5 seconds.
window.reconnectTimeout = function(){
    //console.log("reconnectTimeout");
    setTimeout(function () {
        if(socketPM.connected === false){
            socketPM.connect();
            reconnectTimeout();
        }
    }, 5000);
};
window.socketGS = {};
window.startTime = 0;
window.latency = 0;
/*setInterval(function() {
 startTime = Date.now();
 socketPM.emit('test_ping');
 }, 2000);*/


// Check if there are any previous settings saved in local storage.
// localStorage should return a string, so parse it.
if(JSON.parse(localStorage.getItem("settings"))){
    magicks.settings = JSON.parse(localStorage.getItem("settings"));
}

//console.log("local settings");
//console.log(magicks.settings);

window.defaultSettings = {

    device: {
        desktop: {
            useWebGL: false,
            joystickEnabled: false,
            gamepadEnabled: false,
            particlesEnabled: true,
            mapAnimationEnabled: true,
            spellWheelEnabled: false,
            audioVolume: 1,
            language: 'en',
            controls: {
                moveUp:     'W',
                moveDown:   'S',
                moveLeft:   'A',
                moveRight:  'D',
                addLight:   'ONE',
                addDark:    'TWO',
                addWater:   'THREE',
                addFire:    'FOUR',
                addEarth:   'FIVE',
                addPsychic: 'SIX',
                addVoid:    'SEVEN',
                clearSpellComponents: 'SPACEBAR',
                enterChat:  'ENTER'
            },
            GUI: {
                options:    {x: 0,      y: 0,               scale: 2},
                joystick:   {x: 100,    y: 350,             scale: 0.5}
            }
        },
        mobile: {
            useWebGL: false,
            joystickEnabled: true,
            gamepadEnabled: false,
            particlesEnabled: false,
            mapAnimationEnabled: false,
            spellWheelEnabled: true,
            audioVolume: 1,
            language: 'en',
            controls: {
                moveUp:     '',
                moveDown:   '',
                moveLeft:   '',
                moveRight:  '',
                addLight:   '',
                addDark:    '',
                addWater:   '',
                addFire:    '',
                addEarth:   '',
                addPsychic: '',
                addVoid:    '',
                clearSpellComponents: '',
                enterChat:  ''
            },
            GUI: {
                options:    {x: 0,     y: 0,              scale: 2},
                joystick:   {x: 140,    y: 310,             scale: 1}
            }
        }
    }
};