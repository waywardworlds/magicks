/**
 * Created by User on 03/04/2016.
 */

//import {spellInfo} from '../SpellInfo';
import {textDefs} from '../TextDefs';

magicks.Preload = function () {
};

magicks.Preload.prototype = {

    preload: function () {

        this.load.atlasJSONArray('menus-atlas',     'assets/img/menus-atlas.png',   'assets/img/menus-atlas.json');
        this.load.atlasJSONArray('game-atlas',      'assets/img/game-atlas.png',    'assets/img/game-atlas.json');

        this.load.spritesheet('floors-tileset',     'assets/img/tileset/floors-tileset.png', 16, 16, -1, 0, 1);
        this.load.spritesheet('landforms-tileset',  'assets/img/tileset/landforms-tileset.png', 16, 16, -1, 0, 1);

        this.load.atlas('joystick',             'assets/img/generic-joystick.png', 'assets/img/generic-joystick.json');

        //this.load.json('spell-defs',                'js/SpellDefs.json');
        this.load.json('text-defs',                 'js/TextDefs.json');
        this.load.json('type-catalogue',            'js/TypeCatalogue.json');

        //this.load.nineSlice(undefined, undefined, 6);
    },

    create: function () {
        console.log("in preload");
        // Update the spell infos with the values for the properties.
        //var spellDefs = this.cache.getJSON('spell-defs');
        //var spell = {};
        //for(var spellId in spellDefs){
        //    if(spellDefs.hasOwnProperty(spellId)){
        //        spell = spellDefs[spellId];
        //        spellInfo.newSpell(spellId, spell.name, spell.xpCost, spell.energyCost, spell.description);
        //    }
        //}

        // Load the language definitions for a language.
        textDefs.loadDefs('English', this);

        // Set up the type catalogue of entity
        var typeCatalogue = this.cache.getJSON('type-catalogue');
        // Swap the property values with the keys, so `Rock: 9` becomes `9: "Rock"`.
        var swappedJSON = {};
        for(var key in typeCatalogue){
            swappedJSON[typeCatalogue[key]] = key;
        }
        // Keep an accessible reference to the type catalogue (now in the correct format).
        magicks.typeCatalogue = swappedJSON;

        this.state.start("LogIn");
    }
};