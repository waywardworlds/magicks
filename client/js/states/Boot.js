/**
 * Created by User on 03/04/2016.
 */

"use strict";

import {spellStats} from '../SpellStats';

magicks.Boot = function () {
};

magicks.Boot.prototype = {

    init: function () {
        console.log("in boot");
        window._this = this;

        this.game.time.advancedTiming = true;

        // Keep the game running even when the window loses focus.
        this.stage.disableVisibilityChange = true;

        // Round pixels so text doesn't appear blurry/anti-aliased.
        this.game.renderer.renderSession.roundPixels = true;

        window.addEventListener("click", function () {
            //console.log("click");
            window.focus();
        }, false);

        this.game.forceSingleUpdate = true; //# Test if this makes a difference to fps on other devices.

        // Make the whole game container div and all of it's contents be resizes, not just the game canvas, otherwise the ads wouldn't be visible.
        this.game.scale.fullScreenTarget = document.getElementById('gameContainer');

        // Quick references to the ad elements.
        magicks.menusAd = document.getElementById("menusAd");
        magicks.fullScreenAd = document.getElementById("gameAd");
        magicks.crossPromoteAd = document.getElementById("IOG_CP");

        // Only allow 2 pointer inputs to be active at once.
        this.input.maxPointers = 2;

        this.windowHeight = window.innerHeight;
        this.windowWidth = (this.windowHeight / 9) * 16;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.refresh();
        this.scale.setMinMax(40, 30, this.windowWidth, this.windowHeight);
        window.addEventListener('resize', function(){
            _this.windowHeight = window.innerHeight;
            _this.windowWidth = (_this.windowHeight / 9) * 16;
            _this.scale.setMinMax(40, 30, _this.windowWidth, _this.windowHeight);
        }, true);

        // If not desktop, i.e. phone, tablet.
        if(!this.game.device.desktop){
            this.scale.forceOrientation(true, false);
            this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
            this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
        }

        // Put the game in the middle of the page.
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        // Add plugins.
        if(devMode === true){
            this.game.add.plugin(Phaser.Plugin.Debug);
        }
        this.game.add.plugin(PhaserInput.Plugin);
        this.game.add.plugin(PhaserNineSlice.Plugin);

        spellStats.codes = {};

        Object.keys(spellStats).forEach(function(key) {
            spellStats.codes[spellStats[key].code] = spellStats[key];
        });

    },

    create: function () {
        //_this = this;

        //console.log("this state key: " + this.key);

        this.state.start('Preload');
    },

    enterIncorrectOrientation: function () {
        /*windowHeight = window.innerHeight;
         windowWidth = (windowHeight / 3) * 4;
         this.scale.setMinMax(40, 30, windowWidth, windowHeight);*/

        document.getElementById('orientation').style.display = 'block';
    },

    leaveIncorrectOrientation: function () {
        /*windowHeight = window.innerHeight;
         windowWidth = (windowHeight / 3) * 4;
         this.scale.setMinMax(40, 30, windowWidth, windowHeight);*/

        document.getElementById('orientation').style.display = 'none';
    },

    events: (function () {

        socketPM.on('disconnect', function () {
            console.log("Disconnected from player manager server.");
            // Give the Boot and Preload states a chance to finish what they are doing.
            // '_this' is a state, and the key is the name of the state.
            if(_this.key === 'Boot'
            || _this.key === 'Preload'){
                return;
            }
            // Start the LogIn state if the current state isn't on it already.
            // Might have disconnected while on WorldSelect state.
            if(_this.key === 'WorldSelect'){
                // Check if this player is not in a game world.
                // Stay on the Game state if they are.
                //if(magicks.inGame === false){
                    //_this.state.start("LogIn");
                //}
            }
            // This state is LogIn. Try reconnecting to the server.
            else if(_this.key === 'LogIn'){
                // The state is already LogIn. Start a timeout to try and connect to the server.
                setTimeout(function () {
                    reconnectTimeout();
                }, 5000);
            }
        });

        socketPM.on('hello_pm', function () {
            //console.log("PM server says hi :)");
        });

        socketPM.on('test_pong', function() {
            window.latency = Date.now() - startTime;
        });

    })()
};