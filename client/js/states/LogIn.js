/**
 * Created by User on 03/04/2016.
 */

"use strict";

import {textDefs} from '.././TextDefs';

magicks.LogIn = function () {
};

magicks.LogIn.prototype = {

    create: function () {
        console.log("in login");

        window._this = this;

        // Show the menus ad.
        magicks.menusAd.style.visibility = "visible";
        magicks.crossPromoteAd.style.visibility = "visible";

        // Reconnect to the player manager server if not connected. This client won't be connected if they have just left a world.
        socketPM.connect();
        //console.log("after .connect(), .connected: " + socketPM.connected);

        if(socketPM.connected === false){
            reconnectTimeout();
        }

        // Load the account details from local storage.
        var data = JSON.parse(localStorage.getItem("account"));
        //console.log("local data loaded:");
        //console.log(data);
        if(data){
            if(data.accountID){
                magicks.account.accountID = data.accountID;
            }
            if(data.securityCode){
                magicks.account.securityCode = data.securityCode;
            }
        }

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        var titleText = this.add.image(this.game.width / 2, 10, 'menus-atlas', 'title-logo');
        titleText.scale.setTo(2);
        titleText.anchor.set(0.5, 0);

        // Message of the day.
        var motd = this.add.text(700, 240, "~ Early version! ~\nBugs, crashes,\ndisconnects and\naccount wipes\nwill be common.", {
            font: "18px Alagard",
            fill: magicks.colours.white,
            align: 'center'
        });
        motd.anchor.setTo(0.5);
        //motd.scale.setTo(0.8);

        // Text over the play button.
        var playText = this.add.text(0, 0, 'Play', {
            font: "60px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 8
        });
        playText.anchor.setTo(0.5);
        playText.scale.setTo(0.25);
        // Play button.
        var playButton = new PhaserNineSlice.NineSlice(this, this.game.width / 2, 190, 'menus-atlas', 'nineslice-button', 70, 30, {top: 7});
        //var playButton = this.add.nineSlice(this.game.width / 2, 200, 'menus-atlas', 'nineslice-button', 70, 30);
        playButton.scale.setTo(4);
        playButton.anchor.setTo(0.5);
        playButton.inputEnabled = true;
        playButton.events.onInputDown.add(this.playPressed, this);
        playButton.addChild(playText);
        this.add.existing(playButton);

        // Alert messages to the user will be shown with this.
        var alertStyle = {
            font: "30px Alagard",
            fill: magicks.colours.orange,
            align: 'center'
        };
        this.userAlert = this.add.text(664, 340, '', alertStyle);
        this.userAlert.anchor.set(0.5);
        this.userAlert.scale.set(0.7);

        // Text over the manage account button.
        var manageAccountText = this.add.text(0, 1, 'Manage account', {
            font: "36px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 6
        });
        manageAccountText.anchor.setTo(0.5);
        manageAccountText.scale.setTo(0.18);
        // Manage account button.
        var manageAccountButton = new PhaserNineSlice.NineSlice(this, 670, 400, 'menus-atlas', 'nineslice-button', 60, 17, {top: 7});
        //var manageAccountButton = this.add.nineSlice(this.game.width / 2, 340, 'menus-atlas', 'nineslice-button', 60, 13);
        manageAccountButton.anchor.setTo(0.5);
        manageAccountButton.scale.setTo(3.8);
        manageAccountButton.inputEnabled = true;
        manageAccountButton.events.onInputDown.add(this.manageAccountPressed, this);
        this.add.existing(manageAccountButton);
        manageAccountButton.addChild(manageAccountText);
        //this.add.existing(manageAccountButton);


        // Discord community text.
        var discordText = this.add.text(100, 240, "Discuss the game\nwith fellow wizards\non the official\nDiscord server.", {
            font: "20px Alagard",
            fill: magicks.colours.white,
            align: 'center'
        });//.alpha = 0.8;
        discordText.anchor.setTo(0.5);
        discordText.scale.setTo(0.8);

        // Discord button.
        var discordButton = this.add.button(100, 320, 'menus-atlas', function () {
            window.open("https://discord.gg/w3SwSuA", "_blank")
        }, this);
        discordButton.frameName = 'discord-logo-white';
        discordButton.scale.setTo(0.3);
        discordButton.anchor.setTo(0.5);

        // More Games panel.
        var moreGamesPanel = new PhaserNineSlice.NineSlice(this, 110, this.game.height, 'menus-atlas', 'nineslice-button', 50, 70, {top: 7});
        moreGamesPanel.anchor.setTo(0.5, 0);
        moreGamesPanel.scale.setTo(4);
        moreGamesPanel.isOpen = false;
        this.add.existing(moreGamesPanel);

        // Text over the More Games panel.
        var moreGamesText = this.add.text(0, 0, 'More Games', {font: "26px Alagard", fill: magicks.colours.white, align: "center", stroke: magicks.colours.black, strokeThickness: 5});
        moreGamesText.anchor.setTo(0.5, 1);
        moreGamesText.scale.setTo(0.3);
        moreGamesText.inputEnabled = true;
        moreGamesText.events.onInputDown.add(function () {
            if(moreGamesPanel.isOpen === true){
                moreGamesPanel.isOpen = false;
                discordText.visible = true;
                discordButton.visible = true;
                this.add.tween(moreGamesPanel).to({y: this.game.height}, 250, null, true);
            }
            else {
                moreGamesPanel.isOpen = true;
                discordText.visible = false;
                discordButton.visible = false;
                this.add.tween(moreGamesPanel).to({y: 240}, 250, null, true);
            }

        }, this);

        var style = {font: "26px Acme", fill: magicks.colours.white, align: "center", stroke: magicks.colours.black, strokeThickness: 5};

        // iogames.space
        var iogamesspace = this.add.text(0, +10, 'iogames.space', style);
        iogamesspace.anchor.setTo(0.5);
        iogamesspace.scale.setTo(0.25);
        iogamesspace.inputEnabled = true;
        iogamesspace.events.onInputDown.add(function () {
            window.open("http://iogames.space/", "_blank");
        }, this);
        // gamefarm.io
        var gamefarm = this.add.text(0, +20, 'gamefarm.io', style);
        gamefarm.anchor.setTo(0.5);
        gamefarm.scale.setTo(0.25);
        gamefarm.inputEnabled = true;
        gamefarm.events.onInputDown.add(function () {
            window.open("https://www.gamefarm.io/", "_blank");
        }, this);
        // io-games.io
        /*var iogamesio = this.add.text(0, +30, 'io-games.io', style);
        iogamesio.anchor.setTo(0.5);
        iogamesio.scale.setTo(0.25);
        iogamesio.inputEnabled = true;
        iogamesio.events.onInputDown.add(function () {
            window.open("http://io-games.io/", "_blank");
        }, this);*/
        // titotu.io
        var titotuio = this.add.text(0, +30, 'titotu.io', style);
        titotuio.anchor.setTo(0.5);
        titotuio.scale.setTo(0.25);
        titotuio.inputEnabled = true;
        titotuio.events.onInputDown.add(function () {
            window.open("http://titotu.io/", "_blank");
        }, this);

        moreGamesPanel.addChild(moreGamesText);
        moreGamesPanel.addChild(iogamesspace);
        moreGamesPanel.addChild(gamefarm);
        //moreGamesPanel.addChild(iogamesio);
        moreGamesPanel.addChild(titotuio);

        // Twitter button.
        var twitterButton = this.add.button(this.game.width / 2 - 40, 420, 'menus-atlas', function () {
            window.open("https://twitter.com/waywardworlds", "_blank")
        }, this);
        twitterButton.frameName = 'twitter-logo-transparent';
        twitterButton.scale.setTo(0.4);
        twitterButton.anchor.setTo(0.5);

        // Info button.
        //var infoButton = this.add.button(this.game.width / 2 + 40, 420, 'menus-atlas', function () {
        //
        //    //window.open("https://twitter.com/waywardworlds", "_blank")
        //}, this);
        //infoButton.frameName = 'info-icon';
        //infoButton.scale.setTo(1.8);
        //infoButton.anchor.setTo(0.5);

        // Settings button.
        //var settingsButton = this.add.button(this.game.width / 2 + (80 + 40), 420, 'menus-atlas', function () {
        //
        //}, this);
        //settingsButton.frameName = 'settings-icon';
        //settingsButton.scale.setTo(2);
        //settingsButton.anchor.setTo(0.5);

        // Fullscreen button.
        var fullscreenButton = this.add.button(this.game.width - 60, 60, 'game-atlas', this.fullscreenPressed, this);
        fullscreenButton.frameName = 'btn-fullscreen';
        fullscreenButton.scale.setTo(2);

        // Credits button.
        /*addRectButton(10, 80, 120, 60, textDefs['B2'], function () {
        }, this);*/

        // Add a 'Connection:' text next to the connection status icon.
        this.add.text(640, 13, textDefs['A11'], {
            font: "20px Alagard",
            fill: magicks.colours.white
        });

        // Add the coloured circles to show the status of the connection to the server.
        var graphic = this.add.graphics(0, 0);
        graphic.beginFill(0x00ff00, 0.8);
        graphic.drawCircle(0, 0, 30);
        graphic.endFill();
        this.serverGreenStatus = this.add.image(756, 10, graphic.generateTexture());
        graphic.beginFill(0xff0000, 0.8);
        graphic.drawCircle(0, 0, 30);
        graphic.endFill();
        this.serverRedStatus = this.add.image(756, 10, graphic.generateTexture());
        graphic.destroy();

    },

    update: function () {
        if(socketPM.connected){
            this.serverGreenStatus.visible = true;
            this.serverRedStatus.visible = false;
        }
        else {
            this.serverGreenStatus.visible = false;
            this.serverRedStatus.visible = true;
        }
    },

    shutdown: function () {
        magicks.crossPromoteAd.style.visibility = "hidden";
    },

    playPressed: function () {
        // Check if there is account data stored locally.
        if(magicks.account.accountID){
            console.log("account is set");
            if(magicks.account.securityCode) {
                console.log("sec code is set");
                //console.log("attempting to log in with account:");
                //console.log(magicks.account);
                // Attempt to log in with the locally stored account number and security code.
                socketPM.emit('log_in', {accountID: Number(magicks.account.accountID), securityCode: magicks.account.securityCode});
            }
            else {
                // No valid security code. Request a new account.
                socketPM.emit('new_account');
            }
        }
        // No valid account number. Request a new account.
        else {
            socketPM.emit('new_account');
        }
    },

    manageAccountPressed: function () {
        // Go to account management screen.
        _this.state.start("ManageAccount");

    },

    fullscreenPressed () {
        if(this.game.scale.isFullScreen){
            this.game.scale.stopFullScreen();
        }
        else{
            this.game.scale.startFullScreen(false);
        }
    },

    updateSpellInfo: function () {
        console.log("in updatespellinfo");
        //console.log(spellInfo);
        /*var unlockedSpell;
        for(var i=0; i<magicks.playerData.unlockedSpells.length; i+=1){
            unlockedSpell = magicks.playerData.unlockedSpells[i];
            console.log(unlockedSpell);
            for(var spellId in spellInfo.list){
                if(spellInfo.list.hasOwnProperty(spellId)){
                    console.log(spellId);
                    if(unlockedSpell === spellId){
                        spellInfo.list[spellId].unlocked = true;
                        break;
                    }
                }
            }
        }*/
    },

    events: (function () {

        socketPM.on('log_in_success', function (data) {
            //console.log("log in success, data:");
            //console.log(data);
            magicks.playerData.displayName = data.displayName;
            magicks.playerData.unlockedSpells = data.unlockedSpells;
            //console.log(magicks.playerData);
            //_this.updateSpellInfo();
            // Start next state.
            _this.state.start("Menu");
        });

        socketPM.on('log_in_failed', function (data) {
            //console.log("log in failed: " + data);
            // If the server says this account is already logged in.
            if(data === 1){
                _this.userAlert.text = textDefs['A6'];
            }
            else {
                _this.userAlert.text = textDefs['A7'];
            }
        });

        socketPM.on('new_account_success', function (data) {
            // Some times (especially on mobile) the user alter text carries over somehow. Clear it before moving to next state.
            _this.userAlert.text = "";
            //console.log("new account success, data:");
            //console.log(data);

            // New account created for this device.
            magicks.account.accountID = data.accountID;
            magicks.account.securityCode = data.securityCode;
            // Store the account number and security code in local storage so the user can log in with this account on this device in the future.
            localStorage.setItem("account", JSON.stringify(magicks.account));

            magicks.playerData.displayName = data.gameData.displayName;
            magicks.playerData.unlockedSpells = data.gameData.unlockedSpells;
            //console.log(magicks.playerData);

            //_this.updateSpellInfo();

            // Start next state.
            _this.state.start("Menu")
        });

        socketPM.on('new_account_failed', function (data) {
            //console.log("new account failed");
            if(data === 1){
                _this.userAlert.text = textDefs['A2'];
            }
            else if(data === 2){
                _this.userAlert.text = textDefs['A3'];
            }
            else if(data === 3){
                _this.userAlert.text = textDefs['A8'];
            }
            else if(data === 4){
                _this.userAlert.text = textDefs['A4'];
            }
            else if(data === 5){
                _this.userAlert.text = textDefs['A5'];
            }
            else if(data === 6){
                _this.userAlert.text = textDefs['A9'];
            }
            else {
                _this.userAlert.text = textDefs['A10'];
            }
        });

    })()
};