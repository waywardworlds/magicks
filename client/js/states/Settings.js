/**
 * Created by User on 26/01/2017.
 */

"use strict";

magicks.Settings = function () {
};

magicks.Settings.prototype = {

    create: function () {

        window._this = this;

        this.add.image(0, 0, 'screens', 'screen-generic');
        var titleText = this.add.image(this.game.width/2, 40, 'title-text');
        titleText.anchor.set(0.5);

    },

    backPressed: function () {
        // Update the settings in localStroage. The audio volume might have been changed.
        localStorage.setItem("settings", JSON.stringify(magicks.settings));

        this.state.start("Menu");
    },

    audioOnPressed: function () {
        this.audioOn.visible = false;
        this.audioOff.visible = true;
        magicks.settings.audio.enabled = false;
    },

    audioOffPressed: function () {
        this.audioOn.visible = true;
        this.audioOff.visible = false;
        magicks.settings.audio.enabled = true;
    },

    increaseVolume: function () {
        magicks.settings.audio.volume += 0.2;
        //# PLAY A SOUND TO SHOW THE NEW VOLUME.
    },

    decreaseVolume: function () {
        magicks.settings.audio.volume -= 0.2;
        //# PLAY A SOUND TO SHOW THE NEW VOLUME.
    },

    fullscreenPressed: function () {
        if(this.scale.isFullScreen){
            this.scale.stopFullScreen();
        }
        else{
            this.scale.startFullScreen(false);
        }
    },

    adjustUIPressed: function () {
        this.state.start("AdjustUI");
    },

    controlsPressed: function () {

    }
};