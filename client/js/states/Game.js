/**
 * Created by User on 03/04/2016.
 */

"use strict";

import {GUI} from '../GUI';
import {Input} from '../Input';
import {Tilemap} from '../Tilemap';

import {Pool} from '../entities/Pool';
import {Barrier} from '../entities/Barrier';
import {BigTree} from '../entities/BigTree';
import {CharacterHuman} from '../entities/CharacterHuman';
import {Corpse} from '../entities/Corpse';
import {Critter} from '../entities/Critter';
import {Deflector} from '../entities/Deflector';
import {DrainingBolt} from '../entities/DrainingBolt';
import {EnergyBlob} from '../entities/EnergyBlob';
import {FireBolt} from '../entities/FireBolt';
import {Flamethrower} from '../entities/Flamethrower';
import {GemsPickup} from '../entities/GemsPickup';
import {GemsRock} from '../entities/GemsRock';
import {GoblinMage} from '../entities/GoblinMage';
import {HealOrb} from '../entities/HealOrb';
import {Inverter} from '../entities/Inverter';
import {MindBullet} from '../entities/MindBullet';
import {Nimbus} from '../entities/Nimbus';
import {OrderCore} from '../entities/OrderCore';
import {Player} from '../entities/Player';
import {RavenStrike} from '../entities/RavenStrike';
import {Rock} from '../entities/Rock';
import {Sapling} from '../entities/Sapling';
import {ScrollPickup} from '../entities/ScrollPickup';
import {SmallTree} from '../entities/SmallTree';
import {Splash} from '../entities/Splash';
import {StoneShard} from '../entities/StoneShard';
import {StoneWall} from '../entities/StoneWall';
import {Shrine} from '../entities/Shrine';
import {TargetBolt} from '../entities/TargetBolt';
import {TeleOrb} from '../entities/TeleOrb';
import {WoodDoor} from '../entities/WoodDoor';
import {WoodPickup} from '../entities/WoodPickup';
import {WoodWall} from '../entities/WoodWall';

magicks.Game = function () {
};

magicks.Game.prototype = {

    init: function (zoneName, playerEntityId, entitiesData, mapData, maxHitPoints, maxEnergy) {
        //console.log("in game init");
        //console.log("Game state init, zoneName: " + zoneName);
        this.zoneName = 'dont use this';
        // The ID of the entity that this player is controlling.
        this.playerEntityId = playerEntityId;
        //console.log("player entity ID: " + playerEntityId);
        // A list of data about the entities that are already in the zone.
        this.entitiesData = entitiesData;
        //console.log("entitiesData:");
        //console.log(entitiesData);
        this.mapData = mapData;
        this.maxHitPoints = maxHitPoints || 4;
        this.maxEnergy = maxEnergy || 4;
    },

    create: function () {
        console.log("in game create");
        //console.log("Game create");
        window._this = this;
        this.socketEvents();

        this.currentFrame = 0;

        this.tilemap = new Tilemap(this, this.mapData);
        // Don't need the map data from the server any more.
        delete this.mapData;

        // Add the pools for each type of entity.
        // The order here is also the display order!
        this.pools = {};
        this.pools.Corpse =         new Pool(Corpse);
        this.pools.Critter =        new Pool(Critter);
        this.pools.Barrier =        new Pool(Barrier);
        this.pools.Shrine =         new Pool(Shrine);
        this.pools.Sapling =        new Pool(Sapling);
        this.pools.SmallTree =      new Pool(SmallTree);
        this.pools.BigTree =        new Pool(BigTree);
        this.pools.OrderCore =      new Pool(OrderCore);
        this.pools.WoodWall =       new Pool(WoodWall);
        this.pools.WoodDoor =       new Pool(WoodDoor);
        this.pools.StoneWall =      new Pool(StoneWall);
        this.pools.WoodPickup =     new Pool(WoodPickup);
        this.pools.GemsPickup =     new Pool(GemsPickup);
        this.pools.ScrollPickup =   new Pool(ScrollPickup);
        this.pools.GemsRock =       new Pool(GemsRock);
        this.pools.Nimbus =         new Pool(Nimbus);
        this.pools.GoblinMage =     new Pool(GoblinMage);
        this.pools.Inverter =       new Pool(Inverter);

        this.pools.CharacterHuman = new Pool(CharacterHuman);
        this.pools.Player =         new Pool(Player);

        this.pools.Deflector =      new Pool(Deflector);
        this.pools.DrainingBolt =   new Pool(DrainingBolt);
        this.pools.EnergyBlob =     new Pool(EnergyBlob);
        this.pools.FireBolt =       new Pool(FireBolt);
        this.pools.Flamethrower =   new Pool(Flamethrower);
        this.pools.MindBullet =     new Pool(MindBullet);
        this.pools.HealOrb =        new Pool(HealOrb);
        this.pools.RavenStrike =    new Pool(RavenStrike);
        this.pools.Rock =           new Pool(Rock);
        this.pools.Splash =         new Pool(Splash);
        this.pools.StoneShard =     new Pool(StoneShard);
        this.pools.TargetBolt =     new Pool(TargetBolt);
        this.pools.TeleOrb =        new Pool(TeleOrb);

        // Add the dynamic entities that are in this zone to the game world.
        this.addZoneEntities();

        // Whether the player can move or cast spells. Disabled when the player's character is unconscious.
        // Doesn't affect generic buttons like options or respawn.
        this.gameplayInputEnabled = true;
        // Flag for game update loop.
        this.joystickEnabled = magicks.settings.joystickEnabled;
        this.gamepadEnabled = magicks.settings.gamepadEnabled;

        // Disable the right-click context menu.
        this.game.canvas.oncontextmenu = function (e) {e.preventDefault();};

        // The code of the spell to cast. Also used for frame names.
        // This is a string of letters like 'f', not the '003' thing from before.
        this.spellCode = '';

        this.GUI = new GUI(this);

        this.inputManager = new Input(this);


        //this.setupGamepadControls();

        //if(this.joystickEnabled){
            //this.setupJoystickControls();
        //}
        /*else if(this.gamepadEnabled){

        }*/
        //else {
            //this.setupKeyboardControls();
            //this.setupMousePointerControls();
        //}

        this.visibleRange = 600;
        this.time.events.loop(1000, this.checkVisibleEntities, this);

        if(this.tilemap.animated){
            this.time.events.loop(1000, this.showNextFloorFrame, this);
        }

        //console.log("unlockedSpells:");
        //console.log(magicks.playerData.unlockedSpells);

       // console.log("-   -   -   -");
       // console.log("-   -   -   -");
       // console.log("-   -   -   -");
       // console.log("-   -   -   -");

        //console.log("this player entity ID: " + this.playerEntityId);
        console.log("-   -   -   -");

        //this.floorFrames[0].visible = true;
    },

    update: function () {

        this.inputManager.update();
        //console.log(this.GUI.anyPointerOnGUIButton);

        if(this.entities[this.playerEntityId] !== undefined){
            this.GUI.castRangeCircle.x = this.entities[this.playerEntityId].x;
            this.GUI.castRangeCircle.y = this.entities[this.playerEntityId].y;

            // Shitty hack to fix a really annoying bug...
            this.GUI.elementButtons.children[0].alpha = 1;
            this.GUI.elementButtons.children[1].alpha = 1;
            this.GUI.elementButtons.children[2].alpha = 1;
            this.GUI.elementButtons.children[3].alpha = 1;
            this.GUI.elementButtons.children[4].alpha = 1;
            this.GUI.elementButtons.children[5].alpha = 1;
            this.GUI.elementButtons.children[6].alpha = 1;
        }

    },

    render: function() {
        //var spacing = 25;
        this.game.debug.text('FPS: ' + this.game.time.fps || 'FPS: --', 8, 114, "#ff00ff");
        //this.game.debug.text('Lat: ' + latency, 8, 140, "#ff00ff");
    },

    shutdown: function () {
        // Remove all entity display objects from the game.
        //this.entitiesGroup.destroy(true, false);

        //// Clear all pools.
        //Object.keys(ECS.pools).forEach(function(key) {
        //    ECS.pools[key] = [];
        //});

        //// Empty the entities list.
        //ECS.entities = {};

        //console.log("calling game state shutdown");
        if(this.GUI.stick !== undefined){
            this.GUI.stick.destroy();
        }

        //console.log(this.floorFrames[0]);

        for(var i=0;i<this.tilemap.floorFrames.length;i+=1){
            //console.log(i);
            this.tilemap.floorFrames[i].destroy(true, true);
            //this.tilemap.entitiesFrames[i].destroy(true, true);
            //this.tilemap.decorationsFrames[i].destroy(true, true);

            this.tilemap.floorBitmapDatas[i].destroy();
            //this.tilemap.entitiesBitmapDatas[i].destroy();
            //this.tilemap.decorationsBitmapDatas[i].destroy();
        }

        this.tilemap.landformsFrame.destroy(true, true);
        this.tilemap.landformsBitmapData.destroy();

        // Remove the keyboard key callback, or it will carry over into other states.
        this.input.keyboard.onUpCallback = null;

    },

    showNextFloorFrame: function () {
        //console.log("showNextFloorFrame: " + this.currentFrame);
        // Use the next frame.
        this.currentFrame += 1;
        var floorFrames = this.tilemap.floorFrames;

        for(var i=0; i<2; i+=1){
            // Hide all current frames.
            floorFrames[i].visible = false;

            if(this.currentFrame === 2){
                // Go back to the first frame.
                this.currentFrame = 0;
            }
            else {
                floorFrames[this.currentFrame].visible = true;
            }
        }
    },

    addZoneEntities: function () {
        // Deactivate all pooled entities.
        Object.keys(this.pools).forEach(function(key) {
            //console.log("in foreach pool, key: " + key);
            _this.pools[key].forEach(function (sprite) {
                sprite.kill();
            });
        });

        // Reset the entities object, so it doesn't have any entity id properties.
        this.entities = {};

        // The entities list is is empty, ready to be populated with the entities for this zone using the data from the server.
        for(var i=0, len=this.entitiesData.length; i<len; i+=1){
            // Don't add this entity if there is already one with this ID.
            if(!this.entities[this.entitiesData[i].id]){
                // Create, or activate an existing sprite, for this entity, and add it to the list of entities.
                // this.pools[ENTITY TYPE].create(PASS IN DATA FOR THIS ENTITY FROM THE ENTITIESDATA)
                this.entities[this.entitiesData[i].id] = this.pools[magicks.typeCatalogue[this.entitiesData[i].typeNumber]].create(this.entitiesData[i]);
            }
        }

        // Follow the player's sprite again.
        if(this.entities[this.playerEntityId]){
            //console.log("this player has an entity");
            this.camera.follow(this.entities[this.playerEntityId]);
        }
        //console.log("player entity ID: " + this.playerEntityId);
    },

    checkVisibleEntities: function () { // TODO: only really need to do this when this player or another enetity moves, not all the time. Can be event triggered from socket events.
        var xDist;
        var yDist;
        var playerX;
        var playerY;
        var pools = this.pools;
        var visibleRange = this.visibleRange;
        // Check the player entity exists.
        if(this.entities[this.playerEntityId]){
            playerX = this.entities[this.playerEntityId].x;
            playerY = this.entities[this.playerEntityId].y;
        }
        // If not, use the middle of the camera (where the player would otherwise be) as the point to check the visible range from.
        else {
            playerX = this.camera.x + this.camera.width / 2;
            playerY = this.camera.y + this.camera.height / 2;
        }

        Object.keys(this.pools).forEach(function(key) {
            //console.log("in foreach pool, key: " + key);
            pools[key].forEach(function (sprite) {
                if(sprite.alive === true){
                    xDist = playerX - sprite.x;
                    yDist = playerY - sprite.y;
                    if(Math.sqrt(xDist*xDist + yDist*yDist) < visibleRange){
                        sprite.visible = true;
                    }
                    else {
                        sprite.visible = false;
                    }
                }
            });
        });

    },

    socketEvents: function () {
        /**
         * For when the character of this client moves. Don't need to check against all other characters
         * if it was some other one, as this character doesn't care what the other characters see.
         */
        function checkVisibleCharacterHumans () {
            var playerX = _this.entities[_this.playerEntityId].x;
            var playerY = _this.entities[_this.playerEntityId].y;
            var distanceFromPlayer = 0;
            var xDist;
            var yDist;
            var fontDistanceSize;

            // Make display names visible/bigger as other characters get closer to the player entity.
            _this.pools.CharacterHuman.forEachExists(function(item) {
                xDist = playerX - item.x;
                yDist = playerY - item.y;
                distanceFromPlayer = Math.sqrt(xDist*xDist + yDist*yDist);
                fontDistanceSize = (310 - (distanceFromPlayer-30)) / 10;

                if(distanceFromPlayer > 300){
                    item.displayName.visible = false;
                }
                else if(distanceFromPlayer <= 100){
                    item.displayName.visible = true;
                    item.displayName.setStyle({font: 1.3*12 + "px Acme", fill: "#ffffff", stroke: "#000000", strokeThickness: 4});
                }
                else if(fontDistanceSize > 4){
                    item.displayName.visible = true;
                    item.displayName.setStyle({font: (1.3*fontDistanceSize * 0.5) + "px Acme", fill: "#ffffff", stroke: "#000000", strokeThickness: 3});
                }
            });

            // Show the join order button if they are close enough to an order core.
            var entity = _this.pools.OrderCore.children[0];
            // Check there is even an order core in this zone.
            if(entity !== undefined){
                xDist = playerX - entity.x;
                yDist = playerY - entity.y;
                distanceFromPlayer = Math.sqrt(xDist*xDist + yDist*yDist);
                if(distanceFromPlayer < 94){
                    entity.joinOrderButton.visible = true;
                }
                else {
                    entity.joinOrderButton.visible = false;
                }
            }

            // Show the buy spell scroll button if they are close enough to a shrine.
            entity = _this.pools.Shrine.children[0];
            // Check there is even a shrine in this zone.
            if(entity !== undefined){
                xDist = playerX - entity.x;
                yDist = playerY - entity.y;
                distanceFromPlayer = Math.sqrt(xDist*xDist + yDist*yDist);
                if(distanceFromPlayer < 94){
                    entity.buySpellScrollButton.visible = true;
                }
                else {
                    entity.buySpellScrollButton.visible = false;
                }
            }

        }

        socketGS.on('zone_state', function (data) {
            //console.log("zone_state event:");
            //console.log(data);
            for(var i=0, len=data.length; i<len; i+=1){
                if(_this.entities[data[i].id]){
                    if(data[i].id === _this.playerEntityId){
                        checkVisibleCharacterHumans();
                    }
                    if(_this.entities[data[i].id].move){
                        _this.entities[data[i].id].move(data[i].x, data[i].y);
                    }
                    _this.add.tween(_this.entities[data[i].id]).to({x: data[i].x * GAME_SCALE, y: data[i].y * GAME_SCALE}, 1000/10, null, true);
                    //_this.entities[data[i].id].sprite.x = data[i].x;
                    //_this.entities[data[i].id].sprite.y = data[i].y;
                }
            }
        });

        socketGS.on('response_zone_entities', function (data) {
            //console.log("response_zone_entities event:");
            //console.log(data);
            _this.entitiesData = data;

            _this.addZoneEntities();
        });

        socketGS.on('add_entity', function (data) {
            //console.log("add_entity, id: " + data.id + ", name: " + data.displayName);
            //console.log(data);

            // Make sure there isn't already an entity with that id, otherwise the sprite already at this index wouldn't be removed.
            // The entity to add might be this player's entity, but they will have already added it when they were sent response_zone_entities.
            if(!_this.entities[data.id]){
                // Activate, or create a new entity for this entity.
                _this.entities[data.id] = _this.pools[magicks.typeCatalogue[data.typeNumber]].create(data);
            }
        });

        function removeEntityTweenCallback (sprite) {
            // Don't remove entities. Just kill and deactivate them so the objects can be reused.
            sprite.kill();
        }

        socketGS.on('remove_entity', function (data) {
            //console.log('remove_entity: ' + data.id);
            //console.log(data);
            if(_this.entities[data.id]){
                // Move the entity to the position that is was deactivated at, or it will appear to disappear before it hits anything, as the server is slightly ahead of what the client sees.
                _this.add.tween(_this.entities[data.id]).to({x: data.x * GAME_SCALE, y: data.y * GAME_SCALE}, 1000/10, null, true).onComplete.add(removeEntityTweenCallback, data.id);

                // Delete the property (key and value) that referenced the entity to remove.
                delete _this.entities[data.id];
            }
        });

        socketGS.on('char_unconscious', function (data) {
           //.log('character_unconscious: ' + data);
            if(_this.entities[data]){
                _this.entities[data].animations.play('unconscious');
                _this.entities[data].isUnconscious = true;
                _this.GUI.updateRevive(16);
                // If entity that is now unconscious is this players, then disable their inputs and change the bars.
                if(data === _this.playerEntityId){
                    _this.gameplayInputEnabled = false;
                    _this.GUI.hitPointsBar.visible = false;
                    _this.GUI.energyBar.visible = false;
                    _this.GUI.reviveBar.visible = true;
                    if(_this.GUI.stick !== undefined){
                        _this.GUI.stick.visible = false;
                    }
                    _this.GUI.clearSpellComponents();
                    _this.GUI.elementButtons.setVisibility(false);
                    _this.GUI.spellComponentBar.visible = false;
                }
            }
        });

        socketGS.on('char_revived', function (data) {
            //console.log('char_revived, data: ' + data);
            if(_this.entities[data]){
                _this.entities[data].animations.stop();
                _this.entities[data].animations.play("move");
                //_this.entities[data].frameName = 'character-human-base-move-down-1';
                _this.entities[data].isUnconscious = false;
                // If the entity that is now conscious is this players, then enable their inputs and change the bars.
                if(data === _this.playerEntityId){
                    _this.gameplayInputEnabled = true;
                    _this.GUI.hitPointsBar.visible = true;
                    _this.GUI.energyBar.visible = true;
                    _this.GUI.reviveBar.visible = false;
                    if(_this.GUI.stick !== undefined){
                        _this.GUI.stick.visible = true;
                    }
                    _this.GUI.elementButtons.setVisibility(true);
                    _this.GUI.spellComponentBar.visible = true;
                    _this.GUI.updateHitPoints(_this.maxHitPoints / 2);
                    _this.GUI.updateEnergy(_this.maxEnergy / 2);
                    _this.GUI.updateRevive(_this.maxHitPoints / 2);
                }
            }
        });

        socketGS.on('char_died', function (data) {
            //console.log('char_died, data: ' + data);
            //console.log(data);
            // The character that died was this player's entity. Hide some of the GUI.
            if(data === _this.playerEntityId){
                _this.GUI.hitPointsBar.visible = false;
                _this.GUI.energyBar.visible = false;
                _this.GUI.reviveBar.visible = false;
                if(_this.GUI.stick !== undefined){
                    _this.GUI.stick.visible = false;
                }
                _this.GUI.elementButtons.setVisibility(false);
                // Show the respawn button.
                _this.GUI.respawnButton.visible = true;
                _this.GUI.respawnButton.alpha = 0.7;
                _this.GUI.respawnTimer.start();
                _this.GUI.discordButton.visible = true;
            }
            // Recycle the sprite for the dead character.
            if(_this.entities[data]){
                // Don't remove entities. Just hide and deactivate them so the objects can be reused.
                _this.entities[data].kill();
                // Delete the property that referenced the entity to remove.
                delete _this.entities[data];
            }
        });

        socketGS.on('respawned', function () {
            //console.log("respawn success");
            _this.gameplayInputEnabled = true;
            _this.GUI.hitPointsBar.visible = true;
            _this.GUI.energyBar.visible = true;
            _this.GUI.reviveBar.visible = false;
            if(_this.GUI.stick !== undefined){
                _this.GUI.stick.visible = true;
            }
            _this.GUI.respawnButton.visible = false;
            _this.GUI.discordButton.visible = false;
            _this.GUI.elementButtons.setVisibility(true);
            _this.GUI.spellComponentBar.visible = true;
            _this.GUI.updateHitPoints(_this.maxHitPoints);
            _this.GUI.updateEnergy(_this.maxEnergy);
            _this.camera.follow(_this.entities[_this.playerEntityId]);
        });

        socketGS.on('attack', function (data) {
            //console.log("attack event");
            _this.entities[data.id].attack(data.angle);
        });

        socketGS.on('hitPoints_changed', function (data) {
            _this.GUI.updateHitPoints(data);
            //console.log("hitpoints changed: " + data);
        });

        socketGS.on('reviveProgress_changed', function (data) {
            _this.GUI.updateRevive(data);
            //console.log("revive prog changed: " + data);
        });

        socketGS.on('energy_changed', function (data) {
            _this.GUI.updateEnergy(data);
            //console.log("energy changed: " + data);
        });

        socketGS.on('healed', function (data) {
            if(_this.entities[data]){
                if(_this.entities[data].healed !== undefined){
                    _this.entities[data].healed();
                }
            }
        });

        socketGS.on('damaged', function (data) {
            if(_this.entities[data]){
                if(_this.entities[data].damaged !== undefined){
                    _this.entities[data].damaged();
                }
            }
        });

        socketGS.on('rotate', function (data) {
            if(_this.entities[data.id]){
                _this.entities[data.id].rotation = data.rotation;
            }
        });

        socketGS.on('change_zone', function (data) {
            //console.log("change zone, data:");
            //console.log(data);
            _this.tilemap.changeMap(data);
            //_this.createZone(data);
        });

        socketGS.on('change_floortile', function (data) {
            //console.log("changing floortile, data:");
            //console.log(data);

            for(var i=0; i<_this.tilemap.floorFrames.length; i+=1){
                _this.tilemap.updateFloorTile(data.row, data.col, data.code);
            }
        });

        socketGS.on('change_many_floortiles', function (data) {
            //console.log("changing many floortiles, data:");
            //console.log(data);
            //var tileSize = _this.tileSize;
            var j = 0,
                frameCount = _this.tilemap.floorFrames.length;
            for(var i=0, len=data.length; i<len; i+=1){
                for(j=0; j<frameCount; j+=1){
                    _this.tilemap.updateFloorTile(data[i].row, data[i].col, data[i].code);
                }
            }
        });

        socketGS.on('command', function (data) {
            //console.log("command event: " + data.command);
            if(_this.entities[data.id]){
                if(_this.entities[data.id][data.command] !== undefined){
                    _this.entities[data.id][data.command](data);
                }
            }
        });

        socketGS.on('chat', function (data) {
            var entity = _this.entities[data.id];
            //console.log("chat event, message: " + data.message);
            if(entity){
                if(entity.chatText !== undefined){
                    //_this.entities[data.id].chatText.visible = true;
                    entity.chatText.alpha = 1;
                    entity.chatText.text = data.message;
                    // Add some extra time for each character in the message, so longer messages are easier to read.
                    //_this.time.events.add(4000 + (data.message.length * 120), function () {
                        //console.log("chat callback, state: " + _this.key);

                        // Check the entity is still there when this fires. Might have been removed since chatting.
                        //if(_this.entities[data.id]){
                            //_this.entities[data.id].chatText.visible = false;
                    // Stop the previous tween if it is running, so the alpha doesn't keep getting decreased by it.
                    if(entity.chatTextTween.isRunning){
                        entity.chatTextTween.stop();
                    }
                    // Start a new tween after a delay.
                    entity.chatTextTween = _this.add.tween(entity.chatText).to({alpha: 0}, 2000, null, true, 4000 + (data.message.length * 120));
                        //}
                    //}, this);
                }
            }
        });

        socketGS.on('join_order_success', function (data) {
            //console.log("join order success event, player id: " + _this.playerEntityId);
            //console.log(data);

            // Show the order button.
            //_this.GUI.orderButton.visible = true;
            // Make it bounce in from the top corner.
            //_this.add.tween(_this.GUI.orderButton).to({x: _this.game.width + 6, y: -6}, 1000, Phaser.Easing.Bounce.Out, true);

            // Change the colour of team mates display names to green.
            var entity;
            for(var i= 0, len=data.length; i<len; i+=1){
                entity = _this.entities[data[i]];
                //console.log(entity);
                if(entity !== undefined){
                    if(entity.displayName !== undefined){
                        entity.displayName.addColor(magicks.colours.green, 0);
                    }
                }

            }
            // Show an alert that they are now in an order.
            _this.GUI.updateAlertText('Order joined');

            // Hide the craft core button on the order panel.
            _this.GUI.orderPanel.craftCoreText.visible = false;
            _this.GUI.orderPanel.craftCoreButton.visible = false;

        });

        socketGS.on('add_order_member', function (data) {
            //console.log("add order member event, data: " + data);
            var entity = _this.entities[data];
            if(entity !== undefined){
                //console.log("entity is valid");
                if(entity.displayName !== undefined){
                    //console.log("entity has a display name: " + entity.displayName.text);
                    // Show an alert of who joined the order.
                    _this.GUI.updateAlertText(entity.displayName.text + ' joined the order');
                    // Change their display name colour.
                    entity.displayName.addColor(magicks.colours.green, 0);
                }
            }

        });

        socketGS.on('remove_order_member', function (data) {
            //console.log("remove order member event, data: " + data);
            var entity = _this.entities[data];
            if(entity !== undefined){
                //console.log("entity is valid");
                // If it was this player that left the order.
                if(data === _this.playerEntityId){
                    // Show an alert of who left the order.
                    _this.GUI.updateAlertText('You left the order');
                    // Change their display name colour.
                    entity.displayName.addColor(magicks.colours.white, 0);

                    // Make the craft core button on the order panel visible again.
                    _this.GUI.orderPanel.craftCoreText.visible = true;
                    _this.GUI.orderPanel.craftCoreButton.visible = true;
                }
                else {
                    if(entity.displayName !== undefined){
                        //console.log("entity has a display name: " + entity.displayName.text);
                        // Show an alert of who left the order.
                        _this.GUI.updateAlertText(entity.displayName.text + ' left the order');
                        // Change their display name colour.
                        entity.displayName.addColor(magicks.colours.white, 0);
                    }
                }
            }
        });

        socketGS.on('order_destroyed', function (data) {
            //console.log("order destroyed event");

            // Change the colour of former order member display names back to white.
            var entity;
            for(var i= 0, len=data.length; i<len; i+=1){
                entity = _this.entities[data[i]];
                //console.log(entity);
                if(entity !== undefined){
                    if(entity.displayName !== undefined){
                        entity.displayName.addColor(magicks.colours.white, 0);
                    }
                }
            }
            // Show an alert that they are now in an order.
            _this.GUI.updateAlertText('Order destroyed');

            // Make the craft core button on the order panel visible again.
            _this.GUI.orderPanel.craftCoreText.visible = true;
            _this.GUI.orderPanel.craftCoreButton.visible = true;
        });

        socketGS.on('add_item', function (data) {
            //console.log("add item event, data:");
            //console.log(data);

            var item = _this.GUI.inventoryPanel.items.children[data.index];
            item.frameName = _this.GUI.inventoryPanel.itemTypes[data.type].frameName;
            item.visible = true;

            var slot = _this.GUI.inventoryPanel.slots.children[data.index];
            slot.alpha = 1;

            if(data.config !== undefined){
                if(data.config.spellName !== undefined){
                    slot.itemDetails.itemName = _this.GUI.inventoryPanel.itemTypes[data.type].itemName + ": " + data.config.spellName;
                }
            }
            else {
                slot.itemDetails.itemName = _this.GUI.inventoryPanel.itemTypes[data.type].itemName;
            }

            slot.itemDetails.description = _this.GUI.inventoryPanel.itemTypes[data.type].description;

        });

        socketGS.on('remove_item', function (data) {
            //console.log("remove item event, data:");
            //console.log(data);

            _this.GUI.inventoryPanel.items.children[data].visible = false;

            _this.GUI.inventoryPanel.slots.children[data].alpha = 0.5;
        });

        socketGS.on('remove_many_items', function (data) {
            //console.log("remove many items event, data:");
            //console.log(data);

            for(var i=0, len=data.length; i<len; i+=1){
                _this.GUI.inventoryPanel.items.children[data[i]].visible = false;

                _this.GUI.inventoryPanel.slots.children[data[i]].alpha = 0.5;
            }

        });

        socketGS.on('add_spell', function (data) {
            //console.log("add spell event, data: " + data);
            window.magicks.playerData.unlockedSpells[data] = true;
        });

        socketGS.on('remove_spell', function (data) {
            //console.log("remove spell event, data: " + data);
            delete window.magicks.playerData.unlockedSpells[data];
        });

        socketGS.on('alert_message', function (data) {
            //console.log("alert_message event: ");
            //console.log(data);

            if(data){
                _this.GUI.updateAlertText(data);
            }

        });

        socketGS.on('hide', function (data) {
            //console.log("hide event");

            for(var i=0, len=data.length; i<len; i+=1){

                if(_this.entities[data[i]]){
                    // Don't make this player's entity totally invisible.
                    if(data[i] === _this.playerEntityId){
                        _this.entities[data[i]].alpha = 0.4;
                    }
                    else {
                        _this.entities[data[i]].alpha = 0;
                    }
                }

            }

        });

        socketGS.on('show', function (data) {
            //console.log("show event");

            for(var i=0, len=data.length; i<len; i+=1){
                if(_this.entities[data[i].id]){
                    _this.entities[data[i].id].alpha = 1;
                    _this.entities[data[i].id].x = data[i].x * GAME_SCALE;
                    _this.entities[data[i].id].y = data[i].y * GAME_SCALE;
                }
            }

        });
    }
};