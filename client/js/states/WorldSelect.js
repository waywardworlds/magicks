/**
 * Created by User on 07/06/2016.
 */

"use strict";

import io from 'socket.io-client';

magicks.WorldSelect = function () {
};

magicks.WorldSelect.prototype = {

    create: function () {

        console.log("in worldselect");

        //TODO: REDO THE WHOLE SELECTION GRID THING TO BE LIKE THE SCROLLABLE/DRAGGABLE LIST IN THE SPELL MENU.

        //this.state.start("Game");

        window._this = this;

        //this.add.image(0, 0, 'screens', 'screen-world-select');
        this.add.image(0, 0, 'menus-atlas', 'main-background');

        //addCircleButton(10, 10, 'btn-arrow', this.backPressed, this);
        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        this.add.text(this.game.width / 2, 40, 'World select', {font: "34px Alagard", fill: magicks.colours.white, stroke: magicks.colours.black, strokeThickness: 5, boundsAlignH: "center", boundsAlignV: "middle"}).anchor.set(0.5);

        var textStyle = {
            font: "30px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5
        };

        this.add.text(190 , 80, 'World', textStyle);
        this.add.text(380, 80, 'Players', textStyle);

        // A local store of the info about each world, sent by the player manager.
        this.worldsData = [];

        this.selectionTexts = [];

        // Is a world currently selected?
        this.isWorldSelected = false;
        // Some details about the world that is selected.
        this.selectedWorld = {name: 0, playerCount: 0};

        // How far down the list of worlds the user has gone.
        this.selectionOffset = 0;

        // Add the text objects for the list of worlds.
        this.setupSelectionTexts();

        this.userAlert = this.add.text(390, 76, '', {
            font: "30px Alagard",
            fill: magicks.colours.orange,
            align: 'center'
        });
        this.userAlert.anchor.set(0.5);

        // Add the side buttons.
        var y = 70;
        var space = 94;
        //this.upButton =         addCircleButton(680, y, 'btn-arrow', this.upPressed, this);
        this.upButton = this.add.button(720, y, 'menus-atlas', this.upPressed, this);
        this.upButton.frameName = 'btn-arrow';
        this.upButton.scale.setTo(3);
        this.upButton.anchor.setTo(0.5);
        this.upButton.angle = 90;

        this.downButton = this.add.button(720, y+space, 'menus-atlas', this.downPressed, this);
        this.downButton.frameName = 'btn-arrow';
        this.downButton.scale.setTo(3);
        this.downButton.anchor.setTo(0.5);
        this.downButton.angle = -90;

        this.refreshButton = this.add.button(720, y+(space*2), 'menus-atlas', this.refreshPressed, this);
        this.refreshButton.frameName = 'btn-refresh';
        this.refreshButton.scale.setTo(3);
        this.refreshButton.anchor.setTo(0.5);

        this.acceptButton = this.add.button(720, y+(space*3), 'menus-atlas', this.acceptPressed, this);
        this.acceptButton.frameName = 'btn-accept';
        this.acceptButton.scale.setTo(3);
        this.acceptButton.anchor.setTo(0.5);
        this.acceptButton.alpha = 0.4;

        this.input.keyboard.addKey(Phaser.Keyboard.UP).onDown.add(this.upPressed, this);
        this.input.keyboard.addKey(Phaser.Keyboard.DOWN).onDown.add(this.downPressed, this);
        this.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(this.acceptPressed, this);
        this.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE).onDown.add(this.backPressed, this);

        // Request the data for all worlds from the server.
        socketPM.emit('request_worlds_data');
    },

    shutdown: function () {
        //console.log("calling worldselect shutdown");
        delete this.worldsData;
        delete this.selectionTexts;
        delete this.isWorldSelected;
        delete this.selectedWorld;
        delete this.selectionOffset;
        delete this.userAlert;
        delete this.upButton;
        delete this.downButton;
        delete this.acceptButton;

    },

    setupSelectionTexts: function () {
        var y = 84;
        for(var i=0; i<6; i+=1){
            y += 50;
            this.selectionTexts.push(this.addWorldSelectionTexts(y));
        }
    },

    addWorldSelectionTexts: function (y) {
        var textStyle = {
            font: "30px Alagard",
            fill: magicks.colours.white
        };

        var selection = {};
        var x = 100;

        // Draw a grey rectangle to show the rows.
        selection.rect = this.add.graphics(x-8, y-2);
        selection.rect.beginFill(0xc0c0c0, 0.4);
        selection.rect.drawRect(0, 0, 500, 40);
        selection.rect.endFill();

        selection.worldNameText   = this.add.text(x+16, y, 'a', textStyle);
        selection.playerCountText = this.add.text(x + 300, y, 'b', textStyle);
        selection.marker =          this.add.button(78, y-16, 'menus-atlas', function (btn) {
            //console.log("World selection marker pressed");
            // This will be run when the button for a world is pressed.

            // Hide all other markers.
            for(var i=0; i<this.selectionTexts.length; i+=1){
                // Hide this button if it is not the button that was pressed.
                if(this.selectionTexts[i].marker !== btn){
                    this.selectionTexts[i].marker.renderable = false;
                }
                // This is the button that was pressed.
                else {
                    // Check there is actually a world in this position in worldsData.
                    if(this.worldsData[i+this.selectionOffset]){
                        // Get the id and player count of the world that is selected.
                        this.selectedWorld.name = this.worldsData[i+this.selectionOffset].name;
                        this.selectedWorld.playerCount = this.worldsData[i+this.selectionOffset].playerCount;
                    }
                    // There is no world data for this slot. Make it do nothing.
                    else {
                        this.isWorldSelected = false;
                        this.acceptButton.alpha = 0.4;
                        this.selectionTexts[i].marker.renderable = false;
                        return;
                    }
                }
            }
            // Switch the renderability of the marker that was pressed.
            btn.renderable = !btn.renderable;

            // A world must be selected before the accept button is active.
            if(btn.renderable){
                this.isWorldSelected = true;
                this.acceptButton.alpha = 1;
            }
            else {
                this.isWorldSelected = false;
                this.acceptButton.alpha = 0.4;
            }

        }, this);
        selection.marker.frameName = 'world-selection-marker';
        selection.marker.scale.setTo(8);
        selection.marker.renderable = false;

        selection.marker.alpha = 0.8;

        /*console.log("marker x: " + selection.marker.x);
        console.log("marker y: " + selection.marker.y);
        console.log("");*/

        return selection;
    },

    updateWorldListTexts: function () {
        //console.log("updateWorldListTexts");
        // How far down the list has been scrolled.
        var o = this.selectionOffset;

        for(var i=0; i<this.selectionTexts.length; i+=1){
            var world = this.worldsData[i+o];
            // Check there is some data for the world for this slot.
            if(this.worldsData[i+o]){
                this.selectionTexts[i].worldNameText.setText(world.name);
                this.selectionTexts[i].playerCountText.setText(world.playerCount + "/" + world.maxCapacity);
                // Change colour to show if world has space or not.
                if(world.playerCount < world.maxCapacity){
                    this.selectionTexts[i].playerCountText.fill = magicks.colours.white
                }
                else {
                    this.selectionTexts[i].playerCountText.fill = '#ff0000'
                }
            }
            else {
                //console.log("No worlds data for this selection text");
                this.selectionTexts[i].worldNameText.setText('');
                this.selectionTexts[i].playerCountText.setText('');
            }
        }
    },

    moveWorldsList: function (direction) {
        //console.log("moveWorldsList");
        if(direction === 'up'){
            // Stop the menu from going up past the top of the list of games.
            if(this.selectionOffset > 0){
                // Decrement the counter, so previous elements in the worlds array are accessed.
                this.selectionOffset -= 1;
                // Hide all markers.
                for(var i=0; i<this.selectionTexts.length; i+=1){
                    this.selectionTexts[i].marker.renderable = false;
                }
                this.isWorldSelected = false;
                this.acceptButton.alpha = 0.4;
            }
        }
        else if(direction === 'down'){
            // Stop the menu from going up past the bottom of the list of worlds.
            if(this.selectionOffset < this.worldsData.length-6){
                // Increment the counter, so next elements in the worlds array are accessed.
                this.selectionOffset += 1;
                // Hide all markers.
                for(var i=0; i<this.selectionTexts.length; i+=1){
                    this.selectionTexts[i].marker.renderable = false;
                }
                this.isWorldSelected = false;
                this.acceptButton.alpha = 0.4;
            }
        }

        // If the list is at the top, then change the transparency of
        // the up button to show that can't go up any further.
        if(this.selectionOffset === 0){
            this.upButton.alpha = 0.4;
        }
        else {
            this.upButton.alpha = 1;
        }

        // If the list is at the bottom, then change the transparency of
        // the down button to show that can't go down any further.
        if(this.selectionOffset === this.worldsData.length-6){
            this.downButton.alpha = 0.4;
        }
        else {
            this.downButton.alpha = 1;
        }

        this.updateWorldListTexts();
    },

    backPressed: function () {
        this.state.start("Menu");
    },

    upPressed: function () {
        //console.log("^ up button pressed");
        this.moveWorldsList('up');
    },

    downPressed: function () {
        //console.log("v down button pressed");
        this.moveWorldsList('down');
    },

    refreshPressed: function () {
        //console.log("@ refresh button pressed");
        // Get new world data from server.
        socketPM.emit('request_worlds_data');
        this.userAlert.setText('Refreshing worlds list');
    },

    acceptPressed: function () {
        // Make sure a world is selected.
        if(this.isWorldSelected){
            // Does world have space.
            if(this.selectedWorld.playerCount < 200){
                //console.log("Selected world name: " + this.selectedWorld.name);
                // Ask the player manager to allow this client to connect to the selected world.
                socketPM.emit('connect_world', {name: this.selectedWorld.name});
            }
            else {
                this.userAlert.setText('That world is full');
            }
        }
        // No world is selected.
        else {
            this.userAlert.setText('Select a world to join');
        }
    },

    events: (function () {

        socketPM.on('response_worlds_data', function (data) {
            //console.log("response_worlds_data");
            //console.log(data);

            // Update the worlds data with the new info from the player manager.
            _this.worldsData = data;

            _this.updateWorldListTexts();
            // Change up/down button transparency, as the list may already by at the top/bottom when the world data arrives.
            _this.moveWorldsList();
            _this.userAlert.setText('');
        });

        // The response to the request to join a specified world.
        socketPM.on('connect_world_response', function (data) {
            //console.log("connect_world_response");
            //console.log("Server to join IP: " + "http://" + data.gameServerIPAddress + ":3005");

            // Connect to the game server for this world.
            if(devMode === true){
                // Localhost.
                window.socketGS = io("http://127.0.0.4:3005");
            }
            else {
                // DO server.
                window.socketGS = io("http://" + data.gameServerIPAddress + ":3005");
            }
            //socketGS = io("http://31.220.211.134:3005");

            // Attempt to join the world, using the auth code that was given by the player manager.
            socketGS.emit('join_world', {authCode: data.authCode});

            socketGS.on('hello_game', function () {
                //console.log("Game server says hi :o");
            });

            // If the game server that this player attempted to join accepts them, then start the game.
            socketGS.on('join_world_success', function (data) {
                // This player is now in a game world.
                //magicks.inGame = true;
                // Disconnect from the player manager server.
                socketPM.disconnect();

                //console.log('join_world_success, data:');
                //console.log(data);
                _this.state.start('Game', true, false, data.zoneName, data.playerEntityId, data.entitiesData, data.mapData, data.maxHitPoints, data.maxEnergy);
            });

            // The game server says that the world to join is full.
            socketGS.on('world_full', function () {
                //console.log("world_full");
                // Couldn't join the selected world. It probably became full since the last time the worlds data was updated.
                _this.userAlert.setText('That world is full');
                // Request updated data for all worlds from the server.
                socketPM.emit('request_worlds_data');
            });

            // If the game server has disconnected from this client.
            socketGS.on('disconnect', function () {
                console.log("Disconnected from game server.");
                //magicks.inGame = false;
                if(_this.UI && _this.UI.stick){
                    // Remove the virtual joystick manually, as it is added to Game.stage (above everything else), not the game world.
                    _this.UI.stick.destroy();
                }
                // Call disconnect from this end, in case the server itself died, so
                // that this client doesn't keep trying to automatically reconnect to it.
                socketGS.disconnect();
                // Go back to the log in screen.
                _this.state.start("LogIn");
            });

        });

        // The player manager says that the world to join is full.
        socketPM.on('world_full', function () {
            //console.log("world_full");
            // Couldn't join the selected world. It probably became full since the last time the worlds data was updated.
            _this.userAlert.setText('That world is full');
            // Request updated data for all worlds from the server.
            socketPM.emit('request_worlds_data');
        });

    })()
};