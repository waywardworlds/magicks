/**
 * Created by User on 03/04/2016.
 */

"use strict";

magicks.Menu = function () {
};

magicks.Menu.prototype = {

    create: function () {
        console.log("in menu");

        window._this = this;

        // Hide the menus ad.
        magicks.menusAd.style.visibility = "hidden";

        this.add.image(0, 0, 'menus-atlas', 'main-background');
        var titleText = this.add.image(this.game.width / 2, 10, 'menus-atlas', 'title-logo');
        titleText.scale.setTo(2);
        titleText.anchor.set(0.5, 0);

        var alertStyle = {
            font: "26px Alagard",
            fill: magicks.colours.orange,
            boundsAlignH: 'right'
        };

        //addCircleButton(10, 10, 'btn-exit', this.logOutPressed, this, 0.8);
        var logOutButton = this.add.button(10, 10, 'game-atlas', this.logOutPressed, this);
        logOutButton.frameName = 'btn-exit';
        logOutButton.scale.setTo(2);

        this.userAlert = this.add.text(540, 90, '', alertStyle);
        this.userAlert.anchor.set(1, 0);

        this.addDisplayNameInput();
        this.addWorldSelectButton();
        this.addGameGuideButton();

    },

    shutdown: function () {
        // Remove the keyboard key callback, or it will carry over into other states.
        this.input.keyboard.onUpCallback = null;
    },

    addDisplayNameInput: function () {
        var style = {
            font: '28px Alagard',
            fill: magicks.colours.white,
            fillAlpha: 0.2,
            fontWeight: 'bold',
            width: 220,
            height: 34,
            padding: 18,
            borderWidth: 4,
            max: 20,
            cursorColor: magicks.colours.orange,
            borderColor: magicks.colours.white,
            placeHolder: 'Enter a name',
            placeHolderColor: magicks.colours.grey
        };

        this.displayNameInput = this.add.inputField(454, 120, style);
        // Set the name to the current display name.
        this.displayNameInput.setText(magicks.playerData.displayName);

        this.acceptNameButton = this.add.button(728, 130, 'menus-atlas', this.acceptPressed, this);
        this.acceptNameButton.frameName = 'btn-accept';
        this.acceptNameButton.scale.setTo(2);
        this.acceptNameButton.visible = false;

        var inputHasFocus = false;

        this.input.keyboard.onUpCallback = function(e) {
            console.log("menu kbd onup");
            // Is an Enter key.
            if(e.keyCode === 13){
                console.log(inputHasFocus);
                if(inputHasFocus === true){
                    inputHasFocus = false;
                    //    this.displayNameInput.endFocus();
                    //    // Submit the new display name.
                    _this.acceptPressed();
                }
                else {
                    inputHasFocus = true;
                    _this.displayNameInput.startFocus();
                }
            }
            else {
                if(_this.displayNameInput.value === magicks.playerData.displayName){
                    _this.acceptNameButton.visible = false;
                }
                else {
                    _this.acceptNameButton.visible = true;
                }
            }
            console.log(e.keyCode);
        }

    },

    addWorldSelectButton: function () {
        // Text over the world select button.
        var worldSelectText = this.add.text(0, 0, 'World\nselect', {
            font: "60px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 8
        });
        worldSelectText.anchor.setTo(0.5);
        worldSelectText.scale.setTo(0.30);
        // World select button.
        var worldSelectButton = new PhaserNineSlice.NineSlice(this, this.game.width / 2 - 184, 272, 'menus-atlas', 'nineslice-button', 70, 70, {top: 7});
        worldSelectButton.scale.setTo(4);
        worldSelectButton.anchor.setTo(0.5);
        worldSelectButton.inputEnabled = true;
        worldSelectButton.events.onInputDown.add(this.worldSelectPressed, this);
        worldSelectButton.addChild(worldSelectText);
        this.add.existing(worldSelectButton);
    },

    addGameGuideButton: function () {
        // Text over the world select button.
        var gameGuideText = this.add.text(0, 0, 'Game\nguide', {
            font: "60px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 8
        });
        gameGuideText.anchor.setTo(0.5);
        gameGuideText.scale.setTo(0.25);
        // World select button.
        var gameGuideButton = new PhaserNineSlice.NineSlice(this, this.game.width / 2 + 184, 320, 'menus-atlas', 'nineslice-button', 70, 45, {top: 7});
        gameGuideButton.scale.setTo(4);
        gameGuideButton.anchor.setTo(0.5);
        gameGuideButton.inputEnabled = true;
        gameGuideButton.events.onInputDown.add(this.gameGuidePressed, this);
        gameGuideButton.addChild(gameGuideText);
        this.add.existing(gameGuideButton);
    },

    logOutPressed: function () {
        //console.log("log out pressed");
        socketPM.emit('log_out');
        this.state.start("LogIn");
    },

    acceptPressed: function () {
        console.log("accept pressed");
        if(this.displayNameInput.value == ''){

        }
        else if(this.displayNameInput.value.length > 32){

        }
        else {
            socketPM.emit('change_display_name', this.displayNameInput.value);
        }

    },

    worldSelectPressed: function () {
        this.state.start("WorldSelect");
    },

    gameGuidePressed: function () {
        this.state.start("GameGuide");
    },

    events: (function () {

        socketPM.on('display_name_accepted', function () {
            console.log("display name accepted");
            magicks.playerData.displayName = _this.displayNameInput.value;
            _this.acceptNameButton.visible = false;
            // Reset the text input fields so they have the updated value.
            //_this.state.restart();

        });

        socketPM.on('display_name_rejected', function (data) {
            if(data === 1){
                //_this.userAlert.text = textDefs['A13'];
            }
            else if(data === 2){
                //_this.userAlert.text = textDefs['A14'];
            }
            else if(data === 3){
                //_this.userAlert.text = textDefs['A15'];
            }
            else {
                //_this.userAlert.text = textDefs['A10'];
            }
        });

    })()
};