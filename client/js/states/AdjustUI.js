/**
 * Created by User on 26/01/2017.
 */

"use strict";

magicks.AdjustUI = function () {
};

magicks.AdjustUI.prototype = {

    preload: function () {
        this.load.image('zone-3', 'assets/map/forest/zone-3.png');
    },

    create: function () {
        _this = this;

        ECS.game = this;

        // Add a background image of the game world.
        var bg = this.add.image(0, 0, 'zone-3');
        bg.scale.setTo(magicks.gameScale);
        this.world.setBounds(0, 0, bg.width * bg.scale.x, bg.height * bg.scale.y);

        // Add a human character to the middle.
        var player = ECS.assemblages.characterHuman({id: 5000, x: 17*magicks.gameScale*16, y: 55*magicks.gameScale*16, displayName: 'ddd'});
        this.camera.follow(player.sprite);

        // The UI element that is currently selected for adjusting.
        this.selectedUIElement = {};

        // Container object for all of the UI elements that can be adjusted.
        this.UIElements = {};

        // The 'options' dropdown menu button.
        this.UIElements.options = addCircleButton(magicks.settings.UI.options.x - 40, magicks.settings.UI.options.y - 40, 'btn-options', function () {}, this);
        this.UIElements.options.fixedToCamera = true;
        this.UIElements.options.input.enableDrag(true);
        this.UIElements.options.input.dragDistanceThreshold = 20;
        this.UIElements.options.events.onInputDown.add(this.UIElementPressed, this);
        this.UIElements.options.scale.setTo(magicks.settings.UI.options.scale);
        this.UIElements.options.customGraphic = true;
        // Set default selected UI element to options.
        this.selectedUIElement = this.UIElements.options;

        this.addUIElement(magicks.settings.UI.wood, 'wood-icon', 'wood');
        this.addUIElement(magicks.settings.UI.gems, 'gem-icon', 'gems');
        this.addUIElement(magicks.settings.UI.food, 'food-icon', 'food');
        // HP BAR???

        this.addUIElement(magicks.settings.UI.spell1, 'buttons', 'spell1', 'btn-spell-slot');
        this.addUIElement(magicks.settings.UI.spell2, 'buttons', 'spell2', 'btn-spell-slot');
        this.addUIElement(magicks.settings.UI.spell3, 'buttons', 'spell3', 'btn-spell-slot');
        this.addUIElement(magicks.settings.UI.spell4, 'buttons', 'spell4', 'btn-spell-slot');
        //if(!this.game.device.desktop){
            this.addUIElement(magicks.settings.UI.joystick, 'joystick', 'joystick');
        //}

        // Add the adjustment controls.
        var refresh = addCircleButton(this.game.width/2 - 100 , this.game.height/2-100, 'btn-refresh', this.resetPressed, this);
        var accept = addCircleButton(this.game.width/2 + 20, this.game.height/2-100, 'btn-accept', this.acceptPressed, this);
        var scaleDown = addCircleButton(this.game.width/2 - 100 , this.game.height/2+20, 'btn-scale-down', this.scaleDownPressed, this);
        var scaleUp = addCircleButton(this.game.width/2 + 20, this.game.height/2+20, 'btn-scale-up', this.scaleUpPressed, this);
        refresh.fixedToCamera = true;
        accept.fixedToCamera = true;
        scaleDown.fixedToCamera = true;
        scaleUp.fixedToCamera = true;

    },

    addUIElement: function (setting, frame, name, frameName) {
        // Add the button that will be interacted with.
        this.UIElements[name] = this.add.button(setting.x || -100, setting.y || -100, frame || '', undefined, this);
        // Fix to camera or it will still be in the top left of the game world.
        this.UIElements[name].fixedToCamera = true;
        this.UIElements[name].input.enableDrag(true);
        this.UIElements[name].scale.setTo(setting.scale);
        // Set anchor to middle so they scale from the middle.
        this.UIElements[name].anchor.setTo(0.5);
        // How far this element needs to be dragged before it becomes loose and movable.
        this.UIElements[name].input.dragDistanceThreshold = 20;
        this.UIElements[name].events.onInputDown.add(this.UIElementPressed, this);
        // Make the element look unselected.
        this.UIElements[name].alpha = 0.8;
        // Frame in a texture atlas.
        if(frameName){
            this.UIElements[name].frameName = frameName
        }
    },

    resetPressed: function () {
        // Set the UI settings back to default.
        magicks.settings.UI = {
            options:    {x: 40,     y: 40,              scale: 4},
            wood:       {x: 140,    y: 30,              scale: 4},
            gems:       {x: 200,    y: 30 ,             scale: 4},
            food:       {x: 260,    y: 30 ,             scale: 4},
            HPBar:      {x: 0,      y: 0,               scale: 1},
            joystick:   {x: 100,    y: 350,             scale: 0.5},
            spell1:     {x: 700,    y: 80 + (0 * 90),   scale: 1},
            spell2:     {x: 700,    y: 80 + (1 * 90),   scale: 1},
            spell3:     {x: 700,    y: 80 + (2 * 90),   scale: 1},
            spell4:     {x: 700,    y: 80 + (3 * 90),   scale: 1}
        };
        // Restart the state so the UI elements are shown with the default settings.
        this.state.restart();
    },

    acceptPressed: function () {
        var obj = {};
        // Loop through all of the UI elements.
        for(var key in this.UIElements){
            if(this.UIElements.hasOwnProperty(key)){
                obj = this.UIElements[key];
                // Check if the UI settings object has a property key of the name of this element.
                if(magicks.settings.UI[key]){
                    // Update the values in the UI settings object to the position and scale of each UI element.
                    magicks.settings.UI[key].x = obj.x - this.camera.x;
                    magicks.settings.UI[key].y = obj.y - this.camera.y;
                    magicks.settings.UI[key].scale = obj.scale.x;
                }
            }
        }
        // Update the client local storage so these changes persist across game sessions on this device.
        localStorage.setItem("settings", JSON.stringify(magicks.settings));
        // Go back to the Settings state.
        this.state.start("Settings");
    },

    scaleUpPressed: function () {
        this.selectedUIElement.scale.setTo(this.selectedUIElement.scale.x + 0.2);
        /*console.log(this.UIElements.options.scale);
        console.log(this.UIElements.options.x);
        console.log(this.UIElements.options.y);*/
    },

    scaleDownPressed: function () {
        // Don't let the button be scaled too small.
        if(this.selectedUIElement.scale.x - 0.2 > 0){
            this.selectedUIElement.scale.setTo(this.selectedUIElement.scale.x - 0.2);
        }
        /*console.log(this.UIElements.options.scale);
        console.log(this.UIElements.options.x);
        console.log(this.UIElements.options.y);*/
    },

    UIElementPressed: function (btn) {
        //console.log(btn);
        this.selectedUIElement.alpha = 0.8;
        this.selectedUIElement = btn;
        this.selectedUIElement.alpha = 2;
    }
};