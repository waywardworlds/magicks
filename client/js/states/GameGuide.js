/**
 * Created by User on 19/01/2018.
 */

"use strict";

magicks.GameGuide = function () {
};

magicks.GameGuide.prototype = {

    create: function () {
        console.log("in game guide");

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        this.textStyle = {
            font: "50px Alagard",
            fill: magicks.colours.white,
            align: "center",
            stroke: magicks.colours.black,
            strokeThickness: 7
        };
        this.addElementsButton();
        this.addSpellsButton();
        this.addOrdersButton();
        this.addResourcesButton();

    },

    backPressed: function () {
        this.state.start("Menu");
    },

    addElementsButton: function () {
        // Text over the elements guide button.
        var text = this.add.text(0, 0, 'Elements', this.textStyle);
        text.anchor.setTo(0.5, 1.1);
        text.scale.setTo(0.25);
        // Elements guide button.
        var button = new PhaserNineSlice.NineSlice(this, this.game.width / 2 - 155, 120, 'menus-atlas', 'nineslice-button', 70, 45, {top: 7});
        button.scale.setTo(4);
        button.anchor.setTo(0.5);
        button.inputEnabled = true;
        button.events.onInputDown.add(function () {
            this.state.start("GuideElements");
        }, this);
        button.addChild(text);
        this.add.existing(button);

        var icon = this.add.image(0, 8, 'menus-atlas', 'elements-icon');
        icon.anchor.setTo(0.5);
        icon.scale.setTo(0.7);
        button.addChild(icon);
    },

    addSpellsButton: function () {
        // Text over the spells guide button.
        var text = this.add.text(0, 0, 'Spells', this.textStyle);
        text.anchor.setTo(0.5, 1.1);
        text.scale.setTo(0.25);
        // Spells guide button.
        var button = new PhaserNineSlice.NineSlice(this, this.game.width / 2 + 155, 120, 'menus-atlas', 'nineslice-button', 70, 45, {top: 7});
        button.scale.setTo(4);
        button.anchor.setTo(0.5);
        button.inputEnabled = true;
        button.events.onInputDown.add(function () {
            this.state.start("GuideSpells");
        }, this);
        button.addChild(text);
        this.add.existing(button);

        var icon = this.add.image(0, 8, 'menus-atlas', 'spells-icon');
        icon.anchor.setTo(0.5);
        icon.scale.setTo(1.6);
        button.addChild(icon);
    },

    addOrdersButton: function () {
        // Text over the orders guide button.
        var text = this.add.text(0, 0, 'Orders', this.textStyle);
        text.anchor.setTo(0.5, 1.1);
        text.scale.setTo(0.25);
        // Orders guide button.
        var button = new PhaserNineSlice.NineSlice(this, this.game.width / 2 - 155, 320, 'menus-atlas', 'nineslice-button', 70, 45, {top: 7});
        button.scale.setTo(4);
        button.anchor.setTo(0.5);
        button.inputEnabled = true;
        button.events.onInputDown.add(function () {
            this.state.start("GuideOrders");
        }, this);
        button.addChild(text);
        this.add.existing(button);

        var icon = this.add.image(0, 8, 'menus-atlas', 'order-icon');
        icon.anchor.setTo(0.5);
        icon.scale.setTo(0.8);
        button.addChild(icon);
    },

    addResourcesButton: function () {
        // Text over the resources guide button.
        var text = this.add.text(0, 0, 'Resources', this.textStyle);
        text.anchor.setTo(0.5, 1.1);
        text.scale.setTo(0.25);
        // Resources guide button.
        var button = new PhaserNineSlice.NineSlice(this, this.game.width / 2 + 155, 320, 'menus-atlas', 'nineslice-button', 70, 45, {top: 7});
        button.scale.setTo(4);
        button.anchor.setTo(0.5);
        button.inputEnabled = true;
        button.events.onInputDown.add(function () {
            this.state.start("GuideResources");
        }, this);
        button.addChild(text);
        this.add.existing(button);

        var iconWood = this.add.image(-18, 8, 'menus-atlas', 'wood-icon');
        var iconGems = this.add.image(0, 8, 'menus-atlas', 'gems-icon');
        var iconFood = this.add.image(18, 8, 'menus-atlas', 'food-icon');
        iconWood.anchor.setTo(0.5);
        iconGems.anchor.setTo(0.5);
        iconFood.anchor.setTo(0.5);
        iconWood.scale.setTo(1);
        iconGems.scale.setTo(1);
        iconFood.scale.setTo(1);
        button.addChild(iconWood);
        button.addChild(iconGems);
        button.addChild(iconFood);
    }
};