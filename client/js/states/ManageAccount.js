/**
 * Created by User on 13/01/2018.
 */

/**
 * Created by User on 07/06/2016.
 */

"use strict";

magicks.ManageAccount = function () {
    // Only want to add the input fields once ever, or they get messed up.
    // Just show them when on this screen and hide when not.
    this.inputFieldsAdded = false;
};

magicks.ManageAccount.prototype = {

    create: function () {
        console.log("in manage account");

        window._this = this;

        // Hide the menus ad.
        magicks.menusAd.style.visibility = "hidden";

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        this.add.text(this.game.width / 2, 40, 'Manage account', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"}
        ).anchor.set(0.5);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var textStyle = {
            font: "30px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5
        };

        var middleX = this.game.width / 2;
        var infoText1 = this.add.text(middleX - 260, 250, 'Enter your account\nnumber and security\ncode on another\ndevice to use an\nexisting account.', infoTextStyle);
        var infoText2 = this.add.text(middleX + 260, 250, 'Leave blank to\ncreate a new\naccount when\nyou press Play.', infoTextStyle);
        var accountNumberText = this.add.text(middleX, 160, 'Account number', textStyle);
        var securityCodeText = this.add.text(middleX, 280, 'Security code', textStyle);
        accountNumberText.anchor.setTo(0.5);
        securityCodeText.anchor.setTo(0.5);
        infoText1.anchor.setTo(0.5);
        infoText2.anchor.setTo(0.5);

        if(this.inputFieldsAdded === false){
            this.addInputFields();
        }
        else{
            this.accountNumberInput.visible = true;
            this.securityCodeInput.visible = true;
        }

        // Button to confirm any changes to the account number or security code values.
        this.acceptButton = this.add.button(this.game.width / 2, 400, 'menus-atlas', this.acceptPressed, this);
        this.acceptButton.frameName = 'btn-accept';
        this.acceptButton.scale.setTo(3);
        this.acceptButton.anchor.setTo(0.5);
        this.acceptButton.visible = false;
        // Detect any changes to the values when a key is pressed.
        this.input.keyboard.onUpCallback = function(e) {
            if(_this.accountNumberInput.value == magicks.account.accountID
            && _this.securityCodeInput.value == magicks.account.securityCode){
                _this.acceptButton.visible = false;
            }
            else {
                _this.acceptButton.visible = true;
            }
        };

    },

    addInputFields: function () {
        //console.log("adding input fields");
        var middleX = this.game.width / 2;
        var inputStyle = {
            font: '28px Alagard',
            fill: magicks.colours.white,
            fillAlpha: 0.2,
            fontWeight: 'bold',
            width: 230,
            height: 30,
            padding: 8,
            borderWidth: 4,
            borderColor: magicks.colours.white,
            type: PhaserInput.InputType.number,
            placeHolder: 'Enter a number',
            placeHolderColor: magicks.colours.grey
        };

        this.accountNumberInput = this.game.add.inputField(middleX - inputStyle.width/2 - inputStyle.padding, 180, inputStyle);
        this.accountNumberInput.setText(magicks.account.accountID+''); // Convert to string, or throws an error.
        // Add to the stage instead of the state, or they will get destroyed (sort of) when the state changes.
        this.game.stage.addChild(this.accountNumberInput);
        inputStyle.type = undefined;
        inputStyle.max = 14;
        inputStyle.placeHolder = 'Enter a code';
        this.securityCodeInput = this.game.add.inputField(middleX - inputStyle.width/2 - inputStyle.padding, 300, inputStyle);
        this.securityCodeInput.setText(magicks.account.securityCode);
        // Add to the stage instead of the state, or they will get destroyed (sort of) when the state changes.
        this.game.stage.addChild(this.securityCodeInput);
        // Only want to add the input fields once ever, or they get messed up.
        // Just show them when on this screen and hide when not.
        this.inputFieldsAdded = true;

        this.accountNumberInput.events.onInputDown.add(function () {
            this.accountNumberInput.startFocus();
            this.securityCodeInput.endFocus();
        }, this);

        this.securityCodeInput.events.onInputDown.add(function () {
            this.securityCodeInput.startFocus();
            this.accountNumberInput.endFocus();
        }, this);

    },

    shutdown: function () {
        // Remove the keyboard key callback, or it will carry over into other states.
        this.input.keyboard.onUpCallback = null;
        // Hide the input fields.
        this.accountNumberInput.visible = false;
        this.securityCodeInput.visible = false;
        // Stop focusing on them if they were focused on.
        this.accountNumberInput.endFocus();
        this.securityCodeInput.endFocus();
    },

    backPressed: function () {
        this.state.start("LogIn");
    },

    acceptPressed: function () {
        console.log("accept pressed");
        magicks.account.accountID = _this.accountNumberInput.value;
        magicks.account.securityCode = _this.securityCodeInput.value;
        _this.accountNumberInput.endFocus();
        _this.securityCodeInput.endFocus();
        // Update the values held in local storage so the user can log in with this account on this device in the future.
        localStorage.setItem("account", JSON.stringify(magicks.account));
        this.acceptButton.visible = false;
    },

    events: (function () {

    })()
};