/**
 * Created by User on 19/01/2018.
 */

"use strict";

magicks.GuideResources = function () {
};

magicks.GuideResources.prototype = {

    create: function () {

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        this.add.text(this.game.width / 2, 40, 'Resources', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        }).anchor.set(0.5);

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var middleX = this.game.width / 2;
        var infoText1 = this.add.text(middleX, 250,
            'Wood\n' +
            'Used as a material for building and repairing structures.\n\n' +
            'Gems\n' +
            'Used as a material for building advanced structures, and\n' +
            'as a power source. Can be consumed to regenerate energy.\n\n' +
            'Food\n' +
            'Used to recruit NPC allies for your Order.' +
            '\nCan be consumed to regenerate hitpoints.',
            infoTextStyle);
        infoText1.anchor.setTo(0.5);

        var woodIcon1 = this.add.image(this.game.width / 2 - 70, 90, 'menus-atlas', 'wood-icon');
        var woodIcon2 = this.add.image(this.game.width / 2 + 70, 90, 'menus-atlas', 'wood-icon');
        woodIcon1.scale.setTo(2);
        woodIcon2.scale.setTo(2);
        woodIcon1.anchor.setTo(0.5);
        woodIcon2.anchor.setTo(0.5);

        var gemsIcon1 = this.add.image(this.game.width / 2 - 70, 195, 'menus-atlas', 'gems-icon');
        var gemsIcon2 = this.add.image(this.game.width / 2 + 70, 195, 'menus-atlas', 'gems-icon');
        gemsIcon1.scale.setTo(2);
        gemsIcon2.scale.setTo(2);
        gemsIcon1.anchor.setTo(0.5);
        gemsIcon2.anchor.setTo(0.5);

        var foodIcon1 = this.add.image(this.game.width / 2 - 70, 335, 'menus-atlas', 'food-icon');
        var foodIcon2 = this.add.image(this.game.width / 2 + 70, 335, 'menus-atlas', 'food-icon');
        foodIcon1.scale.setTo(2);
        foodIcon2.scale.setTo(2);
        foodIcon1.anchor.setTo(0.5);
        foodIcon2.anchor.setTo(0.5);

    },

    backPressed: function () {
        this.state.start("GameGuide");
    }

};