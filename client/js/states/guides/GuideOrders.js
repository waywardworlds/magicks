/**
 * Created by User on 19/01/2018.
 */

"use strict";

magicks.GuideOrders = function () {
};

magicks.GuideOrders.prototype = {

    create: function () {

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        this.add.text(this.game.width / 2, 40, 'Orders', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        }).anchor.set(0.5);

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var middleX = this.game.width / 2;
        var infoText1 = this.add.text(middleX, 250,
            'Join or create an Order to team with other players and make a base.\n' +
            'Build an Order Core to start an Order ' +
            'and claim the zone that it is in.\n\n\n' +
            'Only members of the Order that controls a zone\n' +
            'can build structures in that zone.\n\n' +
            'Gather resources and learn more spells\n' +
            'to build more useful structures.',
            infoTextStyle);
        infoText1.anchor.setTo(0.5);

        var coreIcon = this.add.image(this.game.width / 2, 200, 'game-atlas', 'order-core-1');
        coreIcon.scale.setTo(1);
        coreIcon.anchor.setTo(0.5);

    },

    backPressed: function () {
        this.state.start("GameGuide");
    }

};