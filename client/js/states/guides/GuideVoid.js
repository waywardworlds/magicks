/**
 * Created by User on 19/01/2018.
 */

"use strict";

magicks.GuideVoid = function () {
};

magicks.GuideVoid.prototype = {

    create: function () {
        console.log("in void guide");

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        this.add.text(this.game.width / 2, 40, 'Void', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        }).anchor.set(0.5);

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        var icon = this.add.image(300, 40, 'menus-atlas', 'void-icon');
        icon.scale.setTo(3);
        icon.anchor.setTo(0.5);

        icon = this.add.image(500, 40, 'menus-atlas', 'void-icon');
        icon.scale.setTo(3);
        icon.anchor.setTo(0.5);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var middleX = this.game.width / 2;
        var infoText = this.add.text(middleX, 100,
            "Used for miscellaneous spells, such as \n" +
            "teleportation, reflecting projectiles,\n" +
            "and building Order structures.\n\n" +
            "Counters:\n" +
            "All elements\n\n" +
            "Countered by:\n" +
            "All elements",
            infoTextStyle);
        infoText.anchor.setTo(0.5, 0);

    },

    backPressed: function () {
        this.state.start("GuideElements");
    }
};