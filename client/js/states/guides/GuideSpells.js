/**
 * Created by User on 19/01/2018.
 */


"use strict";

magicks.GuideSpells = function () {
};

magicks.GuideSpells.prototype = {

    create: function () {

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        this.add.text(this.game.width / 2, 40, 'Spells', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        }).anchor.set(0.5);

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var middleX = this.game.width / 2;
        var infoText1 = this.add.text(middleX - 180, 250,
            '\nCombine elements to form spells.\n\n' +
            'A spell must be learned\n' +
            'before it can be formed.\n\n' +
            'Learn new spells by buying spell\n' +
            'scrolls at a shrine for gems.\n\n' +
            'Shrines give random spells of\n' +
            'the element they represent.',
            infoTextStyle);
        var infoText2 = this.add.text(middleX + 170, 250,
            '\n\nPrimary element\n' +
            '                       This is what element\n' +
            '                       the spell belongs to.\n' +
            '                       i.e. what it counters\n' +
            '                       and is countered by.\n', infoTextStyle);
        infoText1.anchor.setTo(0.5);
        infoText2.anchor.setTo(0.5);

        var arrow1 = this.add.image(453, 182, 'menus-atlas', 'arrow-icon');
        arrow1.scale.setTo(5);
        arrow1.anchor.setTo(0.5);
        arrow1.angle = 90;

        var componentIcon = this.add.image(500 + (-1 * 50), 120, 'game-atlas', 'component-empty');
        componentIcon.scale.setTo(7);
        componentIcon.anchor.setTo(0.5);
        for(var i=0; i<7; i+=1){
            componentIcon = this.add.image(500 + (i * 44), 120, 'game-atlas', 'component-empty');
            componentIcon.scale.setTo(5);
            componentIcon.anchor.setTo(0.5);
        }

        var spacing = 130;
        var gemsIcon = this.add.image(430, 400, 'game-atlas', 'gems-icon');
        gemsIcon.scale.setTo(3);
        gemsIcon.anchor.setTo(0.5);

        var shrineIcon = this.add.image(430 + spacing, 400, 'game-atlas', 'shrine-void-1');
        //shrineIcon.scale.setTo();
        shrineIcon.anchor.setTo(0.5);

        var scrollIcon = this.add.image(430 + spacing + spacing, 400, 'game-atlas', 'scroll-icon');
        scrollIcon.scale.setTo(3);
        scrollIcon.anchor.setTo(0.5);

        var equalsText = this.add.text(shrineIcon.x, 400, '+          =', {
            font: "34px Acme",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 3,
            align: "center"
        });
        equalsText.scale.setTo(2);
        equalsText.anchor.setTo(0.5);

    },

    backPressed: function () {
        this.state.start("GameGuide");
    }

};