/**
 * Created by User on 19/01/2018.
 */

"use strict";

magicks.GuideLight = function () {
};

magicks.GuideLight.prototype = {

    create: function () {

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        this.add.text(this.game.width / 2, 40, 'Light', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        }).anchor.set(0.5);

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        var icon = this.add.image(300, 40, 'menus-atlas', 'light-icon');
        icon.scale.setTo(3);
        icon.anchor.setTo(0.5);

        icon = this.add.image(500, 40, 'menus-atlas', 'light-icon');
        icon.scale.setTo(3);
        icon.anchor.setTo(0.5);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var middleX = this.game.width / 2;
        var infoText = this.add.text(middleX, 100,
            "Used for healing and reviving.\n" +
            "Can grow living things faster, i.e. plants, animals.\n" +
            "Good at team support.\n\n" +
            "Counters:\n" +
            "Dark\n\n" +
            "Countered by:\n" +
            "Psychic",
            infoTextStyle);
        infoText.anchor.setTo(0.5, 0);

    },

    backPressed: function () {
        this.state.start("GuideElements");
    }
};