/**
 * Created by User on 19/01/2018.
 */

"use strict";

magicks.GuideElements = function () {
};

magicks.GuideElements.prototype = {

    create: function () {
        console.log("in elements guide");

        window._this = this;

        this.add.image(0, 0, 'menus-atlas', 'main-background');

        this.add.text(this.game.width / 2, 40, 'Elements', {
            font: "34px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 5,
            boundsAlignH: "center",
            boundsAlignV: "middle"
        }).anchor.set(0.5);

        var backButton = this.add.button(10, 10, 'menus-atlas', this.backPressed, this);
        backButton.frameName = 'btn-arrow';
        backButton.scale.setTo(2);

        var infoTextStyle = {
            font: "22px Alagard",
            fill: magicks.colours.white,
            stroke: magicks.colours.black,
            strokeThickness: 4,
            align: "center"
        };

        var middleX = this.game.width / 2;
        var infoText1 = this.add.text(middleX - 280, 250, 'Select an\nelement for\nmore info.', infoTextStyle);
        var infoText2 = this.add.text(middleX + 280, 250, 'Elements\ncounter each\nother going\nclockwise.', infoTextStyle);
        infoText1.anchor.setTo(0.5);
        infoText2.anchor.setTo(0.5);

        var arrow1 = this.add.image(580, 140, 'menus-atlas', 'arrow-icon');
        var arrow2 = this.add.image(580, 370, 'menus-atlas', 'arrow-icon');
        arrow1.scale.setTo(5);
        arrow2.scale.setTo(5);
        arrow1.anchor.setTo(0.5);
        arrow2.anchor.setTo(0.5);
        arrow1.rotation = 45;
        arrow2.rotation = 90;

        this.addElementsWheel();

    },

    backPressed: function () {
        this.state.start("GameGuide");
    },

    addElementsWheel: function () {
        this.elementsWheel = this.add.group();

        // Set up the element selection buttons.
        var angleDegrees = 0,
            angleRadians = 0,
            radius = 180,
            centerX = this.game.width / 2,
            centerY = this.game.height / 2 + 30,
            scale = 5;
        // When an element button is pressed, it will add that element to the end of the spell combo for the next spell.
        // If another spell is already active, it will be wiped and this element will become the first in a new spell combo.
        var btn = this.game.add.button(centerX, centerY, 'game-atlas', function () {
            console.log("void btn pressed");
            this.state.start("GuideVoid");
        }, this);
        btn.frameName = 'btn-void';
        btn.scale.setTo(scale * 0.9);
        btn.anchor.setTo(0.5);
        btn.input.pixelPerfectOver = true;
        this.elementsWheel.add(btn);

        for(var i=0; i<6; i+=1){

            angleRadians = angleDegrees * Math.PI / 180;

            var x = Math.cos(angleRadians) * (radius * scale / 6.8) + centerX;
            var y = Math.sin(angleRadians) * (radius * scale / 6.8) + centerY;

            btn = {};
            switch(i){
                case 0:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        console.log("fire btn pressed");
                        this.state.start("GuideFire");
                    }, this);
                    btn.frameName = 'btn-fire';
                    break;
                case 1:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        console.log("water btn pressed");
                        this.state.start("GuideWater");
                    }, this);
                    btn.frameName = 'btn-water';
                    break;
                case 2:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        console.log("dark btn pressed");
                        this.state.start("GuideDark");
                    }, this);
                    btn.frameName = 'btn-dark';
                    break;
                case 3:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        console.log("light btn pressed");
                        this.state.start("GuideLight");
                    }, this);
                    btn.frameName = 'btn-light';
                    break;
                case 4:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        console.log("psychic btn pressed");
                        this.state.start("GuidePsychic");
                    }, this);
                    btn.frameName = 'btn-psychic';
                    break;
                case 5:
                    btn = this.game.add.button(x, y, 'game-atlas', function () {
                        console.log("earth btn pressed");
                        this.state.start("GuideEarth");
                    }, this);
                    btn.frameName = 'btn-earth';
                    break;
            }
            btn.scale.setTo(scale * 0.9);
            btn.anchor.setTo(0.5);
            //btn.x -= 40;
            //btn.y -= 40;
            btn.input.pixelPerfectOver = true;
            this.elementsWheel.add(btn);
            angleDegrees += 60;
        }
    }
};