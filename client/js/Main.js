/**
 * Created by User on 13/04/2016.
 */

"use strict";

(function () {

    // Need to detect whether this device is a mobile, to make it use the canvas renderer. Runs a lot better than WebGL for some reason.
    // Can't use game.device.desktop, as Phaser.Device is only initialised once the renderer is already set.
    /*if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        //console.log("device is mobile");
        var game = new Phaser.Game(800, 450, Phaser.CANVAS, 'gameContainer', null, false, false);
    }
    else {
        //console.log("device is not mobile");
        var game = new Phaser.Game(800, 450, Phaser.WEBGL_MULTI, 'gameContainer', null, false, false);
    }*/

    // Can't use game.device.desktop, as Phaser.Device is only initialised once the renderer is already set.
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent);
    var useWebGL = false;

    // Set up default settings if no existing settings are defined.
    // `magicks.settings === {}` doesn't work, JS weirdness.
    if (Object.keys(magicks.settings).length === 0) {
        console.log("No local settings found, using device defaults.");

        // Desktop settings.
        if(isMobile === false){
            magicks.settings = defaultSettings.device.desktop;
        }
        // Mobile (of some sort) settings.
        else {
            magicks.settings = defaultSettings.device.mobile;
        }
    }
    else {
        console.log("Local settings found.");
    }

    var game;
    //// If settings for whether to use WebGL have been set, use them.
    //if(magicks.settings.useWebGL === true){
    //    useWebGL = true;
    //}
    //// No WebGL preference was found. Decide from device info.
    //else if (isMobile === false) {
    //    // Device is not a mobile. Try WebGL.
    //    useWebGL = true;
    //}

    //if(useWebGL === true){
        // Use WebGL renderer.
        //game = new Phaser.Game(800, 450, Phaser.WEBGL, 'gameContainer', null, false, false);
    //}
    //else {
        // Use Canvas renderer.
        game = new Phaser.Game(800, 450, Phaser.CANVAS, 'gameContainer', null, false, false);
    //}


    game.state.add('Boot',          magicks.Boot);
    game.state.add('Preload',       magicks.Preload);
    game.state.add('LogIn',         magicks.LogIn);
    game.state.add('ManageAccount', magicks.ManageAccount);
    game.state.add('Menu',          magicks.Menu);
    game.state.add('GameGuide',     magicks.GameGuide);
    game.state.add('GuideElements', magicks.GuideElements);
    game.state.add('GuideVoid',     magicks.GuideVoid);
    game.state.add('GuidePsychic',  magicks.GuidePsychic);
    game.state.add('GuideLight',    magicks.GuideLight);
    game.state.add('GuideDark',     magicks.GuideDark);
    game.state.add('GuideWater',    magicks.GuideWater);
    game.state.add('GuideFire',     magicks.GuideFire);
    game.state.add('GuideEarth',    magicks.GuideEarth);
    game.state.add('GuideSpells',   magicks.GuideSpells);
    game.state.add('GuideOrders',   magicks.GuideOrders);
    game.state.add('GuideResources',magicks.GuideResources);
    game.state.add('Settings',      magicks.Settings);
    game.state.add('AdjustUI',      magicks.AdjustUI);
    game.state.add('WorldSelect',   magicks.WorldSelect);
    game.state.add('Game',          magicks.Game);

    game.state.start('Boot')
})();