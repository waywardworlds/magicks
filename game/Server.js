/**
 * Created by User on 31/03/2017.
 */

"use strict";

// A list of the IP addresses of clients that are connected to this server, and how many connections are from that IP address.
// Used to limit the amount of socket connections from each client.
var connectionsFromClientIPAddress = {};
var maxConnectionsPerIPAddress = 20;

// The list of unused auth codes that were given to clients for accessing this server.
var incomingPlayers = [];

// Import the globals object to be able to access all the important stuff.
var globals = require('./js/Setup');

var socketPM = globals.socketPM;
//var database = globals.database;
var world = globals.world;
var ECS = globals.ECS;
// This server.
var io = globals.io;
// Gameplay is just a neat place to store gameplay related socket stuff.
var gameplay = require('./js/Gameplay');

// Set up this game server with the player manager.
socketPM.emit('game_server_setup', {name: globals.serverName, ipAddress: globals.serverIPAddress});

socketPM.on('expect_player', function (data) {
    //console.log('expect_player, data:');
    //console.log(data);

    if(!data){
        return;
    }
    if(!data.hasOwnProperty('authCode')
    || !data.hasOwnProperty('clientIPAddress')
    || !data.hasOwnProperty('accountID')
    || !data.hasOwnProperty('gameData')){
        return;
    }
    // Check that an auth code was given.
    if(typeof data.authCode !== 'string' || data.authCode == ''){
        return;
    }
    // Check that an IP address was given.
    if(typeof data.clientIPAddress !== 'string' || data.clientIPAddress == ''){
        return;
    }

    var emptySlotFound = false;
    // Find an empty slot in the incomingPlayers array.
    for(var i=0, len=incomingPlayers.length; i<len; i+=1){
        if(incomingPlayers[i] === null){
            incomingPlayers[i] = [data.authCode, data.clientIPAddress, data.accountID, data.gameData];
            emptySlotFound = true;
            break;
        }
    }
    if(emptySlotFound === false){
        incomingPlayers.push([data.authCode, data.clientIPAddress, data.accountID, data.gameData]);
    }

    // The auth code the client was sent is only valid for a few seconds before it expires.
    setTimeout(function () {
        //console.log("Checking expired, authCode: " + data.authCode + ", index: " + i);
        //console.log(i);
        //console.log(incomingPlayers[i]);
        if(incomingPlayers[i] !== null){
            //console.log("slot not null");
            // If the client joins successfully, then their place in incomingPlayers will have been emptied.
            // If the auth code is still not used when this timeout is ran, the auth code should expire to prevent the client from joining the world.
            if(incomingPlayers[i][0] === data.authCode){
                //console.log("Auth key expired");
                incomingPlayers[i] = null;
            }
        }
    }, 5000);

});

socketPM.on('server_announcement', function (data) {
    //console.log("server announcement:");
    //console.log();
    io.sockets.emit('alert_message', data);
});

socketPM.on('disconnect', function () {
    console.log("* Player manager server has disconnected. Shutting down game server.");
    // TODO: Do some basic saving of the game world state.

    // Kill the game server.
    process.exit();
});

io.on('connection', function (socket) {
    //console.log("* New client connection: " + socket.id);
    //console.log("* IP address: " + socket.request.connection.remoteAddress + ":" + socket.request.connection.remotePort);

    // Check if a connection from this IP address already exists.
    if(connectionsFromClientIPAddress[socket.request.connection.remoteAddress]){
        // Check if the amount of connections from this IP address is more than the maximum amount of connections per IP address.
        if(connectionsFromClientIPAddress[socket.request.connection.remoteAddress] > maxConnectionsPerIPAddress){
            console.log("Too many connections from the same IP address: " + socket.request.connection.remoteAddress);
            socket.disconnect();
            return
        }
        // There is space for another client connection from this IP address. Add it.
        else {
            connectionsFromClientIPAddress[socket.request.connection.remoteAddress] += 1;
        }
    }
    // No clients are already connected from this IP. Connect the first one.
    else {
        connectionsFromClientIPAddress[socket.request.connection.remoteAddress] = 1;
    }

    socket.emit("hello_game");

    socket.on('join_world', function (data) {
        joinWorld(socket, data);
    });

    // Remove a player from this world. Kicked out, user left world intentionally (exit button).
    // NOT for disconnections, page refreshes, crashes etc, see 'disconnecting' event.
    socket.on('leave_world', function () {
        //console.log("player leave_world: " + socket.id);
        removePlayer(socket);
    });

    // When the client disconnects from the server. Refresh, tab close, crash, timeout, .disconnect(), etc.
    socket.on('disconnecting', function () {
        // Check if this is NOT the only connection from this IP address.
        if(connectionsFromClientIPAddress[socket.request.connection.remoteAddress] > 1){
            // Reduce the amount of connections from this IP address.
            connectionsFromClientIPAddress[socket.request.connection.remoteAddress] -= 1;
        }
        else {
            // This is the last connection from this IP address. Remove the address from the list.
            delete connectionsFromClientIPAddress[socket.request.connection.remoteAddress];
        }
        //console.log("player disconnecting: " + socket.id);

        removePlayer(socket);
    });

    gameplay.connection(socket);

});

function joinWorld(socket, data){
    //console.log('');
    //console.log('join_world, data:');
    //console.log(data);
    if(socket.inGame === true){
        // Disconnect this socket from the game server if it is already in the game.
        socket.disconnect();
        return;
    }
    // Check that an auth code was given.
    if(typeof data.authCode !== 'string' || data.authCode === ''){
        return;
    }
    // Check if this world is full.
    if(world.playerCount >= world.maxCapacity){
        // Tell the client this world is full.
        socket.emit('world_full');
        return;
    }
    // Look through the list of incoming players to see if this player should be allowed in.
    for(var i=0, len=incomingPlayers.length; i<len; i+=1){
        // Skip empty slots in the list.
        if(incomingPlayers[i] === null){
            continue;
        }
        // Auth code matches the one sent by the player manager server.
        if(incomingPlayers[i][0] === data.authCode){
            //console.log("incomingPlayers[i][1]:      " + incomingPlayers[i][1]);
            //console.log("socket.req.conn.remoteAddr: " + socket.request.connection.remoteAddress);
            // IP address of incoming connection matches the one sent by the player manager server. Also check for loop-back address (this device) for testing.
            if(incomingPlayers[i][1] === socket.request.connection.remoteAddress || incomingPlayers[i][1] === '127.0.0.1'){
                //console.log("Looks good to me. Welcome to the world.");

                // Store the account number of this player (given by the player manager) on this socket.
                socket.accountID = incomingPlayers[i][2];
                // Store the game data of this player on this socket.
                socket.gameData = incomingPlayers[i][3];

                //if(globals.devMode === true){
                //    socket.gameData.unlockedSpells = {
                //        'llwwee': true,
                //        'dd': true, 'df': true, 'ddp': true,
                //        'wwd': true,
                //        'ff': true,
                //        'eee': true, 'eeee': true,
                //        'pp': true, 'ppd': true,
                //        'vv': true, 'vvv': true, 'vvd': true, 'vevevev': true, 'vee': true, 'veee': true, 'veev': true, 'veeev': true
                //    }
                //}

                //console.log("join world, unlockedSpells:");
                //console.log(socket.gameData.unlockedSpells);

                // Set this socket connection to be in the game, otherwise they could try and join a world again while already in one.
                socket.inGame = true;
                // Null the entry in the list of incoming players so multiple client's can't join the server using the same token.
                // Do this after giving this socket the account number.
                incomingPlayers[i] = null;
                // Add to this world's player count.
                world.playerCount += 1;

                //var randZone = '000' + globals.PhaserRips.Math.randIntInRange(1, 9);
                // Create an entity for this player.
                //socket.entity = ECS.assemblages.Player({spawnArea: randZone + "-0"/*socket.gameData.respawnArea*/, displayName: socket.gameData.displayName});
                socket.entity = ECS.assemblages.Player({spawnArea: "0008-0", displayName: socket.gameData.displayName});
                // To send the client messages from the entity, the socket object needs to be accessible from the entity.
                socket.entity.socket = socket;

                // Shorthand, bit faster.
                var entity = socket.entity;

                // Subscribe this socket to the room of the zone their entity is in so they can receive state emitter updates.
                socket.join(entity.zone.roomName);

                // Get the entities that are in this zone, ready to be sent to the client.
                var entitiesData = entity.zone.getDynamicEntitiesData();

                var mapData = entity.zone.getMapGridData();

                // Tell the client that they have successfully joined this world. Send them the zone they are in, the ID of their entity, and the data of other dynamic entities in the same zone.
                socket.emit('join_world_success', {
                    zoneName: entity.zone.name,
                    playerEntityId: entity.id,
                    maxHitPoints: entity.getHitPointQuarters(),
                    maxEnergy: entity.getEnergyQuarters(),
                    entitiesData: entitiesData,
                    mapData: mapData
                });

                // Give the player manager a confirmation that this player has successfully joined this game server.
                socketPM.emit('player_accepted', socket.accountID);

                // The other clients that are already in the zone this player just joined don't know about this player. Send them this player's data.
                io.in(entity.zone.roomName).emit('add_entity', {
                    typeNumber: entity.typeNumber,
                    id: entity.id,
                    x: entity.body.centerX,
                    y: entity.body.centerY,
                    displayName: entity.displayName
                });
            }
            else {
                console.log(`Auth code found, but IPs don't match`);
                console.log(incomingPlayers[i][1] + ' < :expected | socket: > ' + socket.request.connection.remoteAddress);
                socket.disconnect();
            }
        }
        else {
            console.log(`Auth code doesn't match`);
            socket.disconnect();
        }
    }
}

// Called when 'leave_world' or 'disconnecting' events are received.
function removePlayer(socket){
    //console.log("* * removePlayer");
    // Check if a player was logged in with this client.
    if(socket.inGame === true){
        //console.log("* * * removePlayer: " + socket.accountID);

        // Decrement this world's player count.
        world.playerCount -= 1;

        // If the entity is in an order, remove them from it.
        // This needs to be here and in Entity.deactivate, as Entity.deactivate isn't called by player entities (so they can respawn).
        if(socket.entity.order !== undefined){
            if(socket.entity.order !== null){
                socket.entity.order.removeOrderMember(socket.entity);
            }
        }

        // Kill the player's entity, so it will be as if it died and gets tidied up properly.
        // A regular .deactivate() won't remove any minions or drop any held resources.
        socket.entity.kill();

        // This entity is no longer being used. Return it to the pool for reuse.
        socket.entity._isActive = false;

        // Leave the room for the zone this player was in.
        socket.leave(socket.entity.zone.roomName);

        // Tell the player manager that this player has disconnected from this game server, so they can be removed from the
        // list of logged in players, and their saved list of unlocked spells updated.
        // This should happen whether it was by 'leave_world' or 'disconnecting'.
        //console.log("remove player, unlockedSpells:");
        //console.log(socket.gameData.unlockedSpells);
        socketPM.emit('player_disconnected', {accountID: socket.accountID, unlockedSpells: socket.gameData.unlockedSpells});

        // This socket is not in the game any more.
        // Reset the socket properties.
        socket.accountID = undefined;
        socket.inGame = false;
        socket.entity = {};
        socket.gameData = {};

        // Disconnect this client from the server.
        socket.disconnect();
    }
}