/**
 * Created by User on 15/01/2018.
 */

/**
 * The login details for this game server to join the player manager.
 * Should be different for each game server instance.
 * @type {string}
 */
var serverName = 'Singapore-1';

module.exports = serverName;