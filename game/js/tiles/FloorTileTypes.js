/**
 * Created by User on 31/08/2017.
 */

"use strict";

/**
 * Available parameters. If something would be the same as the default value, then just omit it.
 *
 *      Parameter                       |   Default value   |   Description
 *  ------------------------------------+-------------------+--------------------------------
 *      code: (String)                  |   *Required*      |   The letter code of this terrain type. Sent to the client so they know what type the tile is.
 *      speedModifier: (Number)         |   1               |   How much this terrain affects an entity's move speed.
 *      canBeBuiltOn: (Boolean)         |   true            |   Can other entities be placed on this terrain type, such as obstacles and structures.
 *      durability: (Number)            |   undefined       |   How many times this tile can be stood on before it progresses into the next terrain type.
 *      duration: (Number)              |   10000           |   How long in milliseconds after being stood on last before this reverts back to the previous terrain type.
 *      progressesTo: (FloorTileType)   |   undefined       |   The terrain type this tile will turn into when stood on enough.
 *      revertsTo: (FloorTileType)      |   undefined       |   The terrain type this tile will turn into when it hasn't been stood on in a while.
 *      attunedElement: (String)        |   undefined       |   The element that this terrain type roughly matches. Used for things like attunement bonuses when stood on matching terrain.
 *      damage: (Number)                |   0               |   How much damage is dealt to an entity that stands on this tile.
 *
 */

var FloorTileType = function (config) {
    //console.log("adding new floor tile type, config:");
    //console.log(config);
    this.code =             config.code             || console.log("* WARNING: A floor tile type is being defined without a code.");
    this.speedModifier =    config.speedModifier    || 1;
    if(config.canBeBuiltOn === false){this.canBeBuiltOn = false} else {this.canBeBuiltOn = true}
    this.durability =       config.durability       || undefined;
    this.duration =         config.duration         || 10000;
    this.progressesTo =     config.progressesTo     || undefined;
    this.revertsTo =        config.revertsTo        || undefined;
    this.attunedElement =   config.attunedElement   || undefined;
    this.damage =           config.damage           || undefined;
};

var ECS = require('../ECS/ECS');

var floorTileTypes = {
    Grass: new FloorTileType(
        {
            code: 'G',
            durability: 30,
            progressesTo: "Dirt"
        }
    ),

    Dirt: new FloorTileType(
        {
            code: 'I',
            speedModifier: 1.2,
            attunedElement: ECS.Elements.EARTH,
            durability: 40,
            duration: 60000*2,
            progressesTo: "Path",
            revertsTo: "Grass"
        }
    ),

    Path: new FloorTileType(
        {
            code: 'P',
            speedModifier: 1.4,
            duration: 60000*5,
            revertsTo: "Dirt"
        }
    ),

    Mud: new FloorTileType(
        {
            code: 'M',
            speedModifier: 0.6,
            canBeBuiltOn: false,
            duration: 60000,
            revertsTo: "Dirt"
        }
    ),

    ShallowWater: new FloorTileType(
        {
            code: 'S',
            speedModifier: 0.6,
            attunedElement: ECS.Elements.WATER,
            canBeBuiltOn: false
        }
    ),

    DeepWater: new FloorTileType(
        {
            code: 'D',
            speedModifier: 0.4,
            attunedElement: ECS.Elements.WATER,
            canBeBuiltOn: false
        }
    ),

    Lava: new FloorTileType(
        {
            code: 'L',
            speedModifier: 0.6,
            attunedElement: ECS.Elements.FIRE,
            canBeBuiltOn: false,
            duration: 120000,
            revertsTo: "Path",
            damage: 10
        }
    ),

    Poison: new FloorTileType(
        {
            code: 'P',
            speedModifier: 0.8,
            attunedElement: ECS.Elements.DARK,
            canBeBuiltOn: false,
            damage: 5
        }
    ),

    Pit: new FloorTileType(
        {
            code: 'T',
            canBeBuiltOn: false,
            damage: 10000
        }
    ),

    Sand: new FloorTileType(
        {
            code: 'A',
            speedModifier: 0.8,
            canBeBuiltOn: false
        }
    ),

    Snow: new FloorTileType(
        {
            code: 'W',
            speedModifier: 0.9,
            canBeBuiltOn: true
        }
    )

};

//console.log("before updating, fTTs:");
//console.log(floorTileTypes);

// Can't set the properties of the floorTileTypes object to reference other
// properties before they are initialised, so do it here based on the strings.
Object.keys(floorTileTypes).forEach(function(key) {
    var type = floorTileTypes[key];
    //console.log("fTT key: " + key);
    //console.log(floorTileTypes[key]);
    if(type.progressesTo !== undefined){
        floorTileTypes[key].progressesTo = floorTileTypes[type.progressesTo];
    }
    if(type.revertsTo !== undefined){
        floorTileTypes[key].revertsTo = floorTileTypes[type.revertsTo];
    }
});

//console.log("all done, fTTs:");
//console.log(floorTileTypes);

module.exports = floorTileTypes;