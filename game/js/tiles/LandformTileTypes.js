/**
 * Created by User on 30/12/2017.
 */


var landformTileTypes = {
    Mountain: 'M',
    Cliff: 'C',
    ShoreRock: 'S'
};

module.exports = landformTileTypes;