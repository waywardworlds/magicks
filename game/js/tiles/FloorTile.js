/**
 * Created by User on 31/08/2017.
 */

"use strict";

class FloorTile {
    constructor (type) {

        this.type = type;

        // How many times has this tile been stood on.
        //TODO: Maybe use this to generate a heatmap of player movement.
        this.occupiedCount = 0;

        // The last time an entity stood on this tile.
        this.lastOccupiedTime = Date.now();

    }

}

module.exports = FloorTile;