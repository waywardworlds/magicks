/**
 * Created by User on 08/04/2017.
 */

// Stuff ripped from Phaser.
PhaserRips = {};
PhaserRips.Math = {};
PhaserRips.Physics = {};

var degreeToRadiansFactor = Math.PI / 180;

/**
 * Convert degrees to radians.
 *
 * @method Phaser.Math#degToRad
 * @param {number} degrees - Angle in degrees.
 * @return {number} Angle in radians.
 */
PhaserRips.Math.degToRad = function degToRad (degrees) {
    return degrees * degreeToRadiansFactor;
};

PhaserRips.Math.randIntInRange = function (min, max) {
    return Math.round(Math.random() * (max - min) + min);
};

PhaserRips.Math.randFloatInRange = function (min, max) {
    return Math.random() * (max - min) + min;
};

/* MOVED TO ENTITY
PhaserRips.repositionToXYFromDistance = function (body, startX, startY, targetX, targetY, distanceFromStart) {

    var angle = Math.atan2(obj2.y - obj1.y, obj2.x - obj1.x);
    projectile.body.x = Math.cos(angle) * distance;
    projectile.body.y = Math.sin(angle) * distance;
};*/

/**
 * Move the given display object towards the x/y coordinates at a steady velocity.
 * If you specify a maxTime then it will adjust the speed (over-writing what you set) so it arrives at the destination in that number of seconds.
 * Timings are approximate due to the way browser timers work. Allow for a variance of +- 50ms.
 * Note: The display object does not continuously track the target. If the target changes location during transit the display object will not modify its course.
 * Note: The display object doesn't stop moving once it reaches the destination coordinates.
 * Note: Doesn't take into account acceleration, maxVelocity or drag (if you've set drag or acceleration too high this object may not move at all)
 *
 * @method Phaser.Physics.Arcade#moveToXY
 * @param {any} body - The display object to move.
 * @param {number} x - The x coordinate to move towards.
 * @param {number} y - The y coordinate to move towards.
 * @param {number} [speed=60] - The speed it will move, in pixels per second (default is 60 pixels/sec)
 * @return {number} The angle (in radians) that the object should be visually set to in order to match its new velocity.
 */
PhaserRips.Physics.moveToXY = function (body, x, y, speed) {
    var angle = Math.atan2(y - body.centerY, x - body.centerX);

    body.velocity.x = Math.cos(angle) * speed;
    body.velocity.y = Math.sin(angle) * speed;

    return angle;
};

/**
 * Given the angle (in degrees) and speed calculate the velocity and return it as a Point object, or set it to the given point object.
 *
 * @method Phaser.Physics.Arcade#velocityFromAngle
 * @param {number} angle - The angle in degrees calculated in clockwise positive direction (down = 90 degrees positive, right = 0 degrees positive, up = 90 degrees negative)
 * @param {number} speed - The speed it will move.
 * @return {Object} - An object where x contains the velocity x value and y contains the velocity y value.
 */
PhaserRips.Physics.velocityFromAngle = function (angle, speed) {
    // The calculation at the bottom doesn't return 0 for one of the axis when given an angle that is straight up, down, left, or right, so catch those here.
    if(angle === 0){
        return {x: speed, y: 0};
    }
    else if(angle === 90){
        return {x: 0, y: speed};
    }
    else if(angle === 180){
        return {x: -speed, y: 0};
    }
    else if(angle === 270){
        return {x: 0, y: -speed};
    }
    else{
        return {x: Math.cos(PhaserRips.Math.degToRad(angle)) * speed, y: Math.sin(PhaserRips.Math.degToRad(angle)) * speed};
    }

};

/**
 * Returns the angle from the body to the target x and y.
 *
 * @method Phaser.Physics.Arcade#angleFromBody
 * @param {number} body - The Crash physics body to use at the start position.
 * @param {number} x - The x position of the target location.
 * @param {number} y - The x position of the target location.
 * @return {number} - An angle (in degrees) from the body to the target x/y.
 */
PhaserRips.Physics.angleFromBody = function (body, x, y) {
    return Math.atan2(y - body.pos.y, x - body.pos.x) * 180 / Math.PI;
};

/**
 * Returns the angle from the body to the target x and y.
 *
 * @method Phaser.Physics.Arcade#angleInRadiansFromBody
 * @param {number} body - The Crash physics body to use at the start position.
 * @param {number} x - The x position of the target location.
 * @param {number} y - The x position of the target location.
 * @return {number} - An angle (in degrees) from the body to the target x/y.
 */
PhaserRips.Physics.angleInRadiansFromBody = function (body, x, y) {
    return Math.atan2(y - body.pos.y, x - body.pos.x);
};


module.exports = PhaserRips;