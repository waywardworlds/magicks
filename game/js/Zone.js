/**
 * Created by User on 01/05/2016.
 */

"use strict";

var ECS = require('./ECS/ECS');
var Quadtree = require('quadtree-lib');
//var Quadtree = require('../node_modules/quadtree-lib/build/js/quadtree.js');

var FloorTile = require('./tiles/FloorTile');
var floorTileTypes = require('./tiles/FloorTileTypes');
var landformTileTypes = require('./tiles/LandformTileTypes');

var floorsTileset = require('./../map/tilesets/floor-tileset.json');
var landformsTileset = require('./../map/tilesets/landforms-tileset.json');
//var decorationsTileset = require('./../map/tilesets/decorations-tileset.json');
var entitiesSmallTileset = require('./../map/tilesets/entities-small-tileset.json');
var entitiesLargeTileset = require('./../map/tilesets/entities-large-tileset.json');

class Zone {
    /**
     * @param {Number} index - The index of this zone in the world.zonesArray list.
     * @param {String} name - The name of the zone, such as '3042'.
     * @param {Object} io - A reference to the game server socket.io object.
     * @param {String} mapDataFileName - The file name of the map data to use for this zone. i.e. 'zone-4567-derpy-town'.
     */
    constructor (index, name, io, mapDataFileName) {
        console.log("Creating zone " + mapDataFileName);

        // The index of this zone in the world.zonesArray list.
        this.index = index;

        // The name of the zone. i.e. '0099'. Used to access this zone using the world.zonesObject list.
        this.name = name;

        this.io = io;

        // All players in this zone will be put in a socket.io room with the name of this zone.
        this.roomName = "zone-" + name;

        this.gridSize = 32;
        this.gridRows = 0;
        this.gridCols = 0;

        // Zone entrances.
        // An entrance is just a rectangular area that the player can spawn inside of, that has a
        // left, top, right and bottom bound.
        // It is an object of arrays, where each array is a set of Entrance entities with the
        // same 'name' (entrance number) in this zone, and each array is indexed by that entrance name.
        /*
         entrances {
           '1': [Entrance, Entrance, Entrance, ...],
           ...
           '4': [Entrance, Entrance, ...]
         }
        */
        this.entrances = {};

        // All the entities in this zone. Both dynamic and static. Dynamic entities have an entityIndex.
        this.entities = [];

        // The grid of what type of terrain tile is at each map tile. Used by some entities to determine the speed modifier, or whether other entities can be placed here i.e. can't build on water.
        this.floorGrid = [];

        // The grid of static entities in this zone that the player should be able to see. Created when the server starts and should never be changed, i.e. nothing added or removed.
        // Just for stuff like zone boundaries like mountains.
        this.landformsGrid = [];
        this.decorationsGrid = [];

        //this.structuresGrid = []; <-- useless, not using.

        // Counter for how many resources are currently in this zone. Don't want the resource generator to get carried away and cover the whole zone...
        // Should be incremented/decremented as resources are added/removed.
        this.resourceNodesCount = 0;
        this.maxResourceNodes = 70;

        // The shrine for this zone for players to buy spell scrolls.
        this.shrine = null;

        //this.orderStructuresCount = 0;
        //this.maxOrderStructures = 100;
        this.order = {
            // References to all of the entities (mostly players) that are in this order.
            members: {},
            // How many members in this order.
            memberCount: 0,
            // How many members can this order have. Can be increased with beds.
            maxMembers: 10,
            // How many structures exist in this zone.
            structuresCount: 0,
            // How many structures can exist in this zone.
            maxStructures: 100,
            core: null,
            stockpile: null,
            library: null,
            workbench: null,
            spellTable: null,
            generator: null,
            teleporter: null,
            respawner: null
        };

        this.mobCount = 0;
        this.maxMobs = 16;

        this.mapData = require('./../map/' + mapDataFileName + '.json');

        //console.log("map data:");
        //console.log(this.mapData);
        //console.log("landforms tileset:");
        //console.log(landformsTileset);

        // Create a quadtree for the entities in this zone.
        this.quadTree = new Quadtree({
            width: this.mapData.width * this.gridSize,
            height: this.mapData.height * this.gridSize,
            maxElements: 2
        });


        // Ignore the test entities zone. Don't want it to start spawning resources or changing the environment.
        if(this.name !== '9999'){
            setTimeout(this.updateEnvironment.bind(this), 12000);
            setTimeout(this.generateResource.bind(this), 5000);
        }

    }

    /**
     * The main update loop for this zone. Runs 20/s (every 50ms).
     */
    update () {
        //console.log("      inside zone update");
        var entity = {};
        // Update the entities in this zone. Only active entities should be in this list, so no `isActive === true` check is needed.
        for(var i=0, len=this.entities.length; i<len; i+=1){
            entity = this.entities[i];
            // Skip this array element if the value is null. No entity there.
            if(entity === null){
                //console.log("entity at " + i + " is null");
                continue
            }
            // Check that this entity has an update method.
            if(entity.update !== undefined){
                entity.update();
            }
        }
        //for(i=0, len=this.spawners.length; i<len; i+=1){
        //    this.spawners[i].update();
        //}
    }

    loadMapData () {

        var mapData = this.mapData;
        var zoneName = this.roomName;
        var tileSize = mapData.tileheight;

        // Load the map objects.
        // Check that there is some map data for the map of this zone.
        if(mapData === undefined){
            console.error("* WARNING: No map data found for this zone when creating zone " + name);
            return;
        }

        if(mapData.properties){
            if(mapData.properties['combat-enabled'] !== undefined){
                this.combatEnabled = mapData.properties['combat-enabled'];
            }
        }

        //console.log("map data:");
        //console.log(this.mapData);
        var i = 0,
            len = 0,
            layer,
            findLayer = function (layerName) {
                for(var i=0; i<mapData.layers.length; i+=1){
                    if(mapData.layers[i].name === layerName){
                        return mapData.layers[i];
                    }
                }
                console.log("* WARNING: Couldn't find tilemap layer '" + layerName + "' for zone " + zoneName + '.');
                return false;
            };

        var tilesData;
        var entitiesData;
        // An tile layer tile on the tilemap data.
        var mapTile;
        // An object layer tile on the tilemap data.
        var mapObject;
        var tileID;
        var entity;
        var type;
        var row;
        var col;

        // Check that the floor layer exists in the map data.
        layer = findLayer('Floor');
        if(layer){
            // Get the floor layer data.
            tilesData = layer.data;
            row = 0;
            col = 0;

            // Initialise an empty grid, with an element for each column.
            for(i=0; i<mapData.width; i+=1){
                this.floorGrid[i] = [];
            }

            //console.log("tilesData:");
            //console.log(tilesData);
            // Add the tiles to the grid.
            for(i=0, len=tilesData.length; i<len; i+=1){
                mapTile = tilesData[i] - 1;
                //console.log("mapTile:");
                //console.log(mapTile);
                // Get and separate the type from the prefix using the tile GID, i.e. 'T-Grass' into 'Grass'.
                type = floorsTileset.tiles[mapTile].type.substring(2);
                // Move to the next row when at the width of the map.
                if(col === mapData.width){
                    col = 0;
                    row += 1;
                }
                // Check that the type of this tile is a valid one.
                if(floorTileTypes[type]){
                    //console.log("valid floor tile type: " + type);
                    //console.log(floorTileTypes[type]);
                    // Create a new FloorTile object of the type of this tile, and add it to the floor grid.
                    this.floorGrid[row][col] = new FloorTile(floorTileTypes[type]);
                    //console.log(this.floorGrid[row][col]);

                }
                else {
                    console.log("* WARNING: Invalid floor mapTile type: " + type);
                }
                // Move to the next column.
                col += 1;
            }
        }

        // Check that the landforms layer exists in the map data.
        layer = findLayer('Landforms');
        if(layer){
            // Get the floor layer data.
            tilesData = layer.data;
            row = 0;
            col = 0;

            // Initialise an empty grid, with an element for each row.
            for(i=0; i<mapData.height; i+=1) {
                this.landformsGrid[i] = [];
                // Add an element for every column.
                for(var j=0; j<mapData.width; j+=1){
                    // Initialise the grid to be ''s.
                    this.landformsGrid[i][j] = '';
                }
            }

            //console.log("tilesData:");
            //console.log(tilesData);
            // Add the tiles to the grid.
            for(i=0, len=tilesData.length; i<len; i+=1){
                mapTile = tilesData[i] - mapData.tilesets[4].firstgid;
                // Skip empty map tiles. This isn't like the floor layer where every cell
                // should have a tile, as large parts of the landforms layer are empty.
                if(col === mapData.width){
                    col = 0;
                    row += 1;
                }
                if(mapTile > 0){
                    //console.log("mapTile:");
                    //console.log(mapTile);
                    // Get and separate the type from the prefix using the tile GID, i.e. 'E-Solid' into 'Solid'.
                    type = landformsTileset.tiles[mapTile].type.substring(2);
                    // Move to the next row when at the width of the map.
                    //console.log(type);

                    // Check that the type of this tile is a valid one.
                    if(landformTileTypes[type]){
                        //console.log("valid landform tile type: " + type + ", row: " + row + ", col: " + col);
                        //console.log(floorTileTypes[type]);
                        // Add the landform type of this tile to the landforms grid.
                        this.landformsGrid[row][col] = landformTileTypes[type];

                        ECS.assemblages.Solid(this, col * this.gridSize, row * this.gridSize);
                        //console.log(this.floorGrid[row][col]);
                    }
                    else {
                        console.log("* WARNING: Invalid landforms mapTile type: " + type);
                    }

                }
                // Move to the next column.
                col += 1;
            }

        }

        //console.log("floor grid size:");
        //console.log(memorySizeOf(this.floorGrid));
        // Check that the layer exists in the map data.
        layer = findLayer('Decorations');
        if(layer){
            // Get the decoration layer data.
            tilesData = layer.data;
            row = 0;
            col = 0;

            // Initialise an empty grid, with an element for each column.
            for(i=0; i<mapData.width; i+=1){
                this.decorationsGrid[i] = [];
            }

            // Add the objects to the game world.
            for(i=0, len=tilesData.length; i<len; i+=1){
                mapTile = tilesData[i];
                // Move to the next row when at the width of the map.
                if(col === mapData.width){
                    col = 0;
                    row += 1;
                }
                // Add the GID of this decoration tile to the decorations grid.
                this.decorationsGrid[row][col] = mapTile;
                // Move to the next column.
                col += 1;
            }
        }

        //console.log("decorations grid size:");
        //console.log(memorySizeOf(this.decorationsGrid));
        // Check that the layer exists in the map data.
        layer = findLayer('Entities');
        if(layer){
            //console.log("entities layer found");
            // Get the entities layer data.
            entitiesData = layer.objects;

            // Initialise an empty grid, with an element for each row.
            /*for(i=0; i<mapData.height; i+=1) {
                this.structuresGrid[i] = [];
                // Add an element for every column.
                for(var j=0; j<mapData.width; j+=1){
                    // Initialise the grid to be 0s.
                    this.structuresGrid[i][j] = 0;
                }
            }*/

            //console.log(this.structuresGrid.length);
            //console.log(this.structuresGrid[0].length);

            for(i=0, len=entitiesData.length; i<len; i+=1){
                mapObject = entitiesData[i];
                //console.log("entity mapTile: ");
                //console.log(mapObject);

                col = mapObject.x / tileSize;
                row = mapObject.y / tileSize;

                // If this entity is a text object, skip it.
                if(mapObject.text !== undefined){
                    continue;
                }

                // The entity tile instance. Need to keep a reference to this to pass to the entity itself so it knows which grid spaces it takes up, so they can be emptied when it is removed.
                /*var entityTile = null;
                // Add the entity GIDs to the entities grid. row-1 as Tiled has object origins in the bottom left, instead of top left.
                // Check if this entity is a large (32x32) one.
                if(mapObject.width === (tileSize*2) && mapObject.height === (tileSize*2)){
                    entityTile = new LargeEntityTile(row-1, col, mapObject.gid);
                    // Set the grid spaces around this entity to reference the same large entity tile object.
                    this.entitiesGrid[row-1][col] =     entityTile;
                    //this.entitiesGrid[row-1][col+1] =   entityTile;
                    //this.entitiesGrid[row-2][col] =     entityTile;
                    //this.entitiesGrid[row-2][col+1] =   entityTile;
                }
                // Must be small (16x16).
                else {
                    entityTile = new SmallEntityTile(row-1, col, mapObject.gid);
                    this.entitiesGrid[row-1][col] = entityTile;
                }*/

                // The grids are set up. Now add some actual entities to the game world.

                // Reset this in case an invalid type was found below, otherwise this would still be the previous valid type.
                type = null;

                // Get the last GID on the tileset for the small (16x16) entities.
                var lastSmallGID = mapData.tilesets[2].firstgid + entitiesSmallTileset.tilecount - 1;

                //console.log("before gid check");

                // Find which tileset this entity tile is in.
                // Check if it is a tile on the small (16x16) tileset.
                if(mapObject.gid < lastSmallGID){
                    //console.log("entity gid is within small range");
                    // Convert the GID to the relative tile ID on the tileset.
                    tileID = mapObject.gid - mapData.tilesets[2].firstgid;
                    //console.log("tileID: " + tileID);
                    // Check this tile ID matches a defined tile type in the small (16x16) tileset.
                    if(entitiesSmallTileset.tiles[tileID]){
                        //console.log("tile ID is valid");
                        // Get and separate the type from the prefix using the tile ID, i.e. 'E-Solid' into 'Solid'.
                        type = entitiesSmallTileset.tiles[tileID].type.substring(2);
                    }
                }
                else {
                    // Convert the GID to the relative tile ID on the tileset.
                    tileID = mapObject.gid - mapData.tilesets[3].firstgid;
                    // Check this tile ID matches a defined tile type in the large (32x32) tileset.
                    if(entitiesLargeTileset.tiles[tileID]){
                        // Get and separate the type from the prefix using the tile ID, i.e. 'E-BigTree' into 'BigTree'.
                        type = entitiesLargeTileset.tiles[tileID].type.substring(2);
                    }
                }

                // POSSIBLY A BUG IF DECORATIONS GID START AFTER THE END OF LARGE ENTITIES TILESET.

                // TODO: Sapling => small tree => big tree

                // Check that the type of this tile is a valid one.
                if(ECS.assemblages[type]){
                    // Create a new entity object of the type of this entity, and add it to the game world.
                    // Need to do `entity.y - entity.height` for the Y, as Tiled has object origins in the bottom left, instead of top left.
                    // * 2 as the grid size in Tiled is 16x16, but in the game it is 32x32.
                    switch (type){
                        case 'Solid':
                            entity = ECS.assemblages.Solid(this, mapObject.x*2, mapObject.y*2 - mapObject.height*2);
                            break;
                        case 'Entrance':
                            entity = ECS.assemblages.Entrance(this, mapObject.x*2, mapObject.y*2 - mapObject.height*2, mapObject.width*2, mapObject.height*2, mapObject.properties['Entrance']);
                            break;
                        case 'Exit':
                            entity = ECS.assemblages.Exit(this, mapObject.x*2, mapObject.y*2 - mapObject.height*2, mapObject.width*2, mapObject.height*2, mapObject.properties['TargetEntrance'], mapObject.properties['TargetZone']);
                            break;/*
                        case 'WoodWall':
                            entity = ECS.assemblages.WoodWall({zone: this, x: mapObject.x*2, y: mapObject.y*2 - mapObject.height*2});
                            break;
                        case 'StoneWall':
                            entity = ECS.assemblages.StoneWall({zone: this, x: mapObject.x*2, y: mapObject.y*2 - mapObject.height*2});
                            break;
                        case 'BigTree':
                            entity = ECS.assemblages.BigTree({zone: this, x: mapObject.x*2, y: mapObject.y*2 - mapObject.height*2});
                            break;*/
                        default:
                            console.log("* WARNING: Entity not created for type: " + type);
                    }
                }
                else {
                    console.log("* WARNING: Invalid entity type given: " + type);
                }
            }
        }

        this.gridRows = this.floorGrid.length;
        this.gridCols = this.floorGrid[0].length;

        //console.log("zone rows: " + this.gridRows + ", cols: " + this.gridCols);

        // No longer need the map data for this zone.
        delete this.mapData;
    }

    /**
     * Move an entity to this zone. Should always be triggered by an Exit.
     * @param {ECS.Entity} entity - The entity to move to this zone.
     * @param {String} [entranceName] - The name of the entrance to reposition the entity inside of. If not given, the entity won't be repositioned.
     */
    moveEntityToZone (entity, entranceName) {
        //console.log("moving entity: " + entity.entityType + " to zone: " + this.roomName + " at entrance: " + entranceName);

        // If this entity has a socket, it is a player's entity.
        if(entity.socket){
            // Tell this player to create the map for this zone.
            entity.socket.emit('change_zone', this.getMapGridData());
            // Unsubscribe this player from the room of the previous zone.
            entity.socket.leave(entity.zone.roomName);
            //console.log("entity removed from zone room");
        }

        //console.log("mETZ, entity.id: " + entity.id);

        // Tell everyone in the room of the previous zone to remove this entity, BEFORE updating the zone of the entity.
        this.io.in(entity.zone.roomName).emit('remove_entity', {id: entity.id});

        // If this entity has a value for entityIndex, then they were previously in a zone.
        if(entity.entityIndex !== null){
            // Null the reference to the entity object in the previous zone before moving to this zone.
            entity.zone.entities[entity.entityIndex] = null;

            // Remove the entity from the quadtree of the previous zone.
            entity.zone.quadTree.remove(entity);
        }
        // Add the entity to the entities list for this zone.
        this.addToEntities(entity, true);

        // Update the zone this entity is in.
        entity.setZone(this);

        var entrance;
        // Check if an entrance name to move into was given. If not, the entity won't be repositioned.
        if(entranceName !== undefined){
            // Check if a random entrance in the zone should be used.
            if(entranceName === '?'){
                // Get a random entrance name.
                entrance = this.getEntrance(entranceName);
            }
            else {
                entrance = this.getEntrance(entranceName);
            }
            // Move the entity to somewhere within the bounds of the target entrance.
            //console.log("moveEntityToZone entrance:");
            //console.log(entrance);
            var detectionRadius = entity.body.detectionRadius;
            entity.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right  - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + detectionRadius, entrance.bottom - detectionRadius)
            );
        }

        // Check if this entity belongs to a player again.
        if(entity.socket){
            // Subscribe this user to the room for this zone, as now the zone is updated. Can then receive state emitter updates.
            entity.socket.join(entity.zone.roomName);
            //console.log("entity added back to zone room");

            // Send the dynamic entities to the client.
            entity.socket.emit("response_zone_entities", this.getDynamicEntitiesData());
        }

        // Prepare the relevant data of this entity to send to everyone in this zone. Not everything on the entity is needed by the client.
        var details = {};
        details.typeNumber = entity.typeNumber;
        details.id = entity.id;
        // Does the entity have a body. I think all entities atm have bodies, so this might be redundant... TODO: remove this
        //if(entity.body){
            details.x = entity.body.centerX;
            details.y = entity.body.centerY;
            // Does the entity have a display name.
            if(entity.displayName){
                details.displayName = entity.displayName;
            }
            // Does the entity have a rotation. i.e. is the entity a spell.
            else if(entity.body.rotation){
                details.rotation = entity.body.rotation;
            }
        //}
        // Send details of this entity to all the players in this zone.
        this.io.in(this.roomName).emit('add_entity', details);
    }

    /**
     * Add an entity to the entities list for this zone.
     * @param {ECS.Entity} entity - The entity to add to this zone.
     * @param {Boolean} observe=false - Should this entity be observed by the quadtree for this zone for changes. i.e. does it move.
     */
    addToEntities (entity, observe) {
        //console.log("adding to entities: " + entity.entityType);

        // Add this entity to the quadtree of this zone.
        this.quadTree.push(entity, observe || false);

        // Find an empty array element to put this entity in.
        for(var i=0, len=this.entities.length; i<len; i+=1){
            // Empty element. Put the entity in it.
            if(this.entities[i] === null){
                this.entities[i] = entity;
                // If this entity is dynamic (has an entityIndex), then set the entityIndex to the index of the element it was added at.
                if(entity.entityIndex !== undefined){
                    // Store the index the entity was added at in entityIndex.
                    entity.entityIndex = i;
                }
                // This entity has been added to the entities list, so stop looping.
                return;
            }
        }
        // No empty element was found in zone.entities. Add this entity to the end of the array.
        // If this entity is dynamic (has an entityIndex), then set the entityIndex to the index of the element it was added at.
        if(entity.entityIndex !== undefined){
            // Return the index the entity was added at.
            // Array.push returns length, so -1 for index.
            entity.entityIndex = this.entities.push(entity)-1;
        }
        // The entity doesn't have an entity index, so it should just be added to zone.entities and nothing else. This entity can't be removed from the zone.
        else {
            this.entities.push(entity);
        }
    }

    removeFromEntities (entity) {
        //console.log("removing from zone entities");
        entity.setVelocity();
        // This entity is no longer being used. Return it to the pool for reuse.
        entity._isActive = false;
        // Remove the link to the entity object from the list of entities in this zone.
        this.entities[entity.entityIndex] = null;
        entity.entityIndex = null;
        // Remove it from the quadtree of the zone it is in so it doesn't interact with anything while dead.
        this.quadTree.remove(entity);
    }

    /**
     * Add an entity to the grid of structures.
     * @param {Number} x
     * @param {Number} y
     * @param {ECS.Entity} entity
     */
    /*addToStructures (x, y, entity) {
        // Round down to avoid decimal indexes.
        var row = Math.floor(y / this.gridSize);
        var col = Math.floor(x / this.gridSize);

        this.structuresGrid[row][col] = entity;
    }*/

    getDynamicEntitiesData () {
        // Prepare the data of all the entities in this zone that are dynamic, to send to the player that requests them.
        var data = [];
        var ent = {};
        var entities = this.entities;
        var details = {};
        for(var i=0, len=entities.length; i<len; i+=1){
            // Check that this entity is not null. Might be an empty array element.
            if(entities[i] !== null){
                ent = entities[i];
                // Check that this entity is dynamic and not just a static feature, like a collision rectangle.
                if(ent.entityIndex){
                    // Skip this entity if it is in the entities grid. It will get added/removed when the grid changes. <-- NOT USING THE ENTITIES GRID ANYMORE I THINK, THAT WAS FOR THE OLD TILEMAP TECHNIQUE. TODO: tidy
                    //if(ent.isInEntitiesGrid === true){
                    //    continue;
                    //}
                    // Empty the object.
                    details = {};
                    // Add the relevant details of this entity to be sent to the client.
                    details.typeNumber = ent.typeNumber;
                    details.id = ent.id;
                    details.x = ent.body.centerX;
                    details.y = ent.body.centerY;
                    // Check if this entity has a display name. Might be a character.
                    if(ent.displayName){
                        details.displayName = ent.displayName;
                    }
                    else if(ent.body.rotation){
                        details.rotation = ent.body.rotation;
                    }
                    //if(ent.wallShape){
                    //    details.wallShape = ent.wallShape;
                    //}
                    if(ent.operating){
                        details.operating = ent.operating;
                    }
                    if(ent.element){
                        details.element = ent.element;
                    }
                    if(ent.invisible === true){
                        details.invisible = true;
                    }
                    data.push(details);
                }
            }
        }
        return data;
    }

    getMapGridData () {
        var mapData = {},
            row,
            col,
            rowCount,
            colCount;

        mapData.floorGrid = [];
        var floorGrid = this.floorGrid;
        for(row=0, rowCount=floorGrid.length; row<rowCount; row+=1){
            mapData.floorGrid[row] = [];
            for(col=0, colCount=floorGrid[row].length; col<colCount; col+=1){
                mapData.floorGrid[row][col] = floorGrid[row][col].type.code;
            }
        }

        mapData.landformsGrid = this.landformsGrid;
        //var landformsGrid = entity.zone.landformsGrid;
        //for(row=0, rowCount=landformsGrid.length; row<rowCount; row+=1){
        //    mapData[1][row] = [];
        //    for(col=0, colCount=landformsGrid[row].length; col<colCount; col+=1){
        //        // Exclude the GIDs of some entities from the data to send to the client.
        //        // Clients don't need to see stuff like entrances/exits.
        //        var entityGID = landformsGrid[row][col];
        //        if(entityGID === 4098 // Entrance
        //        || entityGID === 4099 // Exit
        //        ){
        //            mapData[1][row][col] = 0;
        //            continue;
        //        }
        //        // Add the GID of this entity to the data to send.
        //        mapData[1][row][col] = landformsGrid[row][col];
        //    }
        //}

        //mapData[2] = entity.zone.decorationsGrid;
        //console.log(mapData);

        return mapData;
    }

    /**
     * Returns a random entrance entity in this zone of the given name.
     * @param entranceName
     * @returns {ECS.Entrance} An Entrance entity.
     */
    getEntrance (entranceName) {
        //console.log("getEntrance, entranceName:");
        //console.log(entranceName);
        //console.log("this entrances:");
        //console.log(this.entrances);
        //console.log("this zone:" + this.name);
        var entrances = this.entrances[entranceName];
        return entrances[Math.floor(Math.random() * entrances.length)];
    }

    updateEnvironment () {
        //console.log("updateEnvironment");
        var i,
            j,
            rows = this.floorGrid.length,
            cols = this.floorGrid[0].length,
            floorTile,
            timeNow = Date.now(),
            changedFloorTiles = [];

        for(i=0; i<rows; i+=1){
            for(j=0; j<cols; j+=1){
                floorTile = this.floorGrid[i][j];
                if(floorTile.type.revertsTo !== undefined){
                    if(floorTile.lastOccupiedTime + floorTile.type.duration < timeNow){
                        floorTile.type = floorTile.type.revertsTo;
                        floorTile.lastOccupiedTime = timeNow;
                        floorTile.occupiedCount = 0;

                        changedFloorTiles.push({code: floorTile.type.code, row: i, col: j});
                    }
                }

            }

        }

        if(changedFloorTiles.length > 0){
            this.io.in(this.roomName).emit('change_many_floortiles', changedFloorTiles);
        }

        setTimeout(this.updateEnvironment.bind(this), 10000);
    }

    generateResource () {
        var randRow = ECS.PhaserRips.Math.randIntInRange(0, this.gridRows-1);
        var randCol = ECS.PhaserRips.Math.randIntInRange(0, this.gridCols-1);
        //console.log("generating resource at row: " + randRow + ", col: " + randCol);
        var tile = this.floorGrid[randRow][randCol];
        //console.log(tile);

        // Spawn a shrine if this zone doesn't already have one.
        // Do this before any other resource node, or they will be in the way when this shrine is placed.
        if(this.shrine === null){
            // Check there is space for a shrine.
            if(ECS.isTargetLocationEmpty(this, ECS.testEntities.Shrine, randCol * this.gridSize, randRow * this.gridSize, {
                    solid: true,
                    obstacle: true,
                    entrance: true,
                    exit: true,
                    character: true,
                    minion: true,
                    structure: true
                })){
                // Get a random element.
                var keys = Object.keys(ECS.Elements);
                var randElement = ECS.Elements[keys[keys.length * Math.random() << 0]];
                //console.log("randElement: " + randElement);
                // Create a shrine entity.
                ECS.assemblages.Shrine({zone: this, x: randCol * this.gridSize, y: randRow * this.gridSize, element: randElement});
            }
        }
        //console.log("zone " + this.name + " resource nodes: " + this.resourceNodesCount + " / " + this.maxResourceNodes);
        // Don't go over the max amount of resources nodes in this zone.
        if(this.resourceNodesCount < this.maxResourceNodes){
            //console.log("  below max nodes, time: " + new Date().toLocaleString());
            // Check there is space for a sapling-sized entity.
            if(ECS.isTargetLocationEmpty(this, ECS.testEntities.Sapling, randCol * this.gridSize, randRow * this.gridSize, {
                    solid: true,
                    obstacle: true,
                    entrance: true,
                    exit: true,
                    character: true,
                    minion: true,
                    pickup: true,
                    structure: true
                })){
                //console.log("    location not blocked");

                // If grass, spawn a tree.
                if(tile.type === floorTileTypes.Grass){
                    ECS.assemblages.Sapling({zone: this, x: randCol * this.gridSize, y: randRow * this.gridSize});
                }
                // If snow, spawn a gems rock.
                else if(tile.type === floorTileTypes.Snow){
                    ECS.assemblages.GemsRock({zone: this, x: randCol * this.gridSize, y: randRow * this.gridSize});
                }
            }
            else{
                //console.log("    location is blocked");
            }
        }

        // Don't go over the max amount of mobs in this zone
        if(this.mobCount < this.maxMobs){
            // Don't spawn goblins in water.
            if(tile.type !== floorTileTypes.ShallowWater
            && tile.type !== floorTileTypes.DeepWater){

                // Check there is space for a goblin-sized entity.
                if(ECS.isTargetLocationEmpty(this, ECS.testEntities.GoblinMage, randCol * this.gridSize, randRow * this.gridSize, {
                        solid: true,
                        obstacle: true,
                        character: true,
                        minion: true,
                        structure: true
                    })){

                    ECS.assemblages.GoblinMage({zone: this, x: randCol * this.gridSize, y: randRow * this.gridSize});

                }

            }

        }

        setTimeout(this.generateResource.bind(this), 5000);
    }
}

module.exports = Zone;
