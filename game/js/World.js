/**
 * Created by User on 14/04/2016.
 */

"use strict";

//var database = require('./../Database');
var globals = require('./Setup');
var Zone = require('./Zone');
var ECS = globals.ECS;
//var gameModes = globals.gameModes;
//var count = 0;

// The index each zone will have in zonesArray.
var zoneIndex = 0;

module.exports = {

    io: {},

    // How many players currently in this world.
    playerCount: 0,

    // Maximum amount of players allowed in this world.
    maxCapacity: 200,

    // The zones that make up the game space for this world, in array form for faster looping in update and emit. To access a zone by name, use zonesObject.
    zonesArray: [],

    // The zones that make up the game space for this world, in object form for easier assess by name. To access a zone by index, use zonesArray.
    // Each zone has a 4 character string name, like '0122', which is the key to access that zone.
    zonesObject: {},

    orders: [],

    create: function (io) {
        this.io = io;
        ECS.zonesObject = this.zonesObject;

        this.addZone('zone-9999-TEST-ENTITIES');

        this.addZone('zone-0001-noob-island-north-west');
        this.addZone('zone-0002-noob-island-north');
        this.addZone('zone-0003-noob-island-north-east');
        this.addZone('zone-0004-noob-island-west');
        this.addZone('zone-0005-noob-island-center');
        this.addZone('zone-0006-noob-island-east');
        this.addZone('zone-0007-noob-island-south-west');
        this.addZone('zone-0008-noob-island-south');
        this.addZone('zone-0009-noob-island-south-east');

        this.populateZones();

        // Start the emitter loop.
        this.emitState();
        // Call the update for this world, so it starts the update loop.
        this.update();
    },

    /**
     * The main update loop for the server. Updates all zones and their entities.
     * Tick rate: 20/sec (50 ms)
     */
    update: function () {
        //console.log("* * * * inside update");
        //console.log(this.maxCapacity);
        // Update each of the zones in this world.
        for(var i=0, len=this.zonesArray.length; i<len; i+=1){
            //console.log("zone = " + i);
            this.zonesArray[i].update();
        }

        setTimeout(this.update.bind(this), 50);
    },

    /**
     * The game state emitter. This is what sends updates of what is happening in the game world to connected clients.
     * Tries to only send relevant stuff to each client, such as only things in the same zone.
     * Emit rate: 10/sec (100 ms)
     */
    emitState: function () {
        //console.log("start emitState, count: " + count);
        //count+=1;

        var data = [],
            zone = {},
            entity = {};

        // For every zone in this world.
        for(var i= 0, len=this.zonesArray.length; i<len; i+=1){
            data = [];
            zone = this.zonesArray[i];

            // Prepare the necessary data to be sent to all the clients in this zone.
            for(var j=0, len2=zone.entities.length; j<len2; j+=1){
                entity = zone.entities[j];
                // Skip empty slots in the entities list.
                if(entity === null){
                    continue;
                }

                // Only send the entities that have moved. Ones that haven't moved don't need to be updated on the client.
                // Don't use hasMoved, that is for checking physics, not for updating what the client sees.
                if(entity.positionEmitted === false){

                    entity.setPositionEmitted(true);

                    // Don't send updates for invisible entities.
                    if(entity.invisible === true){
                        // If it is a player's entity, send only them their own position.
                        if(entity.socket !== undefined){
                            entity.socket.emit('zone_state', [{
                                id: entity.id,
                                x: Math.trunc(entity.body.centerX),
                                y: Math.trunc(entity.body.centerY)
                            }]);
                        }
                        continue;
                    }

                    //TODO: do another check here to check the distance something is away from each player is further than the client viewport area, as it shouldn't care what is moving on the other side of the map.

                    // Add the position of this entity to the list of entity data to send.
                    data.push({
                        id: entity.id,
                        // When sending the position of this entity to clients, the center will probably something really long like
                        // 767.7505902298357, which when converted to a string in JSON would be a lot bigger than just 767, which is good enough.
                        // Inline version of entity.getTruncatedCenterPosition, less function calls.
                        x: Math.trunc(entity.body.centerX),
                        y: Math.trunc(entity.body.centerY)
                    });

                }
            }
            if(data.length > 0){
                //console.log("sending entities data for zone: " + zone.roomName);
                //console.log(data);
                // Emit the data to all clients in the current room.
                this.io.in(zone.roomName).emit('zone_state', data);
                //console.log(");
            }
            else {
                //console.log("no data to send")
            }
        }

        setTimeout(this.emitState.bind(this), 100);

        //console.log("end of emit");
    },

    /**
     *
     * @param {String} mapDataFileName - The file name of the map data to use for this zone. i.e. 'zone-4567-derpy-town'.
     * @returns {Zone} The zone that was added to the world.
     */
    addZone: function (mapDataFileName) {
        // Get the '3046' part of the filename, which is the zone name.
        var name = mapDataFileName.substring(5, 9);

        // Create a zone, giving it the index it will have in zonesArray, and the name it will have in zonesObject, and a reference to the socket.io server.
        var zone = new Zone(zoneIndex, name, this.io, mapDataFileName);
        this.zonesArray.push(zone);
        // Check if a zone with this name already exists.
        if(this.zonesObject[name]){
            console.log("* WARNING: Multiple zones in the map data have the same name: " + name);
        }
        else {
            this.zonesObject[name] = zone;
        }

        zoneIndex += 1;

        return zone;
    },

    /**
     * Add the stuff to each zone that is in the map data.
     * The zone objects need to be created first.
     */
    populateZones: function () {
        var zones = this.zonesArray;

        for(var i=0, len=zones.length; i<len; i+=1){
            zones[i].loadMapData();
        }
    },

    addOrder: function () {

        this.orders.push(new globals.Order());
    }

};

