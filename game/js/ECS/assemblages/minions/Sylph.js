/**
 * Created by User on 18/02/2018.
 */

"use strict";

var globals = require('../../../Setup');
var Entity = require('../../Entity');
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

class _Slyph extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 16);
    }

    reset (casterEntity, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setNextCastTime(0);
        this.setBeingPushed(false);
        this.setMaxEnergy(100);
        this.setEnergy(this.maxEnergy);
        this.setEnergyRegenRate(8);
        this.energyRegenRateCount = 0;
        this.setEnergyRegenAmount(6);
        this.setNextCastTime(0);
        /**
         *
         * @type {Entity}
         */
        this.target = false;

        this.setZone(casterEntity.zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.setCaster(casterEntity);
        // Add this minion to the list of minions the caster is the master of.
        casterEntity.minions[this.id] = this;

        // Decrease the energy regen amount of the master by the energy maintenance cost of this minion.
        this.caster.modEnergyRegenAmount(-this.energyRegenCost);

        this.setOccupiedTile();
    }

    onAllHitPointsLost () {
        //console.log("Slyph onallHPlost");
        // Restore the energy regen cost of maintaining this minion to the master.
        this.caster.modEnergyRegenAmount(+this.energyRegenCost);
        //console.log("Slyph onAllHPLost, master regen amount: " + this.caster.energyRegenAmount);
        // Remove this minion from the list of minions on the caster.
        delete this.caster.minions[this.id];
        //console.log("Slyph died, caster minions:");
        //console.log(this.caster.minions);
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();
    }

    update () {
        //console.log("updating Slyph, id: " + this.id);
        if(this.beingPushed === false){



        }

        if(this.occupiedTile.damage !== undefined){
            this.damageEntity(this.occupiedTile.type.damage);
        }

        this.move();

        this.checkPhysics();
    }

}

_Slyph.prototype.entityType = 'Slyph';
_Slyph.prototype.entityCategory = ECS.Categories.MINION;
_Slyph.prototype.pool = [];

_Slyph.prototype.maxSpeed = 5.6;
_Slyph.prototype.maxHitPoints = 128;
_Slyph.prototype.element = ECS.Elements.DARK;
_Slyph.prototype.countersElement = ECS.Elements.WATER;
_Slyph.prototype.entityIndex = null;
_Slyph.prototype.energyRegenCost = 0.5;

_Slyph.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    structure: function (thisEntity, otherEntity) {
        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){

            }
            else {
                thisEntity.pushBodyBack();
            }
        }
        else {
            thisEntity.pushBodyBack();
        }
    },

    character: function (thisEntity) {
        thisEntity.pushBodyBack(0.2);
    },

    minion: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        if(otherEntity.flying === true){
            thisEntity.pushBodyBack(0.8);
        }
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._Slyph = _Slyph;
ECS.testEntities.Slyph = ECS.addTestEntity(_Slyph);

/**
 * Activates a Slyph entity.
 * @param {ECS.Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.Slyph = function (config) {
    var entity = ECS.activateEntity(_Slyph);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};