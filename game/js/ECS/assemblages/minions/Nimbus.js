/**
 * Created by User on 25/12/2017.
 */

"use strict";

var globals = require('../../../Setup');
var Entity = require('../../Entity');
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;
var floorTileTypes = require('../../../tiles/FloorTileTypes');

class _Nimbus extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setHitPoints(100);
        this.setZone(casterEntity.zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.setCaster(casterEntity);
        // Add this minion to the list of minions the caster is the master of.
        casterEntity.minions[this.id] = this;

        // Decrease the energy regen amount of the master by the energy maintenance cost of this minion.
        this.caster.modEnergyRegenAmount(-this.energyRegenCost);

        this.setOccupiedTile();
    }

    onAllHitPointsLost () {
        //console.log("nimbus onallHPlost");
        // Restore the energy regen cost of maintaining this minion to the master.
        this.caster.modEnergyRegenAmount(+this.energyRegenCost);
        //console.log("critter onAllHPLost, master regen amount: " + this.caster.energyRegenAmount);
        // Remove this minion from the list of minions on the caster.
        delete this.caster.minions[this.id];
        //console.log("critter died, caster minions:");
        //console.log(this.caster.minions);
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();
    }

    floorInteractions () {

        if(this.setOccupiedTile() === false){
            return false;
        }

        if(this.occupiedTile.type === floorTileTypes.Dirt){
            this.occupiedTile.type = floorTileTypes.Mud;
            this.occupiedTile.lastOccupiedTime = Date.now();
            this.occupiedTile.occupiedCount = 0;
            ECS.io.in(this.zone.roomName).emit('change_floortile', {code: this.occupiedTile.type.code, row: ~~(this.body.centerY / 32), col: ~~(this.body.centerX / 32)});
        }
        else if(this.occupiedTile.type === floorTileTypes.Lava){
            this.occupiedTile.type = floorTileTypes.Path;
            this.occupiedTile.lastOccupiedTime = Date.now();
            this.occupiedTile.occupiedCount = 0;
            ECS.io.in(this.zone.roomName).emit('change_floortile', {code: this.occupiedTile.type.code, row: ~~(this.body.centerY / 32), col: ~~(this.body.centerX / 32)});
        }
    }

    update () {
        //if(this.isEntityWithinRange(this.caster.body, 10) === false){
        //console.log("-moving towards my master");
        // Start the minion moving towards its master.
        moveToXY(this.body, this.caster.body.centerX, this.caster.body.centerY, this.maxSpeed);
        //}
        //else {
        //console.log("-found my master, stopping, id: " + this.id);
        //this.setVelocity();
        //}

        this.move();

        this.checkPhysics();
    }

}

_Nimbus.prototype.entityType = 'Nimbus';
_Nimbus.prototype.entityCategory = ECS.Categories.MINION;
_Nimbus.prototype.pool = [];

_Nimbus.prototype.maxSpeed = 3;
_Nimbus.prototype.flying = true;
_Nimbus.prototype.element = ECS.Elements.WATER;
_Nimbus.prototype.countersElement = ECS.Elements.FIRE;
_Nimbus.prototype.entityIndex = null;
_Nimbus.prototype.energyRegenCost = 2;

_Nimbus.prototype.intersectFuncs = {
    /*solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },*/

    /*solidAllowProjectiles: function (thisEntity) {
        thisEntity.pushBodyBack();
    },*/

    minion: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        if(otherEntity.flying === true){
            thisEntity.pushBodyBack(0.8);
        }
    },

    character: function (thisEntity, otherEntity) {
        otherEntity.modEnergy(+2);
    }

    /*obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    }*/

};

//var c=1;
/*
ECS.assemblages._Nimbus.prototype.move = function () {
    // Check this entity has a velocity, and therefore should move.
    if(this.body.velocity.x || this.body.velocity.y){



        // Move the body of this entity at normal speed multiplied by any modifier.
        this.body.move(this.body.velocity.x, this.body.velocity.y);

        // The entity has moved. Set hasMoved to true, so it will get it's physics checked for it's new position.
        this.body.hasMoved = true;
        // Also, the clients need to know the new position, so tell the emitter to send details of this entity in the next emit.
        this.positionEmitted = false;
        // The body has moved. Update the center positions.
        this.updateBodyPositions();
        // The entity has moved. Return true.
        return true;
    }
};*/

ECS.assemblages._Nimbus = _Nimbus;
ECS.testEntities.Nimbus = ECS.addTestEntity(_Nimbus);

/**
 * Activates a Nimbus entity.
 * @param {ECS.Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.Nimbus = function (config) {
    var entity = ECS.activateEntity(_Nimbus);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};