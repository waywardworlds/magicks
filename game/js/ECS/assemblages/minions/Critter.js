/**
 * Created by User on 21/05/2017.
 */

"use strict";

var globals = require('../../../Setup');
var Entity = require('../../Entity');
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

class _Critter extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 16);
    }

    reset (casterEntity, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setNextCastTime(0);
        this.setBeingPushed(false);
        /**
         *
         * @type {Entity}
         */
        this.target = false;
        this.carryingItem = false;
        //this.teamNumber =           casterEntity.teamNumber;

        this.setZone(casterEntity.zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.setCaster(casterEntity);
        // Add this minion to the list of minions the caster is the master of.
        casterEntity.minions[this.id] = this;

        // Decrease the energy regen amount of the master by the energy maintenance cost of this minion.
        this.caster.modEnergyRegenAmount(-this.energyRegenCost);

        this.setOccupiedTile();
    }

    onAllHitPointsLost () {
        //console.log("critter onallHPlost");
        // Restore the energy regen cost of maintaining this minion to the master.
        this.caster.modEnergyRegenAmount(+this.energyRegenCost);
        //console.log("critter onAllHPLost, master regen amount: " + this.caster.energyRegenAmount);
        // Remove this minion from the list of minions on the caster.
        delete this.caster.minions[this.id];
        //console.log("critter died, caster minions:");
        //console.log(this.caster.minions);
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();
    }

    update () {
        //console.log("updating critter, id: " + this.id);
        if(this.beingPushed === false){
            // Does the entity have a target to chase.
            if(this.target !== false){
                // If the target has a hitpoints component, attack it.
                if(this.target.hitPoints !== undefined){
                    // Check if the target has no HP.
                    if(this.target.hitPoints <= 0){
                        this.target = false;
                    }
                    // Target has some HP.
                    else {
                        // Within attack range.
                        if(this.isEntityWithinDetectionRadius(this.target.body)){
                            // Check that the spell recast delay is over before trying to cast a spell.
                            if(this.nextCastTime < Date.now()){

                                this.target.damageEntity(20, this);

                                this.zone.io.in(this.zone.roomName).emit('attack', {id: this.id});
                                // Stop moving.
                                this.setVelocity();
                                // Update the nextCastTime for this minion. 2000 is the cooldown.
                                this.setNextCastTime(2000);
                            }
                        }
                        // Within chase range.
                        else if(this.isEntityWithinRange(this.target.body, 280) === true){
                            //console.log("minions target is within range o");
                            moveToXY(this.body, this.target.body.centerX, this.target.body.centerY, this.maxSpeed);
                        }
                        // Target is too far away to chase.
                        else {
                            //console.log("minions target is out of range xxx");
                            this.target = false;
                        }
                    }
                }
                // Target doesn't have a hitpoints component. Is probably a pickup, so go get it.
                else {
                    //if(this.target.isActive === true){
                    //    moveToXY(this.body, this.target.body.centerX, this.target.body.centerY, this.maxSpeed);
                    //}
                    //else {
                        //console.log("target is inactive, stop following");
                        this.target = false;
                    //}

                }

            }
            // No target to chase.
            else {
                /*if(this.carryingWood === false){
                    // Check the master doesn't already have a full inventory.
                    if(this.caster.inventory.isFull === false){
                        // Check for any nearby pickups to collect and bring back to master.
                        var nearestPickup = ECS.getNearestWithinRange(this.zone, this.body.centerX, this.body.centerY, 160, ECS.categories.pickup);
                        if(nearestPickup !== false){
                            this.target = nearestPickup;
                        }
                        // The master can carry more wood, but there is none nearby. Return to master.
                        else {*/
                            if(this.isEntityWithinRange(this.caster.body, 80) === false){
                                //console.log("-moving towards my master");
                                // Start the minion moving towards its master.
                                moveToXY(this.body, this.caster.body.centerX, this.caster.body.centerY, this.maxSpeed);
                            }
                            else {
                                //console.log("-found my master, stopping, id: " + this.id);
                                this.setVelocity();
                            }
                        //}
                    /*}
                    // Master can't carry any more wood. Return to master.
                    else {
                        if(this.isEntityWithinRange(this.caster.body, 80) === false){
                            //console.log("-moving towards my master");
                            // Start the minion moving towards its master.
                            moveToXY(this.body, this.caster.body.centerX, this.caster.body.centerY, this.maxSpeed);
                        }
                        else {
                            //console.log("-found my master, stopping, id: " + this.id);
                            this.setVelocity();
                        }
                    }

                }
                // Is carrying wood, bring back to master.
                else {
                    if(this.isEntityWithinRange(this.caster.body, 40) === false){
                        //console.log("-moving towards my master");
                        // Start the minion moving towards its master.
                        moveToXY(this.body, this.caster.body.centerX, this.caster.body.centerY, this.maxSpeed);
                    }
                    else {
                        //console.log("-found my master, stopping, id: " + this.id);
                        this.setVelocity();
                        //this.carryingWood = false;
                        this.caster.modWood(1);
                        this.zone.io.in(this.zone.roomName).emit('command', {id: this.id, command: 'dropWood'});
                    }
                }*/
                //console.log("nearest pickup to critter: " + nearestPickup);


            }
        }

        if(this.occupiedTile.damage !== undefined){
            this.damageEntity(this.occupiedTile.type.damage);
        }

        this.move();

        this.checkPhysics();
    }

}

_Critter.prototype.entityType = 'Critter';
_Critter.prototype.entityCategory = ECS.Categories.MINION;
_Critter.prototype.pool = [];

_Critter.prototype.maxSpeed = 5.6;
_Critter.prototype.maxHitPoints = 128;
_Critter.prototype.element = ECS.Elements.DARK;
_Critter.prototype.countersElement = ECS.Elements.WATER;
_Critter.prototype.entityIndex = null;
_Critter.prototype.energyRegenCost = 0.5;

_Critter.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    structure: function (thisEntity, otherEntity) {
        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){

            }
            else {
                thisEntity.pushBodyBack();
            }
        }
        else {
            thisEntity.pushBodyBack();
        }
    },

    //solidAllowProjectiles: function (thisEntity) {
    //    thisEntity.pushBodyBack();
    //},

    character: function (thisEntity) {
        thisEntity.pushBodyBack(0.2);
    },

    minion: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.pushBodyBack(0.2);
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    /*pickup: function (thisEntity, otherEntity) {
        if(thisEntity.target === otherEntity){
            thisEntity.target = false;
        }
        // Check the master doesn't already have a full inventory.
        if(thisEntity.caster.inventory.isFull === false){
            if(thisEntity.carryingItem === false){
                thisEntity.carryingItem = true;
                thisEntity.target = false;

                otherEntity.target = thisEntity;//.deactivate();

                //thisEntity.zone.io.in(thisEntity.zone.roomName).emit('command', {id: thisEntity.id, command: 'carryWood'});
            }
        }
    },*/

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._Critter = _Critter;
ECS.testEntities.Critter = ECS.addTestEntity(_Critter);

/**
 * Activates a Critter entity.
 * @param {ECS.Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.Critter = function (config) {
    var entity = ECS.activateEntity(_Critter);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};