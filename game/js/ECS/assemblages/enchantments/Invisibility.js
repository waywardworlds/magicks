/**
 * Created by User on 26/01/2018.
 */

"use strict";

var globals = require('../../../Setup');

globals.Spells.activateEnchantment = function (enchantmentClass) {
    //console.log("in ECS.activateEntity");
    var enchantment;
    var pool = enchantmentClass.prototype.pool;
    // Try to find an inactive entity of this type.
    for(var i=0, len=pool.length; i<len; i+=1){
        if(pool[i]._isActive === false){
            //console.log("inactive entity found, reusing it");
            // Inactive entity found. Let's use it.
            enchantment = pool[i];
            // The entity is now active.
            pool[i]._isActive = true;
            //console.log("reusing entity: " + entity.id + ", " + entity.entityType);
            return enchantment;
        }
    }
    //console.log("no inactive entity found, creating new one");
    // No inactive entity found. Create a new one.
    enchantment = new enchantmentClass();
    // Add it to the pool.
    pool.push(enchantment);
    // The entity is now active.
    enchantment._isActive = true;
    //console.log("created new entity: " + entity.id + ", " + entity.entityType);
    return enchantment;
};

class Enchantment {
    constructor () {
        this._isActive = false;
    }


    start (casterEntity, targets) {

        this.caster = casterEntity;

        this.caster.enchantment = this;

        this.targets = targets || [];

        this.caster.modEnergyRegenAmount(-this.energyRegenCost);

    }

    update () {

    }

    end () {
        // Remove this enchantment from the caster entity.
        this.caster.enchantment = undefined;

        // Restore the energy regen.
        this.caster.modEnergyRegenAmount(+this.energyRegenCost);

        // Remove the caster entity from this enchantment.
        this.caster = undefined;

    }
}


class _Invisibility extends Enchantment {

    constructor () {
        // Create a new instance of the enchantment class.
        super();
    }

    start(casterEntity, targets){
        super.start(casterEntity, targets);

        var targetsDetails = [];

        var entity;

        for(var i=0, len=targets.length; i<len; i+=1){

            entity = targets[i];

            entity.setInvisible(true);

            targetsDetails.push(entity.id);

        }

        // Tell the clients to hide this entity.
        globals.io.in(casterEntity.zone.roomName).emit('hide', targetsDetails);

        //console.log("invisibility enchantment started");

    }

    end () {

        var targetsDetails = [];

        var entity;

        for(var i=0, len=this.targets.length; i<len; i+=1){

            entity = this.targets[i];

            entity.setInvisible(false);

            targetsDetails.push({
                id: entity.id,
                x: Math.trunc(entity.body.centerX),
                y: Math.trunc(entity.body.centerY)
            });

        }

        // Tell the clients to show the entities that were hidden by this enchantment, and update their current positions.
        globals.io.in(this.caster.zone.roomName).emit('show', targetsDetails);

        super.end();

        //console.log("invisibility enchantment ended");

    }

}

_Invisibility.prototype.energyRegenCost = 16;
_Invisibility.prototype.pool = [];

globals.Spells.enchantments.Invisibility = function (config) {
    var enchantment = globals.Spells.activateEnchantment(_Invisibility);
    enchantment.start(config.casterEntity, config.targets);
    return enchantment;
};