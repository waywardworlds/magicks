/**
 * Created by User on 09/06/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Solid extends Entity {

    constructor (zone, x, y) {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(x, y, zone.gridSize, zone.gridSize, true);

        zone.addToEntities(this, false);

        // This entity isn't added to a pool, but it is still active.
        this._isActive = true;
    }

}

_Solid.prototype.entityType = 'Solid';
_Solid.prototype.entityCategory = ECS.Categories.SOLID;

ECS.assemblages._Solid = _Solid;

/**
 * Creates a new Solid entity.
 * Generic rectangle physics enabled entity. Mostly used for solid walls.
 * Solids are not pooled. They are added to the game world and should not be removed.
 * @param {Zone} zone - The zone this entity will be added to the quadtree of.
 * @param {Number} x - The x position to relocate the activated entity to.
 * @param {Number} y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was created.
 */
ECS.assemblages.Solid = function (zone, x, y) {
    return new _Solid(zone, x, y);
};

/*
ECS.assemblages._SolidAllowProjectiles = function (zone, x, y) {
    // Create a new instance of the entity class.
    ECS.Entity.call(this);

    this.setRectangleBody(x, y, width, height, true);

    this.addToEntities(entity);

    // Add this entity to the quadtree of the zone it is in.
    zone.quadTree.push(this, false);

    // This entity isn't added to a pool, but it is still active.
    this.isActive = true;
};
ECS.assemblages._SolidAllowProjectiles.prototype = Object.create(ECS.Entity.prototype);
ECS.assemblages._SolidAllowProjectiles.prototype.entityType = 'SolidAllowProjectiles';
ECS.assemblages._SolidAllowProjectiles.prototype.entityCategory = 'solidAllowProjectiles';

/**
 * Creates a new SolidAllowProjectiles entity.
 * Similar to a regular Solid, but projectiles should be able to pass through.
 * Used for things like tables, benches, counters, fences, and deep water.
 * SolidAllowProjectiles are not pooled. They are added to the game world and should not be removed.
 * @param {Number} x - The x position to relocate the activated entity to.
 * @param {Number} y - The y position to relocate the activated entity to.
 * @param {Number} width - The width of the body of this entity.
 * @param {Number} height - The height of the body of this entity.
 * @return {ECS.Entity} The entity that was created.
 *//*
ECS.assemblages.SolidAllowProjectiles = function (zone, x, y, width, height) {
    return new ECS.assemblages._SolidAllowProjectiles(zone, x, y, width, height);
};*/