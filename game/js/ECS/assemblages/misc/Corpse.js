/**
 * Created by User on 13/04/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Corpse extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 16);
    }

    reset (zone, x, y) {
        //console.log("in corpse reset, id: " + this.id);
        this.setZone(zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // Add a timer that will remove this corpse after the delay has elapsed.
        // How long the entity should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 10000);

        // Tell the players in the zone this entity is now in to add a sprite for it.
        ECS.io.in(this.zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

    }

    update () {
        // Let this corpse get pushed around for fun.
        this.checkPhysics();
    }

}

_Corpse.prototype.entityType = 'Corpse';
_Corpse.prototype.entityCategory = ECS.Categories.CORPSE;
_Corpse.prototype.pool = [];

//_Corpse.prototype.zone = null;
_Corpse.prototype.entityIndex = null;

_Corpse.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },
/*
    character: function (thisEntity, otherEntity) {
        //thisEntity.pushBodyBack(0.2);
        //otherEntity.pushBodyBack(0.2);
    },*/

    minion: function (thisEntity, otherEntity) {
        otherEntity.pushBodyBack();
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._Corpse = _Corpse;
ECS.testEntities.Corpse = ECS.addTestEntity(_Corpse);

/**
 * Activates a Corpse entity.
 * @param {Zone} config.zone - The zone to put this entity in.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.Corpse = function (config) {
    //console.log("activating corpse entity, config: ");
    //console.log(config);
    var entity = ECS.activateEntity(_Corpse);
    entity.reset(config.zone, config.x, config.y);
    //console.log("activated corpse entity, id: " + entity.id);
    return entity;
};