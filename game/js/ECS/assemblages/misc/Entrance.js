/**
 * Created by User on 07/09/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Entrance extends Entity {

    constructor (zone, x, y, width, height, name) {
        super();

        this.setRectangleBody(x+0.5, y+0.5, width-1, height-1, true);

        this.left = x;
        this.right = x + width;
        this.top = y;
        this.bottom = y + height;

        // Check if an array for Entrances with this name already exists in the list of entrances
        // in the zone, as this might be the first entrance with this name to be added.
        // If not, add an array for entrances with this name.
        if(zone.entrances[name] === undefined){
            zone.entrances[name] = [];
        }

        // Add this entrance to the list of other entrances with this name in the same zone.
        zone.entrances[name].push(this);

        zone.addToEntities(this, false);

        // This entity isn't added to a pool, but it is still active.
        this._isActive = true;

    }
}

_Entrance.prototype.entityType = 'Entrance';
_Entrance.prototype.entityCategory = ECS.Categories.ENTRANCE;

ECS.assemblages._Entrance = _Entrance;

/**
 * Creates a new Entrance entity.
 * Entrances are not pooled. They are added to the game world and should not be removed.
 * @param {Zone} zone - The zone to add this entity to.
 * @param {Number} x - The x position of this entrance.
 * @param {Number} y - The y position of this entrance.
 * @param {Number} width - The width of this entrance.
 * @param {Number} height - The height position of this entrance.
 * @param {String} name - The name (entrance number) that this entrance has. Usually a string number.
 * @return {ECS.Entity} The entity that was created.
 */
ECS.assemblages.Entrance = function (zone, x, y, width, height, name) {
    return new _Entrance(zone, x, y, width, height, name);
};