/**
 * Created by User on 09/06/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Exit extends Entity {

    constructor (zone, x, y, width, height, targetEntrance, targetZone) {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(x, y, width, height, true);

        // A reference to the zone that this exit will send entities to.
        this.targetZone = ECS.zonesObject[targetZone];
        // The number name of the entrance bounds the entity will be moved into.
        this.targetEntrance = targetEntrance;

        zone.addToEntities(this, false);

        // This entity isn't added to a pool, but it is still active.
        this._isActive = true;
    }

}

_Exit.prototype.entityType = 'Exit';
_Exit.prototype.entityCategory = ECS.Categories.EXIT;

ECS.assemblages._Exit = _Exit;

/**
 * Creates a new Exit entity.
 * Exits are not pooled. They are added to the game world and should not be removed.
 * @param {Zone} zone - The zone this entity will be added to the quadtree of.
 * @param {Number} x - The x position to relocate the activated entity to.
 * @param {Number} y - The y position to relocate the activated entity to.
 * @param {Number} width - The width of this entity.
 * @param {Number} height - The height of this entity.
 * @param {String} targetEntrance - The name of the entrance this exit leads to, such as '2'.
 * @param {String} targetZone - The name of the zone this exit leads to, such as '3041'.
 * @return {ECS.Entity} The entity that was created.
 */
ECS.assemblages.Exit = function (zone, x, y, width, height, targetEntrance, targetZone) {
    return new _Exit(zone, x, y, width, height, targetEntrance, targetZone);
};