/**
 * Created by User on 12/08/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Inverter extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 16, true);
    }

    reset (casterEntity, x, y) {
        //console.log("in inverter reset");

        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        // How long the inverter should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 40000);
    }

}

_Inverter.prototype.entityType = 'Inverter';
_Inverter.prototype.entityCategory = ECS.Categories.INVERTER;
_Inverter.prototype.pool = [];

_Inverter.prototype.element = ECS.Elements.VOID;
_Inverter.prototype.countersElement = '';
_Inverter.prototype.entityIndex = null;

ECS.assemblages._Inverter = _Inverter;
ECS.testEntities.Inverter = ECS.addTestEntity(_Inverter);

/**
 * Activates a Inverter entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.Inverter = function (config) {
    var entity = ECS.activateEntity(_Inverter);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};