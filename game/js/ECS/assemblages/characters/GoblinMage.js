/**
 * Created by User on 13/02/2018.
 */

"use strict";

var globals = require('../../../Setup');
var Entity = require('../../Entity');
var spells = globals.Spells;
var io = globals.io;
var PhaserRips = globals.PhaserRips;
var moveToXY = globals.PhaserRips.Physics.moveToXY;
var ECS = globals.ECS;

class _GoblinMage extends Entity {

    constructor () {
        super();

        this.setCircleBody(undefined, undefined, 16);
    }

    reset (zone, x, y, unlockedSpells) {
        //console.log("in goblin reset, id: " + this.id);
        this.setMaxHitPoints(256);
        this.setMaxEnergy(500);
        this.setMaxSpeed(5);
        this.setBeingPushed(false);
        this.setHitPoints(this.maxHitPoints);
        this.setEnergy(this.maxEnergy);
        this.setEnergyRegenRate(4);
        this.energyRegenRateCount = 0;
        this.setEnergyRegenAmount(6);
        this.setReviveDegenRateCount(0);
        this.setReviveProgress(this.maxHitPoints / 2);
        this.setNextCastTime(0);
        var possibleSpells = ['e', 'eee', 'f', 'ff', 'w', 'd', 'p'];
        this.unlockedSpells = unlockedSpells || {[possibleSpells[Math.floor(Math.random()*possibleSpells.length)]]: true};
        this.currentSpell = Object.keys(this.unlockedSpells)[0];
        //console.log("goblin unlocked spells: ");
        //console.log(this.unlockedSpells);
        this.setZone(zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);
        this.setOccupiedTile();
        this.target = false;

        this.wanderRate = 80;
        this.wanderRateCount = 0;

        this.stopMoveCount = 0;// DOING SOME STUFF WITH THIS?

        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        this.zone.mobCount+=1;

    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone that this entity is now unconscious.
        ECS.io.in(this.zone.roomName).emit('char_unconscious', this.id);
    }

    onAllReviveProgressLost () {
        //console.log("in GoblinMage onAllReviveProgressLost, id: " + this.id);

        this.zone.mobCount-=1;

        //console.log("gob died, zone mobcount: " + this.zone.mobCount);

        this.zone.removeFromEntities(this);

        // Add a corpse in place of this character entity.
        // ! ! ! Do this BEFORE removing this character entity from zone.entities, or this new corpse will be given the same entityIndex that the dead entity had. Messes things up. ! ! !
        ECS.assemblages.Corpse({zone: this.zone, x: this.body.centerX, y: this.body.centerY});

        // Tell the other players in this zone that this character has died.
        ECS.io.in(this.zone.roomName).emit('char_died', this.id);

        // This character is now dead. Deactivate it.
        this.deactivate();
    }

    revive () {
        //console.log("in GoblinMage revive, id: " + this.id);
        // Bring the entity back with half of their max hit points and energy.
        this.setHitPoints(this.maxHitPoints / 2);
        this.setEnergy(this.maxEnergy / 2);
        // Reset the revive progress, ready for the next time they are made unconscious.
        this.setReviveProgress(this.maxHitPoints / 2);
    }

    /**
     *
     * @param {Number} amount - How much to damage this entity by.
     * @param {ECS.Entity} casterEntity - The entity that cast the spell that caused this damage.
     */
    damageEntity (amount, casterEntity) {
        //console.log("in GoblinMage damageEntity, id: " + this.id);
        if(this.zone.combatEnabled === false){
            //console.log("damage cancelled, non-combat zone");
            return;
        }

        super.damageEntity(amount, casterEntity);

        // This goblin should now target whatever attacked them.
        if(casterEntity !== undefined){
            // This goblin might have ran into their own spell or cast it on themselves, so don't tell to attack themselves.
            if(casterEntity.id !== this.id){
                this.target = casterEntity;
            }
        }

    }

    healEntity (amount, casterEntity) {
        super.healEntity(amount, casterEntity);
        // If the target heals this entity, stop targeting them.
        if(this.target !== false){
            if(casterEntity === this.target){
                this.target = false;
            }
        }
    }

    castSpell (spellCode, targetX, targetY) {
        //console.log("in goblin.castSpell");
        // Check that the entity is alive.
        if(this.hitPoints > 0){
            // Check this entity has unlocked this spell.
            if(this.unlockedSpells[spellCode]){
                // Cast the selected spell.
                if(spells.cast(spellCode, this, targetX, targetY) === true){
                    // The spell was successfully cast.
                    // Tell the clients to play the attack animation for this entity.
                    io.in(this.zone.roomName).emit('attack', {id: this.id, angle: PhaserRips.Physics.angleFromBody(this.body, targetX, targetY)});
                }
            }
        }
    }

    update () {
        // Check if this character is conscious.
        if(this.hitPoints > 0){
            this.regenEnergy();

            this.stopMoveCount += 1;
            if(this.stopMoveCount > 70){
                if(this.beingPushed === false){
                    this.setVelocity();
                    this.stopMoveCount = 0;
                }
            }

            if(this.target !== false){
                // If the target has a hitpoints component, attack it.
                if(this.target.hitPoints !== undefined) {

                    // If the target has no HP, leave them.
                    if(this.target.hitPoints <= 0){
                        this.target = false;
                        if(this.beingPushed === false){
                            this.setVelocity();
                        }
                    }
                    else {

                        // Check that the spell recast delay is over before trying to cast a spell.
                        if(this.nextCastTime < Date.now()){

                            this.castSpell(this.currentSpell, this.target.body.centerX, this.target.body.centerY);

                            this.zone.io.in(this.zone.roomName).emit('attack', {id: this.id});
                            // Stop moving.
                            if(this.beingPushed === false){
                                this.setVelocity();
                            }

                        }
                        else {
                            // Move towards the target if they have moved.
                            //if(this.target.body.hasMoved === true){
                            // Within chase range.
                            if (this.isEntityWithinRange(this.target.body, 180) === true) {
                                //console.log("goblin target is within range");
                                if(this.beingPushed === false){
                                    moveToXY(this.body, this.target.body.centerX, this.target.body.centerY, this.maxSpeed);
                                }
                            }
                            // Target is too far away to chase.
                            else {
                                //console.log("goblin's target is out of range");
                                this.target = false;
                                if(this.beingPushed === false){
                                    this.setVelocity();
                                }
                            }
                            //}
                        }
                    }
                }
            }
            else {
                this.wanderRateCount += 1;
                // How often should this entity wander around.
                if(this.wanderRateCount >= this.wanderRate){
                    this.wanderRateCount = 0;

                    // No target. Wander around.
                    //if(this.target === false){
                        moveToXY(this.body, this.body.centerX + PhaserRips.Math.randIntInRange(-128, 128), this.body.centerY + PhaserRips.Math.randIntInRange(-128, 128), 2);
                    //}

                }
            }

            // Damage this entity if it is stood on a hazardous terrain.
            if(this.occupiedTile.damage !== undefined){
                //console.log("tile has damage: " + this.occupiedTile.damage);
                this.damageEntity(this.occupiedTile.type.damage);
            }

            this.move();

        }
        // Character is unconscious. Let them get resurrected.
        else {
            // Make them gradually lose revive progress so they eventually die without help.
            this.degenReviveProgress();
        }

        this.checkPhysics();

    }

}

_GoblinMage.prototype.entityType = 'GoblinMage';
_GoblinMage.prototype.entityCategory = ECS.Categories.CHARACTER;
_GoblinMage.prototype.pool = [];
_GoblinMage.prototype.entityIndex = null;

_GoblinMage.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    character: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.pushBodyBack(0.2);
        otherEntity.pushBodyBack(0.1);
    },

    minion: function (thisEntity, otherEntity) {
        otherEntity.pushBodyBack(-0.5);
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    structure: function (thisEntity, otherEntity) {

        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){

            }
            else {
                thisEntity.pushBodyBack();
            }
        }
        else {
            thisEntity.pushBodyBack();
        }
    },

    exit: function (thisEntity, otherEntity) {
        //console.log("GoblinMage intersects exit");
        // The crash test that sent the program here checks if bounds are overlapping, but for exits it
        // is nicer if things are only teleported if the center of the entity is within the exit bounds.
        if(thisEntity.isCenterWithinRectangleBody(otherEntity.body) === false){
            return;
        }

        // Check if this exit leads to an entrance in the current zone. If so, then no zone swap is needed.
        if(otherEntity.targetZone === thisEntity.zone){
            //console.log("target zone is this zone, stay here");
            var entrance = thisEntity.zone.getEntrance(otherEntity.targetEntrance);
            var detectionRadius = thisEntity.body.detectionRadius;
            // Move the entity to somewhere in the entrance bounds.
            thisEntity.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right  - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + detectionRadius, entrance.bottom - detectionRadius)
            );
        }
        // Push away from the exit so they don't wander out of the zone bounds.
        else {
            thisEntity.setVelocity();
            thisEntity.pushBodyBack();
        }

    }

};

ECS.assemblages._GoblinMage = _GoblinMage;
ECS.testEntities.GoblinMage = ECS.addTestEntity(_GoblinMage);

/**
 * Activates a GoblinMage entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @param {String} [config.unlockedSpells] - What spells can this entity cast.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.GoblinMage = function (config) {
    //console.log("\nactivating GoblinMage, config:");
    //console.log(config);
    var entity = ECS.activateEntity(_GoblinMage);
    entity.reset(config.zone, config.x, config.y, config.unlockedSpells);
    //console.log("activated goblinmage entity, id: " + entity.id);
    return entity;
};