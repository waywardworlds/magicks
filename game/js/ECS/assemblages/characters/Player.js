/**
 * Created by User on 09/06/2017.
 */

"use strict";

var globals = require('../../../Setup');
var Entity = require('../../Entity');
var Spells = globals.Spells;
var io = globals.io;
var PhaserRips = globals.PhaserRips;
var ECS = globals.ECS;

class _Player extends Entity {

    constructor () {
        super();

        this.setCircleBody(undefined, undefined, 16);
    }

    reset (spawnArea, displayName) {
        //console.log("in player reset, id: " + this.id);
        this.setMaxHitPoints(512);
        this.setMaxEnergy(896);
        this.setMaxSpeed(5);
        this.setBeingPushed(false);
        this.setHitPoints(this.maxHitPoints);
        this.setEnergy(this.maxEnergy);
        this.setEnergyRegenRate(4);
        this.energyRegenRateCount = 0;
        this.setEnergyRegenAmount(6);
        this.lastEmittedEnergy =    0;
        this.setReviveDegenRateCount(0);
        this.setNextCastTime(0);
        this.setReviveProgress(this.maxHitPoints / 2);
        this.setMinions();
        this.setOrder();
        this.setInventory(16);
        this.setDisplayName(displayName);
        if(spawnArea !== undefined){
            this.setSpawnArea(spawnArea);
            this.setZone(ECS.zonesObject[this.spawnArea.zoneName]);
            // Move the entity to somewhere within the bounds of the spawn entrance.
            var entrance = this.zone.getEntrance(this.spawnArea.entranceName);
            var detectionRadius = this.body.detectionRadius;
            this.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top + detectionRadius, entrance.bottom - detectionRadius)
            );
            // Add the entity to the zone it was put in.
            this.zone.addToEntities(this, true);
        }

        this.setOccupiedTile();

        this.elementXP = {
            l: 0,
            d: 0,
            w: 0,
            f: 0,
            e: 0,
            p: 0,
            v: 0
        };

        if(globals.devMode === true){
            for(var i=0; i<4; i+=1){
                ECS.InventoryManager.addItem(this, ECS.InventoryItemTypes.WOOD);
                ECS.InventoryManager.addItem(this, ECS.InventoryItemTypes.GEMS);
            }
        }

    }

    onAllHitPointsLost () {
        //console.log("in player onAllHitPointsLost, id: " + this.id);
        //console.log("onAllHitPointsLost, reviveProgress: " + this.reviveProgress);

        // Deactivate any minions.
        for(var minionId in this.minions){
            if(this.minions.hasOwnProperty(minionId)){
                //console.log("player no HP, killing minion: " + this.minions[minionId].entityType);
                this.minions[minionId].kill();
            }
        }
        // Deactivate enchantment.
        if(this.enchantment !== undefined){
            this.enchantment.end();
        }

        // Drop any inventory contents.
        var i,
            len,
            randInRange = ECS.PhaserRips.Math.randFloatInRange,
            range = 12,
            contents = this.inventory.contents;
        for(i=0, len=contents.length; i<len; i+=1){
            if(contents[i] === null){
                continue;
            }
            if(contents[i].itemType === ECS.InventoryItemTypes.WOOD){
                ECS.assemblages.WoodPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
            }
            else if(contents[i].itemType === ECS.InventoryItemTypes.GEMS){
                ECS.assemblages.GemsPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
            }
            else if(contents[i].itemType === ECS.InventoryItemTypes.FOOD){
                //ECS.assemblages.FoodPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
            }
            else if(contents[i].itemType === ECS.InventoryItemTypes.SCROLL){
                ECS.assemblages.ScrollPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range), itemConfig: contents[i].itemConfig});
            }

            ECS.InventoryManager.removeItemByIndex(this, i);

            this.socket.emit('remove_item', i);

        }

        //console.log("player allHPlost, energyRegenAmount: " + this.energyRegenAmount);

        // Tell all of the players in this zone that this entity is now unconscious.
        ECS.io.in(this.zone.roomName).emit('char_unconscious', this.id);
    }

    onAllReviveProgressLost () {
        //console.log("in player onAllReviveProgressLost, id: " + this.id);

        // Add a corpse in place of this character entity.
        // ! ! ! Do this BEFORE removing this character entity from zone.entities, or this new corpse will be given the same entityIndex that the dead entity had. Messes things up. ! ! !
        ECS.assemblages.Corpse({zone: this.zone, x: this.body.centerX, y: this.body.centerY});

        //this.zone.removeFromEntities(this);

        // Tell the other players in this zone that this character has died.
        ECS.io.in(this.zone.roomName).emit('char_died', this.id);

        // Don't use Entity.deactivate or zone.removeFromEntities for this entity when it dies, as to avoid pooling the entity
        // because we want to keep using the same entity for the same player in case they respawn.

        // Fixed: 25/01/2018
        // There was a serious bug here where multiple clients could control the same entity, caused by isActive being set to
        // false and the entity being removed from the zone, but the client socket still using that same entity, so when another
        // client connected, they were given that same entity from the pool, and if the entity respawned, it was added to the
        // entities list for the zone it is in a second time, so it occupied 2 entity slots and was updated twice (double speed/regen/etc).
        this.setVelocity();
        // Remove the link to the entity object from the list of entities in this zone.
        this.zone.entities[this.entityIndex] = null;
        this.entityIndex = null;
        // Remove it from the quadtree of the zone it is in so it doesn't interact with anything while dead.
        this.zone.quadTree.remove(this);

        /*
        // Remove a random spell from this player.
        var unlockedSpells = this.socket.gameData.unlockedSpells;
        var keys = Object.keys(unlockedSpells);

        if(keys.length !== 0){

            var randSpell = keys[keys.length * Math.random() << 0];
            //console.log("removing random spell: " + randSpell);

            //console.log("player unlocked spells before:");
            //console.log(unlockedSpells);

            delete unlockedSpells[randSpell];

            this.socket.emit("remove_spell", randSpell);
            this.socket.emit("alert_message", "You died!\nA spell has been lost.");


            //console.log("player unlocked spells after:");
            //console.log(unlockedSpells);
        }
        else {
            //console.log("this player has no spells to remove");
        }*/


    }

    revive () {
        //console.log("in player revive, id: " + this.id);
        // Bring the entity back with half of their max hit points and energy.
        this.setHitPoints(this.maxHitPoints / 2);
        this.setEnergy(this.maxEnergy / 2);
        // Reset the revive progress, ready for the next time they are made unconscious.
        this.setReviveProgress(this.maxHitPoints / 2);
    }

    /**
     * Respawn the entity for this player into the spawn area for the entity.
     */
    respawn () {
        //console.log("in player respawn, id: " + this.id);
        // Check that this entity is actually dead.
        if(this.hitPoints <= 0 && this.reviveProgress <= 0){
            // Respawn with full hit points.
            this.setHitPoints(this.maxHitPoints);
            this.setEnergy(this.maxEnergy);

            // Reset revive progress back to default 50% of max hit points.
            this.setReviveProgress(this.maxHitPoints / 2);

            this.setBeingPushed(false);
            //this.setSpawnArea('000' + globals.PhaserRips.Math.randIntInRange(1, 9) + '-0');
            this.setSpawnArea('0008-0');

            // Check if the spawn area is this zone. If so, just reposition the entity.
            if(this.spawnArea.zoneName == this.zone.name){
                //console.log("spawn zone is this zone, stay here");

                var entrance = this.zone.getEntrance(this.spawnArea.entranceName);
                //var entrance = this.zone.entrances[this.spawnArea.entranceName];
                // Move the sprite to somewhere in the entrance bounds.
                this.reposition(
                    ECS.PhaserRips.Math.randIntInRange(entrance.left, entrance.right),
                    ECS.PhaserRips.Math.randIntInRange(entrance.top, entrance.bottom)
                );

                // Get what tile it is now standing on.
                this.setOccupiedTile();

                this.body.velocity.x = 0;
                this.body.velocity.y = 0;

                // Add this entity back to the zone it is in.
                this.zone.addToEntities(this, true);

                // This entity has spawned. Tell the players in this zone to add a sprite for it.
                var dataToSend = {
                    typeNumber: this.typeNumber,
                    id: this.id,
                    x: Math.trunc(this.body.centerX),
                    y: Math.trunc(this.body.centerY)
                };
                // Also send this entities display name if it has one.
                if(this.displayName){
                    dataToSend.displayName = this.displayName;
                }
                io.in(this.zone.roomName).emit('add_entity', dataToSend);

            }
            else {
                //console.log("spawn zone is in another zone, move there");
                // Move this entity to the area it will spawn into.
                ECS.zonesObject[this.spawnArea.zoneName].moveEntityToZone(this, this.spawnArea.entranceName);
            }

            this.socket.emit('respawned');
            // Return true that this entity was revived.
            //return true;
        }
        // Return false that this entity was not revived.
        //return false;
    }

    /**
     * Gets the amount of heart quarters for how many hitpoints this player has.
     * @returns {Number}
     */
    getHitPointQuarters () {
        return Math.ceil(this.hitPoints / (this.maxHitPoints / 32));
    }

    /**
     * Gets the amount of energy quarters for how much energy this player has.
     * @returns {Number}
     */
    getEnergyQuarters () {
        return Math.ceil(this.energy / (this.maxEnergy / 28));
    }

    /**
     * Gets the amount of revive quarters for how much revive progress this player has.
     * @returns {Number}
     */
    getReviveQuarters () {
        return Math.ceil(this.reviveProgress / (this.maxHitPoints / 32));
    }

    emitEnergyQuarters () {
        var quarters = this.getEnergyQuarters();
        // Only emit the energy for this player if it is different than what they were most recently sent.
        // Pointless to send the same amount multiple times.
        if(this.lastEmittedEnergy !== quarters){
            // Tell the player their energy has changed.
            this.socket.emit('energy_changed', quarters);
            this.lastEmittedEnergy = quarters;
        }
    }

    /**
     * Override the Entity move function. Player's (and character humans) need some specific stuff.
     * @returns {Boolean} Whether the entity has moved.
     */
    /*move () {
        // Check this entity has a velocity, and therefore should move.
        if(this.body.velocity.x || this.body.velocity.y){
            //if(this.canMove !== undefined){
            //    if(this.canMove === false){
            //        // Entity can't move. Return false.
            //        return false;
            //    }
            //}

            var modifier = this.updateOccupiedFloorTile();
            if(modifier === false){
                return;
            }

            // The entity has moved. Set hasMoved to true, so it will get it's physics checked for it's new position.
            this.body.hasMoved = true;
            // Also, the clients need to know the new position, so tell the emitter to send details of this entity in the next emit.
            this.positionEmitted = false;
            // The body has moved. Update the center positions.
            this.updateBodyPositions();

            // The entity has moved. Return true.
            return true;
        }

        // The entity has not moved. Return false.
        return false;
    }*/

    /**
     *
     * @param {Number} amount - How much to damage this entity by.
     * @param {ECS.Entity} casterEntity - The entity that cast the spell that caused this damage.
     */
    damageEntity (amount, casterEntity) {
        //console.log("in player damageEntity, id: " + this.id);
        if(this.zone.combatEnabled === false){
            //console.log("damage cancelled, non-combat zone");
            return;
        }

        // Check the player has an order, or null === null would be true.
        if(this.order !== null){
            // Deal half damage to order members.
            if(this.order === casterEntity.order){
                //console.log("in an order: " + this.orderNumber);
                //console.log("player.js, halved damage to fellow order member");
                amount = amount * 0.5;
            }
        }

        super.damageEntity(amount, casterEntity);

        // Get the minions to attack whatever attacked
        if(casterEntity !== undefined){
            // This player might have ran into their own spell or cast it on themselves, so don't tell the minions to attack their master.
            if(casterEntity.id !== this.id){
                var minions = this.minions;
                for(var minionId in minions){
                    if(minions.hasOwnProperty(minionId)){
                        minions[minionId].target = casterEntity;
                    }
                }
            }
        }

    }

    castSpell (spellCode, targetX, targetY) {
        //console.log("in player.castSpell");
        // Check that the entity for this player is alive.
        if(this.hitPoints > 0){
            // Check this player has unlocked this spell.
            if(this.socket.gameData.unlockedSpells[spellCode] || spellCode.length === 1){
                //console.log("before castFunc");

                // Cast the selected spell.
                if(Spells.cast(spellCode, this, targetX, targetY) === true){
                    // The spell was successfully cast.
                    var element = spellCode[0];

                    this.elementXP[element] += Spells.spells[spellCode].xpGain;
                    // If enough XP in this element.
                    if(this.elementXP[element] > 400){
                        // If the inventory is not full.
                        if(this.inventory.isFull === false){
                            var randSpell = Spells.spellsByElement[element][Math.floor(Math.random()*Spells.spellsByElement[element].length)];
                            //ECS.InventoryManager.addItem(this, ECS.InventoryItemTypes.SCROLL, {spellName: randSpell.name, spellCode: randSpell.code});
                            this.socket.emit("add_item", {
                                type: ECS.InventoryItemTypes.SCROLL.value,
                                index: ECS.InventoryManager.addItem(this, ECS.InventoryItemTypes.SCROLL, {spellName: randSpell.name, spellCode: randSpell.code}), config: {spellName: randSpell.name}
                            });
                            this.socket.emit("alert_message", "Spell discovered.\nCheck inventory.");

                            this.elementXP[element] -= 400;
                            //console.log("spell discovered: " + randSpell.name + ", elemXP: " + this.elementXP[element]);
                        }
                    }

                    this.emitEnergyQuarters();

                    // Tell the clients to play the attack animation for this entity.
                    io.in(this.zone.roomName).emit('attack', {id: this.id, angle: PhaserRips.Physics.angleFromBody(this.body, targetX, targetY)});
                }
            }

        }
    }

    update () {
        // Check if this character is conscious.
        if(this.hitPoints > 0){
            this.regenEnergy();

            // Not enough energy to support minions and enchantment. Kill/end them all.
            if(this.energy < 0){
                for(var minionId in this.minions){
                    if(this.minions.hasOwnProperty(minionId)){
                        this.minions[minionId].kill();
                    }
                }

                if(this.enchantment !== undefined){
                    //console.log("player has enchantment");
                    //console.log(this.enchantment);
                    this.enchantment.end();
                }
            }
            // Damage this entity if it is stood on a hazardous terrain.
            if(this.occupiedTile.damage !== undefined){
                //console.log("tile has damage: " + this.occupiedTile.damage);
                this.damageEntity(this.occupiedTile.type.damage);
            }

            this.move();

            //console.log(this.occupiedTile.type.attunedElement);

            if(this.occupiedTile.type.attunedElement === ECS.Elements.WATER){
                this.modEnergy(1);
                //console.log("standing on water");
            }

        }
        // Character is unconscious. Let them get resurrected.
        else{
            // Make them gradually lose revive progress so they eventually die without help.
            this.degenReviveProgress();
        }

        this.checkPhysics();

    }


}

// What type of entity this is. characterHuman, obstacle, projectile, etc. Useful for debugging.
// A Player is basically a special kind of CharacterHuman.
_Player.prototype.entityType = 'Player';
// What category of entity this falls in. Used for other entities to know what to intersect/collide with.
// Basically shorthand way to refer to many different entityTypes that have the same functionality.
_Player.prototype.entityCategory = ECS.Categories.CHARACTER;
// The list of all entities of this type. Used for object pooling.
_Player.prototype.pool = [];

//_Player.prototype.zone = null;
_Player.prototype.entityIndex = null;

_Player.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    character: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.pushBodyBack(0.2);
        otherEntity.pushBodyBack(0.1);
    },

    corpse: function (thisEntity, otherEntity) {
        //thisEntity.pushBodyBack(0.1);
        otherEntity.pushBodyBack(-0.1);
    },

    minion: function (thisEntity, otherEntity) {
        otherEntity.pushBodyBack(-0.5);
    },

    obstacle: function (thisEntity) {
        //console.log("player intersects barrier");
        thisEntity.pushBodyBack();
    },

    pickup: function (thisEntity, otherEntity) {
        // Check the entity doesn't have a full inventory.
        if(thisEntity.inventory.isFull === false){
            //console.log("\nPicked up something, inventory contents:");
            //console.log(thisEntity.inventory.contents);

            thisEntity.socket.emit("add_item", {type: otherEntity.itemType.value, index: ECS.InventoryManager.addItem(thisEntity, otherEntity.itemType, otherEntity.itemConfig), config: otherEntity.itemConfig});
            otherEntity.deactivate();
        }
    },

    structure: function (thisEntity, otherEntity) {

        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){

            }
            else {
                thisEntity.pushBodyBack();
            }
        }
        else {
            thisEntity.pushBodyBack();
        }
    },
/*
    inverter: function (thisEntity, otherEntity) {
        thisEntity.pushBodyBack();
    },*/

    exit: function (thisEntity, otherEntity) {
        //console.log("player intersects exit");
        // The crash test that sent the program here checks if bounds are overlapping, but for exits it
        // is nicer if things are only teleported if the center of the entity is within the exit bounds.
        if(thisEntity.isCenterWithinRectangleBody(otherEntity.body) === false){
            return;
        }

        var targetZone = otherEntity.targetZone;

        // Check if this exit leads to an entrance in the current zone. If so, then no zone swap is needed.
        if(otherEntity.targetZone === thisEntity.zone){
            //console.log("target zone is this zone, stay here");
            var entrance = thisEntity.zone.getEntrance(otherEntity.targetEntrance);
            var detectionRadius = thisEntity.body.detectionRadius;
            // Move the entity to somewhere in the entrance bounds.
            thisEntity.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right  - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + detectionRadius, entrance.bottom - detectionRadius)
            );
        }
        // The target zone is a different zone. Move there.
        else {
            // Move the entity to the next zone.
            targetZone.moveEntityToZone(thisEntity, otherEntity.targetEntrance);

            // Move any minions of this character to the next zone, at the same entrance as the master.
            var centerX = thisEntity.body.centerX,
                centerY = thisEntity.body.centerY,
                minions = thisEntity.minions,
                minionId;

            for(minionId in minions){
                if(minions.hasOwnProperty(minionId)){
                    targetZone.moveEntityToZone(minions[minionId], otherEntity.targetEntrance);
                    // Move the minion to the position of the caster, with a bit of spread.
                    minions[minionId].reposition(centerX + ECS.PhaserRips.Math.randIntInRange(-10, 10), centerY + ECS.PhaserRips.Math.randIntInRange(-10, 10));
                    minions[minionId].checkPhysics();
                }
            }
        }

    }

};

ECS.assemblages._Player = _Player;
ECS.testEntities.Player = ECS.addTestEntity(_Player);

/**
 * Activates a Player entity.
 * @param {String} config.spawnArea - The area (zone and entrance) this entity will spawn into, such as '3041-2'. Can leave this undefined and manually move it to a zone when the entity is returned.
 * @param {String} config.displayName - What to set the displayName component to. Put 'RAND' to use the random name generator.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.Player = function (config) {
    //console.log("activating player, config:");
    //console.log(config);
    var entity = ECS.activateEntity(_Player);
    entity.reset(config.spawnArea, config.displayName);
    //console.log("activated player entity, name: " + entity.displayName + ", id: " + entity.id);
    return entity;
};