/**
 * Created by User on 12/04/2017.
 */

"use strict";

var globals = require('../../../Setup');
var Entity = require('../../Entity');
//var spells = globals.spells;
//var io = globals.io;
//var PhaserRips = globals.PhaserRips;
var ECS = globals.ECS;

class _CharacterHuman extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 16);
    }

    reset (spawnArea, displayName) {
        this.setMaxHitPoints(1024);
        this.setMaxEnergy(896);
        this.setMaxSpeed(5);
        this.setBeingPushed(false);
        this.setHitPoints(this.maxHitPoints);
        this.setEnergy(this.maxEnergy);
        this.setEnergyRegenRate(2);
        this.energyRegenRateCount = 0;
        this.setEnergyRegenAmount(6);
        this.setReviveDegenRateCount(0);
        this.setNextCastTime(0);
        this.setReviveProgress(this.maxHitPoints / 2);
        this.setMinions();
        this.setInventory(16);
        this.setDisplayName(displayName);
        if(spawnArea !== undefined){
            this.setZone(ECS.zonesObject[spawnArea.split('-')[0]]);
            // Move the entity to somewhere within the bounds of the respawn entrance.
            var entrance = this.zone.entrances[spawnArea.split('-')[1]];
            var detectionRadius = this.body.detectionRadius;
            this.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right  - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + detectionRadius, entrance.bottom - detectionRadius)
            );
            // Add the entity to the zone it was put in.
            this.zone.addToEntities(this, true);
        }

        this.setOccupiedTile();

    }

    onAllHitPointsLost () {
        //console.log("onAllHitPointsLost, reviveProgress: " + this.reviveProgress);

        // Deactivate any minions.
        for(var minionId in this.minions){
            if(this.minions.hasOwnProperty(minionId)){
                this.minions[minionId].kill();
            }
        }

        // Tell all of the players in this zone that this entity is now unconscious.
        ECS.io.in(this.zone.roomName).emit('char_unconscious', this.id);
    }

    onAllReviveProgressLost () {

        this.zone.removeFromEntities(this);

        // Add a corpse in place of this character entity.
        // ! ! ! Do this BEFORE removing this character entity from zone.entities, or this new corpse will be given the same entityIndex that the dead entity had. Messes things up. ! ! !
        ECS.assemblages.Corpse({zone: this.zone, x: this.body.centerX, y: this.body.centerY});

        // Tell the other players in this zone that this character has died.
        ECS.io.in(this.zone.roomName).emit('char_died', this.id);

        // This character is now dead. Deactivate it.
        this.deactivate();
    }

    revive () {
        //console.log("in characterHuman revive");
        // Bring the entity back with half of their max hit points and energy.
        this.setHitPoints(this.maxHitPoints / 2);
        this.setEnergy(this.maxEnergy / 2);
        // Reset the revive progress, ready for the next time they are made unconscious.
        this.setReviveProgress(this.maxHitPoints / 2);
    }

    update () {
        // Check if this character is conscious.
        if(this.hitPoints > 0){
            this.regenEnergy();
            this.move();
            this.checkPhysics();
        }
        // Character is unconscious. Let them get resurrected.
        else{
            // Make them gradually lose revive progress so they eventually die without help.
            this.degenReviveProgress();
        }
    }

}

_CharacterHuman.prototype.entityType = 'CharacterHuman';
_CharacterHuman.prototype.entityCategory = ECS.Categories.CHARACTER;
_CharacterHuman.prototype.pool = [];
_CharacterHuman.prototype.entityIndex = null;

_CharacterHuman.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    character: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.pushBodyBack(0.2);
        otherEntity.pushBodyBack(0.1);
    },

    minion: function (thisEntity, otherEntity) {
        otherEntity.pushBodyBack(-0.5);
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    inverter: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    exit: function (thisEntity, otherEntity) {
        // The crash test that sent the program here checks if bounds are overlapping, but for exits it
        // is nicer if things are only teleported if the center of the entity is within the exit bounds.
        if(thisEntity.isCenterWithinRectangleBody(otherEntity.body) === false){
            return;
        }

        var targetZone = otherEntity.targetZone;

        // Check if this exit leads to an entrance in the current zone. If so, then no zone swap is needed.
        if(otherEntity.targetZone === thisEntity.zone){
            var entrance = thisEntity.zone.entrances[otherEntity.targetEntrance];
            var detectionRadius = thisEntity.body.detectionRadius;
            // Move the entity to somewhere in the entrance bounds.
            thisEntity.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right  - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + detectionRadius, entrance.bottom - detectionRadius)
            );

            // Entity moved within the same zone. All done here.
            return;
        }

        // Otherwise, move the entity to the next zone.
        targetZone.moveEntityToZone(thisEntity, otherEntity.targetEntrance);

        // Move any minions too.
        var minions = thisEntity.minions;
        var minionId;

        // Move any minions of this character to the next zone, at the same entrance as the master.
        var centerX = thisEntity.body.centerX,
            centerY = thisEntity.body.centerY;

        for(minionId in minions){
            if(minions.hasOwnProperty(minionId)){
                targetZone.moveEntityToZone(minions[minionId], otherEntity.targetEntrance);
                // Move the minion to the position of the caster, with a bit of spread.
                minions[minionId].reposition(centerX + ECS.PhaserRips.Math.randIntInRange(-10, 10), centerY + ECS.PhaserRips.Math.randIntInRange(-10, 10));
                minions[minionId].checkPhysics();
            }
        }
    }
};

ECS.assemblages._CharacterHuman = _CharacterHuman;
ECS.testEntities.CharacterHuman = ECS.addTestEntity(_CharacterHuman);

/**
 * Activates a CharacterHuman entity.
 * @param {String} config.spawnArea - The area (zone and entrance) this entity will spawn into, such as '3041-2'.
 * @param {String} config.displayName - What to set the displayName component to. Put 'RAND' to use the random name generator.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.CharacterHuman = function (config) {
    //console.log("in CharacterHuman");
    var entity = ECS.activateEntity(_CharacterHuman);
    entity.reset(config.spawnArea, config.displayName);
    return entity;
};