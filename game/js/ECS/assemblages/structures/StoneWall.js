/**
 * Created by User on 23/09/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _StoneWall extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 31, 31, true);
    }

    reset (zone, x, y) {
        this.setMaxHitPoints(800);
        //console.log("in barrier reset");
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        this.reposition(x+0.5, y+0.5);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        //console.log("creating stone wall");

        // Send the entity for this entity to all the players in this zone.
        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        // Increment the order structures counter.
        this.zone.order.structuresCount+=1;

    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        // Decrement the zone order structures counter.
        this.zone.order.structuresCount-=1;
    }
}

_StoneWall.prototype.entityType = 'StoneWall';
_StoneWall.prototype.entityCategory = ECS.Categories.STRUCTURE;
_StoneWall.prototype.pool = [];

_StoneWall.prototype.entityIndex = null;

ECS.assemblages._StoneWall = _StoneWall;
ECS.testEntities.StoneWall = ECS.addTestEntity(_StoneWall);

/**
 * Activates a StoneWall entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.StoneWall = function (config) {
    var entity = ECS.activateEntity(_StoneWall);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};