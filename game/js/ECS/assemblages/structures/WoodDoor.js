/**
 * Created by User on 05/02/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _WoodDoor extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 31, 31, true);
    }

    reset (zone, x, y) {
        this.setMaxHitPoints(300);
        //console.log("in barrier reset");
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        this.reposition(x+0.5, y+0.5);
        this.setOperating(false);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        // Send the details for this entity to all the players in this zone.
        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        // Increment the order structures counter.
        this.zone.order.structuresCount+=1;

    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        // Decrement the zone order structures counter.
        this.zone.order.structuresCount-=1;
    }

    interaction () {
        //console.log("interacting with wood door");
        // Make sure there isn't anything in the way of the door before closing it.
        if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.WoodDoor, this.x, this.y, {[ECS.Categories.CHARACTER]: true, [ECS.Categories.MINION]: true})){
            //console.log("target loc is empty");
            // If the door is closed, open it.
            if(this.operating === true){
                this.setOperating(false);
                // Tell the players in this zone to show this door as open.
                ECS.io.in(this.zone.roomName).emit('command', {
                    command: 'open',
                    id: this.id
                });
            }
            else {
                this.setOperating(true);
                // Tell the players in this zone to show this door as open.
                ECS.io.in(this.zone.roomName).emit('command', {
                    command: 'close',
                    id: this.id
                });
            }
        }
        else {
            //console.log("target loc is blocked");
        }
    }

}

_WoodDoor.prototype.entityType = 'WoodDoor';
_WoodDoor.prototype.entityCategory = ECS.Categories.STRUCTURE;
_WoodDoor.prototype.pool = [];

_WoodDoor.prototype.entityIndex = null;

ECS.assemblages._WoodDoor = _WoodDoor;
ECS.testEntities.WoodDoor = ECS.addTestEntity(_WoodDoor);

/**
 * Activates a WoodDoor entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.WoodDoor = function (config) {
    var entity = ECS.activateEntity(_WoodDoor);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};