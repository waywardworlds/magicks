/**
 * Created by User on 12/01/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _OrderCore extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 60, 60, true);
    }

    reset (zone, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        // Some weirdness with entity body alignment where they
        // were taking up space in the adjacent grid cells,
        // so make them a bit smaller and offset.
        x = ((~~(x / 32)) * 32);
        y = ((~~(y / 32)) * 32);
        this.reposition(x+2, y+2);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        this.zone.order.members = {};
        this.zone.order.memberCount = 0;
        this.zone.order.maxMembers = 20;

        // Increment the order structures counter.
        this.zone.order.structuresCount+=1;
        this.zone.order.core = this;

        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        // Decrement the zone order structures counter.
        this.zone.order.structuresCount-=1;
        this.zone.order.core = null;

        // Create a list of the order members, to send to each member so they can reset each member.
        var member;
        var members = this.zone.order.members;
        var memberList = [];
        for(var key in members){
            if(members.hasOwnProperty(key)){
                member = members[key];
                member.setOrder(null);
                memberList.push(member.id);
            }
        }
        // Loop again to send the list to each member.
        for(var key in members){
            if(members.hasOwnProperty(key)){
                // Only send to players.
                if(members[key].socket !== undefined){
                    members[key].socket.emit('order_destroyed', memberList);
                }
            }
        }
    }

    addOrderMember (entity) {
        // Check this entity isn't already in an order.
        if(entity.order !== null){
            return;
        }
        // Check there is space for another member in this order.
        if(this.zone.order.memberCount  >= this.zone.order.maxMembers){
            return false;
        }
        // Check the entity belongs to a player. Might want to have NPCs able to join orders.
        if(entity.socket){
            // Send the player that joined the order a list of other members of the order.
            var memberList = [];
            var member;
            var members = this.zone.order.members;
            for(var key in members){
                if(members.hasOwnProperty(key)){
                    member = members[key];
                    // Tell everyone in the order who is a player, except the new member, who just joined.
                    if(member.socket !== undefined){
                        //console.log("entity has socket");
                        // Send them the id of the player that joined. The clients should already have the display name.
                        member.socket.emit('add_order_member', entity.id);
                    }
                    // Add the name of this member to the list.
                    memberList.push(member.id);
                }
            }
            // Add this player to the order after the existing members are told about them, or they would be sent their own details twice.
            entity.setOrder(this);
            members[entity.id] = entity;
            this.zone.order.memberCount +=1;
            // Add this entity id onto the list of member ids.
            memberList.push(entity.id);
            // Tell the player they successfully joined the order.
            entity.socket.emit('join_order_success', memberList);

        }
        //console.log("");
        //console.log("order member added: " + entity.id);
        //console.log("current members: ");
        //console.log(memberList);

    }

    removeOrderMember (entity) {
        // Check this entity is even in this order.
        var members = this.zone.order.members;
        if(members[entity.id] === undefined){
            console.log("* WARNING: Trying to remove entity from an order it is not a member of.");
            return;
        }
        // Check the entity belongs to a player. Might want to have NPCs able to join orders.
        if(entity.socket){
            entity.setOrder(null);
            delete members[entity.id];

            this.zone.order.memberCount -=1;

            // Tell every member of this order that this member has left.
            var member;
            for(var key in members){
                if(members.hasOwnProperty(key)){
                    member = members[key];
                    // Tell everyone in the order who is a player, except the new member, who just joined.
                    if(member.socket !== undefined){
                        //console.log("entity has socket");
                        // Send them the id of the player that joined. The clients should already have the display name.
                        member.socket.emit('remove_order_member', entity.id);
                    }
                }
            }

        }
        //console.log("");
        //console.log("order member removed: " + entity.id);
    }

}

_OrderCore.prototype.entityType = 'OrderCore';
_OrderCore.prototype.entityCategory = ECS.Categories.STRUCTURE;
_OrderCore.prototype.pool = [];

_OrderCore.prototype.maxHitPoints = 300;
_OrderCore.prototype.entityIndex = null;

ECS.assemblages._OrderCore = _OrderCore;
ECS.testEntities.OrderCore = ECS.addTestEntity(_OrderCore);

/**
 * Activates an order core entity.
 * @param {Zone} config.zone - The zone that this order core will claim.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.OrderCore = function (config) {
    var entity = ECS.activateEntity(_OrderCore);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};