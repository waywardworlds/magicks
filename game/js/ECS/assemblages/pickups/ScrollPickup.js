/**
 * Created by User on 06/02/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _ScrollPickup extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (zone, x, y, itemConfig) {
        this.setZone(zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.setItemType(ECS.InventoryItemTypes.SCROLL);

        this.itemConfig = itemConfig;

        // How long the pickup should last before disappearing.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 15000);

        ECS.io.in(this.zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        this.checkPhysics();
    }

}

_ScrollPickup.prototype.entityType = 'ScrollPickup';
_ScrollPickup.prototype.entityCategory = ECS.Categories.PICKUP;
_ScrollPickup.prototype.pool = [];

_ScrollPickup.prototype.maxSpeed = 10;
_ScrollPickup.prototype.flying = true;
_ScrollPickup.prototype.entityIndex = null;

_ScrollPickup.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    pickup: function (thisEntity) {
        //console.log("intersecting other pickup");
        thisEntity.pushBodyBack();
        //otherEntity.pushBodyBack();
    }
};

ECS.assemblages._ScrollPickup = _ScrollPickup;
ECS.testEntities.ScrollPickup = ECS.addTestEntity(_ScrollPickup);

/**
 * Activates a ScrollPickup entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @param {Number} config.itemConfig - The config of this item. Should have the code of the spell that this scroll is for.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.ScrollPickup = function (config) {
    var entity = ECS.activateEntity(_ScrollPickup);
    entity.reset(config.zone, config.x, config.y, config.itemConfig);
    return entity;
};