/**
 * Created by User on 08/01/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _WoodPickup extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (zone, x, y) {
        this.setZone(zone);
        this.reposition(x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.setItemType(ECS.InventoryItemTypes.WOOD);

        // How long the pickup should last before disappearing.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 10000);

        ECS.io.in(this.zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        this.checkPhysics();
    }

}

_WoodPickup.prototype.entityType = 'WoodPickup';
_WoodPickup.prototype.entityCategory = ECS.Categories.PICKUP;
_WoodPickup.prototype.pool = [];

_WoodPickup.prototype.maxSpeed = 10;
_WoodPickup.prototype.flying = true;
_WoodPickup.prototype.entityIndex = null;

_WoodPickup.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    obstacle: function (thisEntity) {
        thisEntity.pushBodyBack();
    },

    pickup: function (thisEntity) {
        //console.log("intersecting other pickup");
        thisEntity.pushBodyBack();
        //otherEntity.pushBodyBack();
    }
};

ECS.assemblages._WoodPickup = _WoodPickup;
ECS.testEntities.WoodPickup = ECS.addTestEntity(_WoodPickup);

/**
 * Activates a WoodPickup entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.WoodPickup = function (config) {
    var entity = ECS.activateEntity(_WoodPickup);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};