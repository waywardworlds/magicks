/**
 * Created by User on 06/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _HealOrb extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 2000);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_HealOrb.prototype.entityType = 'HealOrb';
_HealOrb.prototype.entityCategory = ECS.Categories.PROJECTILE;
_HealOrb.prototype.pool = [];

_HealOrb.prototype.maxSpeed = 10;
_HealOrb.prototype.element = ECS.Elements.LIGHT;
_HealOrb.prototype.countersElement = ECS.Elements.DARK;
_HealOrb.prototype.flying = true;
_HealOrb.prototype.entityIndex = null;

_HealOrb.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity) {
        thisEntity.deactivate();
    },

    character: function (thisEntity, otherEntity) {
        //console.log("hit player, " + thisEntity.entityType);
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        // Heal the character.
        otherEntity.healEntity(100, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        // Don't damage casters own minions.
        if(thisEntity.caster === otherEntity.caster){
            return;
        }
        // Damage dark minions instead of healing them.
        if(otherEntity.element === ECS.Elements.DARK){
            otherEntity.damageEntity(100, thisEntity.caster);
        }
        // Heal other kinds of minions.
        else {
            otherEntity.healEntity(100);
        }
        thisEntity.deactivate();
    },

    projectile: function (thisEntity, otherEntity) {
        //console.log("heal orb collided raven strike");
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        // Light projectiles can damage dark obstacles.
        if(otherEntity.element === ECS.Elements.DARK){
            otherEntity.damageEntity(100);
        }
        thisEntity.deactivate();
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._HealOrb = _HealOrb;
ECS.testEntities.HealOrb = ECS.addTestEntity(_HealOrb);

/**
 * Activates a HealOrb entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.HealOrb = function (config) {
    var entity = ECS.activateEntity(_HealOrb);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};



class _HealOrbSmall extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 2000);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_HealOrbSmall.prototype.entityType = 'HealOrbSmall';
_HealOrbSmall.prototype.entityCategory = ECS.Categories.PROJECTILE;
_HealOrbSmall.prototype.pool = [];

_HealOrbSmall.prototype.maxSpeed = 14;
_HealOrbSmall.prototype.element = ECS.Elements.LIGHT;
_HealOrbSmall.prototype.countersElement = ECS.Elements.DARK;
_HealOrbSmall.prototype.flying = true;
_HealOrbSmall.prototype.entityIndex = null;

_HealOrbSmall.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity) {
        thisEntity.deactivate();
    },

    character: function (thisEntity, otherEntity) {
        //console.log("hit player, " + thisEntity.entityType);
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        // Heal the character.
        otherEntity.healEntity(100, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        // Don't damage casters own minions.
        if(thisEntity.caster === otherEntity.caster){
            return;
        }
        // Damage dark minions instead of healing them.
        if(otherEntity.element === ECS.Elements.DARK){
            otherEntity.damageEntity(100, thisEntity.caster);
        }
        // Heal other kinds of minions.
        else {
            otherEntity.healEntity(100);
        }
        thisEntity.deactivate();
    },

    projectile: function (thisEntity, otherEntity) {
        //console.log("heal orb collided raven strike");
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        // Light projectiles can damage dark obstacles.
        if(otherEntity.element === ECS.Elements.DARK){
            otherEntity.damageEntity(100);
        }
        thisEntity.deactivate();
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._HealOrbSmall = _HealOrbSmall;
ECS.testEntities.HealOrbSmall = ECS.addTestEntity(_HealOrbSmall);

/**
 * Activates a HealOrbSmall entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.HealOrbSmall = function (config) {
    var entity = ECS.activateEntity(_HealOrbSmall);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};