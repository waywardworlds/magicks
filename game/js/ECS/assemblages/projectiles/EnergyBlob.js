/**
 * Created by User on 10/02/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _EnergyBlob extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.body.rotation = 0;

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_EnergyBlob.prototype.entityType = 'EnergyBlob';
_EnergyBlob.prototype.entityCategory = ECS.Categories.PROJECTILE;
_EnergyBlob.prototype.pool = [];

_EnergyBlob.prototype.maxSpeed = 15;
//_EnergyBlob.prototype.element = ECS.Elements.PSYCHIC;
//_EnergyBlob.prototype.countersElement = ECS.Elements.LIGHT;
_EnergyBlob.prototype.flying = true;
_EnergyBlob.prototype.entityIndex = null;

_EnergyBlob.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){

            }
            else {
                thisEntity.deactivate();
            }
        }
        else {
            thisEntity.deactivate();
        }
    },

    character: function (thisEntity, otherEntity) {
        // Give the character that this hit some energy.
        otherEntity.modEnergy(+50);

        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity) {
        thisEntity.deactivate();
    },

    obstacle: function (thisEntity) {
        thisEntity.deactivate();
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._EnergyBlob = _EnergyBlob;
ECS.testEntities.EnergyBlob = ECS.addTestEntity(_EnergyBlob);

/**
 * Activates a EnergyBlob entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.EnergyBlob = function (config) {
    var entity = ECS.activateEntity(_EnergyBlob);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};