/**
 * Created by User on 06/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Flamethrower extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        //console.log("resetting flamethrower, casterEntity:");
        //console.log(casterEntity);
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 600);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_Flamethrower.prototype.entityType = 'Flamethrower';
_Flamethrower.prototype.entityCategory = ECS.Categories.PROJECTILE;
_Flamethrower.prototype.pool = [];

_Flamethrower.prototype.maxSpeed = 10; // TODO: add attackPower to other projectiles and minions, etc. use that internally in the intersectsWith funcs.
_Flamethrower.prototype.element = ECS.Elements.FIRE;
_Flamethrower.prototype.countersElement = ECS.Elements.EARTH;
_Flamethrower.prototype.flying = true;
_Flamethrower.prototype.entityIndex = null;

_Flamethrower.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.structure(thisEntity, otherEntity, 10);
    },

    character: function (thisEntity, otherEntity) {
        //console.log("hit player, " + thisEntity.entityType);
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        otherEntity.damageEntity(4, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        //this.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        // Don't use intersectsWith.minion(...) or the projectile will be deactivated.
        // This is basically the same, but without the deactivation, so it can be sprayed over multiple entities.

        // Don't damage casters own minions.
        if(thisEntity.caster === otherEntity.caster){
            return;
        }
        if(thisEntity.countersElement === otherEntity.element){
            otherEntity.damageEntity(8, otherEntity.caster);
        }
        else if(otherEntity.countersElement === thisEntity.element){
            otherEntity.damageEntity(2, otherEntity.caster);
        }
        else {
            otherEntity.damageEntity(4, otherEntity.caster);
        }
        // Set the target of the minion to be the caster of the projectile that damaged it.
        otherEntity.target = thisEntity.caster;
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.obstacle(thisEntity, otherEntity, 10);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._Flamethrower = _Flamethrower;
ECS.testEntities.Flamethrower = ECS.addTestEntity(_Flamethrower);

/**
 * Activates a Flamethrower entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.Flamethrower = function (config) {
    var entity = ECS.activateEntity(_Flamethrower);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};