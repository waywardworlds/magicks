/**
 * Created by User on 06/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _TeleOrb extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 3000);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_TeleOrb.prototype.entityType = 'TeleOrb';
_TeleOrb.prototype.entityCategory = ECS.Categories.PROJECTILE;
_TeleOrb.prototype.pool = [];

_TeleOrb.prototype.maxSpeed = 6;
_TeleOrb.prototype.element = ECS.Elements.VOID;
_TeleOrb.prototype.countersElement = '';
_TeleOrb.prototype.flying = true;
_TeleOrb.prototype.entityIndex = null;

function teleport (thisEntity) {

    //ADD THIS IF THE PROJECTILE IS MADE TO GO THROUGH EXITS TO OTHER ZONES.

    // Check if the tele orb is in a different zone to the caster.
    // The caster could have cast the tele-orb, then when though an exit to
    // another zone, then the tele-orb hits something in the previous zone.
    if(thisEntity.zone.index !== thisEntity.caster.zone.index){
        //console.log("tele orb is in a different zone, move caster there");
        // Move the caster to the zone of the tele orb. It won't be repositioned.
        thisEntity.zone.moveEntityToZone(thisEntity.caster);
    }

    // Move the caster to the location that the projectile hit.
    thisEntity.caster.reposition(thisEntity.body.centerX - ECS.crashResponse.overlapV.x, thisEntity.body.centerY - ECS.crashResponse.overlapV.y);
    // Stop the caster from moving for a short while, so they won't pass through walls.
    thisEntity.caster.setCanMove(false);
    // Start a timer to let the caster move again.
    setTimeout(function () {
        //console.log("caster can now move again");
        this.setCanMove(true);
    }.bind(thisEntity.caster), 400);

    // Projectile consumed, deactivate the entity.
    thisEntity.deactivate();
}

_TeleOrb.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        teleport(thisEntity);
    },

    structure: function (thisEntity) {
        teleport(thisEntity);
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        teleport(thisEntity);
    },

    minion: function (thisEntity) {
        teleport(thisEntity);
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        otherEntity.deactivate();
        teleport(thisEntity);
    },

    obstacle: function (thisEntity) {
        teleport(thisEntity);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._TeleOrb = _TeleOrb;
ECS.testEntities.TeleOrb = ECS.addTestEntity(_TeleOrb);

/**
 * Activates a TeleOrb entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.TeleOrb = function (config) {
    var entity = ECS.activateEntity(_TeleOrb);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};