/**
 * Created by User on 04/02/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Deflector extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 600);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_Deflector.prototype.entityType = 'Deflector';
_Deflector.prototype.entityCategory = ECS.Categories.PROJECTILE;
_Deflector.prototype.pool = [];

_Deflector.prototype.maxSpeed = 16;
_Deflector.prototype.element = ECS.Elements.VOID;
_Deflector.prototype.countersElement = '';
_Deflector.prototype.flying = true;
_Deflector.prototype.entityIndex = null;

_Deflector.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        if(otherEntity.operating === false){
            if(otherEntity.entityType !== ECS.assemblages._WoodDoor.prototype.entityType){
                thisEntity.deactivate();
            }
        }
        else {
            thisEntity.deactivate();
        }
    },

    character: function (thisEntity) {
        thisEntity.deactivate();
    },

    minion: function (thisEntity) {
        thisEntity.deactivate();
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }

        // Send the other entity moving in the opposite direction.
        otherEntity.body.velocity.x *= -1;
        otherEntity.body.velocity.y *= -1;
        // Flip the rotation of the entity.
        otherEntity.body.rotation += Math.PI;

        // Need to manually tell the clients to update the rotation of the sprite.
        ECS.io.in(otherEntity.zone.roomName).emit('rotate', {
            id: otherEntity.id,
            rotation: otherEntity.body.rotation
        });

        thisEntity.deactivate();
    },

    obstacle: function (thisEntity) {
        thisEntity.deactivate();
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._Deflector = _Deflector;
ECS.testEntities.Deflector = ECS.addTestEntity(_Deflector);

/**
 * Activates a Deflector entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.Deflector = function (config) {
    var entity = ECS.activateEntity(_Deflector);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};