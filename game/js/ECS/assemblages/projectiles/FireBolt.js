/**
 * Created by User on 19/07/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _FireBolt extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_FireBolt.prototype.entityType = 'FireBolt';
_FireBolt.prototype.entityCategory = ECS.Categories.PROJECTILE;
_FireBolt.prototype.pool = [];

_FireBolt.prototype.maxSpeed = 12;
_FireBolt.prototype.attackPower = 60;
_FireBolt.prototype.element = ECS.Elements.FIRE;
_FireBolt.prototype.countersElement = ECS.Elements.EARTH;
_FireBolt.prototype.flying = true;
_FireBolt.prototype.entityIndex = null;

_FireBolt.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.structure(thisEntity, otherEntity, 10);
    },

    character: function (thisEntity, otherEntity) {
        //console.log("hit player, " + thisEntity.entityType);
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        otherEntity.damageEntity(thisEntity.attackPower, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        // Don't damage caster's own minions.
        if(thisEntity.caster === otherEntity.caster){
            return;
        }

        thisEntity.intersectsWith.minion(thisEntity, otherEntity);
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.obstacle(thisEntity, otherEntity);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._FireBolt = _FireBolt;
ECS.testEntities.FireBolt = ECS.addTestEntity(_FireBolt);

/**
 * Activates a FireBolt entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.FireBolt = function (config) {
    var entity = ECS.activateEntity(_FireBolt);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};