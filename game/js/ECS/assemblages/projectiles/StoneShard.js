/**
 * Created by User on 19/07/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _StoneShard extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.body.rotation = 0;

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 500);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_StoneShard.prototype.entityType = 'StoneShard';
_StoneShard.prototype.entityCategory = ECS.Categories.PROJECTILE;
_StoneShard.prototype.pool = [];

_StoneShard.prototype.maxSpeed = 14;
_StoneShard.prototype.element = ECS.Elements.EARTH;
_StoneShard.prototype.countersElement = ECS.Elements.PSYCHIC;
_StoneShard.prototype.flying = true;
_StoneShard.prototype.entityIndex = null;

_StoneShard.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.structure(thisEntity, otherEntity, 10);
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        otherEntity.damageEntity(20, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.minion(thisEntity, otherEntity, 20);
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.obstacle(thisEntity, otherEntity, 20);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._StoneShard = _StoneShard;
ECS.testEntities.StoneShard = ECS.addTestEntity(_StoneShard);

/**
 * Activates a StoneShard entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.StoneShard = function (config) {
    var entity = ECS.activateEntity(_StoneShard);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};