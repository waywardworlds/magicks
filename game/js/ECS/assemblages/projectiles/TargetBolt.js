/**
 * Created by User on 19/07/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _TargetBolt extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.body.rotation = 0;

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_TargetBolt.prototype.entityType = 'TargetBolt';
_TargetBolt.prototype.entityCategory = ECS.Categories.PROJECTILE;
_TargetBolt.prototype.pool = [];

_TargetBolt.prototype.maxSpeed = 18;
_TargetBolt.prototype.element = ECS.Elements.DARK;
_TargetBolt.prototype.countersElement = ECS.Elements.WATER;
_TargetBolt.prototype.flying = true;
_TargetBolt.prototype.entityIndex = null;

_TargetBolt.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        // Don't target structures that belong to the caster's order.
        if(thisEntity.caster.order !== otherEntity.zone.order.core
        || otherEntity.zone.order.core === null){
            // Set the minions of the caster of this projectile to target the entity that was hit.
            var minions = thisEntity.caster.minions;
            for(var minionId in minions){
                if(minions.hasOwnProperty(minionId)){
                    minions[minionId].target = otherEntity;
                }
            }
        }
        else {
            console.log("target structure is in same order");
        }

        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        if(otherEntity.id === thisEntity.caster.id){
            // Projectile consumed, deactivate the entity.
            thisEntity.deactivate();
            return;
        }

        // Set the minions of the caster of this projectile to target the character that was hit.
        var minions = thisEntity.caster.minions;
        for(var minionId in minions){
            if(minions.hasOwnProperty(minionId)){
                minions[minionId].target = otherEntity;
            }
        }

        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        // Don't target caster's own minions.
        if(thisEntity.caster === otherEntity.caster){
            return;
        }

        // Set the minions of the caster of this projectile to target the minion that was hit.
        var minions = thisEntity.caster.minions;
        for(var minionId in minions){
            if(minions.hasOwnProperty(minionId)){
                minions[minionId].target = otherEntity;
            }
        }

        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        // Set the minions of the caster of this projectile to target the OBSTACLE that was hit.
        var minions = thisEntity.caster.minions;
        for(var minionId in minions){
            if(minions.hasOwnProperty(minionId)){
                minions[minionId].target = otherEntity;
            }
        }

        thisEntity.deactivate();
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._TargetBolt = _TargetBolt;
ECS.testEntities.TargetBolt = ECS.addTestEntity(_TargetBolt);

/**
 * Activates a TargetBolt entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.TargetBolt = function (config) {
    var entity = ECS.activateEntity(_TargetBolt);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};