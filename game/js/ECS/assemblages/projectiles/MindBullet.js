/**
 * Created by User on 06/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _MindBullet extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.body.rotation = 0;

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_MindBullet.prototype.entityType = 'MindBullet';
_MindBullet.prototype.entityCategory = ECS.Categories.PROJECTILE;
_MindBullet.prototype.pool = [];

_MindBullet.prototype.maxSpeed = 20;
_MindBullet.prototype.element = ECS.Elements.PSYCHIC;
_MindBullet.prototype.countersElement = ECS.Elements.LIGHT;
_MindBullet.prototype.flying = true;
_MindBullet.prototype.entityIndex = null;

_MindBullet.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.structure(thisEntity, otherEntity, 10);
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity === thisEntity.caster){
        //    return;
        //}

        otherEntity.damageEntity(20, thisEntity.caster);

        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.minion(thisEntity, otherEntity, 20);
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.obstacle(thisEntity, otherEntity, 20);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._MindBullet = _MindBullet;
ECS.testEntities.MindBullet = ECS.addTestEntity(_MindBullet);

/**
 * Activates a MindBullet entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.MindBullet = function (config) {
    var entity = ECS.activateEntity(_MindBullet);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};