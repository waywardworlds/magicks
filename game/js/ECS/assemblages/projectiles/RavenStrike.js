/**
 * Created by User on 21/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _RavenStrike extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.body.rotation = 0;

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_RavenStrike.prototype.entityType = 'RavenStrike';
_RavenStrike.prototype.entityCategory = ECS.Categories.PROJECTILE;
_RavenStrike.prototype.pool = [];

_RavenStrike.prototype.maxSpeed = 14;
_RavenStrike.prototype.element = ECS.Elements.DARK;
_RavenStrike.prototype.countersElement = ECS.Elements.WATER;
_RavenStrike.prototype.flying = true;
_RavenStrike.prototype.entityIndex = null;

_RavenStrike.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.structure(thisEntity, otherEntity, 10);
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}
        otherEntity.damageEntity(20, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.minion(thisEntity, otherEntity, 20);
    },

    projectile: function (thisEntity, otherEntity) {
        //console.log("raven strike collided heal orb");
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.obstacle(thisEntity, otherEntity, 20);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._RavenStrike = _RavenStrike;
ECS.testEntities.RavenStrike = ECS.addTestEntity(_RavenStrike);

/**
 * Activates a RavenStrike entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.RavenStrike = function (config) {
    var entity = ECS.activateEntity(_RavenStrike);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};