/**
 * Created by User on 10/02/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');
var spells = require('../../../spells/Spells');

class _DrainingBolt extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 8);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        this.body.rotation = 0;

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_DrainingBolt.prototype.entityType = 'DrainingBolt';
_DrainingBolt.prototype.entityCategory = ECS.Categories.PROJECTILE;
_DrainingBolt.prototype.pool = [];

_DrainingBolt.prototype.maxSpeed = 20;
_DrainingBolt.prototype.element = ECS.Elements.PSYCHIC;
_DrainingBolt.prototype.countersElement = ECS.Elements.LIGHT;
_DrainingBolt.prototype.flying = true;
_DrainingBolt.prototype.entityIndex = null;

_DrainingBolt.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){

            }
            else {
                thisEntity.deactivate();
            }
        }
        else {
            thisEntity.deactivate();
        }
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity === thisEntity.caster){
        //    return;
        //}

        otherEntity.damageEntity(5, thisEntity.caster);

        if(otherEntity.energy > 50){
            otherEntity.modEnergy(-50);
            var thisCasterX = thisEntity.caster.body.centerX,
                thisCasterY = thisEntity.caster.body.centerY;

            var projectile = ECS.assemblages.EnergyBlob({casterEntity: thisEntity.caster, x: otherEntity.body.centerX, y: otherEntity.body.centerY});
            // Move the energy blob away from the entity it is taken from, or it will go straight back to them.
            projectile.repositionAwayFromEntity(otherEntity, thisCasterX, thisCasterY, 8);
            // Start the projectile moving.
            projectile.body.rotation = ECS.PhaserRips.Physics.moveToXY(projectile.body, thisCasterX, thisCasterY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id:         projectile.id,
                rotation:   projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });

        }

        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity) {
        thisEntity.deactivate();
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity) {
        thisEntity.deactivate();
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._DrainingBolt = _DrainingBolt;
ECS.testEntities.DrainingBolt = ECS.addTestEntity(_DrainingBolt);

/**
 * Activates a DrainingBolt entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.DrainingBolt = function (config) {
    var entity = ECS.activateEntity(_DrainingBolt);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};