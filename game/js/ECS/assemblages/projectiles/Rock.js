/**
 * Created by User on 06/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Rock extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setCircleBody(undefined, undefined, 12);
    }

    reset (casterEntity, x, y) {
        this.setCaster(casterEntity);
        this.setZone(casterEntity.zone);
        this.repositionAwayFromEntity(casterEntity, x, y);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, true);

        // How long the projectile should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 1400);
    }

    update () {
        this.move();
        this.checkPhysics();
    }

}

_Rock.prototype.entityType = 'Rock';
_Rock.prototype.entityCategory = ECS.Categories.PROJECTILE;
_Rock.prototype.pool = [];

_Rock.prototype.maxSpeed = 14;
_Rock.prototype.element = ECS.Elements.EARTH;
_Rock.prototype.countersElement = ECS.Elements.PSYCHIC;
_Rock.prototype.flying = true;
_Rock.prototype.entityIndex = null;

_Rock.prototype.intersectFuncs = {
    solid: function (thisEntity) {
        thisEntity.deactivate();
    },

    structure: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.structure(thisEntity, otherEntity, 10);
    },

    character: function (thisEntity, otherEntity) {
        // Don't intersect this projectile with the entity that cast it.
        //if(otherEntity.id === thisEntity.caster.id){
        //    return;
        //}

        // Bump the character entity a bit.
        var overlap = ECS.crashResponse.overlapV;
        if(otherEntity.body.immovable === false){
            // x100, so full movement speed value in that direction. If it is more than maxSpeed, setVelocity will cap it.
            otherEntity.setVelocity({x: overlap.x * 100, y: overlap.y * 100});
            // This entity is now being pushed.
            otherEntity.setBeingPushed(true);
            // How long this push effect should last for.
            setTimeout(function () {
                // Reset the velocity.
                otherEntity.setVelocity();
                // This entity has stopped being pushed.
                otherEntity.setBeingPushed(false);
            }, 600);
        }

        otherEntity.damageEntity(20, thisEntity.caster);
        // Projectile consumed, deactivate the entity.
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity) {
        // Bump the minion entity a bit.
        var overlap = ECS.crashResponse.overlapV;
        if(otherEntity.body.immovable === false){
            // x100, so full movement speed value in that direction. If it is more than maxSpeed, setVelocity will cap it.
            otherEntity.setVelocity({x: overlap.x * 100, y: overlap.y * 100});
            // This entity is now being pushed.
            otherEntity.setBeingPushed(true);
            // How long this push effect should last for.
            setTimeout(function () {
                // Reset the velocity.
                otherEntity.setVelocity();
                // This entity has stopped being pushed.
                otherEntity.setBeingPushed(false);
            }, 600);
        }

        thisEntity.intersectsWith.minion(thisEntity, otherEntity, 20);
    },

    projectile: function (thisEntity, otherEntity) {
        // Check the entities are different. Only need to do it for this category code, as that is what this entity is.
        if(thisEntity.id === otherEntity.id){
            return;
        }
        thisEntity.intersectsWith.projectile(thisEntity, otherEntity);
    },

    obstacle: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.obstacle(thisEntity, otherEntity, 20);
    },

    inverter: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.inverter(thisEntity, otherEntity);
    },

    exit: function (thisEntity, otherEntity) {
        thisEntity.intersectsWith.portal(thisEntity, otherEntity);
    }
};

ECS.assemblages._Rock = _Rock;
ECS.testEntities.Rock = ECS.addTestEntity(_Rock);

/**
 * Activates a Rock entity.
 * @param {Entity} config.casterEntity - The entity that cast cast the spell that called this function.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {Entity} The entity that was activated.
 */
ECS.assemblages.Rock = function (config) {
    var entity = ECS.activateEntity(_Rock);
    entity.reset(config.casterEntity, config.x, config.y);
    return entity;
};
