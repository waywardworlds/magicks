/**
 * Created by User on 06/05/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Barrier extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 30, 30, true);
    }

    reset (zone, x, y) {
        //console.log("in barrier reset");
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        this.reposition(x+1, y+1);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);
        // How long the OBSTACLE should last.
        this.lifespanTimeout = setTimeout(this.deactivate.bind(this), 60000);
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();
    }

}

_Barrier.prototype.entityType = 'Barrier';
_Barrier.prototype.entityCategory = ECS.Categories.OBSTACLE;
_Barrier.prototype.pool = [];

_Barrier.prototype.element = ECS.Elements.EARTH;
_Barrier.prototype.countersElement = ECS.Elements.PSYCHIC;
_Barrier.prototype.maxHitPoints = 200;
_Barrier.prototype.entityIndex = null;

ECS.assemblages._Barrier = _Barrier;
ECS.testEntities.Barrier = ECS.addTestEntity(_Barrier);

/**
 * Activates a Barrier entity.
 * @param {Zone} config.zone - The zone to put this entity in.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.Barrier = function (config) {
    var entity = ECS.activateEntity(_Barrier);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};