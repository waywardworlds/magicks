/**
 * Created by User on 08/01/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

function grow(){

    //this.growAttempCount += 1;
    //console.log("trying to grow ;1");
    // Remove this entity from the quadtree of the zone it is temporarily while the check below is done to see if there is space around to expand into,
    // otherwise this entity will block the next entity.
    this.zone.quadTree.remove(this);

    var isSpace = false;
    var targetX = this.x;
    var targetY = this.y;
    var blockList = {solid: true, obstacle: true, entrance: true, exit: true, character: true, minion: true, pickup: true, structure: true};

    // From top left.
    if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.BigTree, this.x-1, this.y-1, blockList)){
        isSpace = true;
        targetX = this.x-1;
        targetY = this.y-1;
    }
    // From top right.
    else if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.BigTree, this.x-1 - (this.body.width * 0.5), this.y-1, blockList)){
        isSpace = true;
        targetX = this.x-1 - (this.body.width * 0.5);
        targetY = this.y-1;
    }
    // From bottom left.
    else if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.BigTree, this.x-1, this.y-1 - (this.body.height * 0.5), blockList)){
        isSpace = true;
        targetX = this.x-1;
        targetY = this.y-1 - (this.body.height * 0.5);
    }
    // From bottom right.
    else if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.BigTree, this.x-1 - (this.body.width * 0.5), this.y-1 - (this.body.height * 0.5), blockList)){
        isSpace = true;
        targetX = this.x-1 - (this.body.width * 0.5);
        targetY = this.y-1 - (this.body.height * 0.5);
    }

    targetX = ((~~(targetX / 32)) * 32);
    targetY = ((~~(targetY / 32)) * 32);
    //console.log("planting at, x: " + targetX + ", y: " + targetY);
    // Add this entity back to the quadtree of this zone.
    this.zone.quadTree.push(this, false);

    if(isSpace === true){

        //console.log("I grew ;)");
        this.deactivate();

        // Decrement the zone resource counter.
        // Need to do this here as well as in onAllHitPointsLost,
        // or the count won't be reclaimed from this removed node.
        this.zone.resourceNodesCount-=1;

        ECS.assemblages.BigTree({zone: this.zone, x: targetX, y: targetY});

    }
    else {
        //console.log("couldn't grow ;(");
        //if(this.growAttempCount < 3){
            this.lifespanTimeout = setTimeout(grow.bind(this), 20000);
        //}
    }

}

class _SmallTree extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 60, 60, true);
    }

    reset (zone, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        // Some weirdness with tree alignment where they
        // were taking up space in the adjacent grid cells,dd
        // so make them a bit smaller and offset.
        this.reposition(x+2, y+2);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        this.lifespanTimeout = setTimeout(grow.bind(this), 20000);

        this.checkPhysics();

        // Increment the zone resource counter.
        this.zone.resourceNodesCount+=1;
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        var randInRange = ECS.PhaserRips.Math.randFloatInRange;
        var range = 16;
        // How much wood to drop.
        for(var i=0; i<8; i+=1){
            ECS.assemblages.WoodPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
        }

        // Decrement the zone resource counter.
        this.zone.resourceNodesCount-=1;
    }

}

_SmallTree.prototype.entityType = 'SmallTree';
_SmallTree.prototype.entityCategory = ECS.Categories.OBSTACLE;
_SmallTree.prototype.pool = [];

_SmallTree.prototype.maxHitPoints = 400;
_SmallTree.prototype.entityIndex = null;

_SmallTree.prototype.intersectFuncs = {
    pickup: function (thisEntity, otherEntity) {
        //console.log("intersecting other pickup");
        otherEntity.pushBodyBack(-2);
    }
};

ECS.assemblages._SmallTree = _SmallTree;
ECS.testEntities.SmallTree = ECS.addTestEntity(_SmallTree);

/**
 * Activates a small tree entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.SmallTree = function (config) {
    var entity = ECS.activateEntity(_SmallTree);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};