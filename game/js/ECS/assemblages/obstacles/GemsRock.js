/**
 * Created by User on 15/01/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _GemsRock extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 30, 30, true);
    }

    reset (zone, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        // Some weirdness with tree alignment where they
        // were taking up space in the adjacent grid cells,
        // so make them a bit smaller and offset.
        this.reposition(x+1, y+1);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        this.checkPhysics();

        // Send the entity for this entity to all the players in this zone.
        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        // Increment the zone resource counter.
        this.zone.resourceNodesCount+=1;

        //console.log("gems rock spawned");
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        // Decrement the zone resource counter.
        this.zone.resourceNodesCount-=1;

        var randInRange = ECS.PhaserRips.Math.randFloatInRange;
        var range = 8;
        // How many gems to drop.
        for(var i=0; i<4; i+=1){
            ECS.assemblages.GemsPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
        }
    }
}

_GemsRock.prototype.entityType = 'GemsRock';
_GemsRock.prototype.entityCategory = ECS.Categories.OBSTACLE;
_GemsRock.prototype.pool = [];

_GemsRock.prototype.maxHitPoints = 300;
_GemsRock.prototype.entityIndex = null;

_GemsRock.prototype.intersectFuncs = {
    pickup: function (thisEntity, otherEntity) {
        //console.log("intersecting other pickup");
        otherEntity.pushBodyBack(-2);
    }
};

ECS.assemblages._GemsRock = _GemsRock;
ECS.testEntities.GemsRock = ECS.addTestEntity(_GemsRock);

/**
 * Activates a GemsRock entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.GemsRock = function (config) {
    var entity = ECS.activateEntity(_GemsRock);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};