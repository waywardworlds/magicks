/**
 * Created by User on 06/02/2018.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _Shrine extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 62, 62, true);
    }

    reset (zone, x, y, element) {
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        this.setElement(element);
        // Some weirdness with entity alignment where they
        // were taking up space in the adjacent grid cells,
        // so make them a bit smaller and offset.
        this.reposition(x+1, y+1);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        // Send the entity for this entity to all the players in this zone.
        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY),
            element: this.element
        });

        // This zone now has a shrine.
        this.zone.shrine = this;

        //console.log("shrine spawned, elem: " + this.element);
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        // This zone now doesn't have a shrine.
        this.zone.shrine = null;
    }
}

_Shrine.prototype.entityType = 'Shrine';
_Shrine.prototype.entityCategory = ECS.Categories.OBSTACLE;
_Shrine.prototype.pool = [];

_Shrine.prototype.maxHitPoints = 1000;
_Shrine.prototype.entityIndex = null;

ECS.assemblages._Shrine = _Shrine;
ECS.testEntities.Shrine = ECS.addTestEntity(_Shrine);

/**
 * Activates a Shrine entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @param {Number} config.element - The element that this shrine will give spell scrolls of.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.Shrine = function (config) {
    var entity = ECS.activateEntity(_Shrine);
    entity.reset(config.zone, config.x, config.y, config.element);
    return entity;
};