/**
 * Created by User on 24/12/2017.
 */

"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

class _BigTree extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 92, 92, true);
    }

    reset (zone, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        // Some weirdness with tree alignment where they
        // were taking up space in the adjacent grid cells,
        // so make them a bit smaller and offset.
        this.reposition(x+2, y+2);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        this.checkPhysics();

        // Increment the zone resource counter.
        this.zone.resourceNodesCount+=1;

        ECS.io.in(this.zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        //console.log("creating big tree, tarX: " + x/32 + ", tarY: " + y/32);
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        var randInRange = ECS.PhaserRips.Math.randFloatInRange;
        var range = 24;
        // How much wood to drop.
        for(var i=0; i<16; i+=1){
            ECS.assemblages.WoodPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
        }

        // Decrement the zone resource counter.
        this.zone.resourceNodesCount-=1;
    }


}

_BigTree.prototype.entityType = 'BigTree';
_BigTree.prototype.entityCategory = ECS.Categories.OBSTACLE;
_BigTree.prototype.pool = [];

_BigTree.prototype.maxHitPoints = 800;
_BigTree.prototype.entityIndex = null;

_BigTree.prototype.intersectFuncs = {
    pickup: function (thisEntity, otherEntity) {
        //console.log("intersecting other pickup");
        otherEntity.pushBodyBack(-2);
    }
};

ECS.assemblages._BigTree = _BigTree;
ECS.testEntities.BigTree = ECS.addTestEntity(_BigTree);

/**
 * Activates a big tree entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.BigTree = function (config) {
    var entity = ECS.activateEntity(_BigTree);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};