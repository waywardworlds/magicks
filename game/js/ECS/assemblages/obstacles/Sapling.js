/**
 * Created by User on 06/01/2018.
 */
"use strict";

var ECS = require('../../ECS');
var Entity = require('../../Entity');

function grow(){

    //this.growAttempCount += 1;
    //console.log("trying to grow ;1");
    // Remove this entity from the quadtree of the zone it is temporarily while the check below is done to see if there is space around to expand into,
    // otherwise this entity will block the next entity.
    this.zone.quadTree.remove(this);

    var isSpace = false;
    var targetX = this.x;
    var targetY = this.y;
    var blockList = {solid: true, obstacle: true, entrance: true, exit: true, character: true, minion: true, pickup: true, structure: true};

    // From top left.
    if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.SmallTree, this.x-1, this.y-1, blockList)){
        isSpace = true;
        targetX = this.x-1;
        targetY = this.y-1;
    }
    // From top right.
    else if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.SmallTree, this.x-1 - this.body.width, this.y-1, blockList)){
        isSpace = true;
        targetX = this.x-1 - this.body.width;
        targetY = this.y-1;
    }
    // From bottom left.
    else if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.SmallTree, this.x-1, this.y-1 - this.body.height, blockList)){
        isSpace = true;
        targetX = this.x-1;
        targetY = this.y-1 - this.body.height;
    }
    // From bottom right.
    else if(ECS.isTargetLocationEmpty(this.zone, ECS.testEntities.SmallTree, this.x-1 - this.body.width, this.y-1 - this.body.height, blockList)){
        isSpace = true;
        targetX = this.x-1 - this.body.width;
        targetY = this.y-1 - this.body.height;
    }

    targetX = ((~~(targetX / 32)) * 32);
    targetY = ((~~(targetY / 32)) * 32);
    //console.log("planting at, x: " + targetX + ", y: " + targetY);
    // Add this entity back to the quadtree of this zone.
    this.zone.quadTree.push(this, false);

    if(isSpace === true){

        //console.log("I grew ;)");
        this.deactivate();

        // Decrement the zone resource counter.
        // Need to do this here as well as in onAllHitPointsLost,
        // or the count won't be reclaimed from this removed node.
        this.zone.resourceNodesCount-=1;

        var entity = ECS.assemblages.SmallTree({zone: this.zone, x: targetX, y: targetY});

        ECS.io.in(entity.zone.roomName).emit('add_entity', {
            typeNumber: entity.typeNumber,
            id: entity.id,
            x: Math.trunc(entity.body.centerX),
            y: Math.trunc(entity.body.centerY)
        });

    }
    else {
        //console.log("couldn't grow ;(");
        //if(this.growAttempCount < 3){
            this.lifespanTimeout = setTimeout(grow.bind(this), 20000);
        //}
        //else {
        //    this.deactivate();
        //}

    }

}

class _Sapling extends Entity {

    constructor () {
        // Create a new instance of the entity class.
        super();

        this.setRectangleBody(undefined, undefined, 30, 30, true);
    }

    reset (zone, x, y) {
        this.setHitPoints(this.maxHitPoints);
        this.setZone(zone);
        // Some weirdness with tree alignment where they
        // were taking up space in the adjacent grid cells,
        // so make them a bit smaller and offset.
        this.reposition(x+1, y+1);
        // Add the entity to the zone it was put in.
        this.zone.addToEntities(this, false);

        this.lifespanTimeout = setTimeout(grow.bind(this), 20000);

        this.checkPhysics();

        // Send the entity for this entity to all the players in this zone.
        ECS.io.in(zone.roomName).emit('add_entity', {
            typeNumber: this.typeNumber,
            id: this.id,
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        });

        // Increment the zone resource counter.
        this.zone.resourceNodesCount+=1;
    }

    onAllHitPointsLost () {
        // Tell all of the players in this zone to remove this entity.
        this.deactivate();

        // Decrement the zone resource counter.
        this.zone.resourceNodesCount-=1;

        // Saplings don't drop wood.

        //var randInRange = ECS.PhaserRips.Math.randFloatInRange;
        //var range = 8;
        //// How much wood to drop.
        //for(var i=0; i<4; i+=1){
        //    ECS.assemblages.WoodPickup({zone: this.zone, x: this.body.centerX + randInRange(-range, range), y: this.body.centerY + randInRange(-range, range)});
        //}
    }
}

_Sapling.prototype.entityType = 'Sapling';
_Sapling.prototype.entityCategory = ECS.Categories.OBSTACLE;
_Sapling.prototype.pool = [];

_Sapling.prototype.maxHitPoints = 200;
_Sapling.prototype.entityIndex = null;

_Sapling.prototype.intersectFuncs = {
    pickup: function (thisEntity, otherEntity) {
        //console.log("intersecting other pickup");
        otherEntity.pushBodyBack(-2);
    }
};

ECS.assemblages._Sapling = _Sapling;
ECS.testEntities.Sapling = ECS.addTestEntity(_Sapling);

/**
 * Activates a sapling entity.
 * @param {Zone} config.zone - The zone to put this entity into.
 * @param {Number} config.x - The x position to relocate the activated entity to.
 * @param {Number} config.y - The y position to relocate the activated entity to.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.assemblages.Sapling = function (config) {
    var entity = ECS.activateEntity(_Sapling);
    entity.reset(config.zone, config.x, config.y);
    return entity;
};