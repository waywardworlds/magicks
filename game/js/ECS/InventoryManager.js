/**
 * Created by User on 28/01/2018.
 */

var InventoryItemTypes = require('./InventoryItemTypes');

class InventoryManager {

    /**
     * Add an item to the inventory contents of this entity.
     * @param {Entity} entity - The entity that the inventory belongs to.
     * @param {Number} itemType - The type of item to add. See a list of valid item types in InventoryItemTypes.js.
     * @param {Object} [itemConfig] - Any special details about this item. Mainly used for spell scrolls.
     * @param {Number} [index] - A specific index to add this item to in the contents, otherwise the first empty slot will be used.
     * @returns {Boolean|Number} Returns false if inventory is full, or the index that the item was added at.
     */
    static addItem (entity, itemType, itemConfig, index) {

        if(itemType === InventoryItemTypes.SCROLL && itemConfig === undefined){
            throw "* ERROR: scrolls must also have a config object in InventoryManager.addItem";
        }

        var inventory = entity.inventory;
        var emptySlots = 0;

        // Assume the inventory is full to start, as if the inventory only has 1 empty slot, then the inventory WILL be full after adding this item.
        // Mark it as not full if more than 1 empty slot is found.
        inventory.isFull = true;

        if(index === undefined){
            // Search for empty slots in the contents.
            for(var i=0, len=inventory.maxContents; i<len; i+=1){
                if(inventory.contents[i] === null){
                    emptySlots+=1;
                    // If there is more than 1 empty slot, then the inventory WON'T be full after adding this item.
                    if(emptySlots > 1){
                        inventory.isFull = false;
                        break;
                    }
                    index = i;
                }
            }
            if(emptySlots === 0){
                // No empty slot found. Can't add item.
                return false;
            }
        }
        else {
            // Check for out of bounds array index.
            if(inventory.contents[index] === undefined){
                throw "* ERROR: invalid contents index given to InventoryManager.addItem";
            }
        }

        //console.log("adding item to entity inventory: " + itemType);
        inventory.contents[index] = {itemType: itemType, itemConfig: itemConfig || {}};

        // Return the index so it can be sent to the client if this was a player entity.
        return index;

    }

    /**
     * Remove the item from an entity's inventory at the given index.
     * @param {ECS.Entity} entity - The entity whose inventory to remove the item from.
     * @param {Number} index - The index in the inventory contents array.
     * @returns {Boolean} Whether the item was removed successfully.
     */
    static removeItemByIndex (entity, index) {
        if(entity.inventory.contents[index] === undefined){
            return false;
        }
        //console.log("removing item by index:");
        //console.log(entity.inventory.contents[index]);
        entity.inventory.contents[index] = null;
        entity.inventory.isFull = false;

        return true;
    }

    static removeItemByType (entity, itemType) {
        //console.log("removing item by type: " + itemType);

        var contents = entity.inventory.contents;

        for(var i=0, len=entity.inventory.maxContents; i<len; i+=1){
            if(contents[i].itemType === itemType){
                contents[i] = null;
                break;
            }
        }

        //console.log("removed item: " + itemType);

        entity.inventory.isFull = false;

        // Return the index of the item that was removed.
        return i;
    }

    /**
     * Remove multiple items from an entity's inventory of a given item type.
     * @param {ECS.Entity} entity - The entity whose inventory to remove the items from.
     * @param {Object} itemType - The type of item to remove.
     * @param {Number} [amount=1] - How many of this item to remove.
     * @returns {Array} An array of the indexes of the inventory contents array of the items that were removed.
     */
    static removeManyItemsByType (entity, itemType, amount) {

        //console.log("removing many items by type: " + itemType + ", amount: " + amount);

        var contents = entity.inventory.contents;

        amount = amount || 1;

        // A list of the indexes of the items that were removed.
        var indexes = [];

        for(var i=0, len=entity.inventory.maxContents; i<len; i+=1){
            if(contents[i] !== null){
                if(contents[i].itemType === itemType){
                    contents[i] = null;
                    //console.log("item removed: " + itemType.value);
                    indexes.push(i);
                    amount-=1;
                    if(amount === 0){
                        break;
                    }
                }
            }
        }

        //console.log("removed items: " + itemType);

        entity.inventory.isFull = false;

        //console.log("rmiby, indexes: " + indexes);
        return indexes;
    }

    /**
     * Count how many of a certain type of item this entity has in their inventory.
     * @param {Entity} entity - The entity that the inventory belongs to.
     * @param {Number} itemType - The type of item to count.
     * @returns {Number} How many were found.
     */
    static countItems (entity, itemType) {
        if(InventoryItemTypes[itemType.value] === undefined){
            throw "* ERROR: item to count in inventory is not in the valid items list in InventoryManager.countItems";
        }

        var count = 0;

        var inventory = entity.inventory;
        var contents = inventory.contents;
        for(var i= 0, len=inventory.maxContents; i<len; i+=1){
            if(contents[i] !== null){
                if(contents[i].itemType === itemType){
                    count += 1;
                }
            }
        }

        //console.log("countitems, total: " + count);
        return count;
    }


}

module.exports = InventoryManager;