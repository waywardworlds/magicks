/**
 * Created by User on 03/04/2017.
 */

require('./Entity');

require('./Components');
// These need to be defined first as other entities (namely spells) rely on a caster entity.
require('./assemblages/characters/CharacterHuman');
require('./assemblages/characters/Player');
require('./assemblages/characters/GoblinMage');

require('./assemblages/minions/Critter');
require('./assemblages/minions/Nimbus');

require('./assemblages/misc/Corpse');
require('./assemblages/misc/Entrance');
require('./assemblages/misc/Exit');
require('./assemblages/misc/Inverter');
require('./assemblages/misc/Solid');

require('./assemblages/obstacles/Barrier');
require('./assemblages/obstacles/SmallTree');
require('./assemblages/obstacles/BigTree');
require('./assemblages/obstacles/GemsRock');
require('./assemblages/obstacles/Sapling');
require('./assemblages/obstacles/SmallTree');
require('./assemblages/obstacles/Shrine');

require('./assemblages/pickups/GemsPickup');
require('./assemblages/pickups/WoodPickup');
require('./assemblages/pickups/ScrollPickup');

require('./assemblages/projectiles/Deflector');
require('./assemblages/projectiles/DrainingBolt');
require('./assemblages/projectiles/EnergyBlob');
require('./assemblages/projectiles/FireBolt');
require('./assemblages/projectiles/Flamethrower');
require('./assemblages/projectiles/HealOrb');
require('./assemblages/projectiles/MindBullet');
require('./assemblages/projectiles/RavenStrike');
require('./assemblages/projectiles/Rock');
require('./assemblages/projectiles/StoneShard');
require('./assemblages/projectiles/Splash');
require('./assemblages/projectiles/TargetBolt');
require('./assemblages/projectiles/TeleOrb');

require('./assemblages/structures/OrderCore');
require('./assemblages/structures/StoneWall');
require('./assemblages/structures/WoodWall');
require('./assemblages/structures/WoodDoor');
