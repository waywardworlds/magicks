/**
 * Created by User on 03/04/2017.
 */

var ECS = require("./ECS");

/**
 * The central entity object. Used as the prototype of all entities.
 * Any entity that has this as it's prototype should call ECS.Entity.call(this); so it is given a unique ID.
 * @return {ECS.Entity} The entity that was created.
 */
class Entity {

    constructor () {
        // Give this entity a unique id so it can be accessed later, without having to store references everywhere.
        this.id = ECS.entityCount;
        // Increase the id counter.
        ECS.entityCount += 1;

        // Whether this entity is active. It's faster to set this to false and reactivate it when another
        // entity of this type is needed, than to destroy it and create another one from scratch.
        // This is only used for recycling entities, not for any gameplay related stuff like checking physics.
        this._isActive = false;
    }

    toString () {
        return this.entityType;
    }

    print () {
        console.log(JSON.stringify(this, null, 4));
        return this;
    }

    /**
     * Deactivates this entity. Removes it from the zone it is in and returns it to the pool.
     */
    deactivate () {
        //console.log("deactivating entity, type: " + this.entityType + ", id: " + this.id);

        // Remove this entity from the zone it is in (and thus the world), and used when a client disconnects.
        if (this.entityIndex !== null) {

            // Stop the lifespanTimeout from being called if it has one.
            if (this.lifespanTimeout) {
                clearTimeout(this.lifespanTimeout);
            }

            // If the entity is in an order, remove them from it.
            if(this.order !== undefined){
                if(this.order !== null){
                    this.order.removeOrderMember(this);
                }
            }

            this.zone.removeFromEntities(this);

            //this.body.velocity.x = 0;
            //this.body.velocity.y = 0;

            //this.zone.quadTree.remove(this);

            // This entity is no longer being used. Return it to the pool for reuse.
            //this.isActive = false;

            // Remove the link to the entity object from the list of entities in this zone.
            //this.zone.entities[this.entityIndex] = null;

            //this.entityIndex = null;

            // Tell all players in this zone that this entity has been removed. Remove the sprite.
            this.zone.io.in(this.zone.roomName).emit('remove_entity', {
                id: this.id,
                x: Math.trunc(this.body.centerX),
                y: Math.trunc(this.body.centerY)
            });

            //console.log("end of entity deactivate, type: " + this.entityType);
        }
    }

    /**
     * Calls onAllHitPointsLost and onAllReviveProgressLost for an entity if it has them (usually some kind of character, or minion), and then removes it from the zone.
     * A more thorough kind of deactivation. The onAllHitPointsLost or onAllReviveProgressLost of the entity itself should in turn call Entity.deactivate, so it is removed from the zone.
     * If neither of them do, then one of those functions is probably wrong.
     */
    kill () {
        //console.log("killing entity, id: " + this.id);

        // Check to stop entities that are already dead from being killed again.
        if(this.hitPoints > 0 || this.reviveProgress > 0){
            if (this.onAllHitPointsLost !== undefined) {
                this.setHitPoints(0);
                this.onAllHitPointsLost();
                if (this.onAllReviveProgressLost !== undefined) {
                    this.setReviveProgress(0);
                    this.onAllReviveProgressLost();
                }
            }
        }
    }

    /**
     * Used for gradual transition of distance over time. Not for directly putting something somewhere else. See Entity.reposition.
     * Will only move an entity if they have an X or Y velocity that isn't falsy.
     * @returns {Boolean} Whether the entity has moved or not.
     */
    move () {
        var body = this.body;
        // Check this entity has a velocity, and therefore should move.
        if(body.velocity.x || body.velocity.y){

            //console.log("in entity move, id:" + this.id + ", x: " + this.body.centerX + ", y: " + this.body.centerY);

            //if(this.canMove !== undefined){
                if(this.canMove === false){
                    // Entity can't move. Return false.
                    return false;
                }
            //}

            var modifier = 1;

            if(this.flying !== true){
                modifier = this.updateOccupiedFloorTile();
                if(modifier === false){
                    return;
                }
            }

            if(this.nextCastTime !== undefined){
                // Use the recast delay as the slowdown duration.
                if(this.nextCastTime + 1000 > Date.now()) {
                    modifier = modifier * 0.5;
                }
            }

            if(this.floorInteractions !== undefined){
                this.floorInteractions();
            }

            //if(this.customMove !== undefined){
            //    this.customMove();
            //}

            //console.log('moving entity: ' + this.id);
            //console.log(this.body.velocity.x + ' :x|y: ' + this.body.velocity.y);
            // Move the body of this entity.
            //this.body.move(this.body.velocity.x, this.body.velocity.y);

            // Move the body of this entity at normal speed multiplied by any modifier.
            body.move(body.velocity.x * modifier, body.velocity.y * modifier);
            // The entity has moved. Set hasMoved to true, so it will get it's physics checked for it's new position.
            body.hasMoved = true;
            // Also, the clients need to know the new position, so tell the emitter to send details of this entity in the next emit.
            this.setPositionEmitted(false);
            // The body has moved. Update the center positions.
            this.updateBodyPositions();
            // The entity has moved. Return true.
            //console.log(" end of move, return true");
            return true;
        }
        //console.log(" end of move, return false");
        // The entity has not moved. Return false.
        return false;
    }

    /**
     * Relocate the body of this entity to a different position. Used for moving into entrance bounds, teleporting, resetting.
     * @param {Number} x
     * @param {Number} y
     */
    reposition (x, y) {
        this.body.moveTo(x, y);
        // The entity has moved. Set hasMoved to true, so it will get it's physics checked for it's new position.
        this.body.hasMoved = true;
        // Also, the clients need to know the new position, so tell the emitter to send details of this entity in the next emit.
        this.setPositionEmitted(false);
        // The body has moved. Update the center positions.
        this.updateBodyPositions();
    }

    /**
     * Repositions this entity to be on the perimeter of another entity, in the direction of the target X and Y.
     * Calculates the position on the circle (other entity's detectionRadius) this entity should be, which is then passed into Entity.reposition for this entity.
     * Commonly used by spell projectiles so they don't start on the middle of their caster.
     * Also used when dropping items, so they don't get picked back up straight away.
     * @param {ECS.Entity} otherEntity - The entity to move this entity away from.
     * @param {Number} targetX - The X position to be moved in the direction of.
     * @param {Number} targetY - The Y position to be moved in the direction of.
     * @param {Number} [extraRange=0] - How much further to move this entity away from the other entity.
     */
    repositionAwayFromEntity (otherEntity, targetX, targetY, extraRange) {
        //console.log("repositionAwayFromEntity");
        var pos = otherEntity.body.pos;
        var angle = Math.atan2(targetY - pos.y, targetX - pos.x);
        var distance = otherEntity.body.detectionRadius + this.body.detectionRadius + (extraRange || 0);
        this.reposition(
            Math.cos(angle) * distance + pos.x,
            Math.sin(angle) * distance + pos.y
        );
    }

    /**
     * Set the X and/or Y velocity of this entity.
     * Handles checks to cap the velocity in any direction to the max speed for this entity.
     * @param {Object} [velocity] - An object with x and/or y properties that are numbers. Pass in no parameters to set x and y velocities to 0 (stop the entity).
     */
    setVelocity (velocity) {
        // If undefined, reset velocity.
        if(velocity === undefined){
            this.body.velocity.x = 0;
            this.body.velocity.y = 0;
            return;
        }

        // Check a property X was given.
        if(velocity.x !== undefined){
            if(velocity.x === 0){
                this.body.velocity.x = 0;
            }
            // Velocity to set to is not 0.
            else {
                this.body.velocity.x = velocity.x;
            }
        }

        // Check a property Y was given.
        if(velocity.y !== undefined){
            if(velocity.y === 0){
                this.body.velocity.y = 0;
            }
            // Velocity to set to is not 0.
            else {
                this.body.velocity.y = velocity.y;
            }
        }

        // Check if this body now has a velocity that isn't 0 before checking if they are over their max speed.
        if(this.body.velocity.x || this.body.velocity.y){
            // Don't let the new velocities make the entity go faster than max speed.
            var vx, vy;
            vx = this.body.velocity.x;
            vy = this.body.velocity.y;
            if((vx*vx + vy*vy) > (this.maxSpeed * this.maxSpeed)){
                var angle = Math.atan2(vy, vx);
                vx = Math.cos(angle) * this.maxSpeed;
                vy = Math.sin(angle) * this.maxSpeed;
                this.body.velocity.x = vx;
                this.body.velocity.y = vy;
            }
        }
    }

    /**
     * Updates the position of the center of this entity's body and the x/y properties that the quadtree uses.
     */
    updateBodyPositions () {
        // If the body has a height, it must be a box.
        if(this.body.height){
            // Update the position.
            this.x = this.body.pos.x;
            this.y = this.body.pos.y;
            // Update the center position.
            this.body.centerX = this.body.pos.x + this.body.halfWidth;
            this.body.centerY = this.body.pos.y + this.body.halfHeight;
        }
        // No height, must be a circle.
        else {
            // Update the position.
            this.x = this.body.pos.x - this.body.radius;
            this.y = this.body.pos.y - this.body.radius;
            // Update the center position.
            this.body.centerX = this.body.pos.x;
            this.body.centerY = this.body.pos.y;
        }
    }

    /**
     * When sending the position of this entity to clients, the center will probably something really long like
     * 767.7505902298357, which when converted to a string in JSON would be a lot bigger than just 767, which is good enough.
     * @returns {{x: number, y: number}}
     */
    getTruncatedCenterPosition () {
        return {
            x: Math.trunc(this.body.centerX),
            y: Math.trunc(this.body.centerY)
        }
    }

    /**
     * Push this body back by how much they were overlapping another body, so they don't go through it.
     * @param {Number} [multiplier=1] - How much to multiply the overlap separation by. Useful for making entities push each other by different amounts.
     */
    pushBodyBack (multiplier) {
        //console.log("in pushBodyBack");
        var multiplier = multiplier || 1;
        var overlap = ECS.crashResponse.overlapV;
        // Move this entities body by the amount of overlap, scaled by the multiplier.
        this.body.moveBy(-overlap.x * multiplier, -overlap.y * multiplier);
        // This entity has been pushed back, so flag them for a physics check on the next update.
        this.body.hasMoved = true;
        this.setPositionEmitted(false);
        this.updateBodyPositions();
    }

    /**
     * Checks if another entity is within the detection radius of this entity.
     * Used to check if they are close enough to be considered for a more precise intersection check.
     * @param {ECS.Entity.body} body - The Crash physics body of the entity to check against.
     * @returns {Boolean}
     */
    isEntityWithinDetectionRadius (body) {
        var xDist = this.body.centerX - body.centerX;
        var yDist = this.body.centerY - body.centerY;
        //var dist = Math.sqrt(xDist*xDist + yDist*yDist);
        return Math.sqrt(xDist*xDist + yDist*yDist) < (this.body.detectionRadius + body.detectionRadius);
    }

    /**
     * Checks if another entity is within a given range of this entity.
     * Used for specific things that Entity.isEntityWithinDetectionRadius isn't flexible enough
     * for, such as letting minions know if something is too far away to keep chasing.
     * @param {ECS.Entity.body} body - The Crash physics body of the entity to check against.
     * @param {Number} range - The range to check within.
     * @returns {Boolean}
     */
    isEntityWithinRange (body, range) {
        var xDist = this.body.centerX - body.centerX;
        var yDist = this.body.centerY - body.centerY;
        //var dist = Math.sqrt(xDist*xDist + yDist*yDist);
        return Math.sqrt(xDist*xDist + yDist*yDist) < range;
    }

    /**
     * Check if the center point of this entity is within the bounds of a rectangle body.
     * Useful for if an entity needs to be able to overlap another body by up to half of their diameter,
     * before doing something else, such as leaning over a Pit entity before falling in.
     * Kind of like a point vs body intersects check.
     * @param {ECS.Entity.body} rectangleBody - The Crash Box physics body to check against.
     * @returns {Boolean}
     */
    isCenterWithinRectangleBody (rectangleBody) {
        if(this.body.centerX > rectangleBody.pos.x
            && this.body.centerX < (rectangleBody.pos.x + rectangleBody.width)){
            if(this.body.centerY > rectangleBody.pos.y
                && this.body.centerY < (rectangleBody.pos.y + rectangleBody.height)){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a target location is within the detection radius of this entity.
     * @param {Number} targetX
     * @param {Number} targetY
     * @returns {Boolean}
     */
    isEntityWithinTargetPosition (targetX, targetY) {
        var xDist = this.body.centerX - targetX;
        var yDist = this.body.centerY - targetY;
        return Math.sqrt(xDist*xDist + yDist*yDist) < this.body.detectionRadius;
    }

    /**
     * Checks this entity against other entities in the quadtree in the same zone, narrows them
     * down to only the ones that are close enough to be considered, checks if the category of
     * the other entity is something that this type of entity has some kind of physics
     * relationship with, and if so, does a more precise Crash intersects check against that pair,
     * and if they intersect, calls the intersectFuncs function of the category of the other entity.
     */
    checkPhysics () {
        // Check that this entity is moving. Don't need to do checks if it is stationary.
        if(this.body.hasMoved === true){
            this.zone.quadTree.colliding(
                this,
                ECS.checkIntersectsCollisionFunc
            );
            // All the physics stuff is done, and any intersections/collisions have been resolved.
            // Don't need to check physics again until this entity moves again.
            this.body.hasMoved = false;
        }
    }

    /**
     * Updates the floor tile that this entity is standing on, such as the last occupied time and the occupied count, changing it to the next terrain type if stood on enough.
     * Returns the number of the speed modifier of the current tile, or false if the entity body is outside of the zone bounds.
     * @returns {Number|Boolean}
     */
    updateOccupiedFloorTile () {

        if(this.setOccupiedTile() === false){
            return false;
        }

        this.occupiedTile.lastOccupiedTime = Date.now();
        this.occupiedTile.occupiedCount += 1;

        var modifier = this.occupiedTile.type.speedModifier;

        if(this.occupiedTile.type.durability !== undefined){
            if(this.occupiedTile.occupiedCount > this.occupiedTile.type.durability){
                this.occupiedTile.type = this.occupiedTile.type.progressesTo;
                this.occupiedTile.occupiedCount = 0;

                ECS.io.in(this.zone.roomName).emit('change_floortile', {code: this.occupiedTile.type.code, row: ~~(this.body.centerY / 32), col: ~~(this.body.centerX / 32)});
            }

        }

        return modifier;
    }

    regenHitPoints () {
        if(this.hitPoints < this.maxHitPoints){
            this.modHitPoints(+1);
        }
    }

    /**
     * Increase the energy of this entity over time.
     */
    regenEnergy () {
        //console.log("regen energy, entity id: " + this.id);
        this.energyRegenRateCount += 1;
        // How often should this entity regain energy.
        if(this.energyRegenRateCount >= this.energyRegenRate){
            this.modEnergy(+this.energyRegenAmount);
            // Some spells return energy instead of taking it away, so check that
            // the new energy isn't greater than their max energy.
            if(this.energy > this.maxEnergy){
                this.setEnergy(this.maxEnergy);
            }

            // Check if it has a socket (is a players entity).
            if(this.socket){
                this.emitEnergyQuarters();
            }

            this.energyRegenRateCount = 0;
        }
    }

    /**
     * Decrease the reviveProgress of this entity over time, killing them if it reaches 0.
     */
    degenReviveProgress (amount) {
        // If an amount was given, decrease the revive progress straight away. Was probably damaged directly.
        if(amount){
            this.modReviveProgress(-amount);
        }
        // Decrease the revive progress over time.
        else {
            this.reviveDegenRateCount += 1;
            // Change the revive progress every 40 world updates.
            if(this.reviveDegenRateCount > 40){
                if(this.reviveProgress > 0){
                    this.modReviveProgress(-30);
                }
                this.setReviveDegenRateCount(0);
            }
        }

    }

    /**
     * Manages the deduction of hit points and revive progress.
     * @param {Number} amount - How much to damage this entity by.
     * @param {ECS.Entity} [sourceEntity] - The entity that is causing this damage.
     */
    damageEntity (amount, sourceEntity) {
        // Don't damage this entity if combat is disabled in the zone it is in.
        if(this.zone.combatEnabled === false){
            //console.log("damage cancelled, non-combat zone");
            return;
        }
        // Don't damage Order members.
        //if(this.orderNumber !== 0){
        //    if(this.orderNumber === sourceEntity.orderNumber){
        //        //console.log("avoided damaging fellow team member");
        //        return;
        //    }
        //}

        // Don't bother if no amount was given, or 0.
        if(!amount){
            return
        }
        // If this entity is damaging itself, deal less damage.
        if(sourceEntity === this){
            amount = amount* 0.5;
        }

        //console.log("Entity.damageEntity, amount: " + amount);
        // Check that the entity has some HP (is alive).
        if(this.hitPoints > 0){
            // Reduce hit points by the given amount.
            this.modHitPoints(-amount);
            // Check if the entity is now unconscious/dead/destroyed, depending on what type of entity it is. Characters go unconscious, minions die, doors gets destroyed, etc.
            if(this.hitPoints <= 0){
                if(this.onAllHitPointsLost !== undefined){
                    this.onAllHitPointsLost();
                }
            }
            // The entity still has some HP after taking damage.
            // Check if it has a socket (is a players entity).
            else if(this.socket){
                // If the entity that was damaged was a player, tell them their current hitpoints.
                this.socket.emit('hitPoints_changed', this.getHitPointQuarters());
            }

            // Restore some energy to the damaged entity, if it can have energy.
            if(this.energy !== undefined){
                // Damage taken is restored to energy.
                this.modEnergy(amount*2);
                // Check it isn't now more than max energy.
                if(this.energy > this.maxEnergy){
                    this.setEnergy(this.maxEnergy);
                }
                // Check if it has a socket (is a players entity).
                if(this.socket){
                    this.emitEnergyQuarters();
                }
            }
        }
        // The entity is unconscious, reduce their revive progress instead of HP.
        // If the function gets this far, then the entity still exists and is something that can be made unconscious.
        // If it was a door or boulder or whatever, then it should have been destroyed when it reached 0 HP.
        else{
            this.degenReviveProgress(amount);
        }
    }

    /**
     * Manages the incrementation of hit points and revive progress.
     * @param {Number} amount - How much to heal this entity by.
     * @param {ECS.Entity} [sourceEntity] - The entity that is causing this healing.
     */
    healEntity (amount, sourceEntity) {
        //console.log("healEntity, amount: " + amount);
        // Don't bother if no amount was given, or 0.
        if(!amount){
            return
        }
        // If this entity is healing itself, heal for less.
        if(sourceEntity === this){
            amount = amount* 0.5;
        }
        // Check if they are currently alive.
        if(this.hitPoints > 0){
            this.modHitPoints(+amount);
            // If the character now has more than their max HP, set their HP to max.
            if(this.hitPoints > this.maxHitPoints){
                this.setHitPoints(this.maxHitPoints);
            }
            // If the entity that was healed was a player, tell them their current hit points.
            if(this.socket){
                this.socket.emit('hitPoints_changed', this.getHitPointQuarters());
            }
        }
        // Entity is unconscious.
        else{
            // Restore some revive progress.
            this.modReviveProgress(+amount);
            // Check if this entity is now conscious.
            if(this.reviveProgress >= this.maxHitPoints){
                // Revive the entity.
                this.revive();
                // Tell the players in this zone that this entity is now conscious.
                ECS.io.in(this.zone.roomName).emit('char_revived', this.id);
            }
        }
    }


}

Entity.prototype.intersectsWith = {
    // An exit that will take an entity to the linked entrance, and
    // move it to a different zone if that entrance is in another zone.
    exit: function (entity, exit) {
        // The crash test that sent the program here checks if bounds are overlapping, but for exits it
        // is nicer if things are only teleported if the center of the entity is within the exit bounds.
        if(entity.isCenterWithinRectangleBody(exit.body) === false){
            return;
        }
        // Check if this exit leads to an entrance in the current zone. If so, then no zone swap is needed.
        if(exit.targetZone === entity.zone){
            //console.log("target zone is this zone, stay here");
            var entrance = entity.zone.entrances[exit.targetEntrance];
            // If one of the exits has an invalid target entrance, use the default spawn point.
            if(!entrance){
                entrance = entity.zone.entrances['0'];
            }
            var detectionRadius = entity.body.detectionRadius;
            // Move the sprite to somewhere in the entrance bounds.
            entity.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + detectionRadius, entrance.right  - detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + detectionRadius, entrance.bottom - detectionRadius)
            );
        }
        else {
            //console.log("target zone is different zone, move there");
            // Move the entity to the next zone.
            exit.targetZone.moveEntityToZone(entity, exit.targetEntrance);
        }
    },

    // Like an exit, but the target entrance must be in the same zone. A local teleporter.
    portal: function (entity, exit) {
        // The crash test that sent the program here checks if bounds are overlapping, but for exits it
        // is nicer if things are only teleported if the center of the entity is within the exit bounds.
        if(entity.isCenterWithinRectangleBody(exit.body) === false){
            return;
        }
        // Don't move to a different zone. Only go through same zone portals.
        // Check if this exit leads to an entrance in the current zone. If so, then no zone swap is needed.
        if(exit.targetZone === entity.zone){
            //console.log("target zone is this zone, stay here");
            var entrance = entity.zone.getEntrance(exit.targetEntrance);
            // Move the sprite to somewhere in the entrance bounds.
            entity.reposition(
                ECS.PhaserRips.Math.randIntInRange(entrance.left + entity.body.detectionRadius, entrance.right  - entity.body.detectionRadius),
                ECS.PhaserRips.Math.randIntInRange(entrance.top  + entity.body.detectionRadius, entrance.bottom - entity.body.detectionRadius)
            );
        }
    },

    characterHuman: function (thisEntity, otherEntity) {



        //can do order checks here for characters, team damage
    },

    projectile: function (thisEntity, otherEntity) {
        if(otherEntity.element === undefined){
            return;
        }
        // Are the elements of both projectiles the same. Same elements pass through each other.
        if(thisEntity.element !== otherEntity.element){
            // Does the element of this projectile counter the element of the other projectile.
            if(thisEntity.countersElement === otherEntity.element){
                // The other entity should be destroyed by this projectile, and this projectile remains.
                otherEntity.deactivate();
            }
            // Does the element of the other projectile counter the element of this projectile.
            else if(otherEntity.countersElement === thisEntity.element){
                // This entity should be destroyed by the other projectile, and the other projectile remains.
                thisEntity.deactivate();
            }
            // No counters involved. Pass through void projectiles. These projectiles cancel each other out, remove them both.
            else if(otherEntity.element !== ECS.Elements.VOID){
                thisEntity.deactivate();
                otherEntity.deactivate();
            }
        }
    },

    obstacle: function (thisEntity, otherEntity, damage) {
        var damage = thisEntity.attackPower || damage;
        if(thisEntity.countersElement === otherEntity.element){
            //console.log("dealing x2 damage to obstacle: " + damage*2);
            otherEntity.damageEntity(damage * 2);
        }
        else if(otherEntity.countersElement === thisEntity.element){
            //console.log("dealing /2 damage to obstacle: " + damage/2);
            otherEntity.damageEntity(damage / 2);
        }
        else {
            //console.log("dealing normal damage to obstacle: " + damage);
            otherEntity.damageEntity(damage);
        }
        thisEntity.deactivate();
    },

    minion: function (thisEntity, otherEntity, damage) {
        // Don't damage casters own minions.
        if(thisEntity.caster !== otherEntity.caster){
            var damage = thisEntity.attackPower || damage;
            // The element of this projectile counters the element of the minion.
            if(thisEntity.countersElement === otherEntity.element){
                //console.log("dealing x2 damage to minion: " + damage*2);
                // Deal double damage to the minion.
                otherEntity.damageEntity(damage * 2, thisEntity.caster);
            }
            // The element of the minion counters the element of this projectile.
            else if(otherEntity.countersElement === thisEntity.element){
                //console.log("dealing /2 damage to minion: " + damage/2);
                // Deal half damage to the minion.
                otherEntity.damageEntity(damage / 2, thisEntity.caster);
            }
            // No counters involved. Normal effectiveness.
            else {
                //console.log("dealing normal damage to minion: " + damage);
                // Deal normal damage to the minion.
                otherEntity.damageEntity(damage, thisEntity.caster);
            }
            // Set the target of the minion to be the caster of the projectile that damaged it.
            otherEntity.target = thisEntity.caster;

            thisEntity.deactivate();
        }
    },

    inverter: function (thisEntity, otherEntity) {
        // Send the entity moving in the opposite direction.
        thisEntity.body.velocity.x *= -1;
        thisEntity.body.velocity.y *= -1;
        // The entity now belongs to the caster of the inverter.
        //thisEntity.setCaster(otherEntity.caster);
        // Flip the rotation of the entity.
        thisEntity.body.rotation += Math.PI;

        // Need to manually tell the clients to update the rotation of the sprite.
        ECS.io.in(thisEntity.zone.roomName).emit('rotate', {
            id: thisEntity.id,
            rotation: thisEntity.body.rotation
        });
    },

    structure: function (thisEntity, otherEntity, damage) {
        // Pass through open doors.
        if(otherEntity.operating === false){
            if(otherEntity.entityType === ECS.assemblages._WoodDoor.prototype.entityType){
                return
            }
        }
        var damage = thisEntity.attackPower || damage;
        // Check the structure is part of the order of the caster of this entity.
        if(thisEntity.caster.order === otherEntity.zone.order.core
        && otherEntity.zone.order.core !== null){ // If the zone and the caster don't have an order, then they will both be null, so this check will be true. Check for both nulls here.
            // Deal half damage to order structures.
            otherEntity.damageEntity(damage * 0.5);
        }
        else {
            // Deal full damage to non-order structures.
            otherEntity.damageEntity(damage);
        }

        thisEntity.deactivate();
    }
};

ECS.Entity = Entity;

module.exports = Entity;
