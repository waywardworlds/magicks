/**
 * Created by User on 03/04/2017.
 */

/**
 * Created by User on 14/04/2016.
 */

"use strict";

var ECS = require('./ECS');
var Entity = require('./Entity');
var InventoryItemTypes = require('./InventoryItemTypes');

/**
 * Whether this entity has moved since the last zone state emission.
 * To save bandwidth usage, the emitter only sends the position of entities that have moved since the last time their positions were sent.
 * If this entity has moved, then the emitter will set positionEmitted to true when it runs.
 * Set this to false every time an entity's position changes.
 * This may seem similar to body.hasMoved, but has hasMoved is for physics only, whereas this is for updating what the client sees, as
 * the world update and state emitter don't run at the same time.
 * @param {Boolean} value
 */
Entity.prototype.setPositionEmitted = function(value){
    this.positionEmitted = value;
};

/**
 * The maximum velocity in any direction for this entity's physics body. Any entity with this component is movable by definition.
 * @param {Number} value
 */
Entity.prototype.setMaxSpeed = function (value) {
    this.maxSpeed = value;
};

/**
 * Whether this entity can currently move or not at this time. Used to stop an entity from moving for a duration. i.e. freezing, or the delay after teleporting so they don't warp though solids.
 * @param {Boolean} value
 */
Entity.prototype.setCanMove = function (value) {
    this.canMove = value;
};

/**
 * Whether this entity is being pushed by another force or not. Used to stop them from moving in the direction that they want to go, i.e. when being knocked back by a rock.
 * @param {Boolean} value
 */
Entity.prototype.setBeingPushed = function (value) {
    this.beingPushed = value;
};

/**
 * Whether this entity is flying above the ground or not. i.e. a nimbus, bat, etc.
 * @param {Boolean} value
 */
Entity.prototype.setFlying = function (value) {
    this.flying = value;
};

/**
 * Whether this entity is invisible. Invisible entities don't have their positions emitted.
 * @param {Boolean} value
 */
Entity.prototype.setInvisible = function (value) {
    this.invisible = value;
};

/**
 * Get the tile that this entity is over.
 * @returns {Boolean|Object} The tile, or false if the entity is not in the floor grid.
 */
Entity.prototype.getOccupiedTile = function () {
    var floorGridRow = this.zone.floorGrid[~~(this.body.centerY / 32)];
    // Can't access the second dimension of the array (the col) unless the first one (the row) is defined, so check it is defined.
    // Case for if entity goes out of the top or bottom of the map.
    if(floorGridRow === undefined){
        return false;
    }

    var occupiedTile = floorGridRow[~~(this.body.centerX / 32)];
    // If the occupied tile is undefined, the entity must be outside of the floor grid, and thus outside of the zone bounds, so kill them.
    if(occupiedTile === undefined){
        return false;
    }

    return occupiedTile;
};

/**
 * Sets the occupied tile of this entity to be the one they are on top of.
 * Doesn't update the tile itself. See Entity.updateOccupiedFloorTile.
 * @returns {Boolean} Whether the occupied tile was set successfully.
 */
Entity.prototype.setOccupiedTile = function () {
    var floorGridRow = this.zone.floorGrid[~~(this.body.centerY / 32)];
    // Can't access the second dimension of the array (the col) unless the first one (the row) is defined, so check it is defined.
    // Case for if entity goes out of the top or bottom of the map.
    if(floorGridRow === undefined){
        // Kill the entity if it goes out of the zone bounds.
        this.kill();
        return false;
    }

    this.occupiedTile = floorGridRow[~~(this.body.centerX / 32)];
    // If the occupied tile is undefined, the entity must be outside of the floor grid, and thus outside of the zone bounds, so kill them.
    if(this.occupiedTile === undefined){
        // Kill the entity if it goes out of the zone bounds.
        this.kill();
        return false;
    }

    return true;
};

/**
 * What element this entity is. Mainly used for projectiles.
 * First character only. Don't need to do lengthy string comparison.
 * @param {String} [value='v']
 */
Entity.prototype.setElement = function (value) {
    this.element = value || ECS.Elements.VOID;
};

/**
 * What element is countered by the element of this entity. Fire counters Earth.
 * First character only. Don't need to do lengthy string comparison.
 * @param {String} [value='v']
 */
Entity.prototype.setCountersElement = function (value) {
    this.countersElement = value || ECS.Elements.VOID;
};

/**
 * The entity that cast this spell. Useful to avoid caster vs own projectile collisions, and doing stuff with the spell that involves the caster.
 * @param {Entity} value
 */
Entity.prototype.setCaster = function (value) {
    /** @type {Entity} */
    this.caster = value;
};

/**
 * When this entity can next cast a spell. Updated by spells.canCast when a spell is cast.
 * @param {Number} recastCooldown
 */
Entity.prototype.setNextCastTime = function (recastCooldown) {
    this.nextCastTime = Date.now() + recastCooldown;
};

/**
 * How many hitPoints does this entity now have.
 * @param {Number} value
 */
Entity.prototype.setHitPoints = function (value) {
    this.hitPoints = value;
};
Entity.prototype.modHitPoints = function (value) {
    // Healed.
    if(value > 0){
        // Don't emit the healed event if they are already at full HP.
        if(this.hitPoints < this.maxHitPoints){
            ECS.io.in(this.zone.roomName).emit('healed', this.id);
        }
    }
    // Damaged.
    else {
        ECS.io.in(this.zone.roomName).emit('damaged', this.id);
    }

    this.hitPoints += value;
};

/**
 * How often this entity regains the regen amount of hitpoints, measured in world ticks.
 * @param {Number} value
 */
Entity.prototype.setHitPointRegenRate = function (value) {
    this.hitPointRegenRate = value;
};

/**
 * The maximum hitpoints this entity should have.
 * @param {Number} value
 */
Entity.prototype.setMaxHitPoints = function (value) {
    this.maxHitPoints = value;
};

// Function to run when this entity has 0 (or less) hitpoints. Not necessarily 'dead' just yet,
// might be unconscious. But if it is something like a door, then it should be destroyed.
//Entity.prototype.onAllHitPointsLost = undefined;

// Function to run when revive progress for this entity (should be a character) is 0 (or less). Definitely 100% dead...
//Entity.prototype.onAllReviveProgressLost = undefined;

/**
 * How much energy does this entity now have.
 * @param {Number} value
 */
Entity.prototype.setEnergy = function (value) {
    this.energy = value;
};
Entity.prototype.modEnergy = function (value) {
    this.energy += value;
    if(this.energy > this.maxEnergy){
        this.energy = this.maxEnergy;
    }
};

/**
 * How many game world updates should pass before incrementing energy.
 * @param {Number} value
 */
Entity.prototype.setEnergyRegenRate = function (value) {
    this.energyRegenRate = value;
};

// A counter for how many of the above world updates have passed.
Entity.prototype.energyRegenRateCount = undefined;

/**
 * How much energy to restore every time regenEnergy is called.
 * @param {Number} value
 */
Entity.prototype.setEnergyRegenAmount = function (value) {
    this.energyRegenAmount = value;
};
Entity.prototype.modEnergyRegenAmount = function (value) {
    this.energyRegenAmount += value;
};

// How much energy regeneration it costs to maintain a spell. Used for minions that slow the energy regen rate of their master, or for maintained enchantments.
// When a spell like this is activated, the energy regen amount of the caster should be decreased by this cost, and increased when that spell effect ends.
Entity.prototype.energyRegenCost = undefined;

/**
 * How much maximum energy can this entity have.
 * @param {Number} value
 */
Entity.prototype.setMaxEnergy = function (value) {
    this.maxEnergy = value;
};

/**
 * Should start at 50% of max hitpoints. If it started at 100% (full), then it would be revived straight away whenever it loses all hitpoints.
 * @param {Number} value
 */
Entity.prototype.setReviveProgress = function (value) {
    this.reviveProgress = value;
};
Entity.prototype.modReviveProgress = function (value) {
    // Healed.
    if(value > 0){
        ECS.io.in(this.zone.roomName).emit('healed', this.id);
    }
    // Damaged.
    else {
        ECS.io.in(this.zone.roomName).emit('damaged', this.id);
    }

    this.reviveProgress += value;

    // Check if this entity is now fully dead.
    if(this.reviveProgress <= 0){
        this.onAllReviveProgressLost();
    }
    // Entity is still unconscious (not dead). Check if it has a socket (is a player's entity).
    else if(this.socket !== undefined){
        // Tell the player their revive progress has changed.
        this.socket.emit('reviveProgress_changed', this.getReviveQuarters());
    }
};

/**
 * How many revive degen rate count ticks should this entity be on.
 * @param {Number} value
 */
Entity.prototype.setReviveDegenRateCount = function (value) {
    this.reviveDegenRateCount = value;
};

// A list of functions to call when this entity intersects something.
Entity.prototype.intersectFuncs = undefined;

/**
 * Set a reference to the zone this entity is currently in.
 * @param {Zone} value
 */
Entity.prototype.setZone = function (value) {
    this.zone = value;
};

// The index of this entity in the entities array in the zone the entity is in. Used to empty that array element when the zone no longer has this entity.
// Needed for ALL entities that can be added to AND removed from a zone, like characters, projectiles, obstacles, corpses.
// Remove with `zone.entities[entities.entityIndex] = null;`
Entity.prototype.entityIndex = undefined;

// Whether this entity is something in the entities grid in the zone it is in.
// Used to mark this entity so it doesn't get sent to the clients when they request the dynamic entities for a zone. <-- NOT USING THIS, ENTITIES GRID IS REDUNDANT I THINK
//Entity.prototype.isInEntitiesGrid = undefined;

// A reference to the target zone to move an entity to. NOT the index. Used for exits.
// The substring before the - in the name string is the target zone. The 2044 in '2044-1'.
Entity.prototype.targetZone = undefined;

// The number of the entrance in the zone to move to. Used for exits.
// The substring after the - in the name string is the target entrance. The 2 in '5165-2'.
Entity.prototype.targetEntrance = undefined;

// Sets the properties of an entity back to their defaults.
Entity.prototype.reset = undefined;

// A list of references to the minions that this entity is the master of, stored by each minions id. `this.minions[minion.id] = minion;`
Entity.prototype.setMinions = function () {
    this.minions = {};
};

// The target of this entity to chase and attack. Used for minions to attack things that are hostile to their master.
Entity.prototype.target = undefined;

/**
 * What Order is this entity a member of.
 * @param {ECS.OrderCore} [value=null]
 */
Entity.prototype.setOrder = function (value) {
    this.order = value || null;
};

/**
 * Whether this entity (usually a structure) is operating (i.e. it's effect is being used).
 * @param {Boolean} [value=false];
 */
Entity.prototype.setOperating = function (value) {
    this.operating = value || false;
};

/**
 * What type of inventory item does this entity represent when picked up.
 * @param {String} value - A list of entity types can be found in ECS.InventoryItemTypes
 */
Entity.prototype.setItemType = function (value) {
    this.itemType = value;
};

/*
Entity.prototype.WallShapesEnum = {
    HORIZONTAL: "hori",
    VERTICAL: "vert",
    TOP_LEFT: "topL",
    TOP_RIGHT: "topR",
    BOTTOM_LEFT: "botL",
    BOTTOM_RIGHT: "botR"
};*/

/**
 * Give this entity a rectangle physics body.
 * @param {Number} [x=0]
 * @param {Number} [y=0]
 * @param {Number} width
 * @param {Number} height
 * @param {Boolean} immovable - Should this entity be able to move at all.
 */
Entity.prototype.setRectangleBody = function (x, y, width, height, immovable) {
    // Add properties to the entity object itself (not the body) for the quadtree to use.
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    // Add the Crash collider body.
    this.body = new ECS.crash.Box(new ECS.crash.Vector(x || 0, y || 0), width, height);

    this.body.velocity = {x: 0, y: 0};
    this.body.width = width;
    this.body.height = height;
    this.body.halfWidth = width * 0.5;
    this.body.halfHeight = height * 0.5;
    this.body.centerX = this.body.pos.x + this.body.halfWidth;
    this.body.centerY = this.body.pos.y + this.body.halfHeight;

    // Whether this body has moved since the last time it ran checkPhysics.
    // If another body pushes this one, this entity needs to do a check in the next update to see if it was pushed into anything else.
    // Also means that checkPhysics will only be ran when this body has moved, instead of all the time.
    this.body.hasMoved = false;
    this.body.immovable = immovable || false;
    // Add a radius to this entity so entities can figure out if they are close enough together to be considered for intersection/collision.
    this.body.detectionRadius = Math.hypot(width, height) / 2;
};

/**
 * Give this entity a circle physics body.
 * Useful for players to get around corners/through doorways easier.
 * @param {Number} [x=0]
 * @param {Number} [y=0]
 * @param {Number} radius
 * @param {Boolean} immovable - Should this entity be able to move at all.
 */
Entity.prototype.setCircleBody = function (x, y, radius, immovable) {
    // Add properties to the entity object itself (not the body) for the quadtree to use.
    this.x = x;
    this.y = y;
    this.width = radius * 2;
    this.height = radius * 2;

    // Add the Crash collider body.
    this.body = new ECS.crash.Circle(new ECS.crash.Vector(x || 0, y || 0), radius || 8);

    this.body.velocity = {x: 0, y: 0};
    this.body.radius = radius;
    this.body.centerX = this.body.pos.x;
    this.body.centerY = this.body.pos.y;

    // Whether this body has moved since the last time it ran checkPhysics.
    // If another body pushes this one, this entity needs to do a check in the next update to see if it was pushed into anything else.
    // Also means that checkPhysics will only be ran when this body has moved, instead of all the time.
    this.body.hasMoved = false;
    this.body.immovable = immovable || false;
    // Add a radius to this entity so entities can figure out if they are close enough together to be considered for intersection/collision.
    this.body.detectionRadius = radius || 8;
};

/**
 * The name that players see above the sprites of characters (players, minions, NPCs).
 * @param {String} [name='']
 */
Entity.prototype.setDisplayName = function (name) {
    if(name && typeof name === 'string'){
        // If the name to set to is RAND, generate a random name.
        if(name === 'RAND'){
            this.displayName = 'random name here';//generateName();
        }
        // Otherwise, set it to the given name.
        else {
            this.displayName = name;
        }
    }
    // No name was given. Set to empty string.
    else {
        this.displayName = '';
    }
};

/**
 * The index of the zone in the world.zones array, and name of the entrance bounds that this entity will spawn into.
 * @param {String} spawnArea
 */
Entity.prototype.setSpawnArea = function (spawnArea) {
    // If a spawn area was specified, use it.
    if(spawnArea){
        this.spawnArea = {};
        this.spawnArea.zoneName = spawnArea.split('-')[0];
        this.spawnArea.entranceName = spawnArea.split('-')[1];
        //console.log("entity spawnArea name: " + this.spawnArea.zoneName);
    }
    // No spawn area was specified. If this entity already has a spawn area, then don't change it (spawn into the same zone).
    else if(this.spawnArea){

    }
    // This entity doesn't already have a spawn area. Give it a default one so it doesn't cry.
    else {
        console.log("* WARNING: Invalid spawn area given to Entity.setSpawnArea.");
        this.spawnArea.zoneName = '0005';
        this.spawnArea.entranceName = '0';
    }
};

/*
// What resources this entity is carrying.
Entity.prototype.setResources = function (maxPerResource) {
    this.resources = {};
    this.resources.wood = 10;
    this.resources.gems = 10;
    this.resources.food = 10;
    this.resources.max = maxPerResource || 50;
};*/

/**
 * Give this entity an inventory so it can carry items.
 * @param {Number} [maxContents=16] - How many slots in this inventory.
 */
Entity.prototype.setInventory = function (maxContents) {
    this.inventory = {};
    this.inventory.contents = [];
    this.inventory.maxContents = maxContents || 16;
    this.inventory.isFull = false;

    for(var i=0; i<maxContents; i+=1){
        this.inventory.contents[i] = null;
    }
};
/*
Entity.prototype.inventoryManager = {};


Entity.prototype.inventoryManager.addItem = function (itemType, config, index) {

    if(itemType === InventoryItemTypes.SCROLL && config === undefined){
        throw "* ERROR: scrolls must also have a config object in Entity.inventoryManager.addItem";
    }

    var inventory = this.inventory;

    if(index === undefined){
        // Get the first empty slot in the contents.
        for(var i=0, len=inventory.maxContents; i<len; i+=1){
            if(inventory.contents[i] === null){
                index = i;
                inventory.isFull = false;
                break;
            }
        }
        // No empty slot found. Can't add item.
        inventory.isFull = true;
        return false;
    }
    else {
        // Check for out of bounds array index.
        if(inventory.contents[index] === undefined){
            throw "* ERROR: invalid contents index given to Entity.inventoryManager.addItem";
        }
    }

    console.log("adding item: " + itemType);
    inventory.contents[index] = {itemType: itemType, config: config || {}};

};*//*
Entity.prototype.inventoryManager.removeItemByIndex = function (index) {
    if(this.inventory.contents[index] === undefined){
        return false;
    }
    this.inventory.contents[index] = null;
    this.inventory.isFull = false;
};*//*
Entity.prototype.inventoryManager.removeItemByType = function (itemType, amount) {
    var contents = this.inventory.contents;

    amount = amount || 1;

    for(var i=0, len=this.inventory.maxContents; i<len; i+=1){
        if(contents[i].itemType === itemType){
            contents[i] = null;
            amount-=1;
            if(amount === 0){
                break;
            }
        }
    }

    console.log("removed item: " + itemType);

    this.inventory.isFull = false;
};
*/
/**
 * Count how many of a certain type of item this entity has in their inventory.
 * @param {Number} itemType - The type of item to count.
 * @returns {Number} - How many were found.
 *//*
Entity.prototype.inventoryManager.countItems = function (itemType) {
    if(InventoryItemTypes[itemType] === undefined){
        throw "* ERROR: item to count in inventory is not in the valid items list in Entity.inventoryManager.countItems";
    }

    var count = 0;

    var inventory = this.inventory;
    for(var i= 0, len=inventory.maxContents; i<len; i+=1){
        if(inventory.contents[i].item === itemType){
            count += 1;
        }
    }

    return count;
};*/

/*
Entity.prototype.modWood = function (amount) {
    //console.log("in modwood");
    this.resources.wood += amount;
    if(this.resources.wood > this.resources.max){
        this.resources.wood = this.resources.max;
    }
    else if(this.resources.wood < 0){
        this.resources.wood = 0;
    }
    //console.log("wood modified, new wood: " + this.resources.wood + ", total: " + this.resources.total);
    // Check if this is a player's entity.
    if(this.socket !== undefined){
        ECS.io.in(this.zone.roomName).emit('wood_changed', this.resources.wood);
    }
};*/
/*
Entity.prototype.modGems = function (amount) {
    this.resources.gems += amount;
    if(this.resources.gems > this.resources.max){
        this.resources.gems = this.resources.max;
    }
    else if(this.resources.gems < 0){
        this.resources.gems = 0;
    }
    //console.log("wood modified, new wood: " + this.resources.wood + ", total: " + this.resources.total);
    // Check if this is a player's entity.
    if(this.socket !== undefined){
        ECS.io.in(this.zone.roomName).emit('wood_changed', this.resources.wood);
    }
}*/

/*
// Used by walls to tell the clients how they line up with adjacent walls. Which sprite frame to use.
Entity.prototype.setWallShape = function () {
    // Default is horizontal.
    this.wallShape = this.WallShapesEnum.HORIZONTAL;

    var aboveOccupied = false;
    var belowOccupied = false;
    var leftOccupied = false;
    var rightOccupied = false;
    // Round down to avoid decimal indexes.
    var row = Math.floor(this.y / this.zone.gridSize);
    var col = Math.floor(this.x / this.zone.gridSize);

    // Check the array element has a value, or checking entityType will throw an exception.
    if(this.zone.structuresGrid[row-1] !== undefined){
        if(this.zone.structuresGrid[row-1][col]){
            if(this.zone.structuresGrid[row-1][col].entityType === this.entityType){
                aboveOccupied = true;
            }
        }
    }
    if(this.zone.structuresGrid[row+1] !== undefined){
        if(this.zone.structuresGrid[row+1][col]){
            if(this.zone.structuresGrid[row+1][col].entityType === this.entityType){
                belowOccupied = true;
            }
        }
    }
    if(this.zone.structuresGrid[row] !== undefined){
        if(this.zone.structuresGrid[row][col-1]){
            if(this.zone.structuresGrid[row][col-1].entityType === this.entityType){
                leftOccupied = true;
            }
        }
    }
    if(this.zone.structuresGrid[row] !== undefined){
        if(this.zone.structuresGrid[row][col+1]){
            if(this.zone.structuresGrid[row][col+1].entityType === this.entityType){
                rightOccupied = true;
            }
        }
    }

    // Above and left.
    if(aboveOccupied && leftOccupied){
        this.wallShape = this.WallShapesEnum.BOTTOM_RIGHT;
    }
    // Above and right.
    else if(aboveOccupied && rightOccupied){
        this.wallShape = this.WallShapesEnum.BOTTOM_LEFT;
    }
    // Space below.
    if(belowOccupied){
        if(aboveOccupied){
            this.wallShape = this.WallShapesEnum.VERTICAL;
        }
        // Below and left.
        else if(leftOccupied){
            this.wallShape = this.WallShapesEnum.TOP_RIGHT;
        }
        // Below and right.
        else if(rightOccupied){
            this.wallShape = this.WallShapesEnum.TOP_LEFT;
        }
    }
    if(leftOccupied && rightOccupied){
        this.wallShape = this.WallShapesEnum.HORIZONTAL;
    }

};*/
/*
// Recalculate the adjacent walls after one has been added.
Entity.prototype.recalculateAdjacentWallShapes = function (row, col) {
    // Check the array element has a value, or checking entityType will throw an exception.
    if(this.zone.structuresGrid[row-1] !== undefined){
        if(this.zone.structuresGrid[row-1][col]){
            if(this.zone.structuresGrid[row-1][col].entityType === this.entityType){
                this.zone.structuresGrid[row-1][col].setWallShape();
            }
        }
    }
    if(this.zone.structuresGrid[row+1] !== undefined){
        if(this.zone.structuresGrid[row+1][col]){
            if(this.zone.structuresGrid[row+1][col].entityType === this.entityType){
                this.zone.structuresGrid[row+1][col].setWallShape();
            }
        }
    }
    if(this.zone.structuresGrid[row] !== undefined){
        if(this.zone.structuresGrid[row][col-1]){
            if(this.zone.structuresGrid[row][col-1].entityType === this.entityType){
                this.zone.structuresGrid[row][col-1].setWallShape();
            }
        }
    }
    if(this.zone.structuresGrid[row] !== undefined){
        if(this.zone.structuresGrid[row][col+1]){
            if(this.zone.structuresGrid[row][col+1].entityType === this.entityType){
                this.zone.structuresGrid[row][col+1].setWallShape();
            }
        }
    }
};*/
