/**
 * Created by User on 28/01/2018.
 */

var globals = require('../Setup');

/**
 * A list of valid item types that can be put into an inventory contents.
 */
module.exports = {
    WOOD: {
        value: 'WOOD',
        entityAssemblage: undefined,
        use: undefined
    },

    GEMS: {
        value: 'GEMS',
        entityAssemblage: undefined,
        use: function (entity, index) {
            //console.log("using gems");

            if(entity.inventory.contents[index] === null){
                return false;
            }

            entity.modEnergy(100);

            globals.ECS.InventoryManager.removeItemByIndex(entity, index);

            return true;
        }
    },

    FOOD: {
        value: 'FOOD'
    },

    SCROLL: {
        value: 'SCROLL',
        entityAssemblage: undefined,
        use: function (entity, index) {
            //console.log("using scroll");

            var item = entity.inventory.contents[index];
            // Make sure the index is valid.
            if(item === null){
                return false;
            }

            var unlockedSpells = entity.socket.gameData.unlockedSpells;

            // Check this player doesn't already have this spell unlocked.
            if(unlockedSpells[item.itemConfig.spellCode]){
                //console.log("player already has this spell");
                // Tell the client they already have this spell unlocked.
                entity.socket.emit("alert_message", "You already know that spell.");
                return false;
            }

            var spellCode = item.itemConfig.spellCode;

            //console.log("adding spell code: " + spellCode);

            //console.log("player unlocked spells before:");
            //console.log(unlockedSpells);

            entity.socket.gameData.unlockedSpells[spellCode] = true;
            entity.socket.emit("add_spell", spellCode);
            entity.socket.emit("alert_message", "New spell learned!\n" + item.itemConfig.spellName);

            //console.log("player unlocked spells after:");
            //console.log(unlockedSpells);

            //globals.socketPM.emit("add_spell", {accountID: entity.socket.accountID, spellCode: item.itemConfig.spellCode}); //TODO: this seems a bit dodgy. would like for other entities to be able to learn spells too. The entity class itself needs some sort of 'knownSpells' component.

            globals.ECS.InventoryManager.removeItemByIndex(entity, index);

            return true;
        }
    },

    CORE: {
        value: 'CORE',
        entityAssemblage: undefined
    },

    // Can only set the references to the assemblages after the ECS is set up.
    setupAssemblageReferences: function () {
        this.WOOD.entityAssemblage = globals.ECS.assemblages.WoodPickup;
        this.GEMS.entityAssemblage = globals.ECS.assemblages.GemsPickup;
        //this.FOOD.entityAssemblage = globals.ECS.assemblages.FoodPickup;
        this.SCROLL.entityAssemblage = globals.ECS.assemblages.ScrollPickup;
        this.CORE.entityAssemblage = globals.ECS.assemblages.OrderCore;

    }
};