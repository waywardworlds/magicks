/**
 * Created by User on 03/01/2018.
 */

"use strict";

var fs = require('fs');
//var globals = require('../Setup');
var InventoryItemTypes = require('./InventoryItemTypes');
var InventoryManager = require('./InventoryManager');

var ECS = {
    // Reference to the socket io server object.
    io: {},
    // How many Entities have been created. Used for giving each entity a unique id.
    entityCount: 0,
    // The list of components that can be added onto entities.
    components: {},
    // Premade collections of components.
    assemblages: {},

    InventoryItemTypes: InventoryItemTypes,

    InventoryManager: InventoryManager,

    Elements: {
        LIGHT:      'l',
        DARK:       'd',
        WATER:      'w',
        FIRE:       'f',
        EARTH:      'e',
        PSYCHIC:    'p',
        VOID:       'v'
    },

    // The list of entities that are waiting to be added to list of test entities.
    pendingTestEntities: [],
    // An instance of each entity type that is used for collision checks, before actually instantiating the intended entity.
    testEntities: {},

    Categories: {
        OBSTACLE:       'obstacle',
        CHARACTER:      'character',
        CORPSE:         'corpse',
        MINION:         'minion',
        ENTRANCE:       'entrance',
        EXIT:           'exit',
        PROJECTILE:     'projectile',
        INVERTER:       'inverter',
        ENCHANTMENT:    'enchantment',
        SOLID:          'solid',
        STRUCTURE:      'structure',
        PICKUP:         'pickup'
    },
    // Object pools for entities in this world. Each entity type (assemblage) has it's own pool.
    pools: {},
    // Reference to the Crash instance.
    crash: {},
    crashResponse: {},
    // Reference to the stuff ripped from Phaser.
    PhaserRips: {},
    // Reference to the object of zones in the World object. Keys are zone names, i.e. {'4567': zone}. Used by exits to find the zone to move an entity to. There is also a zonesArray in World.js.
    zonesObject: {}
};

/**
 * Gives each type of entity their own unique type number, and then writes all types and their numbers to TypeCatalogue.json in the client.
 * Type numbers are used instead of the entityType strings when telling the client what entity to add. Much less data usage than sending a barrage of potentially long strings.
 */
ECS.setupTypeNumbers = function () {

    var catalogueDefs = {};

    // How many different types of entities there are. Each entity type (assemblage) should be given
    // a unique number for sending to the client, instead of sending lengthy strings using entityType.
    var typeCount = 0;

    // Go though every entity type in the list of assemblages.
    for(var key in ECS.assemblages){
        if(ECS.assemblages.hasOwnProperty(key)){
            // Ignore the public functions that don't start with _.
            if(key[0] !== "_"){
                continue;
            }
            // Give this entity type a number.
            ECS.assemblages[key].prototype.typeNumber = typeCount;
            typeCount += 1;

            // Trim the _ from the front of the type name, and add it with the type number to the catalogue.
            // Don't just use the public name itself, as we need to access the prototype of the private function to get the type number.
            catalogueDefs[key.slice(1)] = ECS.assemblages[key].prototype.typeNumber;
        }
    }

    // Check the type catalogue exists. Catches the case that this is the deployment server
    // and thus doesn't have a client directory, and thus no type catalogue.
    if (fs.existsSync('../client/js/TypeCatalogue.json')) {
        // Turn the data into a string.
        catalogueDefs = JSON.stringify(catalogueDefs);

        // Write the data to the TypeCatalogue file in the client files.
        fs.writeFile('../client/js/TypeCatalogue.json', catalogueDefs);

        console.log("* Entity type catalogue written to file");
    }

    // A Player is basically a CharacterHuman, so share the same type number.
    ECS.assemblages._Player.prototype.typeNumber = ECS.assemblages._CharacterHuman.prototype.typeNumber;
};

/**
 * Activates an entity of the desired type (assemblage).
 * Looks through the pool of entities of that type and gets the first inactive one, or creates a new one if they are all active.
 * @param {Function} assemblage - The public function that is used to activate the desired entity, such as ECS.assemblages.Rock.
 * @return {ECS.Entity} The entity that was activated.
 */
ECS.activateEntity = function (assemblage) {
    //console.log("in ECS.activateEntity");
    var entity;
    var pool = assemblage.prototype.pool;
    // Try to find an inactive entity of this type.
    for(var i=0, len=pool.length; i<len; i+=1){
        if(pool[i]._isActive === false){
            //console.log("inactive entity found, reusing it");
            // Inactive entity found. Let's use it.
            entity = pool[i];
            // The entity is now active.
            pool[i]._isActive = true;
            //console.log("reusing entity: " + entity.id + ", " + entity.entityType);
            return entity;
        }
    }
    //console.log("no inactive entity found, creating new one");
    // No inactive entity found. Create a new one.
    entity = new assemblage();
    // Add it to the pool.
    pool.push(entity);
    // The entity is now active.
    entity._isActive = true;
    //console.log("created new entity: " + entity.id + ", " + entity.entityType);
    return entity;
};

/**
 * Create a test entity to use for collision checks.
 * @param {ECS.Entity} privateClass - The private class assemblage to make a test entity of.
 * @param {Object} [config] - An object of the configuration properties for this entity.
 * @returns {ECS.Entity} testEntity - The test entity.
 * @constructor
 */
ECS.createTestEntity = function (privateClass, config) {

    var entityType = privateClass.prototype.entityType;

    // Set the default zone and position for all test entities to have.
    var configDefaults = config || {};
    configDefaults.zone = ECS.zonesObject['9999'];
    configDefaults.displayName = 'testDisplayName';
    configDefaults.spawnArea = '9999-1';
    configDefaults.casterEntity = ECS.testEntities.Player;
    configDefaults.x = 16;
    configDefaults.y = 16;

    //console.log("configDefaults:");
    //console.log(configDefaults);

    var testEntity = ECS.assemblages[entityType](configDefaults);

    // Don't want this entity to interact with the physics directly, or have it's position emitted.
    testEntity.setPositionEmitted(true);
    testEntity.setMaxSpeed(0);
    testEntity.setCanMove(false);
    testEntity.body.hasMoved = false;
    testEntity.body.immovable = true;

    // Remove the lifespan timeout if it has one, or it will get recycled.
    clearTimeout(testEntity.lifespanTimeout);

    // Stop it from actually doing stuff like moving around on it's own, as we only want this entity for it's body.
    testEntity.update = undefined;
    testEntity.move = undefined;
    testEntity.checkPhysics = undefined;

    // Not doing anything with the quadtree in the test-zone-entities, so remove this entity from it.
    testEntity.zone.quadTree.remove(testEntity);

    //console.log("test entity added:");
    //console.log(testEntity);

    return testEntity;
};

ECS.addTestEntity = function (privateClass, config) {
    ECS.pendingTestEntities.push({privateClass: privateClass, config: config || {}});
    return true;
};

ECS.setupTestEntities = function () {
    for(var i=0; i<ECS.pendingTestEntities.length; i+=1){
        ECS.testEntities[ECS.pendingTestEntities[i].privateClass.prototype.entityType] = ECS.createTestEntity(ECS.pendingTestEntities[i].privateClass, ECS.pendingTestEntities[i].config);
    }
    // Don't need the pending entities list any more.
    delete ECS.pendingTestEntities;
};

/**
 * Checks to see if the given entity is overlapping with any entity that it has a relationship with.
 * The more precise Crash colliders collision check. The quadtree only passes in things that are close enough to be considered, not necessarily overlapping.
 * If it returns true, the element is added to the colliding list that is returned by the quadtree query.
 * @param {Entity} element1
 * @param {Entity} element2
 * @returns {Boolean}
 */
ECS.checkIntersectsCollisionFunc = function (element1, element2) {
    if(element2 === undefined){
        //console.log("---element2 is undefined, skipping it");
        return false;
    }

    if(element1.intersectFuncs[element2.entityCategory] !== undefined){
        //collisionChecks+=1;
        if(ECS.crash.test(element1.body, element2.body, ECS.crashResponse) === true){
            element1.intersectFuncs[element2.entityCategory](element1, element2);
            return true;
        }
    }

    return false;
};

/**
 * Checks to see if the given entity collides with any other entity.
 * The more precise Crash colliders collision check. The quadtree only passes in things that are close enough to be considered, not necessarily overlapping.
 * If it returns true, the element is added to the colliding list that is returned by the quadtree query.
 * @param {Entity} element1
 * @param {Entity} element2
 * @returns {Boolean}
 */
ECS.checkEmptyCollisionFunc = function (element1, element2) {
    if(element2 === undefined){
        //console.log("---element2 is undefined, skipping it");
        return false;
    }
    if(ECS.crash.test(element1.body, element2.body) === true){
        //console.log("Bodies overlap by:");
        //console.log(ECS.crashResponse.overlapV);
        return true;
    }
};

/**
 * Returns true if the target location to place the entity at doesn't have anything blocking it.
 * @param {Zone} zone - The zone to do the collision checking in.
 * @param {Entity} testEntity - The test entity to test for collisions with.
 * @param {Number} targetX - The X position to test.
 * @param {Number} targetY - The Y position to test.
 * @param {Object} blockList - An object of the entity categories to check against.
 * @returns {Boolean}
 */
ECS.isTargetLocationEmpty = function (zone, testEntity, targetX, targetY, blockList) {
    // No test entity given. Is probably a spell that is not entity based. i.e. AOE.
    if(testEntity === false){
        return;
    }
    //console.log("iTLE, testEntity: ");
    //console.log("x: " + testEntity.x + ", y: " + testEntity.y + ", w: " + testEntity.width + ", h: " + testEntity.height);
    // Don't use Entity.reposition to move the entity, as that will update positionEmitted and hasMoved.
    testEntity.body.moveTo(targetX, targetY);
    testEntity.updateBodyPositions();

    // Add the test entity to the quadtree of the zone to check in.
    zone.quadTree.push(
        testEntity,
        false
    );

    // Find any colliding elements.
    var colliding = zone.quadTree.colliding(
        testEntity,
        ECS.checkEmptyCollisionFunc
    );

    // Remove the test entity from the quadtree of the target zone.
    zone.quadTree.remove(testEntity);

    // Check if any of the things being collided with are in the block list.
    for(var i=0, len=colliding.length; i<len; i+=1){
        // Check if the other entity is one of the things that would block this spell entity.
        if(blockList[colliding[i].entityCategory] !== undefined){
            //console.log("target location is blocked by: " + colliding[i].entityType);
            return false;
        }
    }

    // Don't use Entity.reposition to move the entity, as that will update positionEmitted and hasMoved.
    testEntity.body.moveTo(0, 0);
    testEntity.updateBodyPositions();

    // There is nothing blocking the target location.
    return true;
};

ECS.getEntitiesInRect = function (zone, x, y, width, height, centered) {

    var coordinates;
    if(centered === true){
        coordinates = {
            x: x - (width * 0.5),
            y: y - (height * 0.5),
            width: width,
            height: height
        };
    }
    else {
        coordinates = {
            x: x,
            y: y,
            width: width,
            height: height
        };
    }

    var colliding = zone.quadTree.colliding(
        coordinates//,
        //ECS.checkEmptyCollisionFunc
    );

    //console.log(colliding);
    //console.log("^ entities in rect: " + colliding.length);

    return colliding;
};

ECS.isEntityWithinRangeOfXY = function (entity, x, y, range) {

    var xDist = x - entity.body.centerX;
    var yDist = y - entity.body.centerY;
    //console.log("xDist: " + xDist + ", yDist: " + yDist);
    //var dist = Math.sqrt(xDist*xDist + yDist*yDist);
    return Math.sqrt(xDist*xDist + yDist*yDist) < range;

};
//var checked = 0;

ECS.getEntitiesInRange = function (zone, x, y, range, categoryList) {

    //checked=0;

    //console.log("zone name: " + zone.name);

    var coordinates = {
        x: x - range,
        y: y - range,
        width: range*2,
        height: range*2
    };

    var colliding = zone.quadTree.colliding(
        coordinates,
        function (element1, element2){

            //checked+=1;
            //return true;
            //console.log("element1: " + element1);
            //console.log("element2: " + element2);

            //if(element2 === undefined){
            //    //console.log("---element2 is undefined, skipping it");
            //    return false;
            //}
            if(ECS.isEntityWithinRangeOfXY(element2, x, y, range) === true){

                //console.log("entity is within range of player: " + element2);
                // If a category list was given, narrow down the search results to only things in that list.
                if(categoryList !== undefined){
                    for(var i= 0, len=categoryList.length; i<len; i+=1){
                        if(element2.entityCategory === categoryList[i]){
                            //console.log("other entity is in category list: " + element2);
                            return true;
                        }
                    }
                    return false;
                }
                else {
                    return true;
                }
            }

            return false;
        }
    );

    //console.log("entities checked: " + checked);
    //console.log("entities in radius: " + colliding.length);

    return colliding;

};

/**
 * Get the nearest entity within the given range of the target X/Y.
 * @param {Zone} zone - The zone to search in.
 * @param {Number} x - The X position to search from.
 * @param {Number} y - The Y position to search from.
 * @param {Number} range - How far to search.
 * @param {String} [category] - A specific category of entity to search for.
 * @returns {*} The nearest entity if one was found, or else false.
 */
ECS.getNearestWithinRange = function (zone, x, y, range, category) {
    var entitiesInRange;
    if(category === undefined){
        entitiesInRange = ECS.getEntitiesInRange(zone, x, y, range);
    }
    else {
        entitiesInRange = ECS.getEntitiesInRange(zone, x, y, range, [category]);
    }

    //console.log(entitiesInRange.length);

    if(entitiesInRange.length > 0){

        //console.log(">0 entities in range");
        var nearest;// = entitiesInRange[0];
        var smallestDist = range;

        for(var i=0, len=entitiesInRange.length; i<len; i+=1){
            var xDist = x - entitiesInRange[i].body.centerX;
            var yDist = y - entitiesInRange[i].body.centerY;
            //var dist = Math.sqrt(xDist*xDist + yDist*yDist);
            var dist = Math.sqrt(xDist*xDist + yDist*yDist);
            if(dist < smallestDist){
                smallestDist = dist;
                nearest = entitiesInRange[i];
            }
        }

        //console.log("nearest entity dist: " + smallestDist);
        return nearest;
    }

    // No entities are within range of the target position.
    return false;

};

// The module.exports needs to be set to the ECS object before the other files
// use it, or they will receive nothing when they require this Entity file.
module.exports = ECS;