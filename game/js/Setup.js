/**
 * Created by User on 02/06/2017.
 */

var globals = {
    // Need to define some properties before they are referred to, so they have a reference to the object that will get defined below.
    ECS: undefined,
    Spells: undefined
};

module.exports = globals;

globals.devMode = true;

// Set up the game server.
var express = require('express');
var app = express();
// Make a new HTTP server from Express.
var server = require('http').Server(app);
// Tell Socket io about the server.
globals.io = require('socket.io')(server);
// What port to bind the server to. "127.0.0.2" is localhost, this host.
//server.listen(3005, "127.0.0.2");
server.listen(3005, "0.0.0.0");

var serverName = require('../Credentials.js');
globals.serverName = serverName;
console.log("* Server name: " + globals.serverName);

// Connect to the central database.
//globals.database = require('./../Database');
//globals.database.connect();

// Get the IPv4 address of this server.
if(globals.devMode === true){
    // Localhost.
    globals.serverIPAddress = require('os').networkInterfaces()['Local Area Connection'][1]['address'];
}
else {
    // DO server.
    globals.serverIPAddress = require('os').networkInterfaces()['eth0'][0]['address'];
}
console.log("* Game server IPv4 address: " + globals.serverIPAddress);

// Connect this game server to the player manager server.
var ioClient = require('socket.io-client');
if(globals.devMode === true){
    // Localhost.
    globals.socketPM = ioClient.connect("http://127.0.0.4:3004");
}
else {
    // DO server.
    globals.socketPM = ioClient.connect("http://178.62.85.99:3004");
}

globals.PhaserRips = require('./PhaserRips');

globals.floorTileTypes = require('./tiles/FloorTileTypes');

// Set up Crash for collision detection and handling.
var Crash = require('crash-colliders');
var crash = new Crash;
crash.rbush = {};

// Import the spell definitions.
globals.Spells = require('./spells/Spells');
globals.Spells.io = globals.io;

require('./ECS/assemblages/enchantments/Invisibility');

// Set up the Entity Component System.
globals.ECS = require('./ECS/ECS');
globals.ECS.io = globals.io;
globals.ECS.crash = crash;
globals.ECS.crashResponse = new crash.Response();
globals.ECS.PhaserRips = globals.PhaserRips;
require('./ECS/Assemblages');
globals.ECS.setupTypeNumbers();
// Now that the ECS is set up, give the spells object a reference to it.
globals.Spells.ECS = globals.ECS;

globals.floorTileTypes = require('./tiles/FloorTileTypes');

globals.ECS.InventoryItemTypes.setupAssemblageReferences();

// Import the game world object.
globals.world = require('./World');
// Create the world.
globals.world.create(globals.io);

// Need to create the test entities AFTER the world is created, or there would be no zones for the entities to exist in.
globals.ECS.setupTestEntities();

// Do this after the ECS is set up, or the assemblages
// won't have been defined that the spells will refer to.
require('./spells/Elements');

// Remove the moveEntityToZone function from the zone for test entities so no other entities can join it.
globals.ECS.zonesObject['9999'].moveEntityToZone = undefined;
// Disable combat in this zone, in case the test entities try damaging each other somehow.
globals.ECS.zonesObject['9999'].combatEnabled = false;
// Not doing anything with the quadtree in the test entities zone, so remove it.
delete globals.ECS.zonesObject['9999'].quadTree;