/**
 * Created by User on 02/06/2017.
 */

"use strict";

// Gameplay is just a neat place to store gameplay related socket stuff.

var globals = require('./Setup');
//var spellStats = require('./spells/SpellStats');
var ECS = globals.ECS;
var Spells = globals.Spells;

// Quick references to commonly used properties.
//var io = globals.io;
//var spells = globals.spells;
var PhaserRips = globals.PhaserRips;

//console.log("spellCodesWithoutStarters:");
//console.log(spellCodesWithoutStarters);

//console.log("spellsByElement:");
//console.log(spellsByElement);

// Some dummy entities.
//for(var j=0; j<10; j+=1){
//    globals.ECS.assemblages.CharacterHuman('0099-4', 'Mr. Bot');
//}

module.exports.connection = function (socket) {

    // Set up some things for this new socket connection.
    socket.username = '';
    socket.inGame = false;
    socket.entity = {};
    socket.gameData = {};

    // Handles both keyboard and joystick inputs. Just takes in an angle and a force.
    socket.on('move_input', function (data) {
        moveInput(socket, data);
    });

    socket.on('move_input_keyboard_released', function (data) {
        if(!data){
            console.log("* Move input keyboard released. No data given.");
            return;
        }
        // When a key is released, it should only send the axis that was released.
        if(typeof data.axis !== 'string'){
            return;
        }
        //console.log('move_input_keyboard_released');
        if(socket.inGame === true){
            // Don't accept new move inputs if the entity is being pushed.
            if(socket.entity.beingPushed === false){
                var velocity = socket.entity.body.velocity;
                velocity[data.axis] = 0;
                if(data.axis === 'x'){
                    velocity.y = socket.entity.maxSpeed * Math.sign(velocity.y);
                }
                else {
                    velocity.x = socket.entity.maxSpeed * Math.sign(velocity.x);
                }
            }
        }
    });

    socket.on('move_input_joystick_released', function () {
        if(socket.inGame === true){
            // Don't accept new move inputs if the entity is being pushed.
            if(socket.entity.beingPushed === false){
                socket.entity.body.velocity.x = 0;
                socket.entity.body.velocity.y = 0;
            }
        }
    });

    // Cast the spell. data = pointer input world x/y.
    socket.on('cast_spell', function (data) {
        castSpell(socket, data);
    });

    // Respawn this player's entity.
    socket.on('respawn', function () {
        //console.log('respawn');
        if(socket.inGame === true) {
            socket.entity.respawn();
        }
    });

    // The player send a chat message.
    socket.on('chat', function (data) {
        // Chat messages should always be strings.
        if(typeof data !== 'string'){
            return;
        }
        else if(data.length < 1){
            return;
        }
        else if(data.length > 74){
            // If it is longer than 74 characters, truncate it.
            data = data.substring(0, 71) + "...";
        }
        if(socket.inGame === true) {
            globals.io.in(socket.entity.zone.roomName).emit('chat', {id: socket.entity.id, message: data});
        }
    });

    // Respawn this player's entity.
    socket.on('kill_self', function () {
        if(socket.inGame === true) {
            socket.entity.kill();
        }
    });

    // This player wants to join an order.
    socket.on('join_order', function () {
        if(socket.inGame === true) {
            // Check this zone actually has an order core to join.
            if(socket.entity.zone.order.core === null){
                return;
            }
            //if(socket.entity.order !== null){
            //    socket.entity.zone.orderCore.removeOrderMember(socket.entity);
            //}

            // Check the order core isn't too far from the player entity.
            if(socket.entity.isEntityWithinRange(socket.entity.zone.order.core.body, 64) === true){
                // Check the entity isn't already in an order.
                if(socket.entity.order === null){
                    socket.entity.zone.order.core.addOrderMember(socket.entity);
                }
            }
        }
    });

    socket.on('use_item', function (data) {
        //console.log('use item event, data: ' + data);
        if(socket.inGame === true){
            if(Number.isFinite(data) === false){
                console.log("* Use item failed. Data is not a number.");
                return;
            }
            //console.log("data is a number");

            var item = socket.entity.inventory.contents[data];
            //console.log(item);
            if(item === null){
                //console.log("item is null");
                return
            }
            if(item.itemType.use !== undefined){
                //console.log("item is not undefined");
                if(item.itemType.use(socket.entity, data) === true){
                    socket.emit("remove_item", data);
                }
            }
        }
    });

    socket.on('drop_item', function (data) {
        if(socket.inGame === true) {
            //console.log("drop item event, data:");
            //console.log(data);
            if(!data){
                console.log("* Drop item failed. No data given.");
                return;
            }
            else if(Number.isFinite(data.index) === false){
                return;
            }
            else if(Number.isFinite(data.targetX) === false){
                return;
            }
            else if(Number.isFinite(data.targetY) === false){
                return;
            }

            var entity = socket.entity;
            var item = entity.inventory.contents[data.index];
            // Drop an item entity on the floor of this item type if it has an entity form.
            //console.log("dropping item:");
            //console.log(entity.inventory.contents[data.index]);
            // Check there is something in that inventory slot.
            if(item === undefined){
                return;
            }
            if(item === null){
                return;
            }
            if(item.itemType.entityAssemblage !== undefined){
                // TODO: check for space at the target location, might be blocked, isTargetLocationEmpty?
                // ADD SOMETHING TO ITEM TYPES LIKE gridAligned FOR STRUCTURES.
                var itemEntity = item.itemType.entityAssemblage({
                    zone: entity.zone,
                    x: data.targetX,
                    y: data.targetY,
                    itemConfig: item.itemConfig
                });
                // Don't drop the item right on top of the player entity or they will pick it back up.
                itemEntity.repositionAwayFromEntity(entity, data.targetX, data.targetY, 10);
                //console.log("item entity spawned");
            }

            if(ECS.InventoryManager.removeItemByIndex(entity, data.index)){
                //console.log("item removed");
                socket.emit('remove_item', data.index);
            }
        }
    });

    socket.on('interact', function (data) {
        if(socket.inGame === true) {
            //console.log("interact event, data:");
            //console.log(data);
            if(!data){
                console.log("* Interact failed. No data given.");
                return;
            }
            else if(Number.isFinite(data.x) === false){
                return;
            }
            else if(Number.isFinite(data.y) === false){
                return;
            }

            // Get the entity that was interacted with, if any.
            var targetEntity = ECS.getNearestWithinRange(socket.entity.zone, data.x, data.y, 16, ECS.Categories.STRUCTURE);

            //console.log("interact, targetEntity: " + targetEntity.entityType);
            //console.log("interact, socketID: " + socket.id + ", x: " + data.x + ", y: " + data.y);

            // Check an entity was actually found.
            if(targetEntity !== false){
                // Check the target entity isn't too far away.
                if(socket.entity.isEntityWithinRange(targetEntity.body, 64) === true){
                    //console.log("target entity is within range");
                    if(targetEntity.interaction !== undefined){
                        //console.log("target entity has an interaction func");
                        targetEntity.interaction();
                    }
                }
                else {
                    //console.log("target entity too far away");
                }
            }

        }
    });

    socket.on('buy_spell', function () {
        if(socket.inGame === true){

            var entity = socket.entity;
            var shrine = entity.zone.shrine;

            // Check this zone actually has a shrine to buy from.
            if(shrine !== null){
                //Check the distance this entity is from the shrine.
                if(entity.isEntityWithinRange(shrine.body, 64)){
                    //console.log("shrine is within range. shrine element: " + shrine.element);

                    var gemsCount = ECS.InventoryManager.countItems(entity, ECS.InventoryItemTypes.GEMS);
                    // Spell scrolls cost 4 gems.
                    if(gemsCount > 3){
                        // Remove the gems cost.
                        // Tell the client to remove those gems.
                        socket.emit("remove_many_items", ECS.InventoryManager.removeManyItemsByType(entity, ECS.InventoryItemTypes.GEMS, 4));

                        // Get a random spell of the element of the shrine.
                        //var randSpell = spellCodesWithoutStarters[Math.floor(Math.random()*spellCodesWithoutStarters.length)];
                        var randSpell = Spells.spellsByElement[shrine.element][Math.floor(Math.random()*Spells.spellsByElement[shrine.element].length)];

                        //console.log("random spell:");
                        //console.log(randSpell);

                        // Tell the client to add this scroll.
                        socket.emit("add_item", {
                            type: ECS.InventoryItemTypes.SCROLL.value,
                            index: ECS.InventoryManager.addItem(entity, ECS.InventoryItemTypes.SCROLL, {spellName: randSpell.name, spellCode: randSpell.code}), config: {spellName: randSpell.name}
                        });

                        socket.emit("alert_message", "Spell scroll bought\nfor 4 Gems.");

                    }
                    else {
                        socket.emit("alert_message", "4 Gems needed to\nbuy a spell scroll.");
                    }

                }

            }

        }

    });

    socket.on('craft', function (data) {
        console.log("crafting, data: " + data);
        if(!data){
            return
        }
        if(typeof data !== 'string'){
            return
        }
        if(ECS.InventoryItemTypes[data] === undefined){
            console.log("invalid item type");
            return
        }
        if(socket.inGame === true){
            var entity = socket.entity;
            if(entity.inventory.isFull === false){
                // TODO: crafting doesn't cost any resources, probably will need some kind of crafting manager to handle resource costs
                socket.emit('add_item', {
                    type: ECS.InventoryItemTypes[data].value,
                    index: ECS.InventoryManager.addItem(entity, ECS.InventoryItemTypes[data])
                });

                socket.emit("alert_message", "Order Core crafted.");
            }
            else {
                socket.emit("alert_message", "Cannot craft.\nInventory full.");
            }

        }
    });
};

function moveInput(socket, data){
    //console.log('move_input, entity.id: ' + socket.entity.id);
    //console.log("entity : " + socket.entity.body.centerX + " - x|y - " + socket.entity.body.centerY);

    if(socket.inGame === true){
        if(!data){
            console.log("* Move input failed. No data given.");
            return;
        }
        // Check if the angle is a not a number.
        // Use isFinite instead of `typeof === 'Number'` to make sure it isn't Infinity.
        if(Number.isFinite(data.angle) === false){
            console.log("* Move input failed. Angle is not a number. data.x: " + data.x);
            return;
        }
        // Check if the force is a not a number.
        if(Number.isFinite(data.force) === false){
            console.log("* Move input failed. Force is not a number. data.y: " + data.y);
            return;
        }

        var force = data.force || 1;
        // Check the given force isn't greater than 1.
        if(force > 1){
            force = 1;
        }
        var entity = socket.entity;

        // Check that the entity for this player is alive.
        if(entity.hitPoints > 0){
            // Don't accept new move inputs if the entity is being pushed.
            // Don't change the velocity, or the pushed effect will stop.
            if(entity.beingPushed === false){
                entity.body.velocity = PhaserRips.Physics.velocityFromAngle(data.angle, force * entity.maxSpeed);
            }
        }
    }
}

function castSpell(socket, data){
    if(socket.inGame === true){
        if(!data){
            console.log("* Spell cast failed. No data given.");
            return;
        }
        if(!data.hasOwnProperty('spellCode')){
            console.log("* Spell cast failed. No spell code, x and/or y property on the data object.");
            return;
        }
        // Check if the spell code is a not a string, or if it is empty.
        if(typeof data.spellCode !== 'string' || data.spellCode === ''){
            console.log("* Spell cast failed. Spell code is not a string, or is an empty string. data.spellCode: " + data.spellCode);
            return;
        }
        // Check if the x is a not a number.
        if(Number.isFinite(data.x) === false){
            console.log("* Spell cast failed. X is not a number. data.x: " + data.x);
            return;
        }
        // Check if the y is a not a number.
        if(Number.isFinite(data.y) === false){
            console.log("* Spell cast failed. Y is not a number. data.y: " + data.y);
            return;
        }
        //console.log(data);

        //console.log("cast spell received, data: " + data.spellCode);

        socket.entity.castSpell(data.spellCode, data.x, data.y);
    }
}

/*function adminUnlockSpell(socket, data){
    if(socket.gameData.accountType === 'A'){

    }
}*/