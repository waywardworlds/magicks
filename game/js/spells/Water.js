/**
 * Created by User on 01/05/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Splash',
        testEntity: ECS.testEntities.Splash,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.Splash({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                rotation: projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Summon Nimbus',
        testEntity: ECS.testEntities.Nimbus,
        castFunc: function (casterEntity, targetX, targetY) {
            var minion = ECS.assemblages.Nimbus({casterEntity: casterEntity, x: targetX, y: targetY});
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(minion.zone.roomName).emit('add_entity', {
                typeNumber: minion.typeNumber,
                id: minion.id,
                rotation: minion.body.rotation,
                x: Math.trunc(minion.body.centerX),
                y: Math.trunc(minion.body.centerY)
            });
            return minion;
        }
    }
);