/**
 * Created by User on 28/07/2017.
 */

"use strict";

var spellStats = require('./SpellStats');

var Spells = {
    io: undefined,

    ECS: undefined,
    
    spells: {},

    enchantments: {},

    testCasterEntity: undefined,

    spellCodesWithoutStarters: [],

    spellsByElement: {
        'l': [],
        'd': [],
        'w': [],
        'f': [],
        'e': [],
        'p': [],
        'v': []
    }
};

var keys = Object.keys(spellStats);
for(var i=0; i<keys.length; i+=1){
    // Get all the spells that are longer than 1 component long, i.e. are not starter spells.
    if(spellStats[keys[i]].code.length > 1){
        Spells.spellCodesWithoutStarters.push(spellStats[keys[i]].code);

        Spells.spellsByElement[spellStats[keys[i]].code[0]].push({name: keys[i], code: spellStats[keys[i]].code});

    }
}

/**
 * Adds a new spell to the list of spells.
 * @param {Object} config - An object of the configuration properties for this spell.
 * @param {String} [config.name] - The name , i.e. Summon Critter. - Default: 'NO NAME GIVEN'
 * @param {Function} [config.testEntity] - If this spell creates an entity, this should be the assemblage of that entity. - Default: false
 * @param {Function} config.castFunc - A function of specific stuff that this spell should do.
 * @constructor
 */
Spells.Spell = function (config) {

    //console.log("spell stats for spell: " + config.name);
    //console.log(spellStats[config.name]);

    this.code =             spellStats[config.name].code            || console.log("* WARNING: A spell is being defined without a code.");
    this.name =             config.name                             || 'NO NAME GIVEN';
    this.energyCost =       spellStats[config.name].energyCost      || 0;
    this.itemCost =         spellStats[config.name].itemCost        || null;
    this.recastCooldown =   spellStats[config.name].recastCooldown  || 0;
    this.castRange =        spellStats[config.name].castRange       || 1000;
    this.startFromCaster =  spellStats[config.name].startFromCaster || false;
    this.gridAligned =      spellStats[config.name].gridAligned     || false;
    this.blockList =        spellStats[config.name].blockList       || {[Spells.ECS.Categories.SOLID]: true};
    this.xpGain =           spellStats[config.name].xpGain          || 0;

    // Use a dummy entity for this spell to use when checking if the target location is empty.
    // Not all spells are entity based, so some spells won't need one of these, such as if they are instant effect.
    // This avoids activating an entity, then checking, only to then have to deactivate it if the target location is blocked.
    // This entity should NEVER get deactivated.
    if(config.testEntity){
        this.testSpellEntity = config.testEntity;
    }
    else {
        this.testSpellEntity =  false;
    }

    this.castFunc =         config.castFunc;

    // Add this spell to the list of spells.
    Spells.spells[this.code] = this;

};

Spells.cast = function (spellCode, casterEntity, targetX, targetY) {

    // Check a spell with that code is defined.
    if(Spells.spells[spellCode] === undefined) {
        //console.log("Spell does not exist for given spell code: " + spellCode);
        return false;
    }

    // Cast the spell, and return anything that it returns.
    return Spells.spells[spellCode].cast(casterEntity, targetX, targetY);

};

Spells.Spell.prototype.cast = function (casterEntity, targetX, targetY) {

    if(this.canCast(casterEntity, targetX, targetY) === false){
        return false;
    }

    var castSuccess;

    // Do the specific stuff for the spell being cast, such as creating any entities.
    // Change the target position to be grid aligned if needed.
    if(this.gridAligned === true){
        castSuccess = this.castFunc(casterEntity, (~~(targetX / 32)) * 32, (~~(targetY / 32)) * 32);
    }
    else {
        castSuccess = this.castFunc(casterEntity, targetX, targetY);
    }

    if(castSuccess === false){
        // The spell has failed.
        //console.log("spell cast failed");
        return false;
    }
    else {
        // Reduce player's energy by the spell energy cost.
        casterEntity.modEnergy(-this.energyCost);

        // Some spells return energy instead of taking it away, so check that
        // the new energy isn't greater than their max energy.
        if(casterEntity.energy > casterEntity.maxEnergy){
            casterEntity.setEnergy(casterEntity.maxEnergy);
        }

        // Check if this spell requires some items for it to be cast.
        if(this.itemCost !== null){
            // Only entities that have an inventory component should have cast this spell.
            var inventoryManager = Spells.ECS.InventoryManager;
            var itemCost;
            var indexes = [];
            // We already know this entity has enough of each type of item, or canCast would have returned false.
            // Just need to remove the amount of each item type.
            for(var i=0, len=this.itemCost.length; i<len; i+=1){
                itemCost = this.itemCost[i];
                indexes = indexes.concat(inventoryManager.removeManyItemsByType(casterEntity, itemCost.itemType, itemCost.amount));
            }

            // If this is a player's entity, tell them to remove these items from their inventory panel.
            if(casterEntity.socket !== undefined){
                //console.log("remove items emit, indexes: " + indexes);
                casterEntity.socket.emit("remove_many_items", indexes);
            }
        }

        /*if(this.woodCost > 0){
            console.log("spell costs wood: " + this.woodCost);
            // Reduce the player's resources by the cost.
            casterEntity.modWood(-this.woodCost);
            //casterEntity.modWood(-this.woodCost);
            //casterEntity.modWood(-this.woodCost);
        }*/

        // Update the nextCastTime for the caster.
        casterEntity.setNextCastTime(this.recastCooldown);

        // The spell has been cast.
        return true;
    }

};

Spells.Spell.prototype.canCast = function (casterEntity, targetX, targetY) {
    // Check the caster has enough energy.
    if(casterEntity.energy < this.energyCost) {
        return false;
    }

    // Check if this spell requires some items for it to be cast.
    if(this.itemCost !== null){
        // Only entities that have an inventory component should have cast this spell.
        var inventoryManager = Spells.ECS.InventoryManager;
        // Check the caster has enough of each type of item.
        for(var i=0, len=this.itemCost.length; i<len; i+=1){
            var itemCost = this.itemCost[i];
            //console.log("canCast, itemCost:");
            //console.log(itemCost);
            if(inventoryManager.countItems(casterEntity, itemCost.itemType) < itemCost.amount){
                if(casterEntity.socket !== undefined){
                    casterEntity.socket.emit("alert_message", "Not enough resources:\n" + itemCost.itemType.value + " x " + itemCost.amount);
                }
                return false;
            }
        }
    }

    // Check that the spell recast delay is over for the caster before trying to cast a spell.
    if(casterEntity.nextCastTime > Date.now()) {
        return false;
    }

    // Check the target location isn't further than the cast range for this spell.
    var xDist = casterEntity.body.centerX - targetX;
    var yDist = casterEntity.body.centerY - targetY;
    var dist = Math.sqrt(xDist*xDist + yDist*yDist);
    if(dist > this.castRange){
        // Spell cast failed. Target location is too far away.
        //console.log("Spell cast failed, target too far away.");
        return false;
    }

    // Should the entity to create start from the caster.
    // If so, move the target X and Y to away from the caster along the angle to the target.
    // Basically does Entity.repositionAwayFromEntity, but without the other stuff that Entity.reposition does.
    if(this.startFromCaster === true){
        var pos = casterEntity.body.pos;
        var angle = Math.atan2(targetY - pos.y, targetX - pos.x);
        var distance = casterEntity.body.detectionRadius;

        targetX = Math.cos(angle) * distance + pos.x;
        targetY = Math.sin(angle) * distance + pos.y;
        //console.log("Changing target to be from edge of caster detection radius in target direction.");
    }
    // Should the target location of this spell be aligned to the game world grid.
    if(this.gridAligned === true){
        targetX = ((~~(targetX / 32)) * 32) + 1;
        targetY = ((~~(targetY / 32)) * 32) + 1;
        //console.log("Changing target to be on the game world grid");
    }

    if(Spells.ECS.isTargetLocationEmpty(casterEntity.zone, this.testSpellEntity, targetX, targetY, this.blockList) === false){
        //console.log("Casting spell, target location is blocked");
        return false;
    }

    // Spell can be cast.
    return true;
};

module.exports = Spells;