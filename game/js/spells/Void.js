/**
 * Created by User on 12/04/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Deflect',
        testEntity: ECS.testEntities.Deflector,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.Deflector({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                rotation: projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Tele Orb',
        testEntity: ECS.testEntities.TeleOrb,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.TeleOrb({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Recall Minions',
        castFunc: function (casterEntity) {
            var minions = casterEntity.minions,
                centerX = casterEntity.body.centerX,
                centerY = casterEntity.body.centerY;

            for(var minionId in minions){
                if(minions.hasOwnProperty(minionId)){
                    // Move the minion to the position of the caster.
                    minions[minionId].reposition(centerX + ECS.PhaserRips.Math.randIntInRange(-10, 10), centerY + ECS.PhaserRips.Math.randIntInRange(-10, 10));
                    minions[minionId].checkPhysics();
                }
            }
        }
    }
);

new spells.Spell(
    {
        name: 'Summon Inverter',
        testEntity: ECS.testEntities.Inverter,
        castFunc: function (casterEntity, targetX, targetY) {
            var inverter = ECS.assemblages.Inverter({casterEntity: casterEntity, x: targetX, y: targetY});
            // Send the entity for this inverter to all the players in this zone.
            spells.io.in(inverter.zone.roomName).emit('add_entity', {
                typeNumber: inverter.typeNumber,
                id: inverter.id,
                x: Math.trunc(inverter.body.centerX),
                y: Math.trunc(inverter.body.centerY)
            });
            return inverter;
        }
    }
);
/*
new spells.Spell(
    {
        name: 'Build Order Core',
        testEntity: ECS.testEntities.OrderCore,
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("in build order core castfunc");
            // Don't build a core in a zone that already has one.
            if(casterEntity.zone.orderCore !== null){
                if(casterEntity.socket !== undefined){
                    casterEntity.socket.emit("alert_message", "This zone already\nhas an Order core.");
                }
                return false;
            }

            // Check if the target floor tile type can be built on.
            if(casterEntity.zone.floorGrid[targetY / 32][targetX / 32].type.canBeBuiltOn === false){
                return false;
            }

            return ECS.assemblages.OrderCore({zone: casterEntity.zone, x: targetX, y: targetY});
        }
    }
);

new spells.Spell(
    {
        name: 'Build Wood Wall',
        testEntity: ECS.testEntities.WoodWall,
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("in build wood wall castfunc");

            // Check if the target floor tile type can be built on.
            if(casterEntity.zone.floorGrid[targetY / 32][targetX / 32].type.canBeBuiltOn === false){
                return false;
            }

            // Check the entity is in an order.
            if(casterEntity.order){
                //console.log("caster has an order");
                // Don't build in a zone that this entity's order doesn't own.
                if(casterEntity.zone !== casterEntity.order.zone){
                    //console.log("caster is not in order zone");
                    if(casterEntity.socket !== undefined){
                        casterEntity.socket.emit("alert_message", "Your Order does not\nown this zone.");
                    }
                    return false;
                }
                // Check there is space for another order structure.
                if(casterEntity.zone.orderStructuresCount < casterEntity.zone.maxOrderStructures){
                    //console.log("zone has space for more structures");
                    return ECS.assemblages.WoodWall({zone: casterEntity.zone, x: targetX, y: targetY});
                }
            }
            else {
                if(casterEntity.socket !== undefined){
                    casterEntity.socket.emit("alert_message", "You must be in an Order\nto cast this spell.");
                }
            }
            return false;
        }
    }
);

new spells.Spell(
    {
        name: 'Build Stone Wall',
        testEntity: ECS.testEntities.StoneWall,
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("in build stone wall castfunc");

            // Check if the target floor tile type can be built on.
            if(casterEntity.zone.floorGrid[targetY / 32][targetX / 32].type.canBeBuiltOn === false){
                return false;
            }

            // Check the entity is in an order.
            if(casterEntity.order){
                //console.log("caster has an order");
                // Don't build in a zone that this entity's order doesn't own.
                if(casterEntity.zone !== casterEntity.order.zone){
                    //console.log("caster is not in order zone");
                    if(casterEntity.socket !== undefined){
                        casterEntity.socket.emit("alert_message", "Your Order does not\nown this zone.");
                    }
                    return false;
                }
                // Check there is space for another order structure.
                if(casterEntity.zone.orderStructuresCount < casterEntity.zone.maxOrderStructures){
                    //console.log("zone has space for more structures");
                    return ECS.assemblages.StoneWall({zone: casterEntity.zone, x: targetX, y: targetY});
                }
            }
            else {
                if(casterEntity.socket !== undefined){
                    casterEntity.socket.emit("alert_message", "You must be in an Order\nto cast this spell.");
                }
            }
            return false;
        }
    }
);

new spells.Spell(
    {
        name: 'Build Wood Door',
        testEntity: ECS.testEntities.WoodDoor,
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("in build wood door castfunc");

            // Check if the target floor tile type can be built on.
            if(casterEntity.zone.floorGrid[targetY / 32][targetX / 32].type.canBeBuiltOn === false){
                return false;
            }

            // Check the entity is in an order.
            if(casterEntity.order){
                //console.log("caster has an order");
                // Don't build in a zone that this entity's order doesn't own.
                if(casterEntity.zone !== casterEntity.order.zone){
                    //console.log("caster is not in order zone");
                    if(casterEntity.socket !== undefined){
                        casterEntity.socket.emit("alert_message", "Your Order does not\nown this zone.");
                    }
                    return false;
                }
                // Check there is space for another order structure.
                if(casterEntity.zone.orderStructuresCount < casterEntity.zone.maxOrderStructures){
                    //console.log("zone has space for more structures");
                    return ECS.assemblages.WoodDoor({zone: casterEntity.zone, x: targetX, y: targetY});
                }
            }
            else {
                if(casterEntity.socket !== undefined){
                    casterEntity.socket.emit("alert_message", "You must be in an Order\nto cast this spell.");
                }
            }
            return false;
        }
    }
);*/