/**
 * Created by User on 12/04/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Fire Bolt',
        testEntity: ECS.testEntities.FireBolt,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.FireBolt({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                rotation: projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Flamethrower',
        testEntity: ECS.testEntities.Flamethrower,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.Flamethrower({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

/*
 spells.defineSpell('300', 'Fire Ball I', 100, 5, 4, null, "Deal 10 damage to enemy.",
 function () {
 console.log("fire ball I cast");
 },
 function () {
 console.log("fire ball I cast");
 }
 );

 spells.defineSpell('301', 'Fire Ball II', 200, 7, 4, null, "+ AoE Burning on impact.",
 function () {
 console.log("");
 },
 function () {
 console.log("");
 }
 );

 spells.defineSpell('310', 'Explosion I', 200, 7, 4, null, "Deal 15 damage to all nearby enemies.",
 function () {
 console.log("");
 }
 );

 spells.defineSpell('311', 'Explosion II', 200, 7, 4, null, "+ Burning.",
 function () {
 console.log("");
 }
 );

 spells.defineSpell('320', 'Fire Wave I', 200, 7, 4, null, "Deal 5 damage to any enemy it passes over.",
 function () {
 console.log("");
 },
 function () {
 console.log("");
 }
 );

 spells.defineSpell('330', 'Inferno', 200, 7, 4, null, "Inflict burning on enemies in this area. Last 10 seconds.",
 function () {
 console.log("");
 },
 function () {
 console.log("");
 }
 );*/