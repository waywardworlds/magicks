/**
 * Created by User on 28/07/2017.
 */

require('./Light');
require('./Dark');
require('./Water');
require('./Fire');
require('./Earth');
require('./Psychic');
require('./Void');