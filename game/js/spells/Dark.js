/**
 * Created by User on 01/05/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Raven Strike',
        testEntity: ECS.testEntities.RavenStrike,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.RavenStrike({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                rotation: projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Target Bolt',
        testEntity: ECS.testEntities.TargetBolt,
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("casting target bolt");
            var projectile = ECS.assemblages.TargetBolt({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                rotation: projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);


new spells.Spell(
    {
        name: 'Summon Critter',
        testEntity: ECS.testEntities.Critter,
        castFunc: function (casterEntity, targetX, targetY) {
            var minion = ECS.assemblages.Critter({casterEntity: casterEntity, x: targetX, y: targetY});

            //console.log("critter spawned, id: " + minion.id);
            //console.log(minion.zone.floorGrid);
            //console.log();
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(minion.zone.roomName).emit('add_entity', {
                typeNumber: minion.typeNumber,
                id: minion.id,
                x: Math.trunc(minion.body.centerX),
                y: Math.trunc(minion.body.centerY)
            });
            return minion;
        }
    }
);

new spells.Spell(
    {
        name: 'Destroy Minion',
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("destroy minion cast");
            var minions = casterEntity.minions;

            for(var minionId in minions){
                if(minions.hasOwnProperty(minionId)){
                    //console.log("destroying minion type: " + minions[minionId].entityType);
                    // Check if any minion of this entity is under the target position.
                    if(minions[minionId].isEntityWithinTargetPosition(targetX, targetY)){
                        //console.log("minion is within target pos");
                        // Kill the target minion.
                        minions[minionId].kill();
                        // Only kill one minion per cast.
                        //console.log(" - minion destroyed");
                        break;
                    }
                }
            }
        }
    }
);