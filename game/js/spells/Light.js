/**
 * Created by User on 11/04/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Heal Orb',
        testEntity: ECS.testEntities.HealOrb,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.HealOrb({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                rotation: projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Plant Sapling',
        testEntity: ECS.testEntities.Sapling,
        castFunc: function (casterEntity, targetX, targetY) {
            // Don't go over the max amount of resources nodes in this zone.
            if(this.resourceNodesCount >= this.maxResourceNodes){
                return false;
            }
            // Check if the target floor tile type can be built on.
            var floorTileType = casterEntity.zone.floorGrid[targetY / 32][targetX / 32].type;
            if(floorTileType.canBeBuiltOn === false){
                return false;
            }
            // Paths can be built on, but trees shouldn't grow out of it.
            else if(floorTileType === globals.floorTileTypes.Path){
                return false;
            }
            // Snow can be built on, but trees shouldn't grow out of it.
            else if(floorTileType === globals.floorTileTypes.Snow){
                return false;
            }
            //console.log("sapling spell, tarX: " + targetX/32 + ", tarY: " + targetY/32);

            return ECS.assemblages.Sapling({zone: casterEntity.zone, x: targetX, y: targetY});
        }
    }
);