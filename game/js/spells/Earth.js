/**
 * Created by User on 01/05/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Rock Throw',
        testEntity: ECS.testEntities.Rock,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.Rock({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id: projectile.id,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Stone Shards',
        testEntity: ECS.testEntities.StoneShard,
        castFunc: function (casterEntity, targetX, targetY) {
            var config = {casterEntity: casterEntity, x: targetX, y: targetY};
            var projectile1 = ECS.assemblages.StoneShard(config);
            var projectile2 = ECS.assemblages.StoneShard(config);
            var projectile3 = ECS.assemblages.StoneShard(config);

            var angle = ECS.PhaserRips.Physics.angleFromBody(projectile1.body, targetX, targetY);
            var maxSpeed = projectile1.maxSpeed;
            var radians = ECS.PhaserRips.Math.degToRad(angle);
            // How far the side projectiles should be from the middle one.
            var radiansOffset = ECS.PhaserRips.Math.degToRad(22);

            // Start the projectiles moving.
            // projectile1 moves towards the target.
            projectile1.body.rotation = moveToXY(projectile1.body, targetX, targetY, maxSpeed);

            // projectile2 moves a bit clockwise of projectile1.
            projectile2.body.velocity.x = Math.cos(radians + radiansOffset) * maxSpeed;
            projectile2.body.velocity.y = Math.sin(radians + radiansOffset) * maxSpeed;
            projectile2.body.rotation = projectile1.body.rotation + radiansOffset;

            // projectile2 moves a bit anti-clockwise of projectile1.
            projectile3.body.velocity.x = Math.cos(radians - radiansOffset) * maxSpeed;
            projectile3.body.velocity.y = Math.sin(radians - radiansOffset) * maxSpeed;
            projectile3.body.rotation = projectile1.body.rotation - radiansOffset;

            var center = {
                x: Math.trunc(projectile1.body.centerX),
                y: Math.trunc(projectile1.body.centerY)
            };

            // Send the entities for these projectiles to all the players in this zone.
            spells.io.in(projectile1.zone.roomName).emit('add_entity', {typeNumber: projectile1.typeNumber, id: projectile1.id, rotation: projectile1.body.rotation, x: center.x, y: center.y});
            spells.io.in(projectile2.zone.roomName).emit('add_entity', {typeNumber: projectile2.typeNumber, id: projectile2.id, rotation: projectile2.body.rotation, x: center.x, y: center.y});
            spells.io.in(projectile3.zone.roomName).emit('add_entity', {typeNumber: projectile3.typeNumber, id: projectile3.id, rotation: projectile3.body.rotation, x: center.x, y: center.y});
        }
    }
);

new spells.Spell(
    {
        name: 'Barrier',
        testEntity: ECS.testEntities.Barrier,
        castFunc: function (casterEntity, targetX, targetY) {
            // Don't allow barriers to be created in non-combat zones. Too trollable.
            if(casterEntity.zone.combatEnabled === false){
                return false;
            }

            // Check if the target floor tile type can be built on.
            if(casterEntity.zone.floorGrid[targetY / 32][targetX / 32].type.canBeBuiltOn === false){
                return false;
            }
            // Create a barrier, aligned to the map grid.
            var obstacle = ECS.assemblages.Barrier({zone: casterEntity.zone, x: targetX, y: targetY});

            // Tell the players in the zone to add this entity.
            spells.io.in(obstacle.zone.roomName).emit('add_entity', {
                typeNumber: obstacle.typeNumber,
                id: obstacle.id,
                x: Math.trunc(obstacle.body.centerX),
                y: Math.trunc(obstacle.body.centerY)
            });
            return obstacle;
        }
    }
);