/**
 * Created by User on 01/05/2017.
 */

"use strict";

var globals = require('./../Setup');
var spells = globals.Spells;
var ECS = globals.ECS;
var moveToXY = globals.PhaserRips.Physics.moveToXY;

new spells.Spell(
    {
        name: 'Mind Bullet',
        testEntity: ECS.testEntities.MindBullet,
        castFunc: function (casterEntity, targetX, targetY) {
            var projectile = ECS.assemblages.MindBullet({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id:         projectile.id,
                rotation:   projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;
        }
    }
);

new spells.Spell(
    {
        name: 'Draining Bolt',
        testEntity: ECS.testEntities.DrainingBolt,
        castFunc: function (casterEntity, targetX, targetY) {
            //console.log("draining bolt");
            var projectile = ECS.assemblages.DrainingBolt({casterEntity: casterEntity, x: targetX, y: targetY});
            // Start the projectile moving.
            projectile.body.rotation = moveToXY(projectile.body, targetX, targetY, projectile.maxSpeed);
            // Send the entity for this projectile to all the players in this zone.
            spells.io.in(projectile.zone.roomName).emit('add_entity', {
                typeNumber: projectile.typeNumber,
                id:         projectile.id,
                rotation:   projectile.body.rotation,
                x: Math.trunc(projectile.body.centerX),
                y: Math.trunc(projectile.body.centerY)
            });
            return projectile;

        }
    }
);

new spells.Spell(
    {
        name: 'Invisibility',
        castFunc: function (casterEntity) {
            //console.log("invisibility cast");

            if(casterEntity.enchantment !== undefined){
                casterEntity.enchantment.end();
            }
            else {
                spells.enchantments.Invisibility({casterEntity: casterEntity, targets: [casterEntity]});
            }
        }
    }
);
