/**
 * Created by User on 30/11/2017.
 */

var InventoryItemTypes = require('../ECS/InventoryItemTypes');
var Categories = require('../ECS/ECS').Categories;
var ECS = require('../ECS/ECS');

/**
 * Available parameters. If something would be the same as the default value, then just omit it.
 *
 *      Parameter                   |   Default value   |   Description
 *  --------------------------------+-------------------+--------------------------------
 *      code: (String)              |   *Required*      |   The element combo to cast this spell, i.e. 'ddp'. Must be unique. The first character represents the main element of this spell, i.e. 'ewwl' would be an Earth spell.
 *      energyCost: (Number)        |   0               |   How much energy to take from the caster entity if this spell is successfully cast.
 *      itemCost: {Array}           |   null            |   What items this spell requires for it to be cast. i.e. buildings need materials. Should look like [{itemType: InventoryItemTypes.ITEM_NAME, amount: NUMBER}].
 *          itemType: {Number}      |   undefined       |   The type of the required items.
 *          amount: {Number}        |   1               |   How many of this item type are required.
 *      recastCooldown: (Number)    |   0               |   How long in milliseconds after casting this spell before the entity can cast again.
 *      castRange: (Number)         |   1000            |   How far away from the caster can the input position for this spell be.
 *      startFromCaster: (Boolean)  |   false           |   Whether this spell starts from the caster's position.
 *      gridAligned: (Boolean)      |   false           |   Whether this spell should be aligned to the game world grid.
 *      blockList: (Object)         |   {solid: true}   |   An object list of the entity category codes of entities that will block this spell from being cast.
 *      xpGain: (Object)            |   0               |   How much to increase the XP of the element of this spell by on the caster when cast.
 */

var spellStats = {

    //  -   -   Light spells.   -   -
    'Heal Orb': {
        code: 'l',
        energyCost: 50,
        recastCooldown: 600,
        startFromCaster: true,
        xpGain: 10
    },

    'Plant Sapling': {
        code: 'llwwee',
        energyCost: 300,
        recastCooldown: 2000,
        gridAligned: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.PICKUP]: true,
            [Categories.STRUCTURE]: true
        },
        xpGain: 10
    },

    //  -   -   Dark spells.    -   -
    'Raven Strike': {
        code: 'd',
        energyCost: 50,
        recastCooldown: 250,
        startFromCaster: true,
        xpGain: 10
    },

    'Target Bolt': {
        code: 'dd',
        energyCost: 0,
        recastCooldown: 1000,
        startFromCaster: true,
        xpGain: 1
    },

    'Summon Critter': {
        code: 'ddp',
        energyCost: 100,
        recastCooldown: 100,
        castRange: 100,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.STRUCTURE]: true
        },
        xpGain: 10
    },

    'Destroy Minion': {
        code: 'df',
        recastCooldown: 200,
        xpGain: 10
    },

    //  -   -   Water spells.   -   -
    'Splash': {
        code: 'w',
        energyCost: 50,
        recastCooldown: 250,
        startFromCaster: true,
        xpGain: 10
    },

    'Summon Nimbus': {
        code: 'wwd',
        energyCost: 20,
        recastCooldown: 250,
        castRange: 100,
        xpGain: 10
    },

    //  -   -   Fire spells.    -   -
    'Fire Bolt': {
        code: 'f',
        energyCost: 100,
        recastCooldown: 500,
        startFromCaster: true,
        xpGain: 10
    },

    'Flamethrower': {
        code: 'ff',
        energyCost: 20,
        recastCooldown: 100,
        startFromCaster: true,
        xpGain: 1
    },

    //  -   -   Earth spells.   -   -
    'Rock Throw': {
        code:               'e',
        energyCost:         40,
        recastCooldown:     350,
        startFromCaster:    true,
        xpGain: 10
    },

    'Stone Shards': {
        code:               'eee',
        energyCost:         70,
        recastCooldown:     600,
        startFromCaster:    true,
        xpGain: 10
    },

    'Barrier': {
        code:               'eeee',
        energyCost:         100,
        recastCooldown:     200,
        castRange:          100,
        gridAligned:        true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.STRUCTURE]: true
        },
        xpGain: 10
    },

    //  -   -   Psychic spells. -   -
    'Mind Bullet': {
        code: 'p',
        energyCost: 40,
        recastCooldown: 300,
        startFromCaster: true,
        xpGain: 10
    },

    'Draining Bolt': {
        code: 'pp',
        energyCost: 20,
        recastCooldown: 500,
        startFromCaster: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.INVERTER]: true,
            [Categories.STRUCTURE]: true
        },
        xpGain: 10
    },

    'Invisibility': {
        code: 'ppd',
        energyCost: 10,
        recastCooldown: 300,
        xpGain: 10
    },

    //  -   -   Void spells.    -   -
    'Deflect': {
        code: 'v',
        energyCost: 40,
        recastCooldown: 500,
        startFromCaster: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.INVERTER]: true,
            [Categories.STRUCTURE]: true
        },
        xpGain: 10
    },

    'Tele Orb': {
        code: 'vv',
        energyCost: 40,
        recastCooldown: 1000,
        startFromCaster: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.INVERTER]: true,
            [Categories.STRUCTURE]: true
        },
        xpGain: 10
    },

    'Summon Inverter': {
        code: 'vvv',
        energyCost: 100,
        recastCooldown: 1000,
        castRange: 150,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.INVERTER]: true
        },
        xpGain: 10
    },

    'Recall Minions': {
        code: 'vvd',
        energyCost: 100,
        recastCooldown: 1000,
        xpGain: 10
    },

    'Build Order Core':{
        code: 'vevevev',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 6},
            {itemType: InventoryItemTypes.GEMS, amount: 8}
        ],
        castRange: 120,
        recastCooldown: 1000,
        gridAligned: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.STRUCTURE]: true
        }
    },

    'Build Wood Wall': {
        code: 'vee',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 1}
        ],
        castRange: 60,
        recastCooldown: 500,
        gridAligned: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.STRUCTURE]: true
        }
    },

    'Build Stone Wall': {
        code: 'veee',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 1},
            {itemType: InventoryItemTypes.GEMS, amount: 1}
        ],
        castRange: 60,
        recastCooldown: 500,
        gridAligned: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.STRUCTURE]: true
        }
    },

    'Build Wood Door': {
        code: 'veev',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 2}
        ],
        castRange: 60,
        recastCooldown: 500,
        gridAligned: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.STRUCTURE]: true
        }
    }
/*
    'Build Stone Door': {
        code: 'veeev',
        itemCost: [
            {itemType: InventoryItemTypes.WOOD, amount: 2},
            {itemType: InventoryItemTypes.GEMS, amount: 2}
        ],
        castRange: 50,
        recastCooldown: 500,
        gridAligned: true,
        blockList: {
            [Categories.SOLID]: true,
            [Categories.OBSTACLE]: true,
            [Categories.CHARACTER]: true,
            [Categories.MINION]: true,
            [Categories.EXIT]: true,
            [Categories.ENTRANCE]: true,
            [Categories.STRUCTURE]: true
        }
    }
*/
};

module.exports = spellStats;