/**
 * Created by User on 27/10/2017.
 */
/*
var game = new Phaser.Game(800, 600, Phaser.CANVAS, '', {preload: preload, create: create, update: update}, false, false);

var mapData;
var mapCols;
var mapRows;
var tileSize;
var terrainGrid = [];
var gameScale = 1;

var floorBitmapData = [];
var floorFrame = [];
var drawingSprite;
// The GUI buttons that are used to select which terrain to paint with.
var terrainButtons = [];
// Button to move the currently selected terrain up the priority list.
var moveTerrainUpButton;
// Button to move the currently selected terrain down the priority list.
var moveTerrainDownButton;
// The currently selected terrain button.
var selectedTerrain = 'memes';
// Movement keys.
var cursors;
// The list of all terrain objects. The order that they are in this array should reflect their priority.
var terrains = [];
// Add some terrains.
new Terrain('deepWater',    'deep-water',       2,  0);
new Terrain('shallowWater', 'shallow-water',    1,  1);
new Terrain('sand',         'sand',             3,  2);
new Terrain('dirt',         'dirt',             6,  3);
new Terrain('mud',          'mud',              7,  4);
new Terrain('path',         'path',             8,  5);
new Terrain('snow',         'snow',             4,  6);
new Terrain('lava',         'lava',             5,  7);
new Terrain('grass',        'grass',            0,  8);

function getTerrainByName(name) {
    //console.log(terrains);
    for(var i=0; i<terrains.length; i+=1){
        //console.log("Terrain name: " + terrains[i].name);
        if(terrains[i].name === name){
            //console.log("Terrain found: " + name);
            return terrains[i];
        }
    }
    console.log("Could not find terrain with name: " + name);
}

function preload() {
    game.load.image('grass',                    'assets/grass.png');
    game.load.image('shallow-water',            'assets/shallow-water.png');
    game.load.image('deep-water',               'assets/deep-water.png');
    game.load.image('dirt',                     'assets/dirt.png');
    game.load.image('mud',                      'assets/mud.png');
    game.load.image('path',                     'assets/path.png');
    game.load.image('sand',                     'assets/sand.png');
    game.load.image('snow',                     'assets/snow.png');
    game.load.image('lava',                     'assets/lava.png');
    game.load.image('arrow',                    'assets/arrow.png');
    game.load.spritesheet('terrain-tileset',    'assets/terrain-tileset.png', 32, 32, -1, 0, 2);

    game.load.json('mapData', 'assets/zone-0014-west-side.json');
}

function create() {

    game.stage.backgroundColor = "#c0c0c0";

    mapData = game.cache.getJSON('mapData');
    tileSize = mapData.tileheight;
    var layer = mapData.layers[0];

    drawingSprite = game.add.sprite(0, 0, 'terrain-tileset');
    drawingSprite.visible = false;

    mapCols = mapData.width;
    mapRows = mapData.height;

    floorBitmapData = game.make.bitmapData(mapData.width * tileSize, mapData.height * tileSize);
    floorFrame = floorBitmapData.addToWorld(0, 0, 0, 0, gameScale, gameScale);

    game.world.setBounds(0, 0, mapData.width * tileSize * gameScale, mapData.height * tileSize * gameScale);

    cursors = game.input.keyboard.createCursorKeys();

    var i = 0;
    var j = 0;

    // Initialise the terrain grid to be empty.
    for(i=0; i<mapRows; i+=1){
        terrainGrid[i] = [];
        for(j=0; j<mapCols; j+=1){
            terrainGrid[i][j] = -1;
        }
    }

    var row = 0;
    var col = 0;

    var terrainTypesByGID = {
        258: 'G',
        1218: 'SW',
        1410: 'DW',
        450: 'S'
    };

    // Go through the data for all of the tiles, and use it to populate the terrain grid.
    for(j = 0; j<layer.data.length; j+=1){
        // Get the number of the tile.
        var tile = layer.data[j];

        // Go to the next row when at the width of the map.
        if(col === layer.width){
            col = 0;
            row += 1;
        }

        var terrainType = terrainTypesByGID[tile];
        //console.log(terrainType);
        if(terrainType === 'DW'){
            terrainGrid[row][col] = getTerrainByName('deepWater');
        }
        else if(terrainType === 'SW'){
            terrainGrid[row][col] = getTerrainByName('shallowWater');
        }
        else if(terrainType === 'G'){
            terrainGrid[row][col] = getTerrainByName('grass');
        }
        else if(terrainType === 'S'){
            terrainGrid[row][col] = getTerrainByName('sand');
        }
        else {
            terrainGrid[row][col] = getTerrainByName('deepWater');
        }

        col += 1;
    }

    setupFloorTiles();

    var terrainButtonY = 90;
    var addTerrainButton = function(terrain){
        var button = game.add.button(650, terrainButtonY, terrain.buttonFrameName, function(){
            selectedTerrain = terrain;
            // Hide the other buttons a bit.
            for(var i=0; i<terrainButtons.length; i+=1){
                terrainButtons[i].alpha = 0.5;
            }
            // Make this button fully visible.
            button.alpha = 1;
        }, this);
        button.scale.setTo(0.8);
        button.fixedToCamera = true;
        terrainButtons.push(button);
        terrainButtonY += 56;
        terrain.button = button;
    };

    for(i=terrains.length-1; i>-1; i-=1){
        addTerrainButton(terrains[i]);
    }

    // Hide the other buttons a bit.
    for(i=0; i<terrainButtons.length; i+=1){
        terrainButtons[i].alpha = 0.5;
    }
    // Make the first button fully visible.
    terrainButtons[0].alpha = 1;
    selectedTerrain = getTerrainByName('grass');

    // Add the up and down buttons to rearrange the terrain order.
    moveTerrainUpButton = game.add.button(660, 20, 'arrow', function () {
        if(selectedTerrain.priority === terrains.length-1){
            return;
        }
        var thisTerrain = terrains[selectedTerrain.priority];
        var otherTerrain = terrains[selectedTerrain.priority+1];
        terrains[selectedTerrain.priority] = otherTerrain;
        terrains[selectedTerrain.priority+1] = selectedTerrain;
        thisTerrain.priority += 1;
        thisTerrain.button.cameraOffset.y -= 56;
        otherTerrain.priority -= 1;
        otherTerrain.button.cameraOffset.y += 56;
        setupFloorTiles();
    }, this);
    moveTerrainUpButton.fixedToCamera = true;

    moveTerrainDownButton = game.add.button(705, 20, 'arrow', function () {
        if(selectedTerrain.priority === 0){
            return;
        }
        var thisTerrain = terrains[selectedTerrain.priority];
        var otherTerrain = terrains[selectedTerrain.priority-1];
        terrains[selectedTerrain.priority] = otherTerrain;
        terrains[selectedTerrain.priority-1] = selectedTerrain;
        thisTerrain.priority -= 1;
        thisTerrain.button.cameraOffset.y += 56;
        otherTerrain.priority += 1;
        otherTerrain.button.cameraOffset.y -= 56;
        setupFloorTiles();
    }, this);
    moveTerrainDownButton.fixedToCamera = true;
    moveTerrainDownButton.anchor.setTo(1, 1);
    moveTerrainDownButton.angle = 180;
}

function update(){
    if (cursors.up.isDown){
        game.camera.y -= 4;
    }
    else if (cursors.down.isDown){
        game.camera.y += 4;
    }
    if (cursors.left.isDown){
        game.camera.x -= 4;
    }
    else if (cursors.right.isDown){
        game.camera.x += 4;
    }

    if(game.input.activePointer.isDown){
        var pointer = game.input.activePointer;
        paintTerrain(pointer.worldX, pointer.worldY);

    }
}

function setupFloorTiles(){
    for(var row=0; row<mapRows; row+=1){
        for(var col=0; col<mapCols; col+=1){
            changeFloorTile(row, col);
        }
    }
}
*/
/**
 * Update the target tile, and also all of the ones around it so
 * they have the right transitions for the new target tile.
 * @param {Number} row
 * @param {Number} col
 * @param {Terrain} terrain
 *//*
function updateFloorTile(row, col, terrain){
    // Update the target tile.
    changeFloorTile(row, col, terrain);
    // Update the adjacent tiles.
    changeFloorTile(row-1,  col-1);
    changeFloorTile(row-1,  col);
    changeFloorTile(row-1,  col+1);
    changeFloorTile(row,    col-1);
    changeFloorTile(row,    col+1);
    changeFloorTile(row+1,  col-1);
    changeFloorTile(row+1,  col);
    changeFloorTile(row+1,  col+1);
}


function paintTerrain(x, y){
    var row = game.math.snapToFloor(y, tileSize) / tileSize;
    var col = game.math.snapToFloor(x, tileSize) / tileSize;
    updateFloorTile(row, col, selectedTerrain);
}
*/
/**
 *
 * @param {Number} row
 * @param {Number} col
 * @param {Terrain} terrain
 *//*
function checkAdjacentTiles(row, col, terrain){

    var adjacents = [false, false, false, false, false, false, false, false];
    var tilesetRow = terrain.tilesetRow;

    /*
     0 | 1 | 2
     - # - # -
     3 |   | 4
     - # - # -
     5 | 6 | 7
     */
/*
    var position = 0;
    for(var rowOffset = -1; rowOffset<2; rowOffset+=1){
        for(var colOffset = -1; colOffset<2; colOffset+=1){
            // Skip the target cell. Only care about the ones around it.
            if(rowOffset === 0 && colOffset === 0){
                continue;
            }
            // Check for out of grid elements.
            if(row+rowOffset < 0 || col+colOffset < 0 || row+rowOffset > 63 || col+colOffset > 63){
                // Consider areas out of the map grid to be the same as the cardinally adjacent tile.
                adjacents[position] = false;
            }
            else if(terrainGrid[row+rowOffset][col+colOffset] === terrain){
                adjacents[position] = true;
            }
            position+=1;
        }
    }

    // - - - Cardinal directions - - -
    // All edges.
    if(adjacents[1] && adjacents[4] && adjacents[6] && adjacents[3]){
        adjacentTileComponents.allEdges(row, col, tilesetRow);
    }
    // All except left.
    else if(adjacents[1] && adjacents[4] && adjacents[6]){
        adjacentTileComponents.topRightBottomEdge(row, col, tilesetRow);
    }
    // All except top.
    else if(adjacents[4] && adjacents[6] && adjacents[3]){
        adjacentTileComponents.leftRightBottomEdge(row, col, tilesetRow);
    }
    // All except right.
    else if(adjacents[6] && adjacents[3] && adjacents[1]){
        adjacentTileComponents.topBottomLeftEdge(row, col, tilesetRow);
    }
    // All except bottom.
    else if(adjacents[3] && adjacents[1] && adjacents[4]){
        adjacentTileComponents.topRightLeftEdge(row, col, tilesetRow);
    }
    // Top left edge.
    else if(adjacents[1] && adjacents[3]){
        adjacentTileComponents.topLeftEdge(row, col, tilesetRow);
    }
    // Top right edge.
    else if(adjacents[1] && adjacents[4]){
        adjacentTileComponents.topRightEdge(row, col, tilesetRow);
    }
    // Bottom right edge.
    else if(adjacents[4] && adjacents[6]){
        adjacentTileComponents.bottomRightEdge(row, col, tilesetRow);
    }
    // Bottom left edge.
    else if(adjacents[3] && adjacents[6]){
        adjacentTileComponents.bottomLeftEdge(row, col, tilesetRow);
    }
    // Top edge.
    else if(adjacents[1]){
        adjacentTileComponents.topEdge(row, col, tilesetRow);
        if(adjacents[6]){
            adjacentTileComponents.bottomEdge(row, col, tilesetRow);
        }
    }
    // Right edge.
    else if(adjacents[4]){
        adjacentTileComponents.rightEdge(row, col, tilesetRow);
        if(adjacents[3]){
            adjacentTileComponents.leftEdge(row, col, tilesetRow);
        }
    }
    // Bottom edge.
    else if(adjacents[6]){
        adjacentTileComponents.bottomEdge(row, col, tilesetRow);
    }
    // Left edge.
    else if(adjacents[3]){
        adjacentTileComponents.leftEdge(row, col, tilesetRow);
    }

    // - - - Corners - - -
    if(adjacents[1] === false){
        if(adjacents[0] && adjacents[3] === false){
            adjacentTileComponents.topLeftCorner(row, col, tilesetRow);
        }
        if(adjacents[2] && adjacents[4] === false){
            adjacentTileComponents.topRightCorner(row, col, tilesetRow);
        }
    }
    if(adjacents[6] === false){
        if(adjacents[5] && adjacents[3] === false){
            adjacentTileComponents.bottomLeftCorner(row, col, tilesetRow);
        }
        if(adjacents[7] && adjacents[4] === false){
            adjacentTileComponents.bottomRightCorner(row, col, tilesetRow);
        }
    }
}
*/
/**
 *
 * @param {Number} row
 * @param {Number} col
 * @param {Terrain} terrain
 * @returns {boolean}
 *//*
function findAdjacentTerrains (row, col, terrain) {
    for(var rowOffset = -1; rowOffset<2; rowOffset+=1){
        for(var colOffset = -1; colOffset<2; colOffset+=1){
            // Skip the target cell. Only care about the ones around it.
            if(rowOffset === 0 && colOffset === 0){
                continue;
            }
            if(row+rowOffset < 0 || col+colOffset < 0 || row+rowOffset > 63 || col+colOffset > 63){
                // Consider areas out of the map grid to be the same as the cardinally adjacent tile.
                return true;
            }
            if(terrainGrid[row+rowOffset][col+colOffset] === terrain){
                return true;
            }
        }
    }
    return false;
}

/**
 * Change a target floor tile to a specific terrain type.
 * @param {Number} row
 * @param {Number} col
 * @param {Terrain} terrain
 *//*
function changeFloorTile(row, col, terrain){
    // Skip out of grid cells.
    if(row < 0 || row > 63 || col < 0 || col > 63){
        return;
    }

    var targetPriority;
    // If a terrain is specified, use it. Otherwise, use the same terrain but
    // still check the adjacent tiles in case the transitions need updating.
    if(terrain){
        //console.log("changing floor tile, terrain given: " + terrain);
        targetPriority = terrain.priority;
        // Update the grid with the new value for the target cell.
        terrainGrid[row][col] = terrain;
    }
    else {
        targetPriority = terrainGrid[row][col].priority;
        terrain = terrainGrid[row][col];
    }

    // Draw the new tile.
    drawingSprite.frame = tilesetCols * terrain.tilesetRow;
    // Draw that frame onto the bitmap data.
    floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);

    // Check if any of the adjacent tiles are of terrains that are higher than this one.
    // If so, they need to be drawn on top.
    for(var i=0; i<terrains.length; i+=1){
        if(targetPriority < terrains[i].priority){
            if(findAdjacentTerrains(row, col, terrains[i])){
                checkAdjacentTiles(row, col, terrains[i]);
            }
        }

    }
}

var tilesetCols = 20;

var adjacentTileComponents = {
    topEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 1;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    rightEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 2;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    bottomEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 3;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    leftEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 4;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    bottomLeftCorner: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 5;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topLeftCorner: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 6;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topRightCorner: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 7;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    bottomRightCorner: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 8;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topLeftEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 9;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topRightEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 10;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    bottomRightEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 11;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    bottomLeftEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 12;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topRightBottomEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 13;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    leftRightBottomEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 14;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topBottomLeftEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 15;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    topRightLeftEdge: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 16;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    },

    allEdges: function (row, col, tilesetRow) {
        drawingSprite.frame = (tilesetCols * tilesetRow) + 17;
        floorBitmapData.draw(drawingSprite, col * tileSize, row * tileSize);
    }
};*/