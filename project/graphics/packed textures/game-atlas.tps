<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>C:/Users/User/Desktop/magicks/project/graphics/packed textures/game-atlas.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-array</string>
        <key>textureFileName</key>
        <filename>../../../client/assets/img/game-atlas.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../client/assets/img/game-atlas.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../raw assets/buttons/btn-audio-off.png</key>
            <key type="filename">../raw assets/buttons/btn-audio-on.png</key>
            <key type="filename">../raw assets/buttons/btn-chat.png</key>
            <key type="filename">../raw assets/buttons/btn-dark.png</key>
            <key type="filename">../raw assets/buttons/btn-earth.png</key>
            <key type="filename">../raw assets/buttons/btn-exit.png</key>
            <key type="filename">../raw assets/buttons/btn-fire.png</key>
            <key type="filename">../raw assets/buttons/btn-fullscreen.png</key>
            <key type="filename">../raw assets/buttons/btn-inventory.png</key>
            <key type="filename">../raw assets/buttons/btn-light.png</key>
            <key type="filename">../raw assets/buttons/btn-minimap.png</key>
            <key type="filename">../raw assets/buttons/btn-options.png</key>
            <key type="filename">../raw assets/buttons/btn-order.png</key>
            <key type="filename">../raw assets/buttons/btn-psychic.png</key>
            <key type="filename">../raw assets/buttons/btn-scroll.png</key>
            <key type="filename">../raw assets/buttons/btn-void.png</key>
            <key type="filename">../raw assets/buttons/btn-water.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/btn-craft-core.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,12,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,12,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/btn-item-slot.png</key>
            <key type="filename">../raw assets/entities/critter/critter-attack-1.png</key>
            <key type="filename">../raw assets/entities/critter/critter-attack-2.png</key>
            <key type="filename">../raw assets/entities/critter/critter-idle-1.png</key>
            <key type="filename">../raw assets/entities/critter/critter-idle-2.png</key>
            <key type="filename">../raw assets/entities/critter/critter-move-1.png</key>
            <key type="filename">../raw assets/entities/critter/critter-move-2.png</key>
            <key type="filename">../raw assets/entities/critter/critter-move-3.png</key>
            <key type="filename">../raw assets/entities/critter/critter-spawn-1.png</key>
            <key type="filename">../raw assets/entities/critter/critter-spawn-2.png</key>
            <key type="filename">../raw assets/entities/critter/critter-spawn-3.png</key>
            <key type="filename">../raw assets/entities/heal orb/heal-orb-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,11,11</rect>
                <key>scale9Paddings</key>
                <rect>6,6,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/core-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/food-icon.png</key>
            <key type="filename">../raw assets/buttons/wood-icon.png</key>
            <key type="filename">../raw assets/entities/sapling/sapling-1.png</key>
            <key type="filename">../raw assets/entities/stone wall/stone-wall-1.png</key>
            <key type="filename">../raw assets/entities/wood door/wood-door-1.png</key>
            <key type="filename">../raw assets/entities/wood door/wood-door-2.png</key>
            <key type="filename">../raw assets/entities/wood door/wood-door-3.png</key>
            <key type="filename">../raw assets/entities/wood door/wood-door-4.png</key>
            <key type="filename">../raw assets/entities/wood wall/wood-wall-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/gems-icon.png</key>
            <key type="filename">../raw assets/entities/wood pickup/wood-pickup-1.png</key>
            <key type="filename">../raw assets/entities/wood pickup/wood-pickup-2.png</key>
            <key type="filename">../raw assets/entities/wood pickup/wood-pickup-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,8,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,8,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/scroll-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/barrier/barrier-1.png</key>
            <key type="filename">../raw assets/entities/barrier/barrier-2.png</key>
            <key type="filename">../raw assets/entities/barrier/barrier-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,14,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,14,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/big tree/big-tree-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-down-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-down-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-left-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-left-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-right-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-right-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-up-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-attack-up-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-down-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-down-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-down-3.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-left-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-left-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-left-3.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-right-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-right-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-right-3.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-up-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-up-2.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-move-up-3.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-unconscious-1.png</key>
            <key type="filename">../raw assets/entities/character-human-base/character-human-base-unconscious-2.png</key>
            <key type="filename">../raw assets/entities/character-human-corpse/character-human-corpse.png</key>
            <key type="filename">../raw assets/entities/gems rock/gems-rock-1.png</key>
            <key type="filename">../raw assets/entities/goblin/goblin-move-right-1.png</key>
            <key type="filename">../raw assets/entities/goblin/goblin-move-right-2.png</key>
            <key type="filename">../raw assets/entities/goblin/goblin-move-right-3.png</key>
            <key type="filename">../raw assets/entities/goblin/goblin-unconscious-1.png</key>
            <key type="filename">../raw assets/entities/small tree/small-tree-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/deflector/deflector-1.png</key>
            <key type="filename">../raw assets/entities/heal orb/heal-orb-1.png</key>
            <key type="filename">../raw assets/entities/tele orb/tele-orb-1.png</key>
            <key type="filename">../raw assets/entities/tele orb/tele-orb-2.png</key>
            <key type="filename">../raw assets/entities/tele orb/tele-orb-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,13,13</rect>
                <key>scale9Paddings</key>
                <rect>7,7,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/draining bolt/draining-bolt-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,3,13,5</rect>
                <key>scale9Paddings</key>
                <rect>7,3,13,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/energy blob/energy-blob-1.png</key>
            <key type="filename">../raw assets/entities/spalsh/splash-1.png</key>
            <key type="filename">../raw assets/entities/spalsh/splash-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,7</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/fire bolt/fire-bolt-1.png</key>
            <key type="filename">../raw assets/entities/fire bolt/fire-bolt-2.png</key>
            <key type="filename">../raw assets/entities/fire bolt/fire-bolt-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,4,24,9</rect>
                <key>scale9Paddings</key>
                <rect>12,4,24,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,3,5,5</rect>
                <key>scale9Paddings</key>
                <rect>2,3,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,4,4</rect>
                <key>scale9Paddings</key>
                <rect>2,2,4,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,14,13</rect>
                <key>scale9Paddings</key>
                <rect>7,7,14,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,15,13</rect>
                <key>scale9Paddings</key>
                <rect>8,7,15,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,11,9</rect>
                <key>scale9Paddings</key>
                <rect>5,5,11,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,13,15</rect>
                <key>scale9Paddings</key>
                <rect>7,7,13,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/flamethrower/flamethrower-7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,11,13</rect>
                <key>scale9Paddings</key>
                <rect>6,7,11,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/gems/gem-blue-1.png</key>
            <key type="filename">../raw assets/entities/gems/gem-blue-2.png</key>
            <key type="filename">../raw assets/entities/gems/gem-blue-3.png</key>
            <key type="filename">../raw assets/entities/gems/gem-green-1.png</key>
            <key type="filename">../raw assets/entities/gems/gem-green-2.png</key>
            <key type="filename">../raw assets/entities/gems/gem-green-3.png</key>
            <key type="filename">../raw assets/entities/gems/gem-red-1.png</key>
            <key type="filename">../raw assets/entities/gems/gem-red-2.png</key>
            <key type="filename">../raw assets/entities/gems/gem-red-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,5,5,9</rect>
                <key>scale9Paddings</key>
                <rect>3,5,5,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/heal orb/heal-orb-small-1.png</key>
            <key type="filename">../raw assets/entities/heal orb/heal-orb-small-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,5,5</rect>
                <key>scale9Paddings</key>
                <rect>2,2,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/mind bullet/mind-bullet-1.png</key>
            <key type="filename">../raw assets/entities/mind bullet/mind-bullet-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,4,13,7</rect>
                <key>scale9Paddings</key>
                <rect>7,4,13,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/nimbus/nimbus-1.png</key>
            <key type="filename">../raw assets/entities/order core/order-core-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-dark-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-earth-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-fire-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-light-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-psychic-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-void-1.png</key>
            <key type="filename">../raw assets/entities/shrine/shrine-water-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/raven strike/raven-strike-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,4,11,9</rect>
                <key>scale9Paddings</key>
                <rect>5,4,11,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/raven strike/raven-strike-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,9,11,17</rect>
                <key>scale9Paddings</key>
                <rect>5,9,11,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/rock/rock-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,7</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/scroll pickup/scroll-pickup-1.png</key>
            <key type="filename">../raw assets/entities/scroll pickup/scroll-pickup-2.png</key>
            <key type="filename">../raw assets/entities/scroll pickup/scroll-pickup-3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,7,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,7,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/stone shard/stone-shard-1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,2,13,3</rect>
                <key>scale9Paddings</key>
                <rect>7,2,13,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/sylph/sylph-1.png</key>
            <key type="filename">../raw assets/entities/sylph/sylph-2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,9,9</rect>
                <key>scale9Paddings</key>
                <rect>4,4,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/entities/target bolt/target-bolt.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,9,5</rect>
                <key>scale9Paddings</key>
                <rect>4,2,9,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/ui/component-dark.png</key>
            <key type="filename">../raw assets/ui/component-earth.png</key>
            <key type="filename">../raw assets/ui/component-empty.png</key>
            <key type="filename">../raw assets/ui/component-fire.png</key>
            <key type="filename">../raw assets/ui/component-light.png</key>
            <key type="filename">../raw assets/ui/component-psychic.png</key>
            <key type="filename">../raw assets/ui/component-void.png</key>
            <key type="filename">../raw assets/ui/component-water.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,3,3</rect>
                <key>scale9Paddings</key>
                <rect>2,2,3,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/ui/energy-0.png</key>
            <key type="filename">../raw assets/ui/energy-1.png</key>
            <key type="filename">../raw assets/ui/energy-2.png</key>
            <key type="filename">../raw assets/ui/energy-3.png</key>
            <key type="filename">../raw assets/ui/energy-4.png</key>
            <key type="filename">../raw assets/ui/hitpoint-0.png</key>
            <key type="filename">../raw assets/ui/hitpoint-1.png</key>
            <key type="filename">../raw assets/ui/hitpoint-2.png</key>
            <key type="filename">../raw assets/ui/hitpoint-3.png</key>
            <key type="filename">../raw assets/ui/hitpoint-4.png</key>
            <key type="filename">../raw assets/ui/revive-0.png</key>
            <key type="filename">../raw assets/ui/revive-1.png</key>
            <key type="filename">../raw assets/ui/revive-2.png</key>
            <key type="filename">../raw assets/ui/revive-3.png</key>
            <key type="filename">../raw assets/ui/revive-4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,6,6</rect>
                <key>scale9Paddings</key>
                <rect>3,3,6,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../raw assets/buttons/btn-audio-off.png</filename>
            <filename>../raw assets/buttons/btn-audio-on.png</filename>
            <filename>../raw assets/buttons/btn-exit.png</filename>
            <filename>../raw assets/buttons/btn-fullscreen.png</filename>
            <filename>../raw assets/buttons/btn-dark.png</filename>
            <filename>../raw assets/buttons/btn-earth.png</filename>
            <filename>../raw assets/buttons/btn-fire.png</filename>
            <filename>../raw assets/buttons/btn-light.png</filename>
            <filename>../raw assets/buttons/btn-psychic.png</filename>
            <filename>../raw assets/buttons/btn-void.png</filename>
            <filename>../raw assets/buttons/btn-water.png</filename>
            <filename>../raw assets/buttons/btn-options.png</filename>
            <filename>../raw assets/ui/energy-0.png</filename>
            <filename>../raw assets/ui/energy-1.png</filename>
            <filename>../raw assets/ui/energy-2.png</filename>
            <filename>../raw assets/ui/energy-3.png</filename>
            <filename>../raw assets/ui/energy-4.png</filename>
            <filename>../raw assets/ui/hitpoint-0.png</filename>
            <filename>../raw assets/ui/hitpoint-1.png</filename>
            <filename>../raw assets/ui/hitpoint-2.png</filename>
            <filename>../raw assets/ui/hitpoint-3.png</filename>
            <filename>../raw assets/ui/hitpoint-4.png</filename>
            <filename>../raw assets/ui/revive-0.png</filename>
            <filename>../raw assets/ui/revive-1.png</filename>
            <filename>../raw assets/ui/revive-2.png</filename>
            <filename>../raw assets/ui/revive-3.png</filename>
            <filename>../raw assets/ui/revive-4.png</filename>
            <filename>../raw assets/ui/component-dark.png</filename>
            <filename>../raw assets/ui/component-earth.png</filename>
            <filename>../raw assets/ui/component-empty.png</filename>
            <filename>../raw assets/ui/component-fire.png</filename>
            <filename>../raw assets/ui/component-light.png</filename>
            <filename>../raw assets/ui/component-psychic.png</filename>
            <filename>../raw assets/ui/component-void.png</filename>
            <filename>../raw assets/ui/component-water.png</filename>
            <filename>../raw assets/entities/barrier/barrier-3.png</filename>
            <filename>../raw assets/entities/barrier/barrier-1.png</filename>
            <filename>../raw assets/entities/barrier/barrier-2.png</filename>
            <filename>../raw assets/entities/critter/critter-move-3.png</filename>
            <filename>../raw assets/entities/critter/critter-spawn-1.png</filename>
            <filename>../raw assets/entities/critter/critter-spawn-2.png</filename>
            <filename>../raw assets/entities/critter/critter-spawn-3.png</filename>
            <filename>../raw assets/entities/critter/critter-attack-1.png</filename>
            <filename>../raw assets/entities/critter/critter-attack-2.png</filename>
            <filename>../raw assets/entities/critter/critter-idle-1.png</filename>
            <filename>../raw assets/entities/critter/critter-idle-2.png</filename>
            <filename>../raw assets/entities/critter/critter-move-1.png</filename>
            <filename>../raw assets/entities/critter/critter-move-2.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-7.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-1.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-2.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-3.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-4.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-5.png</filename>
            <filename>../raw assets/entities/flamethrower/flamethrower-6.png</filename>
            <filename>../raw assets/entities/heal orb/heal-orb-2.png</filename>
            <filename>../raw assets/entities/heal orb/heal-orb-1.png</filename>
            <filename>../raw assets/entities/mind bullet/mind-bullet-2.png</filename>
            <filename>../raw assets/entities/mind bullet/mind-bullet-1.png</filename>
            <filename>../raw assets/entities/raven strike/raven-strike-2.png</filename>
            <filename>../raw assets/entities/raven strike/raven-strike-1.png</filename>
            <filename>../raw assets/entities/rock/rock-1.png</filename>
            <filename>../raw assets/entities/spalsh/splash-2.png</filename>
            <filename>../raw assets/entities/spalsh/splash-1.png</filename>
            <filename>../raw assets/entities/tele orb/tele-orb-3.png</filename>
            <filename>../raw assets/entities/tele orb/tele-orb-1.png</filename>
            <filename>../raw assets/entities/tele orb/tele-orb-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-down-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-down-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-left-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-left-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-right-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-right-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-up-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-attack-up-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-down-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-down-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-down-3.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-left-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-left-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-left-3.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-right-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-right-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-right-3.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-up-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-up-2.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-move-up-3.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-unconscious-1.png</filename>
            <filename>../raw assets/entities/character-human-base/character-human-base-unconscious-2.png</filename>
            <filename>../raw assets/entities/character-human-corpse/character-human-corpse.png</filename>
            <filename>../raw assets/entities/target bolt/target-bolt.png</filename>
            <filename>../raw assets/entities/fire bolt/fire-bolt-1.png</filename>
            <filename>../raw assets/entities/fire bolt/fire-bolt-2.png</filename>
            <filename>../raw assets/entities/fire bolt/fire-bolt-3.png</filename>
            <filename>../raw assets/entities/stone shard/stone-shard-1.png</filename>
            <filename>../raw assets/entities/big tree/big-tree-1.png</filename>
            <filename>../raw assets/entities/nimbus/nimbus-1.png</filename>
            <filename>../raw assets/entities/sapling/sapling-1.png</filename>
            <filename>../raw assets/entities/small tree/small-tree-1.png</filename>
            <filename>../raw assets/entities/wood pickup/wood-pickup-3.png</filename>
            <filename>../raw assets/entities/wood pickup/wood-pickup-1.png</filename>
            <filename>../raw assets/entities/wood pickup/wood-pickup-2.png</filename>
            <filename>../raw assets/entities/gems/gem-red-3.png</filename>
            <filename>../raw assets/entities/gems/gem-red-1.png</filename>
            <filename>../raw assets/entities/gems/gem-red-2.png</filename>
            <filename>../raw assets/entities/gems/gem-green-3.png</filename>
            <filename>../raw assets/entities/gems/gem-blue-1.png</filename>
            <filename>../raw assets/entities/gems/gem-blue-2.png</filename>
            <filename>../raw assets/entities/gems/gem-blue-3.png</filename>
            <filename>../raw assets/entities/gems/gem-green-1.png</filename>
            <filename>../raw assets/entities/gems/gem-green-2.png</filename>
            <filename>../raw assets/entities/stone wall/stone-wall-1.png</filename>
            <filename>../raw assets/entities/order core/order-core-1.png</filename>
            <filename>../raw assets/entities/gems rock/gems-rock-1.png</filename>
            <filename>../raw assets/buttons/btn-item-slot.png</filename>
            <filename>../raw assets/buttons/btn-inventory.png</filename>
            <filename>../raw assets/buttons/btn-chat.png</filename>
            <filename>../raw assets/buttons/btn-order.png</filename>
            <filename>../raw assets/buttons/wood-icon.png</filename>
            <filename>../raw assets/buttons/gems-icon.png</filename>
            <filename>../raw assets/buttons/food-icon.png</filename>
            <filename>../raw assets/entities/deflector/deflector-1.png</filename>
            <filename>../raw assets/entities/wood door/wood-door-1.png</filename>
            <filename>../raw assets/entities/wood door/wood-door-2.png</filename>
            <filename>../raw assets/entities/wood door/wood-door-3.png</filename>
            <filename>../raw assets/entities/wood door/wood-door-4.png</filename>
            <filename>../raw assets/entities/wood wall/wood-wall-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-void-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-water-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-dark-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-earth-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-fire-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-light-1.png</filename>
            <filename>../raw assets/entities/shrine/shrine-psychic-1.png</filename>
            <filename>../raw assets/buttons/btn-scroll.png</filename>
            <filename>../raw assets/buttons/scroll-icon.png</filename>
            <filename>../raw assets/entities/scroll pickup/scroll-pickup-1.png</filename>
            <filename>../raw assets/entities/scroll pickup/scroll-pickup-2.png</filename>
            <filename>../raw assets/entities/scroll pickup/scroll-pickup-3.png</filename>
            <filename>../raw assets/buttons/btn-minimap.png</filename>
            <filename>../raw assets/entities/draining bolt/draining-bolt-1.png</filename>
            <filename>../raw assets/entities/energy blob/energy-blob-1.png</filename>
            <filename>../raw assets/entities/goblin/goblin-move-right-2.png</filename>
            <filename>../raw assets/entities/goblin/goblin-move-right-3.png</filename>
            <filename>../raw assets/entities/goblin/goblin-move-right-1.png</filename>
            <filename>../raw assets/entities/goblin/goblin-unconscious-1.png</filename>
            <filename>../raw assets/entities/sylph/sylph-1.png</filename>
            <filename>../raw assets/entities/sylph/sylph-2.png</filename>
            <filename>../raw assets/entities/heal orb/heal-orb-small-2.png</filename>
            <filename>../raw assets/entities/heal orb/heal-orb-small-1.png</filename>
            <filename>../raw assets/buttons/btn-craft-core.png</filename>
            <filename>../raw assets/buttons/core-icon.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
