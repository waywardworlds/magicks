<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>C:/Users/User/Desktop/magicks/project/graphics/packed textures/menus-atlas.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-array</string>
        <key>textureFileName</key>
        <filename>menus-atlas.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>menus-atlas.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../other/main-background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>200,113,400,225</rect>
                <key>scale9Paddings</key>
                <rect>200,113,400,225</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/arrow-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,2,7,5</rect>
                <key>scale9Paddings</key>
                <rect>3,2,7,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/book-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,3,9,7</rect>
                <key>scale9Paddings</key>
                <rect>4,3,9,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/btn-accept.png</key>
            <key type="filename">../raw assets/buttons/btn-arrow.png</key>
            <key type="filename">../raw assets/buttons/btn-cancel.png</key>
            <key type="filename">../raw assets/buttons/btn-edit.png</key>
            <key type="filename">../raw assets/buttons/btn-refresh.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,6,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/dark-icon.png</key>
            <key type="filename">../raw assets/buttons/earth-icon.png</key>
            <key type="filename">../raw assets/buttons/light-icon.png</key>
            <key type="filename">../raw assets/buttons/psychic-icon.png</key>
            <key type="filename">../raw assets/buttons/void-icon.png</key>
            <key type="filename">../raw assets/buttons/water-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,7</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/discord-logo-white.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,60,123,120</rect>
                <key>scale9Paddings</key>
                <rect>61,60,123,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/elements-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,27,27</rect>
                <key>scale9Paddings</key>
                <rect>14,14,27,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/fire-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,9,9</rect>
                <key>scale9Paddings</key>
                <rect>4,4,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/food-icon.png</key>
            <key type="filename">../raw assets/buttons/wood-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/gems-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,8,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,8,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/info-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,13,13</rect>
                <key>scale9Paddings</key>
                <rect>7,7,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/nineslice-button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,11,11</rect>
                <key>scale9Paddings</key>
                <rect>5,5,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/order-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,7,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/settings-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,11,11</rect>
                <key>scale9Paddings</key>
                <rect>6,6,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/spells-icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,4,13,7</rect>
                <key>scale9Paddings</key>
                <rect>6,4,13,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/twitter-logo-transparent.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,26,63,51</rect>
                <key>scale9Paddings</key>
                <rect>31,26,63,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/buttons/world-selection-marker.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,2,32,3</rect>
                <key>scale9Paddings</key>
                <rect>16,2,32,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../raw assets/title-logo.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,11,97,23</rect>
                <key>scale9Paddings</key>
                <rect>49,11,97,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../raw assets/buttons/book-icon.png</filename>
            <filename>../raw assets/buttons/btn-refresh.png</filename>
            <filename>../raw assets/buttons/btn-arrow.png</filename>
            <filename>../raw assets/buttons/btn-accept.png</filename>
            <filename>../raw assets/buttons/world-selection-marker.png</filename>
            <filename>../raw assets/buttons/twitter-logo-transparent.png</filename>
            <filename>../raw assets/buttons/discord-logo-white.png</filename>
            <filename>../other/main-background.png</filename>
            <filename>../raw assets/buttons/info-icon.png</filename>
            <filename>../raw assets/buttons/settings-icon.png</filename>
            <filename>../raw assets/title-logo.png</filename>
            <filename>../raw assets/buttons/nineslice-button.png</filename>
            <filename>../raw assets/buttons/btn-edit.png</filename>
            <filename>../raw assets/buttons/btn-cancel.png</filename>
            <filename>../raw assets/buttons/elements-icon.png</filename>
            <filename>../raw assets/buttons/order-icon.png</filename>
            <filename>../raw assets/buttons/spells-icon.png</filename>
            <filename>../raw assets/buttons/gems-icon.png</filename>
            <filename>../raw assets/buttons/food-icon.png</filename>
            <filename>../raw assets/buttons/wood-icon.png</filename>
            <filename>../raw assets/buttons/arrow-icon.png</filename>
            <filename>../raw assets/buttons/void-icon.png</filename>
            <filename>../raw assets/buttons/dark-icon.png</filename>
            <filename>../raw assets/buttons/earth-icon.png</filename>
            <filename>../raw assets/buttons/water-icon.png</filename>
            <filename>../raw assets/buttons/fire-icon.png</filename>
            <filename>../raw assets/buttons/light-icon.png</filename>
            <filename>../raw assets/buttons/psychic-icon.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
