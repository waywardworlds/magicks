/**
 * Created by User on 30/01/2017.
 */

module.exports = function(hasTitle, isMale) {
    var name = '';
    // Generate the values to use. 0 - 1 for probability of each value, 1 being very unlikely.
    var hasTitle =  (hasTitle   === false) ? false : hasTitle   || Math.random() >= 0.8; // Closer to 0 for title.
    var isMale =    (isMale     === false) ? false : isMale     || Math.random() >= 0.5; // Closer to 0 for male.

    // Some random numbers for the different parts of the name.
    var randNum1; // Forename.
    var randNum2; // First part of surname, or title.
    var randNum3; // Second part of surname.

    // Get a forename for the given gender.
    // The name is for a male. Will be male if true is passed in for isMale, or if randomIntInRange returns 1.
    if(isMale){
        // Choose a male forename.
        randNum1 = randomIntInRange(0, maleForenames.length-1);
        name = maleForenames[randNum1];
    }
    // The name is for a female. Will be female if false is passed in for isMale, or if randomIntInRange returns 0.
    else{
        // Choose a female forename.
        randNum1 = randomIntInRange(0, femaleForenames.length-1);
        name = femaleForenames[randNum1];
    }

    // Get a surname or title.
    // Name has a title.
    if(hasTitle){
        randNum2 = randomIntInRange(0, titles.length-1);
        // Append the title to the forename.
        name += ' ' + titles[randNum2];
    }
    // Name has a surname.
    else{
        // The name should have a surname instead of a title.
        randNum2 = randomIntInRange(0, surnamePt1.length-1);
        randNum3 = randomIntInRange(0, surnamePt2.length-1);
        // Append the surname to the forename.
        name += ' ' + surnamePt1[randNum2] + surnamePt2[randNum3];
    }

    return name;
};

var randomIntInRange = function(minimum, maximum){
    return Math.round( Math.random() * (maximum - minimum) + minimum);
};

var maleForenames = [
/* A */ 'Abe', 'Adrian', 'Ali', 'Arte',
/* B */ 'Billy', 'Bjorn', 'Boris', 'Bruce',
/* C */
/* D */ 'Dave',
/* E */ 'Ed', 'Ero',
/* F */
/* G */ 'Geoff',
/* H */
/* I */ 'Ian', 'Ivor',
/* J */ 'Janick', 'Jeff',
/* K */ 'Kurgan',
/* L */ 'Loot', 'Lear',
/* M */ 'Max',
/* N */ 'Nep', 'Nicko', 'Nort',
/* O */
/* P */ 'Pinn',
/* Q */
/* R */ 'Ragnar', 'Rick',
/* S */ 'Steve',
/* T */ 'Ted', 'Terrence', 'Tipp',
/* U */
/* V */ 'Ven'
/* W */
/* X */
/* Y */
/* Z */
];

var femaleForenames = [
/* A */ 'Ahra', 'Ava',
/* B */
/* C */
/* D */
/* E */ 'Eve', 'Emilia',
/* F */
/* G */ 'Ginger',
/* H */
/* I */
/* J */ 'Jane',
/* K */ 'Kasha',
/* L */ 'Liela',
/* M */ 'Mahdra', 'Meg', 'Moon',
/* N */
/* O */
/* P */
/* Q */
/* R */
/* S */ 'Saffron', 'Sal',
/* T */ 'Tica', 'Tarja',
/* U */ 'Urna',
/* V */ 'Veronica',
/* W */
/* X */ 'Xena',
/* Y */
/* Z */ 'Zara'
];

var surnamePt1 = [
/* A */ 'Air',
/* B */ 'Beer', 'Black', 'Blood',
/* C */ 'Cheese', 'Clear',
/* D */ 'Dark', 'Dawn', 'Death', 'Dragon',
/* E */ 'Earth',
/* F */ 'Fire',
/* G */ 'Ginger', 'Gold',
/* H */ 'Half', 'Heavy',
/* I */ 'Ice',
/* J */
/* K */
/* L */ 'Light', 'Lion',
/* M */ 'Moon',
/* N */ 'Nether',
/* O */ 'Over',
/* P */ 'Pickle', 'Psycho',
/* Q */ 'Quick',
/* R */
/* S */ 'Sausage', 'Steady', 'Slow', 'Spirit', 'Star', 'Stone', 'Storm', 'Sun',
/* T */ 'Tiny', 'Troll',
/* U */ 'Under',
/* V */
/* W */ 'Wall', 'Water'
/* X */
/* Y */
/* Z */
];

var surnamePt2 = [
/* A */
/* B */ 'basher', 'berry', 'blood', 'born',
/* C */
/* D */ 'dance', 'dread', 'dasher',
/* E */ 'eater',
/* F */ 'fiend', 'fist',
/* G */
/* H */ 'hair',
/* I */
/* J */
/* K */ 'killer', 'king',
/* L */ 'ling', 'lord',
/* M */ 'master', 'monk',
/* N */
/* O */
/* P */
/* Q */
/* R */
/* S */ 'sbane', 'scratcher', 'skull', 'slayer', 'staff', 'stone', 'sword',
/* T */ 'treader', 'tree',
/* U */
/* V */
/* W */ 'wad', 'war', 'wand', 'weaver', 'wheel', 'wood'
/* X */
/* Y */
/* Z */
];

var titles = [
/* A */
/* B */ 'the Brave',
/* C */ 'the Cowardly',
/* D */ 'the Drunkard',
/* E */ 'the Elder',
/* F */
/* G */
/* H */
/* I */
/* J */
/* K */
/* L */ 'the Legend',
/* M */ 'the Meek',
/* N */
/* O */
/* P */
/* Q */
/* R */ 'the Red', 'the Rich',
/* S */ 'the Strong',
/* T */
/* U */
/* V */ 'the Vegan',
/* W */ 'the Wise',
/* X */
/* Y */ 'the Younger',
/* Z */ 'the Dood'
];
