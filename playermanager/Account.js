/**
 * Created by User on 30/03/2017.
 */

"use strict";

var nameGen = require('./NameGen');
var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

//var fileSystem = require('fs');
var database = require('./Database');
var accountManager = require('./AccountManager');

var createAccount = function (socket, accountType) {
    var account = {};
    // Give this account the number of the most recent increment of the account counter in the accounts database.
    //account.accountNumber = null;
    // Generate a security code.
    account.securityCode = generateSecurityCode();
    // The date that this account was created.
    account.createDate = new Date();
    // The last time this account logged out.
    account.lastActive = account.createDate;
    // The email address for this account for email authentication and account recovery.
    account.emailAddress = ''; // TODO: add email recovery with this --> https://nodemailer.com/about/
    // Whether this account is logged in. Used to stop multiple clients logged in as same account.
    account.isOnline = true;
    // A list of the license keys activated on this account.
    account.purchaseKeys = [];
    // Gameplay related stuff.
    account.gameData = {
        // What type of account this is. B (basic), F (full), A (admin).
        accountType: accountType,
        // The name that other players see this player as.
        displayName: nameGen(),

        // What spells this player has unlocked. These are starter spells.
        unlockedSpells: {
            //'llwwee': true,
            //'dd': true, //'df': true, 'ddp': true,
            ////'wwd': true,
            ////'ff': true,
            //'eee': true, 'eeee': true,
            //'pp': true, 'ppd': true
            //'vv': true, 'vvv': true, 'vvd': true, 'vevevev': true, 'vee': true, 'veee': true, 'veev': true, 'veeev': true
        },
        // What respawn area is selected for this player.
        respawnArea: '0001-4'
    };

    database.insertAccount(account, function (result) {
        //console.log("insertAccount callback");
        // Account was added to the database.
        if(result){
            var accountData = result.ops[0];
            // Add the game data to the socket before logIn, or accountType will be undefined.
            socket.gameData = accountData['gameData'];

            // Log this socket in and store the account number on the socket.
            accountManager.logIn(socket, accountData['_id']);

            // Tell the client they were successful.
            socket.emit('new_account_success', {
                accountID:      accountData['_id'],
                securityCode:   accountData['securityCode'],
                gameData:       accountData['gameData']
            });

            //console.log("- new account success, account number: " + socket.accountID);
        }
        else {
            console.log("* Failed to insert new account into database. ;/");
            socket.emit('new_account_failed');
        }
    });

    return account;
};

module.exports = {

    basic: function (socket) {
        createAccount(socket, 'B');
    },

    full: function (socket) {
        createAccount(socket, 'F');
    },

    admin: function (socket) {
        createAccount(socket, 'A');
    }
};

function generateSecurityCode () {
    var securityCode = '';
    var i;

    for(i=0; i<4; i+=1){
        securityCode += chars[Math.round(Math.random() * 61)];
    }

    securityCode += '-';

    for(i=0; i<4; i+=1){
        securityCode += chars[Math.round(Math.random() * 61)];
    }

    securityCode += '-';

    for(i=0; i<4; i+=1){
        securityCode += chars[Math.round(Math.random() * 61)];
    }

    //console.log("new security code: " + securityCode);

    return securityCode;
}