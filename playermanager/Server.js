/**
 * Created by User on 30/03/2017.
 */

"use strict";

var devMode = true;

var setupIOServer = function (port) {
    var express = require('express');
    var app = express();
    // Make a new HTTP server from Express.
    var server = require('http').Server(app);
    // Tell Socket io about the server.
    var io = require('socket.io')(server);
    // What port to bind the server to. "127.0.0.x" is localhost, this host.
    // server.listen(3003, "127.0.0.4");
    server.listen(port, "0.0.0.0");

    return io;
};

// Set up the player manager server.
var ioToClients = setupIOServer(3003);

// Set up another server to handle communication between the player manager and game servers.
var ioToGameServers = setupIOServer(3004);

// Connect to the central database.
var database = require('./Database');
database.connect(devMode);

var accounts = require('./Account');
var accountManager = require('./AccountManager');

// A list of the IP addresses of clients that are connected to this server, and how many connections are from that IP address.
// Used to limit the amount of socket connections from each client.
var connectionsFromClientIPAddress = {};
var maxConnectionsPerIPAddress = 20;

var gameServerList = {
    /**
     *  'REGION-NUMBER': {
     *      socket: {},
     *      ipAddress: '111.222.333.444',
     *      playerCount: 0,
     *      maxCapacity: 200,
     *      playerAccountIDs: {}
     *  }*/
};

// A list of the valid IP addresses for servers that are allowed to connect as a game server.
var validGameServerIPAddresses = {
    "London-1":         "178.62.81.87",
    "Amsterdam-1":      "128.199.37.110",
    "San-Francisco-1":  "45.55.16.155",
    "San-Francisco-2":  "159.89.147.19",
    "New-York-1":       "159.89.178.109",
    "New-York-2":       "159.65.34.128",
    "Singapore-1":      "188.166.212.54"
};

var worldsData = [];
setInterval(function () {
    var obj;
    worldsData = [];
    for(var key in gameServerList){
        if(gameServerList.hasOwnProperty(key)){
            obj = gameServerList[key];
            worldsData.push({
                name: key,
                playerCount: obj.playerCount,
                maxCapacity: obj.maxCapacity
            });
        }
    }
    //console.log("Worlds data updated:");
    //console.log(worldsData);
}, 2000);

//  *   *   *   * Connections from clients *   *   *   *
ioToClients.on('connection', function (socket) {
    //console.log("* New client connection: " + socket.id);
    //console.log("ip address: " + socket.request.connection.remoteAddress + ":" + socket.request.connection.remotePort);

    // Check if a connection from this IP address already exists.
    if(connectionsFromClientIPAddress[socket.request.connection.remoteAddress]){
        // Check if the amount of connections from this IP address is more than the maximum amount of connections per IP address.
        if(connectionsFromClientIPAddress[socket.request.connection.remoteAddress] > maxConnectionsPerIPAddress){
            console.log("Too many connections from the same IP address");
            socket.disconnect();
            return
        }
        // There is space for another client connection from this IP address. Add it.
        else {
            connectionsFromClientIPAddress[socket.request.connection.remoteAddress] += 1;
        }
    }
    // No clients are already connected from this IP. Connect the first one.
    else {
        connectionsFromClientIPAddress[socket.request.connection.remoteAddress] = 1;
    }

    socket.emit("hello_pm");

    // Not used for anything game server related. Just used to check if this socket is being
    // used for a logged in account, to stop them from logging in to multiple accounts from one connection.
    //socket.loggedIn = false;
    // The account ID number they are logged in with, when they log in.
    socket.accountID = null;
    // The game data for this account from the database, when they log in.
    socket.gameData = {};

    //  *   *   *   * Connections from clients  *   *   *   *
    socket.on('log_in', function (data) {
        //console.log("log in event");
        logIn(socket, data);
    });

    socket.on('log_out', function (data) {
        logOut(socket, data);
    });

    socket.on('new_account', function () {
        newAccount(socket);
    });

    socket.on('disconnect', function () {
        //console.log("disconnect: " + socket.id);
        // Check if this is NOT the only connection from this IP address.
        if(connectionsFromClientIPAddress[socket.request.connection.remoteAddress] > 1){
            // Reduce the amount of connections from this IP address.
            connectionsFromClientIPAddress[socket.request.connection.remoteAddress] -= 1;
        }
        else {
            // This is the last connection from this IP address. Remove the address from the list.
            delete connectionsFromClientIPAddress[socket.request.connection.remoteAddress];
        }
        logOut(socket);
    });

    socket.on('request_worlds_data', function () {
        //console.log("request_worlds_data");
        socket.emit('response_worlds_data', worldsData);
    });

    socket.on('connect_world', function (data) {
        connectWorld(socket, data);
    });

    socket.on('change_display_name', function (data) {
        //console.log('change_display_name');
        //console.log('data:');
        //console.log(data);
        //console.log('accountManager.loggedInList[socket.accountID]:');
        //console.log(accountManager.loggedInList[socket.accountID]);
        // Check this client is logged in with an account before letting them change their display name.
        if(!accountManager.loggedInList[socket.accountID]){
            //console.log('not logged in, socket.accountID:');
            //console.log(socket.accountID);
            return;
        }
        // Check the data object has some value.
        // Someone might send socket.emit('change_display_name') without any data.
        if(!data){
            return;
        }
        // Check if the display name is a not string, or if it is empty.
        if(typeof data !== 'string' || data === ''){
            socket.emit('display_name_rejected', 1);
            return;
        }
        // Check the display name is below 32 characters.
        if(data.length > 32){
            socket.emit('display_name_rejected', 2);
            return;
        }
        // Check the display name contains no special characters.
        if(/[0123456789!@#£$%^&*()_+\=\[\]¬¦`{};:"\\|.<>\/?]/g.test(data)){
            //console.log('display name has invalid characters');
            socket.emit('display_name_rejected', 3);
            return;
        }

        database.update('_id', Number(socket.accountID), 'gameData.displayName', data, function (res) {
            // Display name successfully updated.
            if(res){
                //console.log("success");
                socket.emit('display_name_accepted');
                // Update the display name in the gameData object held on this socket, that will
                // be sent to the game server, otherwise the new display name won't be sent.
                socket.gameData.displayName = data;
                //console.log('display name changed');
            }
            // Something went wrong updating the display name in the database.
            else {
                console.log("* WARNING: Something went wrong updating the display name in the database, data from client:");
                console.log(data);
                socket.emit('display name not changed');
            }
        });
    });

    socket.on('server_announcement', function (data) {
        console.log("server announcement:");
        console.log(data);
        if(!data){
            return;
        }
        // Check if the message is a not string, or if it is empty.
        if(typeof data.message !== 'string' || data.message === ''){
            return;
        }
        // Check if the password is a not string, or if it is empty.
        if(typeof data.password !== 'string' || data.password === ''){
            return;
        }
        // Check the message is below 128 characters.
        if(data.message.length > 150){
            return;
        }
        // Check the password is below 32 characters.
        if(data.password.length > 32){
            return;
        }

        if(data.password === 'hTy41q654'){
            var server;
            for(var key in gameServerList){
                if(gameServerList.hasOwnProperty(key)){
                    server = gameServerList[key];
                    server.socket.emit("server_announcement", data.message);
                }
            }

        }

    });

});

//  *   *   *   * Connections from game servers *   *   *   *
ioToGameServers.on('connection', function (socket) {
    socket.on("game_server_setup", function (data) {
        console.log("* Setting up new game server: " + data.name + ", IP: " + data.ipAddress);

        if(devMode !== true){
            // Make sure this connection is coming from an IP address we know is a legit game server.
            if(data.ipAddress !== validGameServerIPAddresses[data.name]){
                console.log("* WARNING: game_server_setup, a connection with an invalid IP address found.");
                return;
            }
        }

        console.log("* New game server IP address is valid.");

        socket.gameServerName = data.name;
        //console.log(data);
        // Check if there isn't already a world with this name.
        if(!gameServerList[data.name]){
            // Add a new entry to the worlds list.
            gameServerList[data.name] = {
                // The socket connection to this game server.
                socket: socket,
                // The IPv4 address of the game server.
                ipAddress: data.ipAddress,
                // How many players are currently in that world.
                playerCount: 0,
                // How many players maximum.
                maxCapacity: 200,
                // A list of the account IDs of all players connected to that game server. Used for logging those accounts out if the server disconnects.
                playerAccountIDs: {}
            };
        }
        else {
            console.log("* WARNING: game_server_setup, a game server with this server name already exists in the game server list.");
        }

    });

    // The game server has accepted a player that was told to join it.
    socket.on('player_accepted', function (data) {
        //console.log('player_accepted, data:');
        //console.log(data);

        // Store the name of the game server that this player joined on the object for that player in the list of logged in players.
        accountManager.loggedInList[data].gameServerName = socket.gameServerName;

        // Remove this player from the list of outgoing players.
        accountManager.loggedInList[data].outgoing = false;

        // Increment the player count for the world this player joined.
        gameServerList[socket.gameServerName].playerCount += 1;

        // Add the account number of the player that joined the world to the list of players in that world.
        gameServerList[socket.gameServerName].playerAccountIDs[data] = true;

        // This client has been sent to a game server, so disconnect them from the player manager server.
        accountManager.loggedInList[data].socket.disconnect();

        // If they are not connected to the player manager server, they shouldn't have a socket object.
        accountManager.loggedInList[data].socket = null;
    });

    // A player disconnected from the game server they were in.
    socket.on('player_disconnected', function (data) {
        //console.log("player_disconnected, data: ");
        if(!data){
            return
        }
        if(!data.accountID){
            return
        }
        if(!data.unlockedSpells){
            return
        }
        //console.log(data);
        // Decrement the player count for the world this disconnected from.
        gameServerList[accountManager.loggedInList[data.accountID].gameServerName].playerCount -= 1;
        // Remove the account number of the player that was in the world from the list of players in that world.
        delete gameServerList[accountManager.loggedInList[data.accountID].gameServerName].playerAccountIDs[data.accountID];
        // Log the player out of the account they were using.
        accountManager.logOut(data.accountID);
        // Update the saved list of unlocked spells in the database.
        accountManager.updateUnlockedSpells(data.accountID, data.unlockedSpells)

    });

    // If a game server itself disconnects from this player manager.
    socket.on('disconnecting', function () {
        //console.log("game server disconnected");
        // Check a game server is in the list with that name.
        if(!gameServerList[socket.gameServerName]){
            return;
        }

        console.log("* Game server disconnecting: " + socket.gameServerName);
        // Log out any players that were in this world.
        Object.keys(gameServerList[socket.gameServerName].playerAccountIDs).forEach(function(key) {
            //console.log("in loop of logged in players, key: " + key);
            accountManager.logOut(key);
        });

        // Remove this game server from the list of active game servers.
        delete gameServerList[socket.gameServerName];
    });

    /*
    socket.on('add_spell', function (data) {

        console.log("add spell event, data:");
        console.log(data);

        //database.update('_id', data.accountID, '') <-- do the add spell stuff here? need to know the current set of spells for this player in gameData.

    })*/

});

function logIn(socket, data){
    //console.log("log_in, data:");
    //console.log(data);

    // Check if this client connection is already logged in.
    if(accountManager.loggedInList[socket.accountID]){
        console.log("* Log in failed. This client connection is already logged in.");
        socket.emit("log_in_failed");
        return
    }
    if(!data){
        socket.emit("log_in_failed");
        return;
    }
    if(!data.accountID
    || !data.securityCode){
        socket.emit("log_in_failed");
        return;
    }
    // Check if the account number is a not string, or if it is empty.
    if(typeof data.accountID !== 'number'){ // <-- Not sure why this is here. The accountID should be a number anyway.
        console.log("accoutn num is not a number");
        socket.emit("log_in_failed");
        return;
    }
    // Check if the security code is a not string, or if it is empty.
    if(typeof data.securityCode !== 'string' || data.securityCode === ''){
        socket.emit("log_in_failed");
        console.log("sec code is not a string");
        return;
    }

    // Check if this account is in use.
    database.read('_id', data.accountID, function (result) {
        if(result){
            // Security code check.
            if(result['securityCode'] === data.securityCode){
                // Make sure this account is not already in use. Might have multi-loggers.
                if(result['isOnline'] === false){
                    // Limit to one logged in account for each socket connection.
                    // If multiple log-in requests are sent by this socket connection too fast, one will log in an account, then the other one will also log in another account.
                    // If log in requests for accounts 'a' and then 'b' are sent, this socket will get logged in as 'a', then be overwritten by the log in for 'b', without logging out 'a' first.
                    if(accountManager.loggedInList[socket.accountID]){
                        console.log("* Database read callback. This client connection is already logged in.");
                        socket.emit("log_in_failed");
                        return;
                    }

                    socket.gameData = result.gameData;

                    /*
                    if(devMode === true){
                        socket.gameData.unlockedSpells = {
                            'llwwee': true,
                            'dd': true, 'df': true, 'ddp': true,
                            'wwd': true,
                            'ff': true,
                            'eee': true, 'eeee': true,
                            'pp': true, 'ppd': true,
                            'vv': true, 'vvv': true, 'vvd': true
                        };
                    }
                    */

                    // Account name and password match. Send game data to client.
                    socket.emit("log_in_success", socket.gameData);

                    // Log this socket in and store the account number on the socket.
                    accountManager.logIn(socket, data.accountID);
                }
                else {
                    console.log("* Log in failed. This account is already logged in.");
                    socket.emit("log_in_failed", 1);
                }
            }
            // Given security code does not match the code for this account.
            else {
                socket.emit("log_in_failed");
            }
        }
        // No account found with given account number.
        else {
            socket.emit("log_in_failed");
        }
    });
}

function logOut(socket){
    //console.log("logOut func");
    // Check that someone was logged in before updating database. Might have sent the log_out event through console.
    if(accountManager.loggedInList[socket.accountID]){
        //console.log("user is in loggedinlist");
        // This function can be called in the 'disconnect' event callback for client connections, that will be fired when the player manager
        // receives confirmation that a player was accepted into a game server, so the client is only connected to one server at a time.
        // That event will also fire if a client disconnects (for whatever reason) from the player manager (page refresh, close, crash, .disconnect(), etc.).
        // Need to distinguish between the two causes, as the player should remain logged in if they are connected to a game server.

        // Check that the logged in player is connected to a game server. If they are, then don't log them out.
        if(accountManager.loggedInList[socket.accountID].gameServerName){
            //console.log("user is in a game server");
            return;
        }
        // Only log out the player when they disconnect from the player manager without having joined a game server.
        // If they are connected to a game server, then they will instead be logged out when 'player_disconnected' is
        // fired by the game server they are connected to, or if the server dies.
        accountManager.logOut(socket.accountID);
    }
}

// Creates a new basic account, and logs them in.
function newAccount(socket){
    //console.log("new_account");
    //console.log(data);
    // Check if this client connection is already logged in.
    if(accountManager.loggedInList[socket.accountID]){
        console.log("* New account failed. This client connection is already logged in.");
        return;
    }

    accounts.basic(socket);
}

// Connects the client to the game server for the world they want to join.
function connectWorld(socket, data){
    //console.log("connect_world, data:");
    //console.log(data);

    // Check this client is logged in with an account before letting them join a world.
    if(!accountManager.loggedInList[socket.accountID]){
        return;
    }
    if(!data){
        return;
    }
    // Check they sent the name of the world they want to join. i.e. UK-1
    if(!data.hasOwnProperty('name')){
        return;
    }
    // Check if the world name is a not string, or if it is empty.
    if(typeof data.name !== 'string' || data.name === ''){
        return;
    }
    // Check that a world with the given name exists. Client could have sent something like {name: 'abc'}.
    if(gameServerList[data.name] == undefined){
        return;
    }
    // Check this player is not already in the list of outgoing players.
    if(accountManager.loggedInList[socket.accountID].outgoing === true){
        return;
    }

    var world = gameServerList[data.name];

    // Check that the socket property of the entry in the game server list is an actual connection.
    if(!world.socket.connected){
        return;
    }

    // Check there space in the world.
    if(world.playerCount < world.maxCapacity){
        var authCode = gameServerAuthCode.generate();

        // Add this player to the list of outgoing players.
        accountManager.loggedInList[socket.accountID].outgoing = true;

        // Tell the game server to expect this player.
        world.socket.emit('expect_player', {authCode: authCode, clientIPAddress: socket.request.connection.remoteAddress, accountID: socket.accountID, gameData: socket.gameData});

        // Tell the client the IP address of the game server to connect to, and the auth code to use.
        socket.emit('connect_world_response', {gameServerIPAddress: world.ipAddress, authCode: authCode});
    }
    // That world is full.
    else {
        // Tell the client that world is full.
        socket.emit('world_full');
    }
}

var gameServerAuthCode = {
    chars: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',

    randomNumber: function(minimum, maximum){
        return Math.round( Math.random() * (maximum - minimum) + minimum);
    },

    generate: function(){
        var authCode = '';
        for(var i=0; i<8; i+=1){
            authCode += this.chars[this.randomNumber(0, 61)];
        }
        //console.log("Auth code: " + authCode);
        return authCode;
    }
};



