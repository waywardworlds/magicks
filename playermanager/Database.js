/**
 * Created by User on 02/05/2016.
 */

/** Establishes a connection to the MongoDB server and sends requests to it.
 *
 * */

"use strict";

// Import MongoDB node driver.
var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

// Global reference to the database connection. Avoids creating a new connection for every action.
var db = {};

function getNextSequence(name, callback) {
    //console.log("db collections:");
    //console.log(db.collection("counters"));
    db.collection("counters").findOneAndUpdate(
        {_id: name},        // filter
        {$inc: {seq: 1}},   // update
        {
            returnOriginal: false
        },
        callback
    );

    //console.log("---- get next sequence, ret: ");
    //console.log(ret.seq);
    //return ret.seq;
}

module.exports = {
    connect: function (devMode) {
        // Connect to the Mongo server. Connection URL, this is where the mongodb server is running. The MongoDB server should have the IP address of this player manager server whitelisted.
        // mongodb://<PLAYER MANAGER USERNAME>:<PLAYER MANAGER PASSWORD>@< MONGODB SERVER IP ADDRESS>/magicks
        //var url = 'mongodb://localhost:27017/magicks';

        var url;
        if(devMode === true){
            // Localhost.
            url = 'mongodb://playermanager-server:pms000@localhost:27017/magicks';
        }
        else {
            // DO server.
            url = 'mongodb://playermanager-server:PMS00143721@178.62.73.101:27017/magicks';
        }

        //if(process.argv[2] && process.argv[3]){
        //    url = 'mongodb://' + process.argv[2] + ':' + process.argv[3] + '@localhost:27017/magicks';
        //    console.log("* Database connection url: " + url);
        //}

        MongoClient.connect(url, function (err, database) {
            console.log("* Connecting to database.");
            if(err){
                console.log("*   Could not connect to database.");
                console.log(err);
                return;
                //throw err;
            }
            db = database;

            console.log("* Database connection established.");

            // Make sure all accounts are logged out when the player manager starts, or accounts who were
            // logged in when the server stops will still be isOnline === true, so won't be able to log in.
            // If the player manager goes down for whatever reason, all game servers should also shut
            // down, so there can't be multiple users logged in as the same account.
            module.exports.logOutAll();

            //module.exports.deleteAll();

            //console.log(database);
        });
    },
    /**
     * Insert a new entry into the player accounts collection. The result is the gameData object of the account that was added.
     *
     * @param {Object} account - The document to insert into the database.
     * @param {Function} callback - The function to call when the document has been inserted or has failed.
     */
    insertAccount: function (account, callback) {

        //console.log("database insertAccount");
        callback = callback || function () {};

        getNextSequence("userid", function (err, res) {
            account._id = res.value.seq;

            //console.log("in getNextSeq callback func");
            //console.log("error: " + err);
            //console.log("res: ");
            //console.log(res.value.seq);

            db.collection('accounts').insertOne(account, function (error, result) {
                //console.log("account insertion finished, error:");
                //console.log(error);
                if(error === null){
                    //console.log("Document added successfully, result:");
                    //console.log(result);
                    // Run callback. Return everything.
                    callback(result);
                }
            });

        });
        /*
        account._id = getNextSequence("userid");
        console.log("database insertAccount");
        callback = callback || function () {};

        */
    },

    /**
     * Get data for a single account from the accounts collection.
     *
     * @param {String} key - The property name to look for.
     * @param {String|Number|Boolean} value - The property value to look for.
     * @param {Function} callback - Callback to run when the data is found or has failed.
     * @param {Object} callbackContext - Optional - The context the callback function should be run in.
     */
    read: function (key, value, callback, callbackContext) {
        //console.log("database read");
        callback = callback || function () {};

        // Make sure that a key and value were passed in.
        if(!key){
            return;
        }
        if(!value){
            return;
        }

        db.collection('accounts').find({[key]: value}).toArray(function (error, result) {
            assert.equal(null, error);
            // If find returns nothing, then either this account does not exist, or the search criteria are wrong.
            if(result.length === 0){
                console.log("* Failed to read account data: " + key + ": " + value + ". No entry found with those criteria.");
                // Run the callback passing in false.
                callback(false, callbackContext);
                // Return from this 'find' function.
                return;
            }
            // A document with the given key/value was found.
            // If there is not just 1 account by this name, then something is wrong.
            if(result.length !== 1){
                console.log("WARNING: Reading account data. The number of documents found is not 1. Possible duplicates?");
                return;
            }
            // Give the callback the document that was found. The document is an array with 1 element, so just send that element.
            callback(result[0], callbackContext);
        });
    },

    /**
     * Update data for a single account in the player accounts collection.
     *
     * @param {String} key - The property name to find.
     * @param {String|Number|Boolean} value - The property value to find.
     * @param {String} updateKey - The key of the property to update.
     * @param {String|Number|Boolean} updateValue - The new value of the property.
     * @param {Function} callback - Callback to run when the data is updated or has failed.
     */
    update: function (key, value, updateKey, updateValue, callback) {
        //console.log("database update");
        callback = callback || function () {};
        // Check a key and value to search for were entered.
        if(key && value){
            db.collection('accounts').find({[key]: value}).toArray(function (error, result) {
                //console.log("update error:");
                //console.log(error);
                assert.equal(null, error);

                //console.log(result);
                // If find returns nothing, then either this account does not exist, or the search criteria are wrong.
                if(result.length === 0){
                    console.log("* Failed to update account data: " + key + ": " + value + ". No entry found with those criteria.");
                    // Run the callback passing in false.
                    callback(false);
                    // Failed to read document. Return from this 'find' function.
                    return;
                }

                // A document with the given key/value was found.
                // If there is not just 1 account by this name, then something is wrong.
                assert.equal(1, result.length, "The number of documents found is not 1. Possible duplicates?");

                db.collection('accounts').updateOne({[key]: value}, {$set: {[updateKey]: updateValue}}, function (err, result) {
                    //console.log("inner update err:");
                    //console.log(err);
                    assert.equal(null, err);
                    assert.equal(1, result.matchedCount);
                    // Document updated successfully. Run callback.
                    callback(result);
                });
            });
        }
    },

    /**
     * Log out all player accounts that are currently online.
     */
    logOutAll: function () {
        var o = {w: 1};
        o.multi = true;
        db.collection('accounts').updateMany({'isOnline': true}, {$set: {'isOnline': false}}, o, function (error, res) {
            assert.equal(null, error);
            console.log("* All accounts logged out.");
        });
    }

    /**
     * Delete all data for a single account in the player accounts collection.
     *
     * @param {String} accountID - The number ID of the player account to delete.
     * @param {Function} callback - Callback to run when the data has been deleted or has failed.
     */
    /*deleteOne: function (accountID, callback) {
        callback = callback || function () {};

        db.collection('accounts').find({["_id"]: accountID}).toArray(function (error, result) {
            assert.equal(null, error);
            // If find returns nothing, then either this account does not exist, or the search name is wrong.
            if (result.length === 0) {
                console.log("* Failed to delete account: " + accountID + ". No entry found with that account number.");
                // Run the callback passing in false.
                callback(false);
                // Failed to read document. Return from this 'find' function.
                return;
            }
            // An account with the given account name was found.
            // If there is not just 1 account by this name, then something is wrong.
            assert.equal(1, result.length, "The number of documents found is not 1. Possible duplicates?");

            db.collection('accounts').deleteOne({"_id": accountID}, function (error, result) {
                assert.equal(null, error);
                assert.equal(1, result.deletedCount);
                callback(result);
            });
        });
    }*/

    /**
     * Delete all accounts in the player accounts collection.
     */
    /*deleteAll: function () {
        db.collection('accounts').deleteMany({});
        console.log("* All accounts deleted.");
    }*/
};