/**
 * Created by User on 23/12/2017.
 */

var database = require('./Database');

module.exports = {

    // The list of account numbers of all players that are currently logged in, whether they are in a game or not.
    // Even if they are in a game world, their name still stays on this list until they are logged out.
    loggedInList: {},

    logIn: function (socket, accountID) {
        //console.log("logIn, accountID: " + accountID);
        //console.log("type of accountID: " + (typeof accountID));
        //console.log(accountID);
        socket.accountID = accountID;

        // Add this players account number to the list of logged in players.
        // There is a delay between this player being sent to join a game server, and being accepted by that game server, in
        // which the player could attempt to join more game servers.
        // 'outgoing' is used to show which players have been told to connect to a game server, but have not been confirmed to
        // have joined, and is used to make sure they will only be sent to one game server at once.
        // When a game server confirms that a player has successfully joined that server, this will be set to false.
        this.loggedInList[accountID] = {socket: socket, gameServerName: '', outgoing: false};

        //console.log("loggedInList:");
        //console.log(this.loggedInList[accountID].socket.gameData);

        // The account ID must be as a number, not a string, or the update will fail.
        // Update the database to show that this account is now active.
        database.update('_id', Number(accountID), 'isOnline', true);
        // Update this account's last active time.
        database.update('_id', Number(accountID), 'lastActive', new Date());
    },

    logOut: function (accountID) {
        //console.log("logOut, accountID: " + accountID);
        //console.log("type of accountID: " + (typeof accountID));

        // The account ID must be as a number, not a string, or the update will fail.
        // This player is no longer online.
        database.update('_id', Number(accountID), 'isOnline', false);
        // Update this account's last active time.
        database.update('_id', Number(accountID), 'lastActive', new Date());

        // Remove this player's account number from this list of logged in players.
        delete this.loggedInList[accountID];
    },

    /**
     * Updates the saved list of unlocked spells that this account has in the accounts database.
     * @param {Number} accountID - The ID number of the account to update.
     * @param {Array} unlockedSpells - An object with spell codes for properties. i.e. {dd: true, ppd: true, eeee: true}
     */
    updateUnlockedSpells: function (accountID, unlockedSpells) {
        //console.log("updateUnlockedSpells:");
        //console.log(unlockedSpells);

        database.update('_id', Number(accountID), 'gameData.unlockedSpells', unlockedSpells, function (res) {
            if(res){
                // Unlocked spells were successfully updated.
                //console.log("unlockedspells update success");
            }
            // Something went wrong updating the unlocked spells in the database.
            else {
                console.log("* WARNING: Something went wrong updating the list of unlocked spells. AccountManager.updateUnlockedSpells");
            }
        })
    }

};